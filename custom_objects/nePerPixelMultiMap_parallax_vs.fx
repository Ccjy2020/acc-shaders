#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
// alias: nePerPixelMultiMap_parallax_noshadows_vs
// alias: nePerPixelMultiMap_ownCubemap_vs
// alias: smLicensePlate_vs

PS_IN_Nm main(VS_IN vin) {
  PS_IN_Nm vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  return vout;
}
