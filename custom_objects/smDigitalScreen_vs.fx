#define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"
#include "common/digitalScreen.hlsl"

// alias: smDigitalScreenGlassOverlay_vs

PS_IN_SmDigitalScreen main(VS_IN vin) {
  GENERIC_PIECE(PS_IN_SmDigitalScreen);

  float3 up = abs(normalize(vin.NormalL).y) > 0.98 ? float3(0, 0, 1) : float3(0, 1, 0);
  float3 side = normalize(cross(vin.NormalL, up));
  vout.DigitalScreenPos.x = dot(vin.PosL.xyz, side);
  vout.DigitalScreenPos.y = dot(vin.PosL.xyz, up);

  return vout;
}
