#define SHADOWS_FILTER_SIZE 1

#define INCLUDE_PARTICLE_CB
#define NO_CARPAINT
#define NO_EXTAMBIENT
#define LIGHTINGFX_SIMPLEST
// #define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"
#include "include_new/ext_functions/depth_map.fx"

PS_OUT main(PS_IN_Particle pin) {
  #ifdef NO_DEPTH_MAP
    float softK = 1.0;
  #else
    float depthZ = getDepth(pin.PosH);
    float depthC = linearize(pin.PosH.z);
    float softK = saturate((depthZ - depthC) * (1.05 - pin.PosH.z * pin.PosH.z) * 1e5);
  #endif

  float4 txDiffuseValue = txDiffuse.Sample(samLinearSimple, pin.Tex);
  #if defined WITH_SHADOWS && ! defined NO_SHADOWS
    float shadow = getShadowBiasMult(pin.PosC, float3(0, 1, 0), SHADOWS_COORDS, 0, 1);
  #else
    float shadow = 0.7;
  #endif

  float lightK = saturate(1 - pin.Tex.y * txDiffuseValue.a - emissiveBlend);
  float backlitDot = saturate(-dot(normalize(pin.PosC), ksLightDirection.xyz));
  float backlit = pow(backlitDot, 12 * (2 - txDiffuseValue.a)) * (1 - saturate(txDiffuseValue.a) * 0.8);

  float3 color = ksLightColor.rgb * shadow * (lightK * 0.5 + backlit) + getAmbientBaseNonDirectional() * lightK;
  color *= txDiffuseValue.rgb * pin.Color.rgb;
  float3 normalW = float3(0, 0, 0);
  LIGHTINGFX(color);
  color += txDiffuseValue.rgb * pin.Color.rgb * emissiveBlend;

  float fadeNearby = saturate((pin.Extra - minDistance) / minDistance);
  float3 toCamera = normalize(pin.PosC);
  RETURN_BASE(color, saturate(pin.Color.a * txDiffuseValue.a * fadeNearby) * softK);
}
