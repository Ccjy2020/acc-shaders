// #define TXDETAIL_VARIATION
#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT

#ifndef CARPAINT_DAMAGES
  #define EXTRA_SHADOW_DARKER
  #define GETNORMALW_XYZ_TX
  #define GETNORMALW_SAMPLER samLinearSimple
#endif

#define SUPPORTS_AO
#define INPUT_EMISSIVE3 0

#define EXTRA_CARPAINT_FIELDS\
  float stAmbientSpec;\
  float stNondetailSpec;\
  float stDetailRefl;\
  PARAM_CSP(float, _pad0);\
  float extClearCoatThickness;\
  float extClearCoatIntensity;\
  float extClearCoatIOR;\
  PARAM_CSP(float, _pad1);\
  float3 extClearCoatTint;\
  PARAM_CSP(float, _pad2);

#define LIGHTINGFX_SPECULAR_COLOR (txMapsValue.x * txSpecularColor)
#define LIGHTINGFX_SPECULAR_EXP (txMapsValue.y * ksSpecularEXP + 1)
#define LIGHTINGFX_SUNSPECULAR_COLOR (txMapsValue.x * sunSpecular)
#define LIGHTINGFX_SUNSPECULAR_EXP (txMapsValue.y * sunSpecularEXP + 1)
#define LIGHTINGFX_TXDIFFUSE_COLOR txDiffuseValue.rgb

#include "include_new/base/_include_ps.fx"

Texture2D txSpecular : register(t22);

float3 GetReflection(float3 reflected, float specularExp) {
  return txCube.SampleLevel(samLinear, reflected, saturate(1 - specularExp / 255) * 8).rgb;
}

struct LightingOutput {
  float3 diffuse;
  float3 specular;
};

#if !NO_SUNSPEC_MAT
float3 calculateMapsLighting_wSun(float3 posC, float3 toCamera, float3 normalW, 
    float3 txDiffuseValue, float4 txMapsValue, float3 txSpecularColor, float3 txReflectColor, float specularA, float shadow){
  //float r3w = txMapsValue.y * ksSpecularEXP + 1;
  float r3w = pow(0.5, 6-txMapsValue.y*6) * ksSpecularEXP;
  //float r4x = (saturate(2*txMapsValue.z) * txMapsValue.y) * sunSpecular;
  float r4x = (txMapsValue.z * saturate(r3w / 8)) * sunSpecular;
  //float r4y = txMapsValue.y * sunSpecularEXP + 1;
  float r4y = saturate(r3w / 8) * sunSpecularEXP + 1;
  float r4z = saturate(dot(normalW, -ksLightDirection.xyz));
  float3 diffuse = ksLightColor.rgb * ksDiffuse * r4z;
  float3 diffuseAmbient = (diffuse * shadow + calculateAmbient(normalW, 1) * txMapsValue.w);
  diffuseAmbient += calculateEmissive(posC, txDiffuseValue, 1);

  float specularBase = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));

  float baseSpecularResult = pow(specularBase, max(r3w, 1)) * (r3w+1)/101.0 * shadow;
  float sunSpecularResult = pow(specularBase, max(r4y, 1)) * shadow;
  float3 specularPart = baseSpecularResult * txSpecularColor * ksLightColor.rgb
                        + sunSpecularResult * r4x * ksLightColor.rgb * txReflectColor;
  // test new ambient specular
  // old
  //float specularAmbBase = saturate(0.5+0.5*dot(normalize(reflect(toCamera, normalW)),float3(0,1.0,0)));
  // new simpler
  float specularAmbBase = saturate(0.5+0.5*normalize(reflect(toCamera, normalW)).y);
  // todo? decide if a constant 2.5 is good or if it needs to vary w/ spec exp.
  float3 specularAmbPart = pow(specularAmbBase,2.5) * 0.4 * stAmbientSpec * txMapsValue.w * specularA * txSpecularColor * ksAmbientColor_sky.rgb * perObjAmbientMultiplier;
  return diffuseAmbient * txDiffuseValue + specularPart + specularAmbPart;
}
#endif

float3 calculateMapsLighting_woSun(float3 posC, float3 toCamera, float3 normalW, 
    float3 txDiffuseValue, float4 txMapsValue, float3 txSpecularColor, float specularA, float shadow){
  //float r3w = txMapsValue.y * ksSpecularEXP + 1;
  float r3w = pow(0.5, 6-txMapsValue.y*6) * ksSpecularEXP;
  float lightValue = saturate(dot(normalW, -ksLightDirection.xyz));

  float3 diffuse = ksLightColor.rgb * ksDiffuse * lightValue;
  float3 diffuseAmbient = (diffuse * shadow + calculateAmbient(normalW, 1)) * txDiffuseValue;
  diffuseAmbient += calculateEmissive(posC, txDiffuseValue, 1);

  float specularBase = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));
  float3 specularPart = pow(specularBase, max(r3w, 1)) * (r3w+1)/101.0 * shadow * txSpecularColor * ksLightColor.rgb;
  // new ambient specular
  float specularAmbBase = saturate(0.5+0.5*dot(normalize(reflect(toCamera, normalW)),float3(0,1.0,0)));
  float3 specularAmbPart = pow(specularAmbBase,2.5) * 0.4 * stAmbientSpec * txMapsValue.w * specularA * txSpecularColor * ksAmbientColor_sky.rgb * perObjAmbientMultiplier;
  return diffuseAmbient + specularPart + specularAmbPart;
}

float3 calculateMapsLighting2(float3 posC, float3 toCamera, float3 normalW, 
    float3 txDiffuseValue, float4 txMapsValue, float3 txSpecularColor, float3 txReflectColor, float specularA, float shadow){

  #if NO_SUNSPEC_MAT
    return calculateMapsLighting_woSun(posC, toCamera, normalW, txDiffuseValue, txMapsValue, txSpecularColor, specularA, shadow);
  #else
    if (sunSpecular > 0){
      return calculateMapsLighting_wSun(posC, toCamera, normalW, txDiffuseValue, txMapsValue, txSpecularColor, txReflectColor, specularA, shadow);
    } else {
      return calculateMapsLighting_woSun(posC, toCamera, normalW, txDiffuseValue, txMapsValue, txSpecularColor, specularA, shadow);
    }
  #endif
}

LightingOutput calculateMapsLighting(float3 posC, float3 toCamera, float3 normalW, 
    float4 txMapsValue, float3 txSpecularColor, float3 txReflectColor, float specularA, float shadow, float4 ambientMult){
  //float r3w = txMapsValue.y * ksSpecularEXP + 1;
  float r3w = pow(0.5, 6 - txMapsValue.y * 6) * ksSpecularEXP;
  //float r4x = (saturate(2*txMapsValue.z) * txMapsValue.y) * sunSpecular;
  float r4x = (txMapsValue.z * saturate(r3w / 8)) * sunSpecular;
  //float r4y = txMapsValue.y * sunSpecularEXP + 1;
  float r4y = saturate(r3w / 8) * sunSpecularEXP + 1;
  float r4z = saturate(dot(normalW, -ksLightDirection.xyz));
  float3 diffuse = ksLightColor.rgb * ksDiffuse * r4z;

  ambientMult *= sunSpecular > 0 ? txMapsValue.w : 1;
  float3 diffuseAmbient = diffuse * shadow + calculateAmbient(normalW, ambientMult);

  float specularBase = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));
  float baseSpecularResult = pow(specularBase, max(r3w, 1)) * (r3w + 1) / 101.0 * shadow;
  float sunSpecularResult = pow(specularBase, max(r4y, 1)) * shadow;
  float3 specularPart = baseSpecularResult * txSpecularColor * ksLightColor.rgb
                        + sunSpecularResult * r4x * ksLightColor.rgb * txReflectColor;
  // test new ambient specular
  // old
  // float specularAmbBase = saturate(0.5+0.5*dot(normalize(reflect(toCamera, normalW)),float3(0,1.0,0)));
  // new simpler
  float specularAmbBase = saturate(0.5 + 0.5 * normalize(reflect(toCamera, normalW)).y);
  // todo? decide if a constant 2.5 is good or if it needs to vary w/ spec exp.
  float3 specularAmbPart = pow(specularAmbBase, 2.5) * 0.4 * stAmbientSpec * txMapsValue.w * specularA * txSpecularColor * ksAmbientColor_sky.rgb * perObjAmbientMultiplier;

  LightingOutput result;
  result.diffuse = diffuseAmbient;
  result.specular = specularPart + specularAmbPart;
  return result;
}

float3 getReflection(float3 baseResultColor, float3 toCamera, float3 normalW, float3 txDiffuseValue, float4 txMapsValue, float3 txReflectColor, 
    float extraMultiplier, bool isMetallic, bool isCarPaint = false){
  float fresnelMaxLevelAdj = fresnelMaxLevel * extraMultiplier;
  float blurness = saturate(1 - txMapsValue.y * ksSpecularEXP / (isCarPaint ? 8 : 255)) * 6;
  float3 reflected = normalize(reflect(toCamera, normalW));

  float coefficient = saturate(1 - dot(normalW, -toCamera));
  float intensity = coefficient > 0 
    ? min(
      pow(
        coefficient, 
        isMetallic && !isCarPaint 
          ? max(fresnelEXP, 1) 
          : fresnelEXP) 
        + lerp(fresnelC,fresnelMaxLevelAdj,saturate(2*txMapsValue.z-1)) * extraMultiplier, 
      fresnelMaxLevelAdj) 
    : 0;

  reflected.x *= -1;

  float3 color;
  if (blurness <= 3){
    color = txCube.SampleBias(samLinear, reflected, blurness).rgb;
  } else {      
    color = txCube.SampleLevel(samLinear, reflected, blurness).rgb;
  }
  color = color * txReflectColor;
  float mapSat = saturate(2.0*txMapsValue.z);
  if (isMetallic){    
    float3 r2 = color - baseResultColor;
    return intensity * r2 * mapSat + baseResultColor;
  } else {
    return color * intensity * mapSat + baseResultColor;
  }
}

void considerDetailsSpec(float2 uv, inout float4 txDiffuseValue, inout float specularModifier){
  if (useDetail){
    float4 txDetailValue = txDetail.Sample(samLinear, uv * detailUVMultiplier);
    txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
    specularModifier = specularModifier * lerp(stNondetailSpec, txDetailValue.a * 3.1875, 1 - txDiffuseValue.a);
  }
}

#include "pbr/clear_coat.hlsl"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  // damage-related
  #ifdef CARPAINT_DAMAGES
    #ifdef NO_OPTIONAL_MAPS
      float4 txDamageMaskValue = 0;
    #else
      float4 txDamageMaskValue = txDamageMask.Sample(samLinear, pin.Tex);
    #endif
    float4 txDamageValue = txDamage.Sample(samLinear, pin.Tex);
    float normalMultiplier = dot(txDamageMaskValue, damageZones);
    float damageInPoint = saturate(normalMultiplier * txDamageValue.a);
  #endif

  // normals piece
  float normalAlpha;
  #ifdef CARPAINT_DAMAGES
    normalW = getDamageNormalW(pin.Tex, normalW, tangentW, bitangentW, normalMultiplier, normalAlpha);
  #else    
    normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW);
    normalAlpha = 1;
  #endif

  // usual stuff
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);

  // new Stereo stuff
  txMapsValue.z = txMapsValue.z * txMapsValue.w;
  float4 txSpecularValue = txSpecular.Sample(samLinear, pin.Tex);
  float specularAmbient = txSpecularValue.w;
  float specularModifier = 1;
  considerDetailsSpec(pin.Tex, txDiffuseValue, specularModifier);

  #ifdef CARPAINT_DAMAGES
    // new damaged txDiffuseValue
    txDiffuseValue.xyz = damageInPoint > 0 
        ? lerp(txDiffuseValue.xyz, txDamageValue.xyz, damageInPoint) 
        : txDiffuseValue.xyz;

    // dirt
    float4 txDustValue = txDust.Sample(samLinear, pin.Tex);
    float isDusty = dirt > 0;
    float dirtLevel = txDustValue.a * dirt;
    txDiffuseValue.xyz = isDusty
        ? lerp(txDiffuseValue.xyz, txDustValue.xyz, dirtLevel) 
        : txDiffuseValue.xyz;

    // both damages and dirt affecting txMaps
    float damageInverted = 1 - damageInPoint;
    float mapsMultiplier = saturate(1 - dirtLevel * 10);
    float damageFactor = damageInPoint * (normalAlpha - 1) + 1;
    mapsMultiplier *= damageInverted;
    mapsMultiplier *= damageFactor;
    txMapsValue.x *= mapsMultiplier;
  #else
    float mapsMultiplier = 1;
    float dirtLevel = 0;
  #endif
  
  float3 txReflectColor = lerp(lerp(1, specularModifier, stDetailRefl) * txSpecularValue.rgb,  1, saturate(2 * txMapsValue.x));
  float3 txSpecularColor = ksSpecular * specularModifier * lerp(txSpecularValue.rgb, 1, saturate(2 * txMapsValue.x - 1));

  // clear coat
  ClearCoatParams ccP;
  ccP.intensity = extClearCoatIntensity * (1 - dirtLevel);
  ccP.thickness = extClearCoatThickness;
  ccP.ior = extClearCoatIOR;
  ClearCoat cc = CalculateClearCoat(toCamera, normalW, ccP);

  // lighting
  LightingOutput lightingOutput = calculateMapsLighting(pin.PosC, toCamera, normalW, txMapsValue, txSpecularColor, txReflectColor, specularAmbient, shadow, AO_LIGHTING);

  float3 lighting = lightingOutput.diffuse * txDiffuseValue.rgb;
  lighting = cc.ProcessDiffuse(lighting, fresnelMaxLevel);
  lighting += lightingOutput.diffuse * extClearCoatTint * (1 - cc.absorption);
  lighting += lightingOutput.specular;

  // lighting = calculateMapsLighting2(toCamera, normalW, txDiffuseValue.rgb, txMapsValue, txSpecularColor, txReflectColor, specularAmbient, shadow);

  LIGHTINGFX(lighting);
  #ifdef SIMPLEST_LIGHTING
    lighting = txDiffuseValue.rgb * INPUT_AMBIENT_K;
  #endif

  // RESULT_TYPE RRR;
  // if (isAdditive == 1){
  //   RRR.result = withFog(getReflection(lighting, toCamera, normalW, txDiffuseValue.rgb, txMapsValue, txReflectColor,  1.0, false), pin.Fog, 2);
  // }
  // if (isAdditive == 2){
  //   RRR.result = withFog(getReflection(lighting, toCamera, normalW, txDiffuseValue.rgb, txMapsValue, txReflectColor,  1.0, true, true), pin.Fog, normalAlpha);
  // } else {
  //   RRR.result = withFog(getReflection(lighting, toCamera, normalW, txDiffuseValue.rgb, txMapsValue, txReflectColor,  1.0, true), pin.Fog, normalAlpha);
  // }
  // RRR.result = saturate(RRR.result) * 0.0001 + txSpecularValue;
  // return RRR;

  // reflections
  float extraMultiplier = isAdditive ? mapsMultiplier * EXTRA_SHADOW_REFLECTION : EXTRA_SHADOW_REFLECTION;
  ReflParams R;
  R.fresnelMaxLevel = fresnelMaxLevel * extraMultiplier;
  R.fresnelC = fresnelC /*  * extraMultiplier */;
  R.fresnelEXP = fresnelEXP;
  R.ksSpecularEXP = txMapsValue.y * ksSpecularEXP;
  R.finalMult = txMapsValue.z * AO_REFLECTION;
  R.coloredReflections = 1;
  R.coloredReflectionsColor = txReflectColor;
  R.metallicFix = 1;
  R.useStereoApproach = true;
  R.stereoExtraMult = extraMultiplier;  
  R.reflectionSampleParam = 0;
  R.useBias = true;
  R.isCarPaint = true;
  R.useSkyColor = false;
  R.forceReflectedDir = false;
  R.forcedReflectedDir = 0;
  float3 withReflection = calculateReflection(float4(lighting, 1), toCamera, pin.PosC, normalW, R).rgb;
  // withReflection += lightingOutput.specular;
  RETURN(withReflection, normalAlpha);
}
