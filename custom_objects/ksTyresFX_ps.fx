#define INCLUDE_TYRE_CB
#define CARPAINT_NM
#define USE_BLURRED_NM
#define SPECULAR_DIFFUSE_FIX
#define AMBIENT_TYRES_FIX
// #define FORCE_BLURREST_REFLECTIONS
#define EXTRA_SHADOW_DARKER
#define EXTRA_SHADOW_AFFECTS_REFLECTION
// #define LIGHTING_MODEL_COOKTORRANCE
#define GETSHADOW_BIAS_MULTIPLIER 4

#define CB_MATERIAL_EXTRA_4\
	float extGroundOcclusion;

#include "include_new/base/_flags.fx"
#ifdef ALLOW_TYRESFX_DIRT
#define CALCULATEREFLECTION_USEFRESNELMULT_AS_VALUE
#endif

#include "include_new/base/_include_ps.fx"

#ifdef ALLOW_TYRESFX
  #include "include_new/ext_tyresfx/_include_ps.fx"
#endif

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  #ifdef ALLOW_TYRESFX
    float shadow = getShadowBiasMult(pin.PosC, normalW, SHADOWS_COORDS, shadowBiasMult, AO_FALLBACK_SHADOW);
  #else
    float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  #endif
  APPLY_EXTRA_SHADOW_INC_DIFFUSE

  #ifdef ALLOW_TYRESFX_DIRT
    float4 txStaticValue = txDiffuse.Sample(samLinear, pin.Tex);
    float4 txBlurValue = txBlur.Sample(samLinear, pin.Tex);
    float4 txDiffuseValue = lerp(txStaticValue, txBlurValue, blurLevel);
    // txDiffuseValue.a = txStaticValue.a;

    float alpha;
    normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

    float lightingMult = 1.0;
    float3 matParams = float3(ksSpecular * txDiffuseValue.a, ksSpecularEXP, fresnelMaxLevel);
    TYRESFX_DIRT(txDiffuseValue.xyz, lightingMult, matParams);
    float specMult = matParams.x;
    float specExpMult = matParams.y;
    float reflectionMult = matParams.z;
  #else
    float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
    float4 txBlurValue = txBlur.Sample(samLinear, pin.Tex);
    float4 txDirtyValue = txDirty.Sample(samLinear, pin.Tex);
    txDiffuseValue = lerp(txDiffuseValue, txBlurValue, blurLevel);

    float dirtyLevelAdj = dirtyLevel * txDirtyValue.a;
    txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txDirtyValue.rgb, dirtyLevelAdj);

    float alpha;
    normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

    float dirtyInv = 1 - dirtyLevel;
    float specMult = dirtyInv * ksSpecular * txDiffuseValue.a;
    float specExpMult = 1.0;
    float lightingMult = 1.0;
    float reflectionMult = dirtyInv;
    float water = 0;
    TYRESFX(lightingMult, specMult, specExpMult, dirtyInv);
  #endif

  float wetK = water * saturate(txDiffuseValue.a * 4);
  // txDiffuseValue.xyz = float3(water, 1 - water, 0);
  txDiffuseValue.xyz *= lerp(1, 0.2, wetK);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz * lightingMult, shadow, AO_LIGHTING);
  L.multAO(lightingMult * saturate(extraShadow.y + saturate(-normalW.y)));
  L.specularValue = specMult;
  L.specularExp = specExpMult;
  L.specularValue = lerp(L.specularValue, 1, wetK);
  L.specularExp = lerp(L.specularExp, 400, wetK);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  lighting *= lerp(1, sqrt(saturate(normalW.y + 1)), extGroundOcclusion);

  ReflParams R = getReflParamsBase(reflectionMult * EXTRA_SHADOW_REFLECTION * AO_REFLECTION);

  R.finalMult = saturate(normalW.y + 1) * EXTRA_SHADOW_REFLECTION;
  R.fresnelC = lerp(R.fresnelC, 0.04, wetK);
  R.fresnelMaxLevel = lerp(R.fresnelMaxLevel, 0.3, wetK);
  R.fresnelEXP = lerp(R.fresnelEXP, 3, wetK);
  R.ksSpecularEXP = lerp(R.ksSpecularEXP, 100, wetK);

  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  R.resultColor = R.resultPower = 0;
  RETURN(withReflection, 1);
}
