#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
// alias: smCarPaint_chameleon_vs
// alias: smCarPaint_rainbow_vs
// alias: smCarPaint_rainbowChrome_vs
// alias: smCarPaint_old_vs
// alias: stPerPixelMultiMap_specular_vs
// alias: stPerPixelMultiMap_specular_damage_dirt_vs

PS_IN_Nm main(VS_IN vin) {
  PS_IN_Nm vout;
  float4 posW, posV;

  // vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normals(vin.NormalL);

  posW = mul(vin.PosL, ksWorld);
  // posW.xyz += normalize(cross(normalize(vout.NormalW), float3(0, 1, 0))) * 0.1 * sin(ksGameTime * 0.003);
  // posW.xyz += normalize(vout.NormalW) * 0.1;
  posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  RAINFX_VERTEX(vout);
  return vout;
}
