#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define EXTRA_SHADOW_DARKER
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple

// #define NO_INTERIOR_LIGHTING

#define SUPPORTS_AO
#ifndef NO_INTERIOR_LIGHTING
  #define INPUT_EMISSIVE3 0
#endif
#include "include_new/base/_include_ps.fx"

cbuffer cbEmissiveLight : register(b12) {  
  float extTxEmissiveMode;
  float extRoomScale;
  float extRoomOffset;
  float extRoomRotation;

  float2 extRoomCeilingOffset;
  float2 extRoomCeilingScale;

  float extSnapToTexCoords;
  float extDebugMode;
  float extNearEdgeOffset;
  float extFarDistanceInv;

  float extBrightnessBase;
  float extBrightnessVolume;
  float extBrightnessLights;
  float extBrightnessGlow;

  float shapeXS;
  float shapeXES0Neg;
  float shapeXES1Neg;
  float shapePower;

  float shapeYS;
  float shapeYES0Neg;
  float shapeYES1Neg;
  float shapeChance;

  float shapeX1S;
  float shapeX1ES0Neg;
  float shapeX1ES1Neg;
  float shapeX1Mult;

  float shapeY1S;
  float shapeY1ES0Neg;
  float shapeY1ES1Neg;
  float shapeY1Mult;

  float extTexCoordsSnapScale;
  float extEmissiveDiffuseMult;
  float extEmissiveDiffuseEXP;
  float extNormalsFactor;

  float extTexCoordsSnapOffset;
  float extBrightnessAmbientVolume;
  float2 extPad0;
}

Texture2D txEmissive : register(t4);

float2x2 rotate2d(float _angle){
  float sinX = sin ( _angle );
  float cosX = cos ( _angle );
  float sinY = sin ( _angle );
  return float2x2( cosX, -sinX, sinY, cosX);
}

float closeTo(float v, float w = 0.5, float s = 6){
  float e0 = 0.5 - w / 2 - 1 / s;
  float e1 = 0.5 + w / 2 + 1 / s;
  return saturate((v - e0) * s) * saturate((e1 - v) * s);
}

float closeToP(float v, float s, float es0, float es1){
  return saturate(v * s + es0) * saturate(-v * s + es1);
}

float3 shapeSquares(float2 v){
  return float3( closeTo(v.x, 0.2), closeTo(v.y, 0.2), 1 );
}

float3 shapeCircles(float2 v){
  return float3( closeTo(v.x, 0.0, 1.5), closeTo(v.y, 0.0, 1.5), 3 );
}

float3 shapeOffice(float2 v){
  return float3( closeTo(v.x, 0.1), closeTo(v.y, 0.6), 1 );
}

float3 shapeOfficeDouble(float2 v){
  return float3( closeTo(v.x, 0.14) - closeTo(v.x, 0.02, 100) * 0.05, closeTo(v.y, 0.7), 1 );
}

float3 shapePar(float2 v){
  return float3( 
    closeToP(v.x, shapeXS, shapeXES0Neg, shapeXES1Neg)
      + closeToP(v.x, shapeX1S, shapeX1ES0Neg, shapeX1ES1Neg) * shapeX1Mult, 
    closeToP(v.y, shapeYS, shapeYES0Neg, shapeYES1Neg)
      + closeToP(v.y, shapeY1S, shapeY1ES0Neg, shapeY1ES1Neg) * shapeY1Mult, 
    shapePower);
}

float3 shape(float2 v){
  float2 u = frac(v);
  float2 w = floor(v);
  float3 noise = txNoise.SampleLevel(samPoint, w / 32, 0).xyz;
  return noise.x < shapeChance ? shapePar(u) : 0.01;
}

float distanceContour(float3 shape){
  shape.xy = saturate(shape.xy);
  return pow( 
    saturate(1.0001 - pow( length(float2(1 - shape.x, 1 - shape.y)) , shape.z) ),
    shape.z);
}

RESULT_TYPE main(PS_IN_Windows pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 txMapsValue = txMaps.Sample(samLinear, pin.Tex).xyz;
  float4 txEmissiveValue = txEmissive.Sample(samLinear, pin.Tex);

  float3 emissiveDiffuse = saturate(txDiffuseValue.xyz * extEmissiveDiffuseMult);
  if (extEmissiveDiffuseEXP){
    emissiveDiffuse = pow(luminance(emissiveDiffuse), extEmissiveDiffuseEXP);
  }

  if (extTxEmissiveMode == 0){
    txEmissiveValue.xyz = emissiveDiffuse;
  } else if (extTxEmissiveMode == 1){
    txEmissiveValue.xyz *= emissiveDiffuse;
  } else if (extTxEmissiveMode == 2){
    txEmissiveValue.xyz = lerp(emissiveDiffuse, txEmissiveValue.xyz, txEmissiveValue.a);
  } else {
    txEmissiveValue.xyz = 1;
  }

  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue);

  float lightingMult = 0;
  #ifdef NO_INTERIOR_LIGHTING
    lightingMult = 1;
  #endif

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.txEmissiveValue = lightingMult;
  L.applyTxMaps(txMapsValue.xyz);
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  float NdotV = lerp(0.2, saturate(dot(-toCamera, normalW)), extNormalsFactor);
  #ifdef NO_INTERIOR_LIGHTING
    NdotV = 0;
  #endif
  lighting *= 1 - NdotV;

  float roomScale = extRoomScale;
  float roomOffset = extRoomOffset;
  float roomRotation = extRoomRotation;
  float2 roomCeilingOffset = extRoomCeilingOffset;
  float2 roomCeilingScale = extRoomCeilingScale;

  float3 posW = pin.PosC + ksCameraPosition.xyz;
  float3 posAdjW = posW;
  float3 roomOrigin;

  [branch]
  if (extSnapToTexCoords){
    float3 difV = posW - pin.PosV;
    float difVT = dot(difV, tangentW);
    float difVB = dot(difV, bitangentW);
    float2 uvDir = pin.Tex - pin.TexV;
    float2 texR = frac(pin.Tex / extTexCoordsSnapScale + extTexCoordsSnapOffset) * extTexCoordsSnapScale;
    roomOrigin = posW 
      - tangentW * difVT * texR.x / uvDir.x
      - bitangentW * difVB * texR.y / uvDir.y;
  } else {
    posAdjW.y -= roomOffset;
    roomOrigin = ceil(posAdjW / roomScale) * roomScale;
  }

  float difY = roomOrigin.y - posAdjW.y;
  float3 roomRay = toCamera * difY / toCamera.y;
  float3 roomPoint = posAdjW + roomRay;
  float3 roomOriginAdj = posAdjW;
  roomOriginAdj.y = roomOrigin.y;
  float ceilingDistance = abs(dot(roomOriginAdj - roomPoint, normalize(pin.NormalW)));
  roomPoint.xz = mul(rotate2d(roomRotation), roomPoint.xz);
  roomPoint.xz += roomCeilingOffset;
  roomPoint.xz *= roomCeilingScale;

  float distCheck = saturate(ceilingDistance * 4 - extNearEdgeOffset);
  float lightsShape = distanceContour(shape(roomPoint.xz)) * distCheck;

  lightsShape = saturate(lightsShape);
  lightsShape = extBrightnessLights * saturate((lightsShape > 0.99) + lightsShape * lightsShape * extBrightnessGlow);
  lightsShape *= pow(saturate(2 - length(roomRay) * extFarDistanceInv), 2);
  if (toCamera.y < 0) lightsShape = 0;

  float interiorVolume = saturate(difY / roomScale - pow(abs(toCamera.y), 1));
  float interiorLight = extBrightnessBase + extBrightnessVolume * interiorVolume;
  // float3 interiorCombined = txEmissiveValue.xyz * (interiorLight + lightsShape) * getEmissiveMult() * NdotV;
  float3 interiorCombined = ksEmissive * txEmissiveValue.xyz * (interiorLight + lightsShape) * getEmissiveMult() * NdotV;

  lighting += interiorVolume * ksAmbient * NdotV * extBrightnessAmbientVolume;
  lighting += interiorCombined;
  if (extDebugMode) {
    lighting.r += difY < 0.15 ? 10 : 0;
    lighting.g += difY > (roomScale - 0.15) ? 10 * extSnapToTexCoords : 0;
    lighting.b += length(posW - roomOrigin) < 0.2 ? 10 : 0;
  }

  ReflParams R = getReflParamsBase(EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue);
  R.useBias = true;
  R.isCarPaint = true;
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  RETURN(withReflection, txDiffuseValue.a);
}
