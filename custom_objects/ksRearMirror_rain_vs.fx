#define NO_PERVERTEX_AO
#include "common/rainWindscreen.hlsl"
#include "include_new/base/_include_vs.fx"

PS_IN_PerPixelCustom main(VS_IN vin) {
  GENERIC_PIECE(PS_IN_PerPixelCustom);
  FILL_VS(vin, vout);
  return vout;
}
