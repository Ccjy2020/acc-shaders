#define SUPPORTS_COLORFUL_AO
#define INCLUDE_FLAGS_CB
#include "include_new/base/_include_vs.fx"
#include "common/flagsFX.hlsl"

PS_IN_PerPixel main(VS_IN vin) {
  float4 vinPosL = vin.PosL;
  float3 vinNormalL = vin.NormalL;

  #ifdef MODE_KUNOS
    vin.PosL.xyz += kunosWave(vin.Tex);
    float aoHeight = 1;
  #else
    float flagWidth = length(vin.NormalL) - 1000.0;
    if (abs(flagWidth) > 20) flagWidth = 0;
    float2 aoHeight = FFX_loadVector(asuint(vin.TangentPacked.x));
    customWaveNm(vin.Tex, vin.PosL.xyz, vin.NormalL, float2(flagWidth, aoHeight.y), vin.TangentPacked.y, vin.TangentPacked.z,
      extWindVel, extWindSpeed, extWindWave);
  #endif

  GENERIC_PIECE(PS_IN_PerPixel);
  GENERIC_PIECE_MOTION(vin.PosL);
  #ifndef NO_SHADOWS
    // vout.ShadowTex0.z -= 0.01;
    // vout.ShadowTex1.z -= 0.01;
    // vout.ShadowTex2.z -= 0.01;
  #endif
  #ifdef ALLOW_PERVERTEX_AO
    vout.Ao = aoHeight.x;
  #endif

  return vout;
}
