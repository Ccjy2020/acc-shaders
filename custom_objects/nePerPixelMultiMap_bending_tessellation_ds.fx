#define SUPPORTS_NORMALS_AO
#include "common/bending.hlsl"
#include "include_new/base/_include_vs.fx"
#include "common/tessellationParallaxShared.hlsl"
#include "common/tessellation.hlsl"

BENDING_FX_IMPL
#define GET(V) (bary.x * tri[0].V + bary.y * tri[1].V + bary.z * tri[2].V)
#define SET(V) dout.V = GET(V);

[domain("tri")]
PS_IN_Nm main(PatchTess patchTess, float3 bary : SV_DomainLocation, const OutputPatch<HS_OUT, 3> tri) {
  PS_IN_Nm dout;

  SET(Tex);
  SET(NormalW);
  #ifndef NO_NORMALMAPS
    SET(TangentW);
    SET(BitangentW);
  #endif
  dout.Fog = 0;
  #ifdef ALLOW_PERVERTEX_AO
    SET(Ao);
  #endif

  float4 posL = float4(GET(PosC), 1);
  float4 posW, posV;
  dout.PosH = toScreenSpace(posL, posW, posV);
  dout.PosC = posW.xyz - ksCameraPosition.xyz;

  #define vout dout
  dout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  GENERIC_PIECE_MOTION(posL);  
  return dout;
}