// Redefining ksEmissive in a different cbuffer, so it would be 
// available in vertex shader without extra hacks
#define REDEFINE_KSEMISSIVE
#define INPUT_EMISSIVE3 0

#define NO_CARPAINT
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

#define EXTRA_PARAMETERS_2\
  float extUseTxEmissive;\
  float extPad0;

#include "common/distantGlow.hlsl"

Texture2D txEmissive : register(t4);

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txEmissiveValue = txEmissive.Sample(samLinear, pin.Tex);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txEmissiveValue = 0;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  lighting += calculateEmissivePart(pin.PosC, normalW, 
    extUseTxEmissive ? txEmissiveValue.xyz * txDiffuseValue.xyz : txDiffuseValue.xyz);
  RETURN_BASE(lighting, txDiffuseValue.a * alpha);
}
