#define GBUFF_NORMALMAPS_DISTANCE 5
#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_NORMALIZED_TB
#define CARPAINT_NMDETAILS
#define GETNORMALW_XYZ_TX
#define SUPPORTS_AO
#define CARPAINT_NMDETAILS_ALPHA

#define CUSTOM_STRUCT_FIELDS\
  float Depth : DEPTH;

#ifdef ALLOW_FUR
	#define LIGHTINGFX_FUR
#endif
#include "include_new/base/_include_ps.fx"

cbuffer cbMaterialVS : register(b6) {
  float furScale; /* = 0.0025 */
  float furFacingMult; /* = 0.2 */
  float furVerticalOffset; /* = -0.7 */
  float furFidelityScale; /* = 2.7 */
  float furThresholdExp; /* = 0.8 */
  float furThresholdMult; /* = 0.8 */
  float furOcclusion; /* = 0.5 */
  float furRimExp;
  float furAmbient; /* = 1.0 */
  float furDiffuse; /* = 0.5 */
}

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);

  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, false);
  float displacement;
  considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue.xyz, normalW, displacement);
  ADJUSTCOLOR(txDiffuseValue);

  #ifdef ALLOW_FUR
    if (furFidelityScale){
      float noise = txNoise.Sample(samLinear, pin.Tex * detailUVMultiplier * abs(furFidelityScale)).x;
      displacement = furFidelityScale < 0 ? noise : saturate(displacement * noise * txMapsValue.a * 2);
    }
    clip(displacement - pow(saturate(pin.Depth), furThresholdExp) * furThresholdMult);
  #endif

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.applyTxMaps(txMapsValue.xyz);
  RAINFX_SHINY(L);
  float3 lighting = calculateMapsLighting(L);

  #ifdef ALLOW_FUR
    float rim = pow(saturate(1 - abs(dot(toCamera, normalW))), furRimExp) * min(AO_LIGHTING.g, txMapsValue.r) * pin.Depth;

    #ifdef ALLOW_LIGHTINGFX
      float furLightingMult = rim * furDiffuse;
      LIGHTINGFX(lighting);
    #endif

    lighting *= lerp(1, furOcclusion, (1 - displacement) * (1 - pin.Depth));
    lighting += rim * furAmbient * SAMPLE_REFLECTION_FN(toCamera, 5, false, REFL_SAMPLE_PARAM_DEFAULT);
    lighting += rim * furDiffuse * saturate(dot(-ksLightDirection.xyz, toCamera)) * saturate(dot(-ksLightDirection.xyz, normalW) * 2 + 0.5) * ksLightColor.xyz * shadow;
  #else
    LIGHTINGFX(lighting);
  #endif

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);

  #if MODE_MAIN_NOFX
    withReflection.g += saturate(furScale + furFacingMult + furVerticalOffset) * 0.00001;
  #endif

  RAINFX_WATER(withReflection);
  RETURN(withReflection, 1);
}
