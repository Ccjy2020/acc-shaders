#define SUPPORTS_COLORFUL_AO
#define AO_MIX_INPUT 0
#define USE_OPTIMIZED_SHADOWS
#define NO_SHADOWS_CASCADES_TRANSITION
#define RAINFX_STATIC_OBJECT
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_functions/wind.fx"

PS_IN_PerPixel main(VS_IN vin) {
  float4 posL = vin.PosL;
  vin.PosL.xz += windOffset(vin.PosL.xyz, windFromTex(vin.Tex), 1) * 2;
  GENERIC_PIECE(PS_IN_PerPixel);
  posL.xz += windOffsetPrev(posL.xyz, windFromTex(vin.Tex), 1) * 2;
  GENERIC_PIECE_MOTION(posL);
  return vout;
}
