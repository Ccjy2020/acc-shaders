#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
#include "common/tessellationParallaxShared.hlsl"
#include "common/tessellation.hlsl"

// Texture2D txDiffuse : register(t0);
Texture2D txNormal : register(t1);

SamplerState samLinear : register(s0) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

#define SET(V)\
  dout.V = bary.x * tri[0].V + bary.y * tri[1].V + bary.z * tri[2].V;

[domain("tri")]
DS_OUT main(PatchTess patchTess, float3 bary : SV_DomainLocation, const OutputPatch<HS_OUT, 3> tri) {
  DS_OUT dout;

  SET(PosC);
  SET(Tex);
  SET(NormalW);
  dout.Fog = 0;
  #ifndef NO_NORMALMAPS
    SET(TangentW);
    SET(BitangentW);
  #endif
  #ifdef ALLOW_PERVERTEX_AO
    SET(Ao);
  #endif

  dout.NormalW = normalize(dout.NormalW);

  const float MipInterval = 20.0f;
  float mipLevel = clamp( (length(dout.PosC) - MipInterval) / MipInterval, 0.0f, 6.0f);
  float h = txNormal.SampleLevel(samLinearSimple, dout.Tex, mipLevel).a;
  if (!extDisplacementInvert) h = 1 - h;
  dout.PosC += (extHeightScale * h) * dout.NormalW;

  float4 posW = float4(dout.PosC + ksCameraPosition.xyz, 1.0f);
  float4 posV = mul(posW, ksView);
  dout.PosH = mul(posV, ksProjection);

  #define vout dout
  dout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  GENERIC_PIECE_STATIC(posW);
  
  return dout;
}