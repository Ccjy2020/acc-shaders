#define CARPAINT_NM
#define GETNORMALW_OS_SAMPLER samLinear
#define GETNORMALW_SAMPLER samLinear
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"
#include "common/heating.hlsl"

RESULT_TYPE main(PS_IN_NmPosL pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float alpha;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.txEmissiveValue = 0;
  L.txSpecularValue = GET_SPEC_COLOR;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, 1);
  R.useBias = true;
  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, pin.PosC, normalW, R);
  HEATINGFX(withReflection.xyz);
  RETURN(withReflection, withReflection.a);
}
