#define NO_CARPAINT
#define NO_SSAO
#define NO_PERVERTEX_AO

#include "common/rainWindscreen.hlsl"
#include "include_new/base/_include_ps.fx"
#include "common/rainWindscreen_impl.hlsl"

RESULT_TYPE main(PS_IN_PerPixelCustom pin) {
  READ_VECTORS
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 lighting = txDiffuseValue.xyz;

	#if defined(APPLY_RAIN) && !defined(MODE_GBUFFER) && !defined(MODE_GBUFFER)
		WindscreenRainData WR = getWindscreenRainData(pin.TexAlt, pin.NormalL, pin.NormalW, toCamera, float3(0, 1, 0));
    float fresnel = saturate(pow(1 + dot(toCamera, WR.normal), 2));
    float3 refraction = lerp(toCamera, -WR.normal, 0.5);
    float3 reflection = reflect(toCamera, WR.normal);

    float3 refractionColor = txDiffuse.SampleBias(samLinearClamp, pin.Tex + WR.fakeRefraction * float2(-0.05, 0.2), 0).rgb;
    float3 reflectionColor = sampleEnv(reflection, 3, 0);
    lighting = lerp(lighting, lerp(refractionColor, reflectionColor, fresnel), saturate(WR.water * 2 - 1));

		float shapeMult = lerp(1, saturate(dot(WR.normal, normalW) + (dot(WR.normal, toCamera) + 0.85) * 0.85), saturate(WR.water * 2));

		float3 rainNormal = WR.normal;
		#ifdef ALLOW_RAINFX
			RainParams RP = (RainParams)0;
		#endif
		normalW = -WR.normal;

		LightingParams L = getLightingParams(pin.PosC, toCamera, WR.normal, 0, shadow, 1);
		L.txDiffuseValue = 0;
		L.txSpecularValue = saturate(WR.water * 2);
		L.txEmissiveValue = 0;
		L.specularExp = 8;
		L.specularValue = 2;
  	LIGHTINGFX(lighting);

		lighting *= shapeMult;
    lighting = lerp(lighting, sampleEnv(toCamera, 3, 0), WR.condensation);
		if (WR.debugMode) {
			lighting = WR.debugColor;
		}
	#endif

  RETURN_BASE(lighting, txDiffuseValue.a);
}
