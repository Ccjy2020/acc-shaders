// alias: smCarPaint_bending_ps

// #undef ALLOW_RAINFX
// #define USE_OLD_CODE
#ifdef USE_OLD_CODE
// # include "../recreated/__ksPerPixelMultiMap_damage_dirt_ps.fx"
#else

#define LIGHTINGFX_TXDIFFUSE_COLOR txDiffuseValue.rgb
#define TXDETAIL_VARIATION extApplyTilingFix
#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define CARPAINT_DAMAGES
#define SUPPORTS_AO
#define INPUT_EMISSIVE3 0

#ifndef EXTRA_CARPAINT_FIELDS_2
  #define EXTRA_CARPAINT_FIELDS_2
#endif
#ifndef ALTER_NORMAL
  #define ALTER_NORMAL
#endif
#ifndef ALTER_DIFFUSE_COLOR
  #define ALTER_DIFFUSE_COLOR
#endif
#ifndef ALTER_LIGHTING
  #define ALTER_LIGHTING
#endif

#define EXTRA_CARPAINT_FIELDS\
  float extClearCoatSpecular;\
  float3 extClearCoatTint;\
  \
  float extClearCoatThickness;\
  float extClearCoatIntensity;\
  float extClearCoatIOR;\
  float extClearCoarMask;\
  \
  float extSunMult;\
  float3 extSpecColor;\
  \
  float extPearlSpecular;\
  float extColoredSpecular;\
  float stAmbientSpec;\
  float stAmbientEXP;\
  \
  float extApplyTilingFix;\
  float extEffectsMask;\
  float extFlakesK;\
  float extColoredReflections;\
  \
  float extDiffuseSmoothnessFrom;\
  float extDiffuseSmoothnessTo;\
  float extNormalizeAO;\
  EXTRA_CARPAINT_FIELDS_2

#define LIGHTINGFX_SPECULAR_EXP (txMapsValue.y * ksSpecularEXP + 1)
#define LIGHTINGFX_SPECULAR_COLOR (L.specMult * txMapsValue.x * ksSpecular)
#define LIGHTINGFX_SUNSPECULAR_EXP (txMapsValue.y * sunSpecularEXP + 1)
#define LIGHTINGFX_SUNSPECULAR_COLOR (L.specMult * txMapsValue.x * sunSpecular)
#include "include_new/base/_include_ps.fx"
#include "common/ambientSpecular.hlsl"

// Please don’t copy this code as a base for your shader
// It’s all very experimental and WIP, and will be moved to utils_ps.fx as soon as it’s sorted and tested

struct LightingParamsCar {
  float3 txMapsValue;
  float3 specMult;
  float4 ambientAoMult;
  float shadow;
  float ccAbsorption;
  float ccMask;
  float baseEffectsMask;
};

struct LightingOutput {
  float3 ambient;
  float3 diffuse;
  float3 specular;
  float3 ccSpecular;
};

float getSpecValue(float k, float power, float exp){
  return pow(k, max(exp, 1)) * power;
}

float3 pearlSpecularColorOffset(float3 base){
  float3 result = base;
  result += float3(0.0, 1, 0.4) * saturate(base.r - base.g) * extPearlSpecular;
  result += float3(0.4, 0.0, 0.2) * saturate(base.g - base.r) * extPearlSpecular;
  result += float3(0.1, 0.4, 0.0) * saturate(base.b * 3 - base.r * 3) * extPearlSpecular;
  return lerp(1, result * 2, extColoredSpecular * saturate(max(base.x, max(base.y, base.z)) * 5));
}

float3 estimateSpecularColor(float3 base){
  #ifdef SIMPLEST_LIGHTING
    return 0;
  #endif
  return pearlSpecularColorOffset(base) * extSpecColor;
}

LightingOutput calculateLighting(float3 posC, float extraMask, float3 toCamera, float3 normalW, float3 tangentW, float3 bitangentW, LightingParamsCar P){
  LightingOutput result;
  result.ambient = calculateAmbient(normalW, P.ambientAoMult);

  float lightValue = saturate(dot(normalW, -ksLightDirection.xyz));
  P.shadow *= lightValue;

  result.diffuse = ksLightColor.rgb * INPUT_DIFFUSE_K * P.shadow;

  float specBasePower = P.txMapsValue.x * INPUT_SPECULAR_K;
  float specBaseExp = P.txMapsValue.y * INPUT_SPECULAR_EXP + 1;
  float specSunPower = (P.txMapsValue.z * P.txMapsValue.y) * sunSpecular;
  float specSunExp = P.txMapsValue.y * sunSpecularEXP + 1;
  float specK = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));
  float3 specM = P.shadow * extSpecularColor;

  result.specular = 0;
  #ifndef SIMPLIFED_SPECULARS
    result.specular += getSpecValue(specK, specBasePower, specBaseExp) * P.specMult * specM;
  #endif
  ALTER_LIGHTING
  
  result.specular += getAmbientSpecular(toCamera, normalW, stAmbientSpec, stAmbientEXP) * lerp(1, P.specMult, P.ccAbsorption) * P.baseEffectsMask;
  result.specular += getSpecValue(specK, specSunPower * (1 + extSunMult * extSunMult), specSunExp * (1 + extSunMult)) * (extClearCoatSpecular ? (extClearCoatSpecular == 2 ? extClearCoatTint : 1) : P.specMult) * specM * P.ccMask;
  return result;
}

#include "pbr/clear_coat.hlsl"
// Texture2D txLocalNormal : register(t23);

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  // damage-related
  #ifdef NO_OPTIONAL_MAPS
    float4 txDamageMaskValue = 0;
  #else
    float4 txDamageMaskValue = txDamageMask.Sample(samLinear, pin.Tex);
  #endif
  float4 txDamageValue = txDamage.Sample(samLinear, pin.Tex);
  float normalMultiplier = dot(txDamageMaskValue, damageZones);
  float damageInPoint = saturate(normalMultiplier * txDamageValue.a);

  // float3 normalW2 = normalize(mul(txLocalNormal.SampleLevel(samLinear, pin.Tex, 3.5).xyz * 2 - 1, (float3x3)ksWorld));
  // normalW = normalize(lerp(normalW, normalW2, saturate(dot(normalW, normalW2) * 4 - 3)));

  // normals piece
  float normalAlpha;
  normalW = getDamageNormalW(pin.Tex, normalW, tangentW, bitangentW, normalMultiplier, normalAlpha);
  ALTER_NORMAL

  // usual stuff
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  RAINFX_INIT;

  float baseOcclusion = lerp(1, max(max(txDiffuseValue.x, max(txDiffuseValue.y, txDiffuseValue.z)), 0.05), extNormalizeAO);
  txDiffuseValue.xyz /= baseOcclusion;

  float flakesValue = considerDetails(pin.Tex, txDiffuseValue, true);
  txMapsValue.x *= lerp(flakesValue, 1, extFlakesK);

  #ifdef HAS_CHAMELEON_MASK
    float chameleonMask = (extChameleonMask ? (extChameleonMask == 2 ? txDiffuseValue.a : 1 - txDiffuseValue.a) : 1);
  #endif

  // chameleon effect
  #ifndef SIMPLEST_LIGHTING
    ALTER_DIFFUSE_COLOR;
  #endif

  // new damaged txDiffuseValue
  txDiffuseValue.xyz = damageInPoint > 0 
      ? lerp(txDiffuseValue.xyz, txDamageValue.xyz, damageInPoint) 
      : txDiffuseValue.xyz;

  // dirt
  float4 txDustValue = txDust.Sample(samLinear, pin.Tex);
  RAINFX_WET_DIM(txDustValue);
  float isDusty = dirt > 0;
  float dirtLevel = txDustValue.a * dirt;
  txDiffuseValue.xyz = isDusty
      ? lerp(txDiffuseValue.xyz, txDustValue.xyz, dirtLevel) 
      : txDiffuseValue.xyz;

  // both damages and dirt affecting txMaps
  float damageInverted = 1 - damageInPoint;
  float mapsMultiplier = saturate(1 - dirtLevel * 10);
  float damageFactor = damageInPoint * (normalAlpha - 1) + 1;
  mapsMultiplier *= damageInverted;
  mapsMultiplier *= damageFactor;
  txMapsValue.x *= mapsMultiplier;
  float damageEffectsMult = 1 - dirtLevel;

  float baseEffectsMask = (extEffectsMask ? (extEffectsMask == 2 ? txDiffuseValue.a : 1 - txDiffuseValue.a) : 1) * damageEffectsMult;
  float clearCoarMask = (extClearCoarMask ? (extClearCoarMask == 2 ? txDiffuseValue.a : 1 - txDiffuseValue.a) : 1) * damageEffectsMult;
  #ifdef HAS_RAINBOW_MASK
    float extraMask = (extRainbowMask ? (extRainbowMask == 2 ? txDiffuseValue.a : 1 - txDiffuseValue.a) : 1) * damageEffectsMult;
  #else
    float extraMask = 1;
  #endif

  // clear coat
  ClearCoatParams ccP;
  ccP.intensity = extClearCoatIntensity * clearCoarMask;
  ccP.thickness = extClearCoatThickness;
  ccP.ior = extClearCoatIOR;
  ClearCoat cc = CalculateClearCoat(toCamera, normalW, ccP);

  if (extDiffuseSmoothnessTo){
    float m = max(txDiffuseValue.r, max(txDiffuseValue.g, txDiffuseValue.b));
    float k = saturate(remap(m, extDiffuseSmoothnessFrom, extDiffuseSmoothnessTo, 0, 1));
    txMapsValue.x *= saturate(lerp(k, 1, 0.2));
    txMapsValue.y *= saturate(lerp(k, 1, 0.07));
    txMapsValue.z *= saturate(lerp(k, 1, 0.8));
  }

  // lighting
  LightingParamsCar L;
  txMapsValue.y *= lerp(1, 0.8 + flakesValue * 0.8, extFlakesK * baseEffectsMask);
  L.txMapsValue = txMapsValue.xyz;
  L.ambientAoMult = extraShadow.y * AO_LIGHTING_OCC(baseOcclusion);
  L.specMult = lerp(1, estimateSpecularColor(txDiffuseValue.rgb), baseEffectsMask);
  L.shadow = shadow;
  L.ccAbsorption = cc.absorption;
  L.ccMask = clearCoarMask;
  L.baseEffectsMask = baseEffectsMask;
  LightingOutput lightingOutput = calculateLighting(pin.PosC, extraMask, toCamera, normalW, tangentW, bitangentW, L);
  
  float3 lighting = (lightingOutput.ambient + lightingOutput.diffuse) * txDiffuseValue.rgb;
  float emissiveMult = 1;
  lighting += calculateEmissive(pin.PosC, txDiffuseValue.rgb, emissiveMult);
  lighting = cc.ProcessDiffuse(lighting, fresnelMaxLevel);
  lighting += (lightingOutput.ambient + lightingOutput.diffuse) * extClearCoatTint * (1 - cc.absorption);

  LIGHTINGFX(lighting);
  #ifdef SIMPLEST_LIGHTING
    lighting = txDiffuseValue.rgb * INPUT_AMBIENT_K;
  #endif

  // reflections
  float extraMultiplier = isAdditive ? mapsMultiplier * EXTRA_SHADOW_REFLECTION : EXTRA_SHADOW_REFLECTION;
  ReflParams R;
  R.fresnelMaxLevel = fresnelMaxLevel * extraMultiplier;
  R.fresnelC = fresnelC * extraMultiplier;
  R.fresnelEXP = fresnelEXP;
  R.ksSpecularEXP = txMapsValue.y * ksSpecularEXP;
  R.finalMult = txMapsValue.z;
  R.coloredReflections = 1;
  R.coloredReflectionsColor = lerp(1, L.specMult, extColoredReflections);
  R.metallicFix = R.finalMult;
  R.useStereoApproach = false;
  R.stereoExtraMult = 0;
  R.reflectionSampleParam = 0;
  R.forceReflectedDir = false;
  R.forcedReflectedDir = 0;

  #ifdef RAINBOW_REFL_COLOR
    float rainbowIn = saturate(-dot(toCamera, normalW) * abs(extRainbowChromeK));
    float3 rainbowColor = rainbow(extRainbowChromeK < 0 ? 1 - rainbowIn : rainbowIn);
    if (extRainbowChromeMult < 0) rainbowColor = 1 - rainbowColor;
    rainbowColor = lerp(1, rainbowColor, saturate(dot(rainbowColor, 1)));
    rainbowColor = lerp(1, rainbowColor, saturate(rainbowIn * 2) * abs(extRainbowChromeMult) * extraMask);
    R.coloredReflectionsColor = lerp(rainbowColor, txDiffuseValue.rgb, extColoredReflections);
  #endif
  
  R.useBias = true;
  R.isCarPaint = true;
  R.useSkyColor = false;
  RAINFX_REFLECTIVE_SKIP(R);
  float3 withReflection = calculateReflection(float4(lighting, 1), toCamera, pin.PosC, normalW, R).rgb;
  #ifndef SIMPLEST_LIGHTING
    withReflection += lightingOutput.specular;
  #endif

  RAINFX_WATER(withReflection);
  RETURN(withReflection, txMapsValue.a);
}

#endif