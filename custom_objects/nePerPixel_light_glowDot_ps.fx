// Redefining ksEmissive in a different cbuffer, so it would be 
// available in vertex shader without extra hacks
#define REDEFINE_KSEMISSIVE
#define INPUT_EMISSIVE3 0

#define NO_CARPAINT
#define SUPPORTS_AO
#define NO_SSAO
#define NO_SSGI
#include "include_new/base/_include_ps.fx"
#include "common/distantGlow.hlsl"

RESULT_TYPE main(PS_IN_PerPixelExtra1 pin) {
  READ_VECTORS

  {
    // float alpha = 1;
    // float3 lighting = float3(50, 1, 1);
    // RETURN_BASE(lighting, alpha);
  }

  #if defined(ORIGINAL_MODE)
    clip(-1);
  #endif

  float4 txDiffuseValue = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 2);
  txDiffuseValue = lerp(1, txDiffuseValue, extDistantUseTexture);

  float3 glowBase = txDiffuseValue.rgb * ksEmissive;
  float3 glowNormalized = normalizeGlow(glowBase) * extDotBrightnessBase;
  float3 lighting = glowNormalized * getEmissiveMult();

  float glowK = smoothstep(0, 1, saturate(1 - length(pin.NormalW.xy)));
  lighting *= 1 + extDotBrightnessCenter * pow(glowK, extDotCenterEXP);
  lighting *= glowK;
  lighting *= pin.Extra; 

  float alpha = 0.001;
  lighting /= alpha;

  RETURN_BASE(lighting, alpha);
}
