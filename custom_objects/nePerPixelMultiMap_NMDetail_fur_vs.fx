#define SUPPORTS_NORMALS_AO

#define CUSTOM_STRUCT_FIELDS\
  float Depth : DEPTH;

#include "include_new/base/_include_vs.fx"

cbuffer cbMaterialVS : register(b6) {
  float furScale; /* = 0.0025 */
  float furFacingMult; /* = 0.2 */
  float furVerticalOffset; /* = -0.7 */
  float furFidelityScale; /* = 2.7 */
  float furThresholdExp; /* = 0.8 */
  float furThresholdMult; /* = 0.8 */
  float furOcclusion; /* = 0.5 */
  float furRimExp;
  float furAmbient; /* = 1.0 */
  float furDiffuse; /* = 0.5 */
}

cbuffer cbMaterialVS : register(b10) {
  float furInstanceMult;
}

PS_IN_Nm main(VS_IN vin, uint instanceID : SV_InstanceID) {
  PS_IN_Nm vout;

  vout.Depth = (float)instanceID * furInstanceMult;
  vout.NormalW = normals(vin.NormalL);

  float4 posW = mul(vin.PosL, ksWorld);
  float displacementScale = furScale;
  float grazing = abs(dot(vout.NormalW, normalize(posW.xyz - ksCameraPosition.xyz)));
  displacementScale *= lerp(1, furFacingMult, grazing);
  float3 displacementOffset = normalize(vout.NormalW + float3(0, furVerticalOffset, 0)) * displacementScale * vout.Depth;

  posW.xyz += displacementOffset;
  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);

  #ifdef CREATE_MOTION_BUFFER
    vout.PosCS0 = vout.PosH;
    vout.PosCS1 = getMotionWorld(float4(mul(vin.PosL, ksWorldPrev).xyz + displacementOffset, 1), vout.PosH);
  #endif

  return vout;
}
