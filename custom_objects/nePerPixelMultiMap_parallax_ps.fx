#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple

float getHeight(float2 uv, float2 dx, float2 dy);

#define GET_HEIGHT(UV, DX, DY) getHeight(UV, DX, DY)
#define parallaxScale extParallaxScale

#ifndef FORCED_PARALLAX_MODE
// #define USE_BASIC_PARALLAX
// #define USE_STEEP_PARALLAX
// #define USE_OCCLUSION_PARALLAX
#define USE_RELIEF_PARALLAX
#endif

#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"
#include "common/tessellationParallaxShared.hlsl"
#include "common/parallax.hlsl"

float getHeight(float2 uv, float2 dx, float2 dy){
  float v = txNormal.SampleGrad(samLinearSimple, uv, dx, dy).a;
  v = pow(saturate(v), extParallaxHeightEXP);
  if (extDisplacementInvert) v = 1 - v;
  return v;
}

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float d = length(pin.PosC);
  float tess = saturate((extMinDisplacementDistance - d) / (extMinDisplacementDistance - extMaxDisplacementDistance));
  float height = 1;

  [branch]
  if (tess > 0){
    float3x3 tangentToWorldSpace;
    tangentToWorldSpace[0] = tangentW;
    tangentToWorldSpace[1] = bitangentW;
    tangentToWorldSpace[2] = normalW;
    float3x3 worldToTangentSpace = transpose(tangentToWorldSpace);

    float3 eyeTS;
    eyeTS.x = dot(-toCamera, tangentW);
    eyeTS.y = dot(-toCamera, bitangentW);
    eyeTS.z = dot(-toCamera, normalW);
    eyeTS = normalize(eyeTS);

    float2 texDx = ddx(pin.Tex);
    float2 texDy = ddy(pin.Tex);
    pin.Tex = parallaxMap(pin.Tex, eyeTS, texDx, texDy, tess, height);

    #ifdef USE_RELIEF_PARALLAX
      float3 lightTS;
      lightTS.x = dot(-ksLightDirection.xyz, tangentW);
      lightTS.y = dot(-ksLightDirection.xyz, bitangentW);
      lightTS.z = dot(-ksLightDirection.xyz, normalW);
      lightTS = normalize(lightTS);    
      shadow *= parallaxSoftShadowMultiplier(lightTS, pin.Tex, height, texDx, texDy, tess);
    #endif
  }

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);

  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);
  // txDiffuseValue = 1 - txDiffuseValue;

  float tesselationOcclusion = lerp(1, lerp(extOcclusionValue, 1, height), tess);
  shadow *= tesselationOcclusion;
  extraShadow *= tesselationOcclusion;

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.applyTxMaps(txMapsValue.xyz);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  // lighting = txDiffuseValue.rgb * prShadow;

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  RETURN(withReflection, saturate(1 + useEverything()));
}
