#define SET_AO_TO_ONE

#if defined(EMISSIVE_MAPPING)
  #include "emissiveMapping.hlsl"
#else
  #include "include_new/base/_include_vs.fx"
#endif

#ifdef USE_PHOTOELASTICITY_EFFECT
  #define OUTPUT_TYPE PS_IN_NmPosL
#elif defined(EMISSIVE_MAPPING)
  #define OUTPUT_TYPE PS_IN_NmExtra4
#else 
  #define OUTPUT_TYPE PS_IN_Nm
#endif

OUTPUT_TYPE main(VS_IN vin) {
  GENERIC_PIECE(OUTPUT_TYPE);
  #ifdef USE_PHOTOELASTICITY_EFFECT
    vout.PosL = vin.PosL.xyz;
    vout.NormalL = vin.NormalL.xyz;
  #endif
  PREPARE_TANGENT;
  #ifdef EMISSIVE_MAPPING
    vout.Extra = getExtraValue(vin.PosL.xyz);
  #endif
  RAINFX_VERTEX(vout);
  return vout;
}
