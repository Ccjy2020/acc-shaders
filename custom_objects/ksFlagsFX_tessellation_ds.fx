#define SUPPORTS_COLORFUL_AO
#define NO_NORMALMAPS
#define INCLUDE_FLAGS_CB
#include "include_new/base/_include_vs.fx"
#include "common/flagsFX.hlsl"
#include "common/tessellationParallaxShared.hlsl"
#include "common/tessellation.hlsl"

#define GET(V) (bary.x * tri[0].V + bary.y * tri[1].V + bary.z * tri[2].V)
#define SET(V) dout.V = GET(V);

[domain("tri")]
PS_IN_PerPixel main(PatchTess patchTess, float3 bary : SV_DomainLocation, const OutputPatch<HS_OUT, 3> tri) {
  PS_IN_PerPixel dout;

  SET(PosC);
  SET(Tex);
  SET(NormalW);
  dout.Fog = 0;
  #ifdef ALLOW_PERVERTEX_AO
    SET(Ao);
  #endif

  // float extWindSpeed = 0;
  // float2 extWindVel = 0;
  // float extWindWave = 0;

  #ifndef MODE_KUNOS
    customWaveNm(dout.Tex, dout.PosC.xyz, dout.NormalW, tri[0].FlagSize, GET(FlagYRel), tri[0].WindOffset,
      extWindVel, extWindSpeed, extWindWave);
  #endif

  float4 posW = float4(dout.PosC, 1);
  float4 posV = mul(posW, ksView);
  dout.PosH = mul(posV, ksProjection);
  dout.PosC -= ksCameraPosition.xyz;

  #define vout dout
  dout.Fog = calculateFog(posV);
  float4 posWShadow = posW + float4(ksLightDirection.xyz * -0.2, 0);
  shadows(posWShadow, SHADOWS_COORDS);
  #ifndef NO_SHADOWS
    // vout.ShadowTex0.z -= 0.01;
    // vout.ShadowTex1.z -= 0.01;
    // vout.ShadowTex2.z -= 0.01;
  #endif
  GENERIC_PIECE_STATIC(posW);  
  return dout;
}