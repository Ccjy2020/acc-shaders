#define EXTRA_CARPAINT_FIELDS\
  float useAlphaFromDiffuse;\
  float useAlphaFromNormal;\
  float unevenWaterFix;

#define SSLR_FORCE !(useAlphaFromDiffuse || useAlphaFromNormal)
#define CARPAINT_NM
#define GETNORMALW_OS_SAMPLER samLinear
#define GETNORMALW_SAMPLER samLinear
// #define FORCE_BLURREST_REFLECTIONS
// #define SUPPORTS_AO
#define SHADOWS_FILTER_SIZE 2
#define GBUFF_NORMALMAPS_DISTANCE 400
#define GBUFF_NORMALMAPS_NEUTRAL float3(0, 1, 0)
#define IS_ADDITIVE_VAR 0

#define INPUT_DIFFUSE_K 0.05
#define INPUT_AMBIENT_K 0.14
#define INPUT_EMISSIVE3 0
#define INPUT_SPECULAR_K 1
#define INPUT_SPECULAR_EXP 360

#include "include_new/base/_include_ps.fx"

bool isPool(){
  return ksAlphaRef == 1;
}

bool isFilmed(){
  return ksAlphaRef == 3;
}

bool isLake(){
  return ksAlphaRef == -1;
}

bool isNatural(){
  return ksAlphaRef <= 0;
}

bool isEnclosed(){
  return int(ksAlphaRef) & 1;
}

bool withWhiteBits(){
  return ksAlphaRef == -2;
}

#define MAX_WIND_SPEED 10

void sampleTextures(float2 uv, float scale, out float4 tx1, out float4 tx2){
  float time = ksGameTime * 0.00001;
  float2 uv1 = uv * scale + float2(time, 0);
  float2 uv2 = uv * scale + float2(0, time);

  #ifdef NO_OPTIONAL_MAPS
    tx1 = txNormal.SampleBias(samLinearSimple, uv1, 1);
    tx2 = txNormal.SampleBias(samLinearSimple, uv2 * 3.7, 1);
  #elif defined(ADVANCED_WATER)
    tx1 = textureSampleVariation(txNormal, GETNORMALW_SAMPLER, uv1, 1.5);
    tx2 = textureSampleVariation(txDetailNM, GETNORMALW_SAMPLER, uv2 * 3.7, 1.5);
  #else
    tx1 = txNormal.SampleBias(samLinearSimple, uv1, 1);
    tx2 = txDetailNM.SampleBias(samLinearSimple, uv2 * 3.7, 1);
  #endif
}

#ifdef MODE_KUNOS
  #define extWindSpeed 4
  #define extWindWave (ksGameTime / 1000)
#endif

float3 getNormalF(float2 uv, out float2 uvWave){
  float openK = isEnclosed() ? 0.05 : 1;
  float sizeK = openK + isLake() * 0.4;
  float waveScale = saturate(0.01 + extWindSpeed * sizeK / MAX_WIND_SPEED);

  uv -= extWindWave * openK * 8;
  uvWave = uv;

  // float scale = lerp(0.02, 0.005, waveScale) * (2 - openK);

  #ifdef ADVANCED_WATER
    float4 txA1, txA2;
    float4 txB1, txB2;
    sampleTextures(uv, 0.02 * (2 - sizeK), txA1, txA2);
    sampleTextures(uv, 0.005 * (2 - sizeK), txB1, txB2);

    float4 tx1 = lerp(txA1, txB1, waveScale);
    float4 tx2 = lerp(txA2, txB2, waveScale);
  #else
    float4 tx1, tx2;
    sampleTextures(uv, 0.01 * (2 - openK), tx1, tx2);
  #endif

  float4 v1 = tx1 * 2 - 1;
  float4 v2 = tx2 * 2 - 1;
  v1.xy *= -1;

  float4 vA = (v1 + v2) * (2 + waveScale * 3) / 4 + v1 * v2;
  vA = lerp(vA, float4(0, 0, 1, 0), 0.9 - waveScale * 0.5);
  vA.xy *= -1;
  return normalize(vA.xzy);
}

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS
  // pin.NormalW = float3(0, 1, 0);

  float openK = isEnclosed() ? 0.03 : 1;
  float waveScale = saturate(0.01 + extWindSpeed * openK / MAX_WIND_SPEED);
  
  #ifdef ADVANCED_WATER
    float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
    APPLY_EXTRA_SHADOW
  #else
    float shadow = 1;
    float2 extraShadow = 1;
  #endif

  float2 uvWave;
  normalW = getNormalF((pin.PosC + ksCameraPosition.xyz).xz, uvWave);
  // normalW = normalize(pin.NormalW);

  if (dot(normalW, toCamera) > -0.2){
    normalW -= toCamera * saturate(remap(dot(normalW, toCamera), -0.2, -0.1, 0, 1)) * unevenWaterFix;
    normalW = normalize(normalW);
  }

  float dist = length(pin.PosC);
  float distK = pow(saturate(dist / 1500 - 0.1), 0.3);
  float directK = 0.1 * saturate(-dot(normalW, toCamera));

  #ifdef MODE_LIGHTMAP
    toCamera = -normalW;
    directK = 0.1;
  #endif

  float3 normalWB = normalW;
  float3 posW = pin.PosC + ksCameraPosition.xyz;

  float alphaValue = 1;
  if (useAlphaFromDiffuse) alphaValue *= txDiffuse.Sample(samLinearSimple, pin.Tex).a;
  if (useAlphaFromNormal) alphaValue *= txNormal.Sample(samLinearSimple, pin.Tex).a;

  float4 txDiffuseValue = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 100);
  float3 color = txDiffuseValue.rgb * (isNatural() ? (float3)0.3 : float3(0, 0.9, 1));
  color += directK * (isPool() ? 1 : float3(0.3, 0.4, 0.3) * (1 - waveScale));

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y);
  float3 lighting = L.calculate();
  #ifdef ADVANCED_WATER
    lighting *= pow(shadow, 4);
    float sunSpecularBase = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));
    float3 sunSpecularPart = pow(sunSpecularBase, 1500) * pow(shadow, 4) * 10 * extSpecularColor;
    lighting += sunSpecularPart;
  #endif

  ReflParams R;
  R.fresnelMaxLevel = 0.5 - 0.3 * isPool() + 0.2 * isFilmed();
  R.fresnelC = 0.1;
  R.fresnelEXP = isFilmed() ? 3 : 6;
  R.ksSpecularEXP = 300;
  R.finalMult = 1;
  R.metallicFix = 1;
  R.useBias = true;
  R.isCarPaint = false;
  R.useSkyColor = false;
  R.coloredReflections = 0;
  R.coloredReflectionsColor = 0;
  R.useStereoApproach = false;
  R.stereoExtraMult = 0;
  R.reflectionSampleParam = 0;
  R.forceReflectedDir = false;
  R.forcedReflectedDir = 0;

  normalW = normalize(normalW + float3(0, saturate(dist / 500) * 10, 0));
  float3 withReflection = calculateReflection(float4(lighting, 1), toCamera, pin.PosC, normalW, R).rgb;

  #ifdef ADVANCED_WATER_SHADING
    float waterThing = pow(saturate(saturate(dot(normalW, ksLightDirection.xyz) * 2 + 0.8) 
        * saturate(dot(toCamera, -ksLightDirection.xyz))
        - 0.2), 4) * 3 * openK * shadow;
    withReflection += 
        (dot(ksLightColor.rgb, 0.33) 
          * txDiffuseValue.rgb 
          * float3(0.1 + waterThing * 0.5, 0.4 + waterThing * 0.2, 0.4))
        * waterThing;

    [branch]
    if (withWhiteBits()){
      float foamP = pow(saturate(1 - dot(abs(txNormal.SampleLevel(samLinearSimple, uvWave, 6) - 0.5).xy, 1)), 
        lerp(400, 40, waveScale));
      float yDif = abs(ddx(normalWB.y)) + abs(ddy(normalWB.y));
      float foamK = saturate(1 - yDif * lerp(10000, 2000, waveScale)) * pow(saturate(normalWB.y), lerp(40000, 2000, waveScale));
      withReflection += foamK * dot(ksAmbientColor_sky.rgb, 0.2) 
        * saturate(1 - dist / 100) 
        * saturate(waveScale * 10 - 5) * openK * foamP;
    }
  #endif

  float alphaBase = 1 - directK;
  float alphaResult = saturate(alphaBase * alphaBase * alphaValue);
  R.resultColor *= alphaResult;
  R.resultPower *= alphaResult;
  RETURN(withReflection, alphaResult);
}
