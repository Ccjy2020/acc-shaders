#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO

#include "include_new/base/_include_ps.fx"

#define MULTIMAP_MODE
#define USE_GLITCHING
#include "common/digitalScreen.hlsl"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);

  DigitalScreenData D = digitalScreenInit(pin.Tex, 0);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 screenFix = digitalScreenCalculate(D, pin.PosC + ksCameraPosition.xyz, 
    pin.Tex, normalW, toCamera, txDiffuseValue);

  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);

  txDiffuseValue *= 1 - D.mask;
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.txEmissiveValue = 0;
  L.applyTxMaps(txMapsValue.xyz);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  lighting += screenFix * ksEmissive;

  ReflParams R = getReflParamsBase(AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  RETURN(withReflection, txMapsValue.a);
}
