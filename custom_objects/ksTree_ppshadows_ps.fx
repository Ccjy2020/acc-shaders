#define A2C_SHARPENED_THRESHOLD 0.3
#define ALPHATEST_THRESHOLD 10
#define A2C_SHARPENED
#define NO_CARPAINT
#define SUPPORTS_AO
#define RAINFX_STATIC_OBJECT
#define RAINFX_NO_RAINDROPS
#include "include_new/base/_include_ps.fx"
#include "smoothA2C.hlsl"

RESULT_TYPE main(PS_IN_PerPixel pin PS_INPUT_EXTRA) {
  READ_VECTORS
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR_PV(txDiffuseValue);
  RAINFX_WET_FOLIAGE(txDiffuseValue);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  // pin.Fog = 0;

  RAINFX_REFLECTIVE_FOLIAGE(lighting);
  A2C_ALPHA(txDiffuseValue.a);
  RETURN_BASE(lighting, txDiffuseValue.a);
}
