// I think, it’s safe to assume there is absolutely no need for AO for this shader
#define SET_AO_TO_ONE

#include "include_new/base/_include_vs.fx"
#include "common/brakeDiscFX.hlsl"

PS_IN_BrakeDiscFX main(VS_IN vin) {
  PS_IN_BrakeDiscFX vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  vout.WheelCenterC = mul(float4(extWheelPos, 1), ksWorld).xyz - ksCameraPosition.xyz;
  vout.WheelNormalW = mul(extWheelNormal, (float3x3)ksWorld);
  vout.WheelUpW = mul(extWheelUp, (float3x3)ksWorld);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  return vout;
}
