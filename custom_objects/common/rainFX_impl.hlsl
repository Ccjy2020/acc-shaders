float4 _rainFx_water_impl(float4 base, _RAINFX_GETWATERREFL_ARGS){
  #ifdef MODE_GBUFFER
    return base;
  #endif

  float VdotN = saturate(-dot(R.normal, toCamera));
  float alpha = R.alpha;
  float3 reflDir = lerp(reflect(toCamera, R.normal), R.normal, 0);
  float3 reflColor = SAMPLE_REFLECTION_FN(reflDir, 2, false, REFL_SAMPLE_PARAM_DEFAULT);
  float reflStrength = lerp(0, 0.85, pow(saturate(1 - dot(R.normal, -toCamera)), 4));

  #ifdef RAINFX_GLASS_BACKLIT
    // float2 screenUV = posH.xy * extScreenSize.zw;
    // base.rgb += txPrevFrame.SampleLevel(samLinearBorder0, screenUV, 3.5).rgb * alpha * dimmingK;

    float refractionIntensity = saturate(R.alpha * 4 - 2);
    float2 offset = clamp(float2(dot(R.normal, normalize(cross(extDirUp, toCamera))), dot(R.normal, extDirUp)), -0.2, 0.2) * float2(1, 9./16) / length(posC);
    float4 refractionColor = txPrevFrame.SampleBias(samLinearClamp, posH.xy * extScreenSize.zw + offset * refractionIntensity * 0.1, 1);
    
    refractionIntensity *= 1 - base.a;
    base.rgb = lerp(base.rgb, refractionColor.rgb, refractionIntensity);
    base.a = lerp(base.a, 1, refractionIntensity);
  #endif

  float3 specular = reflectanceModel(-toCamera, -ksLightDirection.xyz, R.normal, 40) * shadow * extSpecularColor;
  base.rgb += (reflStrength * reflColor + specular) * alpha;
  base.a = lerp(base.a, 1, reflStrength * alpha);
  base.rgb *= lerp(1, R.occlusion, R.alpha);

  float t = abs(dot(R.normal, toCamera));
  // t = saturate(dot(R.normal, normalWBase));
  // t = R.alpha;
  // base.rgb = lerp(base.rgb, float3(t, 1 - t, 0), saturate(R.alpha * 10));
  // base.rgb = lerp(base.rgb, saturate(float3(dot(R.normal, extDirUp), dot(R.normal, normalize(cross(extDirUp, toCamera))), 0)), saturate(R.alpha * 10));
  // base.a = 1;

  return base;
}