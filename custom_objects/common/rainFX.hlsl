#include "include/common.hlsl"
#include "include/rain.hlsl"
#include "include_new/base/_flags.fx"

// #undef RAINFX_USE_PUDDLES_MASK
#define PUDDLES_MAP_SIDE 7
#define PUDDLES_MAX_OFFSET ((PUDDLES_MAP_SIDE - 1) / 2)

cbuffer cbMaterial : register(b5) {
  float4x4 extTransform;
  float4x4 extMaskTransform;

  float3 extUp;
  float extUpAligned;

  float3 extLeft;
  float extRainMult;

  float3 extFwd;
  float extObjectWetness;

  float3 extVelocity;
  float extRainPad3;

  float4x4 extShadowTransform;
  float4x4 extReliefTransform;
  float4 extPuddleIndex[PUDDLES_MAP_SIDE * PUDDLES_MAP_SIDE];
}

#define LIGHTINGFX_RAINFX

#ifdef TARGET_VS

  #ifdef RAINFX_USE_REGISTERS
    #define RAINFX_VERTEX(vout)\
      vout.PosR = mul(float4(vout.PosC.xyz, 1), extTransform).xyz;\
      vout.NormalR = mul(vout.NormalW.xyz, (float3x3)extTransform);
  #else
    #define RAINFX_VERTEX(vout)
  #endif

#else

  Texture2D<float> txPuddles : register(t26);
  // Texture2D<float> txPuddles : register(t27);
  Texture2D txRainDrops : register(t29);
  #ifdef RAINFX_USE_PUDDLES_MASK
    Texture2DArray<float2> txPuddlesMask : register(t28);  
    Texture2D<float2> txWetSkidmarks : register(t19);  
  #else
    Texture2D txRainStreaks : register(t30);
    Texture2D txRainTiny : register(t17);
  #endif
  Texture2D<float> txRainRelief : register(t27);  
  Texture2DArray<float> txRainShadow : register(t31);

  #ifdef USE_WIPERS_MASK
    Texture2D<float> txWipersMask : register(t28);
  #endif

  #include "include/poisson.hlsl"

  float2 sampleShadow(float2 uv, float comparison){
    float2 sum = 0;
    float sampleScale = 0.002;
    if (comparison >= 1) return 1; 

    #define RAIN_DISK_SIZE 6
    for (uint i = 0; i < RAIN_DISK_SIZE; ++i) {
      float2 sampleUV = uv.xy + SAMPLE_POISSON(RAIN_DISK_SIZE, i) * sampleScale;
      float comparisonUV = comparison - 0.001 * (1 + SAMPLE_POISSON_LENGTH(RAIN_DISK_SIZE, i));
      sum.x += txRainShadow.SampleCmpLevelZero(samShadow, float3(sampleUV, 0), comparisonUV);
      sum.y += txRainShadow.SampleCmpLevelZero(samShadow, float3(sampleUV, 1), comparisonUV);
    }
    return saturate(float2(sum.x, sum.y) / RAIN_DISK_SIZE);
  }

  float4 textureSampleVariation(Texture2D tx, SamplerState sam, float2 uv, float bias, float noiseMult);

  float3 remapNormalRipple(float2 val, float3 up, float3 side, float3 nm){
    float3 v = float3(val, 0);
    v.z = sqrt(saturate(1 - dot2(v.xy)));
    return v.x * side + v.y * up + v.z * nm;
  }

  float4 calculateRipple(float2 rainUV, float distance, float bigMult, float tinyMult, out float spot, out float wetSpot){
    #ifdef RAINFX_USE_PUDDLES_MASK
      float4 rainMapValue = textureSampleVariation(txRainDrops, samLinear, rainUV * 0.16, 1, 0.05);
    #else
      rainUV *= HAS_FLAG(FLAG_ISTRACKMATERIAL) ? 0.2 : 1;
      float4 rainMapValue = textureSampleVariation(txRainDrops, samLinear, rainUV, 1, 0.05);
      // rainMapValue.w = saturate(rainMapValue.w * 10);

      float4 tiny = txRainTiny.Sample(samLinear, rainUV * 1.5);
      tiny.xy = 1 - tiny.xy;
      tiny.a = saturate((tiny.a + (tinyMult - 1)) * lerp(8, 1, saturate(distance)));
      rainMapValue = lerp(tiny, rainMapValue, saturate(rainMapValue.z * 4) * bigMult);
    #endif
    #ifdef RAINFX_USE_PUDDLES_MASK
      float3 nmValue = remapNormalRipple(rainMapValue.xy, float3(0, 0, 1), float3(1, 0, 0), float3(0, 1, 0));
    #else
      float3 nmValue = remapNormal(rainMapValue.xy, float3(0, 0, 1), float3(1, 0, 0), float3(0, 1, 0));
    #endif
    spot = rainMapValue.z;
    #ifdef RAINFX_USE_PUDDLES_MASK
      wetSpot = saturate(rainMapValue.w * 11);
      return float4(nmValue, saturate((rainMapValue.w - 0.1) / 0.9));
    #else
      wetSpot = 0;
      return float4(nmValue, rainMapValue.w);
    #endif
  }

  struct StreakMapping {
    float2 uv1;
    float2 uv2;
    float share;
  };

  StreakMapping calculateStreakMapping(float3 posL, float3 normalW){
    StreakMapping ret;
    posL *= 5;
    float2 normalH = normalize(normalW.xz);
    ret.uv1 = abs(normalH.y) < 0.7071 ? posL.zy : posL.xy;
    if (max(normalH.x, -normalH.y) > 0.7071) ret.uv1.x = -ret.uv1.x;
    float2 rainUVTilt = normalH.x * normalH.y < 0 ? 0.7071 : float2(0.7071, -0.7071);
    ret.uv2 = float2(dot(posL.xz, rainUVTilt), posL.y);
    ret.uv1 *= float2(1, -1);
    ret.uv2 *= float2(1, -1);
    if (normalH.y < 0) ret.uv2.x *= -1;
    ret.share = saturate(abs(dot(normalH, rainUVTilt * float2(-1, 1))) * 4 - 3);
    return ret;
  }

  void calculateStreakSample(float2 uv, float4 duv, float k, out float4 retStreat, out float4 retTiny){     
    #ifdef RAINFX_USE_PUDDLES_MASK
      retStreat = 0;
      retTiny = 0;
    #else
      float l = k * 8;
      float i = floor(l);
      float f = frac(l);
      float2 offa = sin(float2(3, 7) * i);
      float2 offb = sin(float2(3, 7) * (i + 1));
      float4 cola = txRainStreaks.SampleGrad(samLinear, uv + offa, duv.xy, duv.zw);
      float4 colb = txRainStreaks.SampleGrad(samLinear, uv + offb, duv.xy, duv.zw);
      float4 tinya = txRainTiny.SampleGrad(samLinear, (uv + offa) * 2, duv.xy, duv.zw);
      float4 tinyb = txRainTiny.SampleGrad(samLinear, (uv + offb) * 2, duv.xy, duv.zw);

      // retStreat = lerp(cola, colb, smoothstep(0.2, 0.8, f - 0.1 * dot(cola - colb, 1)));
      // retTiny = lerp(tinya, tinyb, smoothstep(0.2, 0.8, f - 0.1 * dot(tinya - tinyb, 1)));

      retStreat = cola;
      retTiny = tinya;
    #endif
  }

  float4 calculateStreak(float3 posL, float3 normalL, float3 normalW, float bigMult, float tinyMult, float distance){
    #ifndef RAINFX_USE_PUDDLES_MASK
      StreakMapping SM = calculateStreakMapping(posL, normalL);
      float4 duv1 = float4(ddx(SM.uv1), ddy(SM.uv1));
      float4 duv2 = float4(ddx(SM.uv2), ddy(SM.uv2));

      float k0 = txNoise.Sample(samLinearSimple, 0.05 * SM.uv1).x;
      float k1 = txNoise.Sample(samLinearSimple, 0.05 * SM.uv2).x;
      float k = lerp(k0, k1, SM.share);

      float varianceOffset = 0;
      float4 streak1, streak2, tiny1, tiny2;
      calculateStreakSample(SM.uv1, duv1, k, streak1, tiny1);
      calculateStreakSample(SM.uv2, duv2, k, streak2, tiny2);
      float4 streak = lerp(streak1, streak2, SM.share);
      float4 tiny = lerp(tiny1, tiny2, SM.share);
      tiny.a = saturate((tiny.a - streak.z + (tinyMult - 1)) * lerp(8, 1, saturate(distance)));
      streak = lerp(tiny, streak, saturate(streak.z * 4) * bigMult);

      float alpha = saturate(streak.a * 1.4);
      float3 sideW = normalize(cross(normalW, float3(0, -1, 0)));
      float3 upW = normalize(cross(normalW, sideW));
      float3 nm = remapNormal(streak.xy, upW, sideW, normalW);
      return float4(nm, alpha);
    #else
      return 0;
    #endif
  }

  #define RIPPLE_START 0.4
  #define RIPPLE_END 0.8

  struct RainParams {
    float3 normal;
    float alpha;
    float damp;
    float wetness;
    float water;
    float dimming;
    float occlusion;
    float specMask;
    bool taaDisable;
  };

  float2 _rainFx_rainShadow(float3 posW, float3 normalW){
    float4 posH = mul(float4(posW, 1), extShadowTransform);
    return any(abs(posH.xy - 0.5) > 0.5) ? 1 : sampleShadow(posH.xy, posH.z);
  }

  float _rainFx_rainRelief(float3 posW){
    float4 posH = mul(float4(posW, 1), extReliefTransform);
    return any(abs(posH.xy - 0.5) > 0.5) ? 1 : saturate((txRainRelief.SampleLevel(samLinear, posH.xy, 2.5) - posH.z) * -500);
  }

  float _rainFx_wipersMask(float3 posW){
    #ifdef USE_WIPERS_MASK
      float4 posH = mul(float4(posW, 1), extMaskTransform);
      // return txWipersMask.Sample(samLinearBorder1, posH.xy);

      float level = max(0.5, txWipersMask.CalculateLevelOfDetail(samLinearBorder1, posH.xy));
      float sum = 0;
      #define WIPERS_DISK_SIZE 6
      for (uint i = 0; i < WIPERS_DISK_SIZE; ++i) {
        sum += txWipersMask.SampleLevel(samLinearBorder1, posH.xy + SAMPLE_POISSON(WIPERS_DISK_SIZE, i) * 0.01, level);
      }
      return sum / WIPERS_DISK_SIZE;
    #else
      return 1;
    #endif
  }

  float2 _rainFx_puddlesSampleBlur(float2 uv, float index){
    #ifdef RAINFX_USE_PUDDLES_MASK
      float2 ret = 0;      
      #define PUDDLES_DISK_SIZE 6
      for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
        float2 sampleUV = uv + SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.001;
        ret += txPuddlesMask.SampleLevel(samLinear, float3(sampleUV, index), 0);
      }
      return saturate(ret / PUDDLES_DISK_SIZE);
    #else
      return 0.5;
    #endif
  }

  float2 _rainFx_puddlesMask(float3 posW){
    #ifdef RAINFX_USE_PUDDLES_MASK
      float2 uv = posW.xz * extPuddleIndex[0].y + extPuddleIndex[0].zw;
      float2 uvFloor = floor(uv);
      int2 index = (int2)uvFloor;
      int indexBase = (index.y + PUDDLES_MAX_OFFSET) * PUDDLES_MAP_SIDE + (index.x + PUDDLES_MAX_OFFSET);
      float arrayIndex = any(abs(index) > PUDDLES_MAX_OFFSET) || extPuddleIndex[indexBase].x < 0 ? 63 : extPuddleIndex[indexBase].x;
      float paddingAdj = 0.54;
      // return txPuddlesMask.SampleLevel(samLinearSimple, float3(1 - uv, arrayIndex), 0);
      // return txPuddlesMask.SampleLevel(samLinearClamp, float3(frac(1 - uv), arrayIndex), 0);
      float2 uvAdj = (frac(1 - uv) * 2 - 1) * (0.5 / paddingAdj) * 0.5 + 0.5;
      return txPuddlesMask.SampleLevel(samLinearSimple, float3(uvAdj, arrayIndex), 0);
    #else
      return 0.5;
    #endif
  }

  struct RainSurfaceInput {
    float4 posH;
    float3 posC;
    float3 posW;
    float3 normalW;
    float3 posL;
    float3 normalL;
    float3 toCamera;
    #ifdef RAINFX_USE_PUDDLES_MASK
      float2 wetSkidmarks;
    #endif
  };

  float _rainFx_sceneWetness(){
    return extSceneWetness;
  }

  float _rainFx_sceneWater(){
    return extSceneWater;
  }

  void _rainFx_rainMapDrops(RainSurfaceInput input, float waterMult, out float wetSpot, out float puddleSpot, out float mixedAlpha, inout RainParams ret){    
    wetSpot = _rainFx_sceneWetness();
    puddleSpot = 0;
    mixedAlpha = 0;

    #if !defined(RAINFX_NO_RAINDROPS) && (!defined(MODE_GBUFFER) || defined(RAINFX_USE_PUDDLES_MASK))
      float distance = length(input.posC.xyz);
      float distanceK = distance / (abs(input.normalW.y) > RIPPLE_START ? 30 : 20);
      float angleK = saturate(remap(input.normalW.y, -0.2, -0.4, 1, 0));
      [branch]
      if (distanceK > 1 || !angleK){ 
        ret.normal = input.normalW;
      } else {
        #ifndef RAINFX_USE_PUDDLES_MASK
          float3 posFixed = float3(dot(input.posL, extLeft), dot(input.posL, extUp), dot(input.posL, extFwd));
          input.posL = lerp(posFixed, input.posL, extUpAligned);
          input.posL.xz += extVelocity.xz * (1 - input.posL.y) * 0.02;
          input.normalL = float3(dot(input.normalL, extLeft), dot(input.normalL, extUp), dot(input.normalL, extFwd));
        #endif

        float2 rainUV = input.posL.xz * 4;
        float mask = 1;
        float maskEdge = 1;
        #ifdef USE_WIPERS_MASK
          float wipersMask = _rainFx_wipersMask(input.posW);
          maskEdge = fwidth(wipersMask);
          mask *= wipersMask * wipersMask;
        #endif

        float2 rainShadow = _rainFx_rainShadow(input.posW, input.normalW);
        #ifdef RAINFX_USE_PUDDLES_MASK
          float bigMult = 1;
          float tinyMult = 0;
        #else
          float bigMult = rainShadow.y * saturate(extSceneRain * 2);
          float tinyMult = extObjectWetness;
        #endif

        float4 ripple;
        float distanceMult = 0;
        [branch]
        if (input.normalW.y > RIPPLE_START){
          float wetSpotBase;
          ripple = calculateRipple(rainUV, distance, bigMult, tinyMult, puddleSpot, wetSpotBase);
          #ifdef RAINFX_USE_PUDDLES_MASK
            distanceMult = saturate(remap(distance, 3, 30, 1, 0));
            ret.taaDisable = distanceMult > 0;
          #else
            distanceMult = saturate(remap(distance, 20, 30, 1, 0));
          #endif
          ripple.a *= distanceMult;
          if (!HAS_FLAG(FLAG_RAINFX_SMOOTH)){
            // wetSpot = clamp(wetSpotBase * distanceMult * extSceneRain * wetSpot * 2, wetSpot, 1);
            wetSpot = lerp(wetSpot, saturate(wetSpot * wetSpotBase * 3), saturate(extSceneRain * 2) * (1 - wetSpot));
          }
        } else {
          ripple = float4(input.normalW, 0);
        }
        
        float4 streak;
        #ifdef RAINFX_USE_PUDDLES_MASK
          streak = float4(input.normalW, 0);
        #else
          [branch]
          if (HAS_FLAG(FLAG_RAINFX_SMOOTH) && input.normalW.y < RIPPLE_END && distance < 20){
            streak = calculateStreak(input.posL, input.normalL, input.normalW, bigMult, tinyMult, distance);
            streak.a *= saturate(remap(distance, 14, 20, 1, 0));
          } else {
            streak = float4(input.normalW, 0);
          }  
        #endif
        
        float4 mixed = lerp(streak, ripple, saturate(remap(input.normalL.y, RIPPLE_START, RIPPLE_END, 0, 1)));
        #ifdef USE_WIPERS_MASK
          float4 mult1 = txNoise.SampleLevel(samLinear, rainUV, 0);
          mixed.a = lerp(mixed.a, 1, saturate(maskEdge * 10));
          if (mixed.a < (1 - mask) * lerp(3, 4, mult1.x)){
            mixed.a = 0;
          }
        #endif

        #ifdef RAINFX_USE_PUDDLES_MASK
          mixedAlpha = ripple.a;
          ret.alpha = rainShadow.y * saturate(extSceneRain * 2) * waterMult;
          ret.normal = normalize(lerp(input.normalW, ripple.xyz, distanceMult * ret.alpha * 0.2));
        #else
          ret.alpha = mixed.a * waterMult;
          ret.normal = normalize(lerp(input.normalW, mixed.xyz, ret.alpha));
        #endif

        float occlusionBase = dot(ret.normal, input.normalW);
        float occlusionOffset = 1 + dot(ret.normal, input.toCamera);
        ret.occlusion = pow(saturate(occlusionBase + occlusionOffset), 2);
      }
    #endif
  }

  float _rainFx_puddleValue(float randomMask, float waterMask, float hillMask0, float hillMask1, float randomFreqMask){
    float adjustedMask = waterMask;
    adjustedMask += (randomFreqMask - 0.5) * 0.1;
    adjustedMask = saturate((adjustedMask - min(hillMask0, 0.2)) * 6) * (1 - hillMask1);

    return max(
      randomMask * (0.5 + 0.1 * saturate(1 - hillMask0 * 10)) + adjustedMask * saturate(remap(randomMask, 0, 1, 0.1, 1)),
      saturate(waterMask * 5 - 2.5 - randomFreqMask * 2) * 0.5);
  }

  void _rainFx_rainCalculate(float sceneWet, float sceneWater, float puddleValue, float reliefValue, inout RainParams ret){
    rainCalculate(sceneWet, sceneWater, puddleValue, reliefValue, ret.damp, ret.wetness, ret.water);
  }

  RainParams _rainFx_rainMap(RainSurfaceInput input, inout float4 txDiffuseValue, float vaoValue){
    input.posW = input.posC + ksCameraPosition.xyz;
    RainParams ret = (RainParams)0;

    // Input locational layers
    float randomMask = 0.5;
    float randomFreqMask = 0;
    float reliefMask = 0;
    float waterMask = 0;
    float dryingMask = 0;
    float horizontalMask = 0;

    // Hill coefficient
    float hillMask0 = 0;
    float hillMask1 = 0;
    float hillWaves = 0;

    #ifdef RAINFX_USE_PUDDLES_MASK
      randomMask = txPuddles.Sample(samLinearSimple, input.posW.xz * 0.021);
      randomFreqMask = txPuddles.Sample(samLinearSimple, input.posW.xz * 0.071);
      reliefMask = _rainFx_rainRelief(input.posW);

      float2 puddlesMask = _rainFx_puddlesMask(input.posW);
      waterMask = puddlesMask.x;
      dryingMask = puddlesMask.y;

      horizontalMask = saturate(remap(input.normalW.y, RIPPLE_START, RIPPLE_END, 0, 1));
      hillMask0 = saturate(remap(input.normalW.y, 0.985, 0.995, 1, 0));
      hillMask1 = saturate(remap(input.normalW.y, 0.98, 0.985, 1, 0));
      hillWaves = saturate(remap(input.normalW.y, 0.997, 0.9995, 1, 0));
    #endif

    // Occlusion
    float occlusionMult = saturate(remap(vaoValue, 0.15, 0.6, 0, 1));
    float waterMult = saturate(remap(vaoValue, 0.3, 0.35, 0, 1));

    // Rain drops & wet spot
    float sceneWet, puddleSpot, mixedAlpha;
    _rainFx_rainMapDrops(input, waterMult, sceneWet, puddleSpot, mixedAlpha, ret);

    // Water thickness
    float sceneWater = _rainFx_sceneWater();

    // Output material layers
    #ifdef RAINFX_USE_PUDDLES_MASK
      float puddleValue = _rainFx_puddleValue(randomMask, waterMask, hillMask0, hillMask1, randomFreqMask) * occlusionMult;
      _rainFx_rainCalculate(sceneWet, sceneWater, puddleValue, reliefMask, ret);

      ret.water = lerp(ret.water, 1, saturate(input.wetSkidmarks.x - 0.5) * ret.wetness);
      ret.wetness = lerp(ret.wetness, 1, saturate(input.wetSkidmarks.x * 2 - 1) * ret.damp);
      ret.damp = lerp(ret.damp, 1, saturate(input.wetSkidmarks.x * 3));
    #else
      float nmOcc = saturate(input.normalW.y) * 0.5 + 0.5;
      ret.damp = sceneWet * saturate(occlusionMult / nmOcc) * HAS_FLAG(FLAG_ISTRACKMATERIAL);
    #endif

    // Fix for non-horizontal surfaces 
    #ifdef RAINFX_USE_PUDDLES_MASK
      ret.water *= horizontalMask;
      ret.wetness *= horizontalMask;
      ret.damp = lerp(_rainFx_sceneWetness(), ret.damp, horizontalMask);
    #endif

    // Water rolling down for tilted puddles
    #ifdef RAINFX_USE_PUDDLES_MASK
      float rK = saturate(extSceneRain * 2) * hillWaves * ret.water * saturate(randomMask * 2 - 1) * 0.5; 
      float rI0 = input.posW.y * 200 + ksGameTime * 0.01 + randomMask * M_PI * 40;
      float rI1 = input.posW.y * 1000 + ksGameTime * 0.01 + randomMask * M_PI * 40;
      float rW = 0.5 + 0.5 * lerp(sin(rI0), sin(rI1), saturate(remap(input.normalW.y, 0.99, 0.995, 1, 0)));
      ret.normal = normalize(ret.normal * (1 + float3(1, 0, 1) * rK * rW));
    #endif

    // Dimming for damp surfaces
    float dimmingWet = 1 - 0.35 * !HAS_FLAG(FLAG_RAINFX_SMOOTH) - 0.35 * HAS_FLAG(FLAG_RAINFX_SOAKING);
    ret.dimming = lerp(1, dimmingWet, ret.damp);

    // Other parameters
    ret.specMask = HAS_FLAG(FLAG_RAINFX_SMOOTH) ? 1 : saturate(dot(txDiffuseValue.g, 2./3.));

    // Puddle-receiving material drop effect
    #ifdef RAINFX_USE_PUDDLES_MASK
      float waterFocus = saturate(ret.water * 6 - 5);
      ret.alpha *= lerp(mixedAlpha, puddleSpot, waterFocus);
      ret.alpha *= 1 - waterFocus * 0.9;
    #endif
    ret.taaDisable = ret.taaDisable && saturate(extSceneRain * 4) * ret.water > 0.99;

    // Debug
    // txDiffuseValue.rgb = float3(puddleValue, 1 - puddleValue, 0);
    // txDiffuseValue.rgb = float3(waterMask, 1 - waterMask, 0);
    // txDiffuseValue.rgb = float3(sqrt(hill), 1 - sqrt(hill), 0);
    // txDiffuseValue.rgb = float3(sceneWet, 1 - sceneWet, 0);
    // txDiffuseValue.rgb = float3(ret.wetness, 1 - ret.wetness, 0);
    // txDiffuseValue.rgb = float3(ret.water, 1 - ret.water, 0);
    // txDiffuseValue.rgb = float3(ret.damp, 1 - ret.damp, 0);
    // txDiffuseValue.rgb = float3(input.wetSkidmarks.x, 1 - input.wetSkidmarks.x, 0);

    #ifdef MODE_PUDDLES
      #ifdef RAINFX_USE_PUDDLES_MASK
        ret.wetness = reliefMask;
        ret.water = puddleValue;
      #else
        ret.wetness = 0;
        ret.water = 0;
      #endif
    #endif

    return ret;
  }

  #define _RAINFX_GETWATERREFL_ARGS float4 posH, float3 posW, float3 normalWBase, float3 posC, float3 toCamera, float shadow, RainParams R
  #define _RAINFX_GETWATERREFL_CALL posH, posW, normalWBase, posC, toCamera, shadow, R

  float4 _rainFx_water_impl(float4 base, _RAINFX_GETWATERREFL_ARGS);

  float4 _rainFx_water(float4 base, _RAINFX_GETWATERREFL_ARGS){
    return _rainFx_water_impl(base, _RAINFX_GETWATERREFL_CALL);
  }

  float3 _rainFx_water(float3 base, _RAINFX_GETWATERREFL_ARGS){
    return _rainFx_water_impl(float4(base, 1), _RAINFX_GETWATERREFL_CALL).rgb;
  }

  float3 _rainFx_applyDim(float3 color, float dimming) { return color * dimming; }
  float4 _rainFx_applyDim(float4 color, float dimming) { return color * float4(dimming.xxx, 1); }

  void _rainFx_applyShiny(RainParams RP, float3 normalWBase, inout float3 normalW, inout float specExp, inout float specValue){
    specExp = lerp(specExp, 40, RP.damp);
    specValue = lerp(specValue, 0.3, RP.damp * RP.specMask);

    #ifdef RAINFX_USE_PUDDLES_MASK
      specExp = lerp(specExp, 120, RP.wetness * lerp(0.5, 1, RP.specMask));
      specValue = lerp(specValue, 1.0, RP.wetness * RP.specMask);
      specExp = lerp(specExp, 700, RP.water);
      specValue = lerp(specValue, 2.0, RP.water);

      normalW = lerp(normalW, RP.normal, min(RP.wetness * 2, 0.8));
      normalW = normalize(lerp(normalW, RP.normal, RP.water));
    #endif
  }

  void _rainFx_applyReflective(RainParams RP, inout float fresnelC, inout float fresnelMaxLevel, 
      inout float fresnelEXP, inout float specExp, inout float finalMult){
    if (HAS_FLAG(FLAG_RAINFX_ROUGH)) return;
    
    specExp = max(2, lerp(specExp, max(specExp, 2), RP.damp));
    fresnelMaxLevel = lerp(fresnelMaxLevel, 0.05, RP.damp);
    fresnelC = lerp(fresnelC, 0.02, RP.damp);

    specExp = lerp(specExp, 200, RP.wetness);
    fresnelMaxLevel = lerp(fresnelMaxLevel, 0.5, RP.wetness);
    fresnelC = lerp(fresnelC, 0.04, RP.wetness);

    specExp = lerp(specExp, 300, RP.water);
    fresnelMaxLevel = lerp(fresnelMaxLevel, 0.7, RP.water);
    fresnelC = lerp(fresnelC, 0.06, RP.water);
    
    fresnelEXP = lerp(fresnelEXP, 5, RP.damp);
    finalMult = lerp(finalMult, 1, RP.damp);
  }

  #ifdef RAINFX_USE_PUDDLES_MASK
    #define _RAINFX_INIT_SCREENSPACE RP_input.wetSkidmarks = txWetSkidmarks.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0);
  #else
    #define _RAINFX_INIT_SCREENSPACE
  #endif

  #define _RAINFX_INIT_BASE \
    float3 normalWBase = normalize(pin.NormalW);\
    RainSurfaceInput RP_input;\
    RP_input.posH = pin.PosH;\
    RP_input.posC = pin.PosC;\
    RP_input.normalW = normalWBase;\
    RP_input.toCamera = toCamera;\
    _RAINFX_INIT_SCREENSPACE

  #ifdef RAINFX_USE_REGISTERS
    #define RAINFX_INIT \
      _RAINFX_INIT_BASE\
      RP_input.posL = pin.PosR;\
      RP_input.normalL = pin.NormalR;\
      RainParams RP = _rainFx_rainMap(RP_input, txDiffuseValue, ((float3)(_AO_VAO)).g);\
      float3 rainNormal = RP.normal;
  #else
    #define RAINFX_INIT \
      _RAINFX_INIT_BASE\
      RP_input.posL = pin.PosC + ksCameraPosition.xyz;\
      RP_input.normalL = pin.NormalW;\
      RainParams RP = _rainFx_rainMap(RP_input, txDiffuseValue, ((float3)(_AO_VAO)).g);\
      float3 rainNormal = RP.normal;
  #endif

  #define RAINFX_WET(X) RAINFX_INIT; RAINFX_WET_DIM(X.rgb);
  #define RAINFX_WET_FOLIAGE(X) RAINFX_INIT; X.rgb *= lerp(X.g, 1, saturate(remap(RP.dimming, 0.3, 1, 0, 1)));
  #define RAINFX_WET_DIM(X) X = _rainFx_applyDim(X, RP.dimming);
  #define RAINFX_SHINY(X) \
    _rainFx_applyShiny(RP, normalWBase, normalW, X.specularExp, X.specularValue);\
    X.normalW = normalW;
  #define RAINFX_REFLECTIVE_SKIP(X) \
    X.fresnelCDry = X.fresnelC;\
    X.fresnelMaxLevelDry = X.fresnelMaxLevel;\
    X.fresnelEXPDry = X.fresnelEXP;

  #if !defined(MODE_GBUFFER)
    #define RAINFX_REFLECTIVE_FOLIAGE(X) \
      if (!HAS_FLAG(FLAG_RAINFX_ROUGH)){\
        ReflParams R = getReflParamsZero();\
        RAINFX_REFLECTIVE_SKIP(R);\
        R.finalMult = RP.damp;\
        R.fresnelMaxLevel = saturate(remap(txDiffuseValue.g, 0.1, 0.4, 0, 1)) * lerp(0.2, 0.3, extSceneRain);\
        R.fresnelC = 1;\
        R.fresnelEXP = 1;\
        X = calculateReflection(X, toCamera, pin.PosC, normalW, R);\
      }
  #else
    #define RAINFX_REFLECTIVE_FOLIAGE(X)
  #endif

  #define RAINFX_REFLECTIVE(X) \
    RAINFX_REFLECTIVE_SKIP(X);\
    _rainFx_applyReflective(RP, X.fresnelC, X.fresnelMaxLevel, X.fresnelEXP, X.ksSpecularEXP, X.finalMult);
  // #define RAINFX_WATER(X) 
  #define RAINFX_WATER(X) X = _rainFx_water(X, pin.PosH, pin.PosC + ksCameraPosition.xyz, normalWBase, pin.PosC, toCamera, shadow, RP);
  #define RAINFX_WATER_ALPHA(X, Y) { float4 v = _rainFx_water(float4(X, Y), pin.PosH, pin.PosC + ksCameraPosition.xyz, normalWBase, pin.PosC, toCamera, shadow, RP); X = v.xyz; Y = v.w; }

  #define RAINFX_REFLECTIVE_WATER_ROUGH(X) \
    ReflParams R = getReflParamsZero();\
    if (!HAS_FLAG(FLAG_RAINFX_ROUGH)) {\
      RAINFX_REFLECTIVE(R);\
      X = calculateReflection(X, toCamera, pin.PosC, normalW, R);\
      RAINFX_WATER(X);\
    }

#endif