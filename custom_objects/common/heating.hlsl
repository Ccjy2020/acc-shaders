#define EMISSIVE_SET(x)\
  float3 ev##x##_pos;\
  float ev##x##_inner;\
  float ev##x##_thicknessInv;\
  float ev##x##_tint;\
  float ev##x##_normalsK;\
  float ev##x##_symmetryX;\
  float3 ev##x##_scale;\
  float ev##x##_pad;\

cbuffer cbExhaustVolumes : register(b10) {  
  EMISSIVE_SET(0)
  EMISSIVE_SET(1)
}

#define EMISSIVE_GET(x)\
  EmissiveGet(posL, normalW, ev##x##_pos, ev##x##_inner, ev##x##_thicknessInv, ev##x##_tint, ev##x##_normalsK, ev##x##_symmetryX, ev##x##_scale, ev##x##_pad);

float3 EmissiveGet(float3 posL, float3 normalW, float3 pos, float inner, float thicknessInv, float tint, float normalsK, float symmetryX, float3 scale, float pad){
  if (symmetryX && posL.x < 0) {
    posL.x = abs(posL.x);
    normalW.x = -normalW.x;
  }
  float3 dif3 = posL - pos;
  dif3 *= scale;
  float difL = length(dif3);
  float3 difN = dif3 / difL;
  float dif = 1 - saturate((difL - inner) * thicknessInv);
  float nK = saturate(-dot(difN, normalW) + pad);
  return dif * dif * tint * lerp(1, nK, normalsK);
}

float3 EmissiveGetAll(float3 posL, float3 normalW){
  float3 emissiveMult = 0;
  emissiveMult += EMISSIVE_GET(0);
  emissiveMult += EMISSIVE_GET(1);
  return emissiveMult;
}

#define HEATINGFX(lighting)\
  lighting += EmissiveGetAll(pin.PosL, pin.NormalL) * ksEmissive * getEmissiveMult();