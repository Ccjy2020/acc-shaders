#ifdef TARGET_VS

  float3 rainBillboard(uint vertexID, float3 posW, float3 velC, float particleSize,  
      out float2 tex, out float3 normalBase, out float blur){
    float2 quadPos = RAIN_BILLBOARD[vertexID];

    float3 billboardAxis = normalize(posW - ksCameraPosition.xyz);
    #ifdef MODE_SHADOW
      billboardAxis = ksLightDirection.xyz;
    #endif

    float3 velocity = dot(velC, 1) ? velC : float3(0, 1, 0);
    float speed = length(velocity);
    float3 velocityDir = velocity / speed;

    float3 side = normalize(cross(billboardAxis, velocityDir));
    float3 up = normalize(cross(billboardAxis, side));

    tex = quadPos;
    normalBase = up * tex.y + side * tex.x;
    blur = extFrameTime * saturate(remap(length(velC), 2, 3, 0, 1));

    float3 offset = (side * quadPos.x + up * quadPos.y) * particleSize;
    float3 stretchOffset = velocity * dot(velocityDir, normalize(offset));
    offset += 0.005 * stretchOffset;
    return offset;
  }

#endif
