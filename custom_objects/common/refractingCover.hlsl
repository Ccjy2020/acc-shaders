cbuffer cbRefractingCover : register(b12) {  
  float3 extInnerPos;
  float extInnerRadius;

  float3 extInnerDir;
  float extInnerDepth;

  float3 extInnerUp;
  float extIOR;

  float3 extInnerSide;
  float extIORInv;

  float extF0;
  float extAbsorptionFactor;
  float extNmShareExt;
  float extNmShareInt;

  float extReflectionMult;
  float extReflectionDiffuseMult;
  float extRaytraceStepStart;
  float extRaytraceStepIncrease;

  float extLODBias;
  float3 extGlassColor;

  float extBaseEmissiveK;
  float extAmbientMult;
  float extInnerSpecular;
  float extInnerSpecularEXP;

  float extBouncedBackMult; // 0.4
  float extExtraSideThickness; // 100
  float extGlassExtraThickness; // 0.25
  float extGlassEmissiveMult; // 0.25

  float extKsEmissiveMult;
  float extIORFlyOut; // extIOR→1
  float extSideFalloff;
  float extHasOverlay;

  float4 extMirrorPlane;

  float extUseKsEmissive;
  float extDiffuseMapMult;
  float extReflectiveGamma;
  float extDiffuseMapEmissiveMult;

  float ksEmissiveSeparate;
  float3 ksEmissive1;

  float3 ksEmissive2;
  float extBulbReflectionK;

  float3 ksEmissive3;
  float extBlurMult;

  float3 ksEmissive4;
  float extUseNormalAlpha;

  float3 ksEmissive5;
  float emMirrorChannel2As5;

  float4x4 extNormalTransform;
}

#ifdef USE_MULTISAMPLING_SHADING
  #define MULTISAMPLE_INPUT sample
#else
  #define MULTISAMPLE_INPUT
#endif

struct PS_IN_RefractingCover {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  MULTISAMPLE_INPUT float3 PosC : TEXCOORD1;
  MULTISAMPLE_INPUT float3 PosV : POSITIONLOCAL;
  float IsMirrored : SOMEFLAG;
  MULTISAMPLE_INPUT float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float Fog : TEXCOORD8;
  AO_NORMALS
};

#ifdef TARGET_PS
  Texture2D txSurfaceNormal : register(t4);
  Texture2D txInnerNormal : register(t5);
  Texture2D txInnerDiffuse : register(t3);
  Texture2D txInnerEmissive : register(t2);
  Texture2D<float> txInnerDepth : register(t21);
  Texture2D txOverlayMask : register(t23);
#endif