## Custom Shaders for Assetto Corsa

[![License: MIT](https://img.shields.io/badge/license-MIT-orange.svg)](https://trello.com/b/xq54vHsX/ac-patch) 
[![More information: https://trello.com/b/xq54vHsX/ac-patch](https://img.shields.io/badge/trello-more%20info-brightgreen.svg)](https://trello.com/b/xq54vHsX/ac-patch) 
[![Join the chat in Discord: https://discord.gg/buxkYNT](https://img.shields.io/badge/discord-join%20chat-brightgreen.svg)](https://discord.gg/W2KQCMH)

This is a part of [Custom Shaders Patch](https://trello.com/b/xq54vHsX/ac-patch), an extension for racing simulator Assetto Corsa adding some small visual enhancements here and there. Some shaders here were recreated by hand to match original ones in look as close as possible, while others are new and used for some extra features like Weather FX.

[![Screenshot by kelnor34](https://i.imgur.com/JnqMoEY.png)](https://i.imgur.com/JnqMoEY.png)

For more information check [this webpage](https://acstuff.ru/patch/).

## Notes

* There is a compilation script working with Node.JS, don’t forget to run `npm install` from `.build` folder first (or use VS Code and agree on executing auto-task);
* Feel free to create your own shaders based on these;
* But please consider sending some PR back: unless shaders are included with Shaders Patch, they might not always work as intended.
  * One example: with Weather FX release, new fog algorithm was added, so shaders not included with patch (and therefore not supporting it initially) could look out of place for some time;
  * Same goes for dynamic lighting and such;
* To run `compile.js`, first set `SHADERS_FXC_LOCATION` envinromental variable, or just look at [.vscode/tasks.json](/ac-custom-shaders-patch/public/acc-shaders/blob/master/.vscode/tasks.json) (or, again, just use VS Code);
* Compiled shaders are stored in `.output` folder, drop junctions to those to `assettocorsa/extension/shaders` to test them.

## Credits

* First of all, of course, we’d first like to thank the Kunos developers for all of the effort they put into making AC such a great simulator and giving us an excellent basis for our work.
* Lighting algorithm is based on [a fantastic idea](https://worldoffries.wordpress.com/2015/02/19/simple-alternative-to-clustered-shading-for-thousands-of-lights/) of **Luke Mamacos**;
* Sky shader is forked from this [great shader](https://www.shadertoy.com/view/Ml2cWG) made by **robobo1221**;
* GPU-accelerated particles are based on this [incredibly helpful article](https://wickedengine.net/2017/11/07/gpu-based-particle-simulation/) by **turanszkij**.


## More credits

* [CMAA](https://github.com/GameTechDev/CMAA2/);
* [FXAA](https://github.com/NVIDIAGameWorks/GraphicsSamples/tree/master/samples/es3-kepler/FXAA);
* [MLAA](https://github.com/GPUOpen-LibrariesAndSDKs/MLAA11/);
* [SMAA](https://github.com/iryoku/smaa).