#include "pfxSkidWet.hlsl"

void process(inout Particle particle, uint3 DTid) {
  particle.life -= DTid.x > gALS2 ? gFrameTime * 10 : particle.life > 0.5 || DTid.x > gALS1 ? gFrameTime * 0.5 : 0;
}
