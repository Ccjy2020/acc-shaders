#include "pfxWindscreen.hlsl"
#include "include/normal_encode.hlsl"
#include "include/samplers.hlsl"

// Texture2D txNormalsPacked : register(t6);
// Texture2D txNormalsPacked : register(t6);
Texture2D txNormalsX : register(t5);
Texture2D txNormalsY : register(t6);
Texture2D txWipers : register(t7);
Texture2D txWater : register(t8);
Texture2D txOcclusion : register(t10);
Texture2D txNoise : register(t20);
RWTexture2D<float4> rwWiperDirt : register(u7);

struct FailedOffDrop {
  float2 pos;
  float2 velocity;
  float size;
  float3 pad;
};

AppendStructuredBuffer<FailedOffDrop> buFailedOff: register(u6);

float trail(float4 v){
  return saturate(v.z - v.a * 2);
}

void process(inout Particle particle, uint3 DTid) {
  if (gFrameTime == 0) return;

  // float4 normalsPacked = txNormalsPacked.SampleLevel(samLinearSimple, particle.pos, 0);
  // if (dot(normalsPacked, 1) == 0){
  //   particle.life = 0;
  //   return;
  // }
  // float3 dirX = normalDecode(normalsPacked.xy);
  // float3 dirY = normalDecode(normalsPacked.zw);

  const float dragTracing = 1.0 / (1.0 + gFrameTime * 10);

  float4 prevState = txWater.SampleLevel(samLinearSimple, particle.pos, 2);
  float4 prevStateBlurred = txWater.SampleLevel(samLinearSimple, particle.pos, 3);
  float2 localVelocity = prevState.xy / 10;
  float localDensity = saturate(prevStateBlurred.a * 2.5 - 1.5);
  // float localDensity = saturate(prevStateBlurred.a * 3.5 - 2.5);

  if (particle.squished == 0) {
    float looseningMult = lerp(0.1, 1.8, particle.size);
    particle.sticky += gFrameTime * -0.02;
    particle.sticky -= saturate(length(localVelocity) * 3 * looseningMult - 0.1);
    particle.sticky -= localDensity * 0.1 * looseningMult;
    // particle.sticky -= saturate(prevStateBlurred.a * 3.5 - 2.5) * 0.1;
    // particle.sticky -= saturate(pow(length(gVelocity) * lerp(0.2, 0.4, particle.rand), 2) * 5 - 4) * 0.2;
    particle.sticky = clamp(particle.sticky, -0.2, 1);
    // particle.debug = particle.sticky;
  }

  const float dragWiper = 1.0 / (1.0 + gFrameTime * 10);
  const float dragEasy = 1.0 / (1.0 + gFrameTime * 10);
  const float dragWiperHeavy = 1.0 / (1.0 + gFrameTime * 40);

  float2 wipersPos = particle.pos + (dot(particle.velocity, 1) 
    ? normalize(particle.velocity) * particle.wiperLag * gActualScale * 0.008 : 0);
  particle.wiperLag = lerp(particle.moved ? 1 : 0, particle.wiperLag, dragWiperHeavy);

  float4 wipers = txWipers.SampleLevel(samPointWrap, wipersPos, 0);
  float2 wiperVelocity = (wipers.xy * 2 - 1) * 3.6 * gActualScale / 0.877453;

  float4 wipersPoint = txWipers.SampleLevel(samLinearSimple, wipersPos, 0);
  if (wipersPoint.a > 0.9){
    uint2 dirtPoint = uint2(wipersPoint.z * 255, 0);
    rwWiperDirt[dirtPoint] += clamp(dot2(wiperVelocity) - 0.1, 0, 0.1);
  }

  if (particle.moved < 0){
    particle.moved = min(0, particle.moved + gFrameTime * 10);
  } else if (wipers.a > 0.5){
    if (particle.squished > 0 || frac(particle.rand * 197) > 0.995 && dot2(wiperVelocity) > MIN_WIPERS_SPEED){
    // if (particle.squished > 0 || frac(particle.rand * 197) > 0.98 && dot2(wiperVelocity) > MIN_WIPERS_SPEED){
      if (!particle.squished){
        particle.sizeMult = particle.pos.x;
        particle.sticky = particle.pos.y;
      }
      particle.squished += gFrameTime * 5;
      // particle.velocity = wiperVelocity;
      particle.velocity = lerp(wiperVelocity, particle.velocity, dragWiper);
      particle.pos += gScale * wiperVelocity * 0.8 * gFrameTime;
      particle.life = 1;
      if (dot2(wiperVelocity) < MIN_WIPERS_SPEED){
        particle.life = 0;
      }
    } else {
      if (dot2(wiperVelocity) > MIN_WIPERS_SPEED && !(particle.moved > 0 && dot(particle.velocity, wiperVelocity) < 0)){
        particle.velocity = lerp(wiperVelocity * 0.4, particle.velocity, dragWiperHeavy);
        particle.pos += gScale * wiperVelocity * gFrameTime;
        particle.sticky = 0;
        particle.moved = 1;
      } else {
        if (dot2(particle.velocity) < MIN_WIPERS_SPEED) {
          particle.velocity = 0;
          particle.sticky = 1;
          particle.moved = 0;
          particle.life -= gFrameTime;
        } else {
          particle.moved = -1;
          // particle.pos += gScale * wiperVelocity * 20 * gFrameTime;
        }
      }
    }
  } else {
    particle.moved = saturate(particle.moved - 0.5);
    // particle.moved = 0;
    if (particle.squished > 0) {
      particle.life = min(1, particle.life - gFrameTime * 20);
      // particle.life = 0;
    }
  }

  float4 dirX = txNormalsX.SampleLevel(samLinearSimple, particle.pos, 0);
  float4 dirY = txNormalsY.SampleLevel(samLinearSimple, particle.pos, 0);
  float4 occlusion = txOcclusion.SampleLevel(samLinearSimple, particle.pos, 0);
  if (occlusion.x < lerp(0.1, 0.5, frac(particle.rand * 165167.931))){
    particle.life = 0;
  }

  if (particle.moved > 0){
    return;
  }

  if (particle.squished) {
    const float dragHeavy = 1.0 / (1.0 + gFrameTime * 30);
    // particle.pos += gScale * gFrameTime * particle.velocity;
    // particle.sizeMult = lerp(particle.pos.x, particle.sizeMult, dragHeavy);
    // particle.sticky = lerp(particle.pos.y, particle.sticky, dragHeavy);
    // particle.pos += gScale * gFrameTime * particle.velocity;
    particle.sizeMult = lerp(particle.pos.x, particle.sizeMult, dragHeavy);
    particle.sticky = lerp(particle.pos.y, particle.sticky, dragHeavy);
  } else {
    particle.life -= DTid.x > gALS2 ? gFrameTime * 30 : particle.life < 1 || DTid.x > gALS1 ? gFrameTime : gFrameTime * 0.25;
    particle.life -= localDensity * gFrameTime * 5;

    float blurredMask = txNormalsX.SampleLevel(samLinearSimple, particle.pos, 4).a;
    float actualEdge = saturate(remap(blurredMask, 0.9, 0.6, 0, 1));
    float edge = saturate(remap(blurredMask, 0.9, 0.7, 0, 1)) * 0.9;

    particle.life -= saturate(edge * 4 - 3) * gFrameTime * 2;
    particle.splashMult *= dragEasy;

    if (gFlyOff){
      edge = 0;
    }

    float3 normal = normalize(cross(dirX.xyz, dirY.xyz));
    float airResistanceK = saturate(dot(normal, gCarVelocity) * lerp(1, -1, saturate(dirY.w)) / 40);
    particle.sizeMult = lerp(lerp(0.75, 0.55, airResistanceK), particle.sizeMult, dragEasy);
    particle.life -= airResistanceK * gFrameTime * 0.5;

    if (gCollisionIntensity > 0 
      && dot(gCollisionDir, normal) > 0.1
      && particle.splashMult < 0.5 
      && gCollisionIntensity > frac(particle.rand * 196173.61) * 0.5) {
      particle.life = 0;
    }

    if (gFlyOff){
      float lessBlurredMask = txNormalsX.SampleLevel(samLinearSimple, particle.pos, 2).a;
      if (lessBlurredMask < 0.9) {
        particle.life = 0;

        float openValue = occlusion.y;
        if (openValue > 0.95) {
          FailedOffDrop fod = (FailedOffDrop)0;
          fod.pos = particle.pos;
          fod.velocity = gScale * particle.velocity;
          fod.size = particle.size * particle.sizeMult;
          buFailedOff.Append(fod);
        }
      }
    } 
    
    if (particle.moved == 0 && (!gFlyOff || actualEdge < 0.8)) {
      float2 gravityU = float2(dot(gVelocity, dirX.xyz), dot(gVelocity, dirY.xyz));
      float4 noise = txNoise.SampleLevel(samLinearSimple, particle.pos / gScale * 128 * gScale, 0);
      float2 offsetBase = (noise.xy * 2 - 1) * (1 - abs(dot(normal, gVelocity)));
      float2 offset = sign(offsetBase.xy) * pow(saturate(abs(offsetBase.xy) * 2.5 - 1), 1.6);
      float3 velocityW = gVelocity * 0.5; 
      velocityW += (gVelocitySide0 * offset.x + gVelocitySide1 * offset.y) * saturate(noise.z * 2) * length(gVelocity) * 0.5;
      velocityW += float3(0, gCarLift * (occlusion.z * 2 - 1), 0);
      // velocityW = float3(0, -2, 0);
      // velocityW = 0.0001;
      float2 velocityU = float2(dot(velocityW, dirX.xyz), dot(velocityW, dirY.xyz));

      // Force pushing raindrops apart
      float2 nextPos = particle.pos + particle.velocity * 0.0;
      float4 waterC = txWater.SampleLevel(samLinearSimple, nextPos, 3);
      float4 waterL = txWater.SampleLevel(samLinearSimple, nextPos + float2(-0.001, 0), 3);
      float4 waterT = txWater.SampleLevel(samLinearSimple, nextPos + float2(0, -0.001), 3);
      float2 pushingForce = float2(waterL.a - waterC.a, waterT.a - waterC.a);
      pushingForce = dot(pushingForce, 1) ? normalize(pushingForce) : 0;
      pushingForce *= particle.sticky <= 0 && particle.moved == 0 ? saturate(prevStateBlurred.a * 2.5 - 0.5) : 0;
      // particle.velocity += 0.4 * gFrameTime * pushingForce * saturate(dot(normalize(pushingForce), normalize(velocityU)) * 10);

      // Force moving raindrops to trails of other raindrops
      if (length(velocityU) > 0.001){
        float2 direction = normalize(velocityU);
        float2 samplingDir = direction.yx * float2(1, -1);
        float2 samplingCenter = particle.pos + direction * 0.01 * gScale;
        float waterC = trail(txWater.SampleLevel(samLinearSimple, samplingCenter, 2.5));
        float waterL = trail(txWater.SampleLevel(samLinearSimple, samplingCenter + samplingDir * 0.01 * gScale, 2.5));
        float waterR = trail(txWater.SampleLevel(samLinearSimple, samplingCenter - samplingDir * 0.01 * gScale, 2.5));
        velocityU += samplingDir * ((waterL - waterC) + (waterC - waterR)) * 0.001;
      }
      
      if (particle.life > PARTICLE_LIFESPAN){
        particle.life -= gFrameTime;
        particle.velocity += velocityU * gFrameTime;
      } else {
        float2 targetVelocity = velocityU * (particle.sticky <= 0 ? lerp(40, 10, edge) : 0) * saturate(1 - particle.splashMult);
        const float drag = 1.0 / (1.0 + gFrameTime * lerp(5, 10, edge + prevStateBlurred.a));
        particle.velocity = lerp(targetVelocity, particle.velocity, drag);
        particle.velocity = lerp(localVelocity, particle.velocity, lerp(1, dragEasy, localDensity));
        particle.velocity = lerp(0, particle.velocity, 1.0 / (1.0 + gFrameTime * pow(noise.z, 8) * 20));
      }

      particle.sticky -= max(length(velocityU) - 0.003, 0) * 5;
      particle.sticky += length(particle.velocity) * lerp(10, 1, pow(particle.rand, 2)) * gFrameTime;
      particle.size = max(particle.size - length(particle.velocity) * gFrameTime, 0);
    }

    float2 movement = gScale * gFrameTime * particle.velocity;
    float movementValue = length(movement);
    if (movementValue > 0){
      particle.pos += particle.moved ? movement : movement * min(0.008, movementValue) / movementValue;
    }
  }

  if (dirX.a == 0 || any(abs(particle.pos - 0.5) > 0.5) || particle.size < 0){
    particle.life = 0;
  }
}
