#include "pfxCarDrops.hlsl"

struct FailedOffDrop {
  float2 pos;
  float2 velocity;
  float size;
  float3 pad;
};

ConsumeStructuredBuffer<FailedOffDrop> buFlewOffDrops : register(u6);
Texture2D txWindscreenPos : register(t0);
Texture2DArray<float> txRainShadow : register(t1);

ByteAddressBuffer meshVertices0 : register(t4);
StructuredBuffer<uint> meshIndices0 : register(t5);
ByteAddressBuffer meshVertices1 : register(t6);
StructuredBuffer<uint> meshIndices1 : register(t7);
ByteAddressBuffer meshVertices2 : register(t8);
StructuredBuffer<uint> meshIndices2 : register(t9);

bool alignWithMesh(inout float3 pos, EmittingSpot E, ByteAddressBuffer vertices, StructuredBuffer<uint> indices, RAND_DECL){
	MeshPoint M = randomMeshPoint(vertices, indices, E.meshIndexCount, E.transform, RAND_ARGS);

	pos = M.pos - M.normal * 0.05;
	// return true;
	return dot(M.normal, E.direction) > -0.1;
}

cbuffer cbCounter : register(b11) {
	uint gCount;
	float3 _pad0;
};

float3 randomDirection(RAND_DECL){
	float3 ret = float3(RAND, RAND, RAND) * 2 - 1;
	return normalize(ret / cos(ret));
}

void emit(inout Particle particle, EmittingSpot E, RAND_DECL){
	particle.life = 1;
	particle.size = lerp(0.001, 0.002, RAND);
	particle.velocity = lerp(1, lerp(0.25, 1.75, RAND), E.velocityVariance) * E.velocity;
	particle.velocity = lerp(particle.velocity, randomDirection(RAND_ARGS) * length(particle.velocity), 0.1 * E.velocityVariance);
	particle.collisionPlane = E.collisionPlane;

	if (E.noCarCollisions){
		particle.size *= -1;
	}

	bool set;

	[branch]
	if (E.meshID == 1){
		set = alignWithMesh(particle.pos, E, meshVertices0, meshIndices0, RAND_ARGS);
	} else if (E.meshID == 2){
		set = alignWithMesh(particle.pos, E, meshVertices1, meshIndices1, RAND_ARGS);
	} else if (E.meshID == 3){
		set = alignWithMesh(particle.pos, E, meshVertices2, meshIndices2, RAND_ARGS);
	} else if (E.meshID == 4){
		float2 uv = float2(RAND, RAND);
		float4 height = txCarCollision.GatherRed(samLinearClamp, uv);

		float heightMax = max(max(height.x, height.y), max(height.z, height.w));
		float heightMin = min(min(height.x, height.y), min(height.z, height.w));
		set = heightMax == 1 && heightMin < 0.99;

		float4 posH = float4(uv, heightMin, 1);
		float4 posW = mul(posH, E.transform);
		particle.pos = posW.xyz;
		// set = false;
	} else {
		FailedOffDrop flew = buFlewOffDrops.Consume();
		set = dot(flew.pos, 1) != 0;
		if (set){
			const float velocityEstimationMult = -0.005;
			float2 uvPos0 = flew.pos + flew.velocity * 2 * velocityEstimationMult;
			float2 uvPos1 = flew.pos + flew.velocity * 3 * velocityEstimationMult;
			float4 carPos0 = txWindscreenPos.SampleLevel(samLinearSimple, uvPos0, 0);
			float4 carPos1 = txWindscreenPos.SampleLevel(samLinearSimple, uvPos1, 0);
			if ((carPos1.y - carPos0.y) / velocityEstimationMult < 0.01) set = false;

			particle.pos = mul(E.transform, float4(carPos0.xyz, 1)).xyz;
			float3 velocityOffset = (mul(E.transform, float4(carPos1.xyz, 1)).xyz - particle.pos) / velocityEstimationMult;
			particle.velocity += velocityOffset;
			particle.pos += velocityOffset * abs(velocityEstimationMult) * 2;
			particle.size = -0.2 * lerp(0.002, 0.004, flew.size);
			particle.life = 0.9;
		}
	}
	
	if (!set) {
		particle.life = 0;
	}

	if (E.meshID != 0){
    float4 posH = mul(float4(particle.pos, 1), gShadowTransform);
		if (lerp(0.1, 0.9, RAND) > txRainShadow.SampleCmpLevelZero(samShadow, float3(posH.xy, 1), posH.z)){
			particle.life = 0;
		}
	}
}
