#include "pfxCarDrops.hlsl"

bool carCollisionCheck(float3 pos){
  float4 uv = mul(float4(pos, 1), gCarCollisionTransform);
  return txCarCollision.SampleLevel(samLinearBorder1, uv.xy, 0) < min(uv.z, 0.999);
}

void forceFields(float3 pos, inout float3 velocity) {
  for (uint i = 0; i < FORCE_FIELDS; i++) {
    if (gForceFields[i].radiusSqrInv == 0) break;
    float3 diff = pos - gForceFields[i].pos;
    float dist = dot2(diff);
    float distK = saturate((1 - dist * gForceFields[i].radiusSqrInv) * 2);
    float3 forcePushAway = normalize(diff) * gForceFields[i].forceMult;
    velocity += distK * distK * forcePushAway * gFrameTime * 80;

    float3 forceVelSync = gForceFields[i].velocity - velocity;
    velocity += saturate(distK * distK * gFrameTime * 20) * forceVelSync;
  }
}

void process(inout Particle particle, uint3 DTid) {
  bool isNoCollisions = particle.size < 0;
  float drag = 1.0 / (1.0 + gFrameTime * 0.5);
  particle.life -= gFrameTime * 2;
  particle.pos += particle.velocity * gFrameTime;

  if (collisionCheck(particle.pos, particle.velocity, particle.collisionPlane, 0.005, 0, 0) <= 0){
    particle.life = 0;
  }

  if (!isNoCollisions && carCollisionCheck(particle.pos)){
    particle.life = 0;
  }

  forceFields(particle.pos, particle.velocity);
  particle.velocity *= drag;
  particle.velocity.y += gGravity * gFrameTime;
}
