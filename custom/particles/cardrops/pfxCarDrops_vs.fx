#include "pfxCarDrops.hlsl"
#include "include_new/ext_shadows/_include_vs.fx"
#include "include_new/ext_lightingfx/_include_ps.fx"
#include "../common/rainUtils_impl.hlsl"

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

	PS_IN vout;
	
	float size = abs(particle.size) * saturate((1 - particle.life) * 10) * saturate(particle.life * 10) * 2;
	float3 offset = rainBillboard(vertexID, particle.pos, particle.velocity - gCameraVelocity, size, 
		vout.Tex, vout.NormalBase, vout.BlurK);

	float4 posW = float4(particle.pos + offset, 1);
	float4 posV = mul(posW, ksView);
	vout.PosH = mul(posV, ksProjection);

	COMMON_SHADING(vout);
	return vout;
}