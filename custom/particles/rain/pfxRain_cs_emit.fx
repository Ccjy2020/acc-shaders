#define PARTICLE_CUSTOM_IMPL
#include "pfxRain.hlsl"

void emit(inout Particle particle, EmittingSpot E, float3 pos, bool3 wpAABB3, bool wpAABBD, RAND_DECL) {
	// if (RAND > 0.01) return;

	particle.pos = pos;

	// particle.pos.y = E.pos.y - 10;

	if (!wpAABBD){
		float spaceY = E.pos.y - E.posPrev.y;
		particle.pos.y = E.pos.y - E.size.y * 0.5 - spaceY * RAND;
	} else if (!all(wpAABB3)){
		if (wpAABB3.y){
			particle.pos.y = E.pos.y + E.size.y * lerp(-0.2, 0.5, RAND);
		} else {
			float spaceY = E.pos.y - E.posPrev.y;
			if (spaceY > 0){
				particle.pos.y = E.pos.y + E.size.y * 0.5 - spaceY * RAND;
			} else {
				particle.pos.y = E.pos.y - E.size.y * 0.5 - spaceY * RAND;
			}
		}		
	}

	// particle.pos = E.pos + float3(posWithin.x * 20, 1, posWithin.y * 20);

	// particle.pos = RAND > 0.5
	// 	? (RAND > 0.5 ? E.pos : E.offset0)
	// 	: (RAND > 0.5 ? E.offset1 : E.offset2);

	particle.velocity = float3(0, -20, 0);
	// particle.velocity.y += 4;
	// particle.pos += (float3(RAND, RAND, RAND) - 0.5) * 1;
	// particle.velocity += (float3(RAND, RAND, RAND) - 0.5) * length(particle.velocity) / 10;
	particle.size = 0.05 * (1 + RAND) * 5;
	particle.random = all(wpAABB3);
	particle.life = 1000;
	
	// particle.life *= 0.1;
	// particle.velocity *= 0.02;
}

[numthreads(32, 32, 1)]
void main(uint3 DTid : SV_DispatchThreadID) {
	EmittingSpot E = emittingSpots[DTid.z];

	RAND_INIT(0.12345, float2(gEmitterRandomness + dot(DTid, 0.01), (float)DTid.x / (float)32));

	float2 posWithin = (float2)DTid.xy / 31;
	float3 pos = E.pos + E.size * float3(posWithin.x - 0.5, 0.5, posWithin.y - 0.5);
	// pos.x = round(pos.x);
	// pos.y = round(pos.y);
	// pos.z = round(pos.z);

	bool3 wpAABB3 = withinPrevAABB3(pos, E.posPrev, E.size);
	bool wpAABBD = withinPrevAABB3(pos - float3(0, E.size.y, 0), E.posPrev, E.size).y;
	bool wpAABB = all(wpAABB3);

	uint count = all(wpAABB3) && wpAABBD ? (RAND > 0.04 ? 0 : 1) : 1;
	for (uint i = 0; i < count; ++i){
		Particle particle = (Particle)0;
		emit(particle, E, pos, wpAABB3, wpAABBD, RAND_ARGS);

		uint deadCount;
		counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_DEADCOUNT, -1, deadCount);

		uint newParticleIndex = deadBuffer[deadCount - 1];
		particleBuffer[newParticleIndex] = PFX_pack(particle);

		uint aliveCount;
		counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_ALIVECOUNT, 1, aliveCount);
		aliveBuffer_CURRENT[aliveCount] = newParticleIndex;
	}
}
