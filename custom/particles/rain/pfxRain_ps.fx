#include "pfxRain.hlsl"

#ifndef NO_DEPTH_MAP
  #include "include_new/ext_functions/depth_map.fx"
#endif

float3 sampleEnv(float3 ray, float bias, float dimming){
  // float dimmingMult = ray.y > 0;
  float dimmingMult = saturate(remap(ray.y, -0.1, 0.1, 0, 1));
  return SAMPLE_REFLECTION_FN(ray, bias, false, REFL_SAMPLE_PARAM_DEFAULT) * lerp(1, dimmingMult, dimming);
}

PS_OUT main(PS_IN pin) {
  float3 toCamera = normalize(pin.PosC);

  #ifdef NO_DEPTH_MAP
    float softK = 1.0;
  #else
    float depthZ = getDepthAccurate(pin.PosH);
    float depthC = linearizeAccurate(pin.PosH.z);
    float softK = saturate((depthZ - depthC) * 10);
  #endif

  float sharpness = lerp(0.99, 0, abs(pin.Tex.y) * pin.StretchedK);
  float facingShareSqr = saturate(1 - dot2(pin.Tex));
  float facingShare = sqrt(facingShareSqr);
  float3 normal = normalize(pin.DirUp * pin.Tex.y 
    + pin.DirSide * pin.Tex.x 
    + -toCamera * facingShare);
  float fresnel = saturate(pow((1 + dot(toCamera, normal)) * lerp(4, 1, sharpness), 3));

  // float3 refrDir = toCamera;
  float3 refraction = lerp(toCamera, -normal, 0.5);
  float3 reflection = reflect(toCamera, normal);
  float3 reflectionColor = sampleEnv(reflection, lerp(3, 5, pin.StretchedK), 0.5);
  float3 refractionColor = sampleEnv(refraction, lerp(1, 3, pin.StretchedK), 0) * lerp(0.5, 1, pow(1 - facingShare, 2));

  // #ifdef MODE_SIMPLE
  //   float4 txNoiseValue = 0;
  // #else
  //   float4 txNoiseValue = txNoise.Sample(samLinearSimple, pin.Tex / 10 + pin.RandomK);
  // #endif

  float3 lighting = lerp(refractionColor, reflectionColor, fresnel);
  float alpha = saturate(remap(length(pin.Tex), sharpness, 1, 1, 0)) * softK;
  alpha *= lerp(1, 1 - abs(pin.Tex.y), pin.StretchedK);

  // lighting *= float3(pin.RandomK, 1 - pin.RandomK, 0);
  // alpha = 1;

  RETURN_BASE(lighting, alpha);
}