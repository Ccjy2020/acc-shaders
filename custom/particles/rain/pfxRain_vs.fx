#include "pfxRain.hlsl"

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
	float distance = length((particle.pos - ksCameraPosition.xyz).xz);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif
	
	float stretchedK = 0;
	// particle.size *= 1 + distance * 0.05;
	particle.size *= 20;

  float3 side = normalize(cross(billboardAxis, float3(0, -1, 0)));
  float3 up = normalize(cross(billboardAxis, side));
	float3 offset = (side * (quadPos.x * 0.04 + quadPos.y * (particle.random - 0.5) * 0.2 * stretchedK) + up * quadPos.y * lerp(0.04, 1, stretchedK)) * particle.size;
	// float3 offset = (side * (quadPos.x * 0.1 + quadPos.y * (particle.random - 0.5) * 0.2 * stretchedK) + up * quadPos.y * lerp(0.1, 2, stretchedK)) * particle.size;
	// float3 offset = (side * (quadPos.x + quadPos.y * (particle.random - 0.5) * 0.2) + up * quadPos.y) * particle.size;
	
	float4 posW = float4(particle.pos, 1);
	// offset -= particle.collisionPlane1.xyz * dot(offset, particle.collisionPlane1.xyz) * 0.5;
	posW.xyz += offset;

	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

	PS_IN vout;
	vout.PosH = posH;
	vout.Tex = quadPos.xy;
	vout.AmbientMult = 0.07 * lerp(BILLBOARD[vertexID].y * 0.5 + 0.5, saturate(-billboardAxis.y), abs(billboardAxis.y));
	vout.CenterK = abs(billboardAxis.y);
	vout.RandomK = particle.random;

	vout.DirUp = up;
	vout.DirSide = side;
	vout.StretchedK = stretchedK;

	#ifndef MODE_SHADOW
		vout.PosC = posW.xyz - ksCameraPosition.xyz;
		#ifdef MODE_GBUFFER
			GENERIC_PIECE_VELOCITY(posW, particle.velocity * gDeltaTime);
		#else
			vout.Fog = calculateFog(posV);
		#endif
	#endif

	return vout;
}