// #define USE_GENERIC
#include "pfxRain.hlsl"

RWByteAddressBuffer counterBuffer : register(u4);
RWByteAddressBuffer indirectBuffers : register(u5);

[numthreads(1, 1, 1)]
void main(uint3 DTid : SV_DispatchThreadID) {
	uint deadCount = counterBuffer.Load(PARTICLECOUNTER_OFFSET_DEADCOUNT);
	uint aliveCount_NEW = counterBuffer.Load(PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION);

	uint realEmitCountTotal = gEmitCountTotal;
	uint maxEmitPerGroup = gEmittersCount <= 0 ? 0 : gMaxEmitCountPerGroup;

	uint maxEmitMult = (uint)floor(sqrt((float)deadCount / (32 * 32)));
	uint emitMult = min(4, maxEmitMult);
	realEmitCountTotal = deadCount > 32 * 32 ? 32 * 32 : 0;

	// uint side = (uint)ceil(sqrt((float)maxEmitPerGroup / (float)THREADCOUNT_EMIT_TOTAL));
	indirectBuffers.Store3(ARGUMENTBUFFER_OFFSET_DISPATCHEMIT, realEmitCountTotal == 0 ? uint3(0, 0, 0) : uint3(1, 1, 1));
	indirectBuffers.Store3(ARGUMENTBUFFER_OFFSET_DISPATCHSIMULATION, uint3(ceil((float)(aliveCount_NEW + realEmitCountTotal) / (float)THREADCOUNT_SIMULATION), 1, 1));
	counterBuffer.Store(PARTICLECOUNTER_OFFSET_ALIVECOUNT, aliveCount_NEW);
	counterBuffer.Store(PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION, 0);
	counterBuffer.Store(PARTICLECOUNTER_OFFSET_REALEMITCOUNT, maxEmitPerGroup);
}
