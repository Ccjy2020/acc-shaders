#include "pfxPieces.hlsl"
#include "include_new/ext_shadows/_include_vs.fx"

#define AO_EXTRA_LIGHTING 1
#define GI_LIGHTING 0
#define LIGHTINGFX_NOSPECULAR
#define LIGHTINGFX_KSDIFFUSE 0.4
#define LIGHTINGFX_SIMPLEST
// #ifndef SHADER_MIRROR
// 	#define LIGHTINGFX_FIND_MAIN
// #endif
#define POS_CAMERA posCPerVertexLighting
#include "include_new/base/common_ps.fx"
#include "include_new/ext_lightingfx/_include_ps.fx"
#include "include/ssgi_vs.hlsl"

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD
	
	float angle = particle.angle;
	float angleSin = sin(angle);
	float angleCos = cos(angle);
	quadPos.xy = mul(rotate2d(angleSin, angleCos), quadPos.xy);
	quadPos *= particle.pad2;

	// float3 velocity = mul(particle.velocity, (float3x3)ksView);
	// float speed = length(velocity) + 0.01;
	// float quadDot = dot(quadPos, velocity);
	// quadPos += velocity * quadDot / speed / 2;	

	float3 posC = particle.posSmooth - ksCameraPosition.xyz;
	float toCameraDistance = length(posC);
	float3 toCamera = posC / toCameraDistance;
	float landedK = IS_CHUNK(particle) ? 0.5 : particle.landedK * (IS_SHARD(particle) ? 0.98 : 0.9);
	// particle.landNormal = float3(0, 1, 0);
	// particle.toCameraFixed = toCamera;
  float3 side = normalize(cross(particle.toCameraFixed, particle.landNormal));
  float3 up = normalize(cross(lerp(toCamera, -particle.landNormal, landedK), side));	
	particle.posSmooth.y += particle.halfSize * particle.landedK * (IS_SHARD(particle) ? 0.05 : 0.2);	

	float3 offset = side * quadPos.x;
	offset -= up * quadPos.y;
	float3 velocity = particle.velocitySmooth;
	float speed = length(velocity);
	if (speed > 0.01){
		particle.posSmooth += normalize(velocity) * pow(saturate(speed * 4), 4) 
			* dot(normalize(velocity), normalize(offset)) * saturate(velocity.y) * (IS_CHUNK(particle) ? 0.05 : 0.1);		
	}

	particle.posSmooth += particle.landNormal * frac(particle.angle * 1000) * 0.01;

	float4 posW = float4(particle.posSmooth, 1);
	posW.xyz += offset;

	float4 posV = mul(posW, ksView);

	PS_IN vout;
	vout.PosH = mul(posV, ksProjection);

	#ifdef MODE_GBUFFER
		GENERIC_PIECE_VELOCITY(posW, particle.velocity * gDeltaTime);
	#endif

	vout.PosC = particle.posSmooth - ksCameraPosition.xyz;
	vout.SinCos = float2(angleSin, angleCos);
	if (IS_CHUNK(particle)){
		vout.UseTexture = 0;
		vout.Tex = BILLBOARD[vertexID].xy;
	} else {
		vout.UseTexture = 1;
		vout.Tex = BILLBOARD[vertexID].xy * 0.5 + 0.5;
		vout.Tex.y = (vout.Tex.y + particle.type) / TYPE_COUNT;
		if (particle.flipUV > 0.5) vout.Tex.x = 1 - vout.Tex.x;
	}

	vout.Color = particle.color;
	vout.NormalW = normalize(cross(up, side));
	vout.Fog = calculateFog(posV);

	#ifndef MODE_SIMPLIFIED_FX
		vout.UseReflections = IS_SHARD(particle);
		vout.TangentW = -side;
		vout.BitangentW = up;
	#endif

	#ifndef MODE_GBUFFER
		float4 posWPerVertexLighting = posW;
		posWPerVertexLighting -= ksLightDirection * (0.15 - 0.2 * saturate(1 - particle.lifePassed));

		#ifndef MODE_SIMPLIFIED_FX
			float4 tex0 = mul(posWPerVertexLighting, ksShadowMatrix0);
			float4 tex1 = mul(posWPerVertexLighting, ksShadowMatrix1);
			float4 tex2 = mul(posWPerVertexLighting, ksShadowMatrix2);
			vout.Shadow = getShadowBiasMult(vout.PosC, float3(0, 1, 0), tex0, tex1, tex2, 0, 1);
			vout.Specular = IS_SHARD(particle) ? 1 : 0;
		#endif

		float3 posCPerVertexLighting = posWPerVertexLighting.xyz - ksCameraPosition.xyz;
		vout.Lighting = 0;
		LFX_MainLight mainLight;
		LIGHTINGFX(vout.Lighting);
		#ifndef SHADER_MIRROR
			// float lightingFocus = saturate(mainLight.power / max(dot(vout.Lighting, 1), 1));
			// vout.LightingDir = lerp(float3(0, 1, 0), mainLight.dir, lightingFocus);
		#endif
		vout.Lighting += getSSGI(vout.PosH);
		// vout.Lighting *= vDynamicLightingMult;
		// vout.Lighting *= 5;
	#endif

	return vout;
}