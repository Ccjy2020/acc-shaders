#define PARTICLES_PACKING
#define PARTICLES_MESH_SPAWN
#define STENCIL_VALUE 0.2
#include "particles/common/include_decl.hlsl"

#define TYPE_COUNT 12
#define TYPE_CHUNK TYPE_COUNT
#define IS_GRASS(_P) (_P.type <= 3.5)
#define IS_SOIL(_P) (_P.type > 3.5 && _P.type <= 7.5)
#define IS_SHARD(_P) (_P.type > 7.5 && _P.type <= 11.5)
#define IS_CHUNK(_P) (_P.type > (TYPE_COUNT - 0.5))
#define DRAG_MULT(_P) ((2.5 - pow(saturate(_P.type / 8), 2)) * 0.75)
#define DRAG_CONTACT_MULT(_P) (IS_SOIL(_P) ? 20 : 8)
#define GRAVITY_MULT(_P) (0.3 + 0.7 * saturate(_P.type / 7))
#define GEN_OUTSIDE (RAND > 0.5 ? TYPE_CHUNK : floor(RAND * 7.99))
#define GEN_OUTSIDE_GRASS (floor(RAND * 3.99))
#define GEN_OUTSIDE_NOGRASS (RAND > 0.5 ? TYPE_CHUNK : 4 + floor(RAND * 3.99))
#define GEN_OUTSIDE_ADJ(_GRASS_CHANCE) (RAND < (_GRASS_CHANCE) ? GEN_OUTSIDE_GRASS : GEN_OUTSIDE_NOGRASS)
#define GEN_COLLISION_CAR (8 + floor(RAND * 3.99))
#define GEN_COLLISION_TRACK TYPE_CHUNK
#define COLLECTED_GRASS_SAMPLES_SIZE 32

#define SRCTYPE_OUTSIDE 1
#define SRCTYPE_COLLISION_CAR 2
#define SRCTYPE_COLLISION_TRACK 3
#define SRCTYPE_MESH_TEST 4
#define SRCTYPE_LAWNMOWER 5
#define SRCTYPE_LAWNMOWER_LEAK 6

struct Particle {
	float3 pos;
	float life;

	float3 velocity;
	float type;

	float4 collisionPlane1;
	float4 collisionPlane2;

	float3 color;
	float halfSize;

	float spinSpeed;
	float landedK;
	float lifePassed;
	float angle;

	float3 velocitySmooth;
	float flipUV;

	float3 posSmooth;
	float groundWorking;

	float3 toCameraFixed;
	float pad1;

	float3 landNormal;
	float pad2;
};

struct Particle_packed {
	float3 pos;
	float angle;

	float4 collisionPlane1;
	float4 collisionPlane2;

	uint2 velocity_life;
	uint2 velocitySmooth_flipUV;

	uint2 color_halfSize;
	uint2 spinSpeed_landedK_lifePassed_type;

	float3 posSmooth;
	float groundWorking;

	uint2 toCameraFixed_pad1;
	uint2 landNormal_pad2;
};

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.angle, _U.angle);\
	FN(_P.collisionPlane1, _U.collisionPlane1);\
	FN(_P.collisionPlane2, _U.collisionPlane2);\
	FN(_P.velocity_life, _U.velocity, _U.life);\
	FN(_P.velocitySmooth_flipUV, _U.velocitySmooth, _U.flipUV);\
	FN(_P.color_halfSize, _U.color, _U.halfSize);\
	FN(_P.spinSpeed_landedK_lifePassed_type, _U.spinSpeed, _U.landedK, _U.lifePassed, _U.type);\
	FN(_P.posSmooth, _U.posSmooth);\
	FN(_P.groundWorking, _U.groundWorking);\
	FN(_P.toCameraFixed_pad1, _U.toCameraFixed, _U.pad1);\
	FN(_P.landNormal_pad2, _U.landNormal, _U.pad2);

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 emitterPos1;

		uint type;
		float3 emitterVel1;

		uint emitterExtraPoints;
		float3 emitterPos2;

		float meshThresholdSqr;
		float3 emitterVel2;

		float typeGrassChance;
		float3 emitterPos3;
		
		float randomMult;
		float3 emitterVel3;

		float4 emitterPlane1;
		float4 emitterPlane2;

		uint textureID;
		uint meshID;
		uint meshIndexCount;  // also sets row for SRCTYPE_LAWNMOWER/SRCTYPE_LAWNMOWER_LEAK
		float posSpread;

		float isSurfaceGrassFallback;
		float3 grassFallbackColor;

		float3 soilColor;
		float pad0;

		float4x4 transform;
	};

	#define FORCE_FIELDS 4
	#define COLLISION_SPHERES 8

	struct ForceField {
		float3 pos;
		float radiusSqrInv;
		float3 velocity;
		float forceMult;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		float gGravity;
		uint gALS1;
		uint gALS2;
		uint _pad1;

		float2 gMapPointA;
		float2 gMapPointB;

		ForceField gForceFields[FORCE_FIELDS];
		float4 gCollisionSpheres[COLLISION_SPHERES];
	}
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		PFX_PS_IN
		
		float UseTexture : TEXCOORD2;
		float2 SinCos : TEXCOORD4;
		float3 Color : COLOR0;

		#ifndef MODE_SIMPLIFIED_FX
			float UseReflections : TEXCOORD3;
			float3 TangentW : TEXCOORD8;
			float3 BitangentW : TEXCOORD9;
		#endif

		#ifndef MODE_GBUFFER
			#ifndef MODE_SIMPLIFIED_FX
				float Shadow : COLOR1;
				float Specular : TEXCOORD7;
			#endif
			float3 Lighting : COLOR2;
		#endif
	};

	struct PS_IN_shadow {
		float4 PosH : SV_POSITION;
		float2 Tex : TEXCOORD1;
		float UseTexture : TEXCOORD2;
		float3 Color : COLOR0;
		float Opacity : COLOR1;
	};

	cbuffer cbPieces : register(CBUFFER_DRAW_SLOT) {
		float gDeltaTime;
		float3 gDustColorA;
		float _pad0;
		float3 gDustColorB;
	}
#endif

#include "particles/common/include_impl.hlsl"
