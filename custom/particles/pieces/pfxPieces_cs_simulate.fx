#include "pfxPieces.hlsl"

void forceFields(float3 pos, inout float3 velocity) {
  for (uint i = 0; i < FORCE_FIELDS; i++) {
    float3 diff = pos - gForceFields[i].pos;
    float dist = dot(diff, diff);
    float distK = saturate(1 - dist * gForceFields[i].radiusSqrInv);
    float3 forcePushAway = normalize(diff) * gForceFields[i].forceMult * 180;
    float3 forceVelSync = (gForceFields[i].velocity - velocity) * abs(gForceFields[i].velocity) * 0.5;
    velocity += distK * distK * (forcePushAway + forceVelSync) * gFrameTime;
  }
}

void collisionSpheres(inout float3 pos, inout float3 velocity) {  
  for (uint i = 0; i < COLLISION_SPHERES; i++) {
    float3 diff = pos - gCollisionSpheres[i].xyz;
    float dist = length(diff);
    if (dist < gCollisionSpheres[i].w) {
      pos += diff / dist * (gCollisionSpheres[i].w - dist);
    }
  }
}

void process(inout Particle particle, uint3 DTid) {
  float drag = 1.0 / (1.0 + gFrameTime * DRAG_MULT(particle));
  float dragLerp = 1.0 / (1.0 + gFrameTime * 32);
  float dragContact = 1.0 / (1.0 + gFrameTime * DRAG_CONTACT_MULT(particle));

  #ifdef DEPTH_COLLISIONS
    particle.life -= (particle.landedK > 0.5 ? gFrameTime * 2 : gFrameTime)
      * (particle.life < 0.5 ? 1 : DTid.x > gALS2 ? 100 : DTid.x > gALS1 ? 1 : 0);
  #else
    particle.life -= (particle.landedK > 0.5 ? gFrameTime * 2 : gFrameTime)
      * (particle.life < 0.5 ? 1 : DTid.x > gALS2 ? 100 : DTid.x > gALS1 ? 4 : 2);
  #endif

  particle.lifePassed += gFrameTime;

  float3 dpos = gFrameTime * particle.velocity;
  float3 newVelocity = particle.velocity * drag;
  bool alive = length(particle.velocitySmooth) > 0.2 || particle.landedK < 0.997;

  if (alive) {
    particle.pos += dpos;
		particle.life -= max(-dpos.y - 0.01, 0);
    newVelocity.y += gGravity * GRAVITY_MULT(particle);
  } else {
    newVelocity = 0.0;
  }

  float particleHeight = 0.01;

  if (IS_CHUNK(particle)) {
    particleHeight = particle.halfSize * 0.5 * saturate(particle.life * 2);
  } else if (alive && !IS_SHARD(particle)) {
    particle.halfSize += gFrameTime * length(particle.velocity) / 100;
  }
  particle.angle += gFrameTime * saturate(length(particle.velocity) / 40 - 0.01) * particle.spinSpeed 
    * (1 - particle.landedK * 0.95);

  float bounceK = 1;
  if (alive && (IS_CHUNK(particle) || IS_SHARD(particle))) {
    bounceK = 1 + 0.2 + 0.2 * frac(particle.spinSpeed);
  }

  float distanceToGround = 10;
  float distanceToGround2 = 10;
  if (particle.groundWorking == 0) {
    distanceToGround = collisionCheck(particle.pos, newVelocity, particle.collisionPlane1, particleHeight, bounceK, dragContact);
    distanceToGround2 = min(
      collisionCheck(particle.pos, newVelocity, particle.collisionPlane2, particleHeight, bounceK, dragContact),
      distanceToGround);
  }

  #ifdef DEPTH_COLLISIONS
    float3 posC = particle.pos - de_CameraPosition;
    float depthCalc = length(posC);
    float2 ssUV = ssGetUV(posC).xy;
    particle.groundWorking = 0;

    [branch]
    if (particle.lifePassed > 0.3 && depthCalc < 200 && gFrameTime > 0 && ssUV.x > 0 && ssUV.x < 1 && ssUV.y > 0 && ssUV.y < 1) {
      float3 p0 = ssGetPos(ssUV, txDepth.SampleLevel(samPointClamp, ssUV, 0));
      float3 surfaceNormal = ssNormalDecode(txNormals.SampleLevel(samPointClamp, ssUV, 0).xyz);
      float distanceDepth = dot(posC - p0, surfaceNormal);
      float falledDown = -distanceDepth;
          
      if (distanceDepth > distanceToGround2 || posC.y > 0 && particle.landedK > 0.95){
        if (distanceDepth > distanceToGround2 && distanceDepth < 1000 && surfaceNormal.y > 0.35){
          particle.collisionPlane1.xyz = surfaceNormal;
          particle.collisionPlane1.w = dot(surfaceNormal, p0 + de_CameraPosition);
        } else {
          particle.collisionPlane1.w -= 0.5;
        }
        particle.collisionPlane2 = particle.collisionPlane1;
        particle.landedK *= dragLerp;
      }

      bool isNotFar = distanceDepth > -0.1 && length(posC.xz - p0.xz) < max(0.25, abs(distanceDepth) * 5);
      if (isNotFar && surfaceNormal.y > 0.35) {
        distanceToGround = min(distanceToGround, max(distanceDepth - 0.01, 0));
      }

      [branch]
      if (distanceDepth < particleHeight && isNotFar) {
        newVelocity -= surfaceNormal * dot(newVelocity, surfaceNormal) * bounceK;
        newVelocity *= dragContact;
        particle.pos += surfaceNormal * (particleHeight - distanceDepth / 2);

        float newPlaneDistance = dot(surfaceNormal, p0 + de_CameraPosition);
        if (surfaceNormal.y > 0.35 /* && distanceDepth — mistake? something missing? */) {
          // if (distanceToPlane(surfaceNormal, newPlaneDistance, particle.pos) > distanceToPlane(particle.collisionPlane1, particle.pos)) {
            if (particle.collisionPlane1.y == particle.collisionPlane2.y){
              particle.collisionPlane2.xyz = surfaceNormal;
              particle.collisionPlane2.w = newPlaneDistance;
            }

            particle.collisionPlane1.xyz = surfaceNormal;
            particle.collisionPlane1.w = newPlaneDistance;
          // }
        } else {
          particle.collisionPlane2.xyz = surfaceNormal;
          particle.collisionPlane2.w = newPlaneDistance;
        }
      }
    }
  #endif

  particle.landedK = max(particle.landedK * (alive ? 0.99 : 0.999), saturate(particle.lifePassed - 0.3) 
    * saturate(1.01  - (distanceToGround - particleHeight) / particle.halfSize * 0.25));

  if (alive && IS_SOIL(particle) && particle.landedK > 0.995) {
    particle.halfSize += gFrameTime * length(particle.velocity) / 10;
  }
  
  // Debug ground as primary collider:
  // particle.color = float3(10 - particle.groundWorking * 10, particle.groundWorking * 10, 0);
  // particle.halfSize = 1 - particle.groundWorking * 0.8;

  // Debug forces:
  // particle.color = float3(10, 0, 0);
  // particle.halfSize = 0.25;

  // Debug adaptive life span:
  // if (DTid.x > gALS2) {
  //   particle.color = float3(0, 10, 0);
  // } else if (DTid.x > gALS1) {
  //   particle.color = float3(10, 10, 0);
  // } else {
  //   particle.color = float3(10, 0, 0);
  // }

  particle.halfSize = min(particle.halfSize, 0.3);
  // forceFields(particle.pos, newVelocity);

  [branch]
  if (particle.lifePassed > 0.1 && particle.lifePassed < 0.3) {
    collisionSpheres(particle.pos, newVelocity);
  }

  particle.velocity = newVelocity;
  particle.velocitySmooth = lerp(newVelocity, particle.velocitySmooth, dragLerp);
  particle.posSmooth = lerp(particle.pos, particle.posSmooth, dragLerp);
  particle.landNormal = normalize(lerp(particle.collisionPlane1.xyz, particle.landNormal, dragLerp));
  if (particle.landedK < 0.999) {
    particle.toCameraFixed = normalize(particle.posSmooth - gCameraPosition.xyz);
  }
  particle.pad2 = particle.halfSize * sqrt(saturate(particle.life * 2) * saturate(particle.lifePassed * 5));
}
