#include "pfxTraces.hlsl"

void forceFields(inout float3 pos){
  for (uint i = 0; i < FORCE_FIELDS; i++){
    float3 dist = pos - xForceFields[i].xyz;
    pos += xForceFields[i].w * pow(saturate(1 - dot(dist, dist) / 8), 4) * normalize(dist);
  }
}

void process(inout Particle particle, uint3 DTid){
  particle.life -= gFrameTime * xLifespanMultInv;
  if (gFrameTime){
    particle.lifeB = particle.life - gFrameTime * xLifespanMultInv;
    forceFields(particle.posA);
    forceFields(particle.posB);
    forceFields(particle.posM);
  }
}
