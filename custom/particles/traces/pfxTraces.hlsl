#define PARTICLES_WITHOUT_NORMALS 
#define VERTICES_PER_PARTICLE 12
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 posA;
	float life;

	float3 posB;
	float size;

	float3 posM;
	float lifeB;
};

#define FORCE_FIELDS 16

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 emitterPos;

		float glow;
		float3 emitterPosPrev;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		uint gEmittersCount;
		uint gEmitCountTotal;
		uint gMaxEmitCountPerGroup;
		float gEmitterRandomness;

		float gFrameTime;
		float3 gCameraPosition;

		float xLifespanMultInv;
		float3 xPad0;

		float4 xForceFields[FORCE_FIELDS];
	}
#elif defined(TARGET_VS) || defined(TARGET_PS)
	cbuffer cbTraces : register(CBUFFER_DRAW_SLOT) {
		float3 xColor;
		float xThickness;
		float xNarrowingK;
		float xLifespanMultInv;
		float xFovK;
		float xPaddingK;
	}

	struct PS_IN {
		PFX_PS_IN
		nointerpolation float Brightness : TEXCOORD2;
		nointerpolation float RoundK : TEXCOORD3;
	};
#endif

#include "particles/common/include_impl.hlsl"

