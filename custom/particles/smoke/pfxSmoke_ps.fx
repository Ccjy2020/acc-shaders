// #define SHADOWS_FILTER_SIZE 2
// #define MICROOPTIMIZATIONS
// #define OPTIMIZE_SHADOWS
#define WITH_SHADOWS
// #define DEPTH_LR

#include "pfxSmoke.hlsl"
#include "include_new/base/_include_ps.fx"
#include "pfxSmoke_draw.hlsl"

#ifndef NO_DEPTH_MAP
  #include "include_new/ext_functions/depth_map.fx"
  Texture2D<float> txDepthLR : register(t1);

  float getDepthAccurate2(float4 pinPosH){
    float v = txDepthLR.Load(int3(pinPosH.xy, 0)).x;
    return v ? linearizeAccurate(v) : ksFarPlane;
  }
#endif

Texture2D txSmokeNoise : register(t4);

RESULT_TYPE result_red(PS_IN pin){
  float3 color = float3(3, 0, 0);
  float alpha = 1;
  RETURN_BASE(color, alpha);
}

void __clip(float v){
  clip(v);
}

RESULT_TYPE main(PS_IN pin) {

  #ifndef SHADER_TILE
    // if (max(pin.Tex.x, pin.Tex.y) > 0.99 || min(pin.Tex.x, pin.Tex.y) < -0.99) return result_red(pin);
  #endif
  // if (length(pin.Tex) < 0.01) return result_red(pin);
  // if (abs(pin.Tex.x) < 0.005 || abs(pin.Tex.y) < 0.005) return result_red(pin);
  // return 0;

  float2 circ = pin.Tex;
  #ifndef SHADER_TILE
    __clip(1 - dot(circ, circ));
  #endif
  float sizeK = saturate(pin.Size * 2 - 0.35);


  #if defined( WITH_SHADOWS ) && ! defined( NO_SHADOWS )
    float shadow = pin.Shadow;
  #else
    float shadow = 0.7;
  #endif

  float4 inputColor = unpackColor(pin.Color);
  float2 uv = pin.Tex * float2(1, -1);
  uv *= 1 + saturate(pin.DustMix - 1);
  uv *= 1 + saturate(pin.Size / 2 - 0.5) * 2;
  #ifndef SHADER_TILE
    float4 txSmokeValue1 = txSmokeNoise.Sample(samLinearSimple, uv * vUvMult1 + pin.Movement); 
    float4 txSmokeValue2 = txSmokeNoise.Sample(samLinearSimple, uv * vUvMult2 + pin.Movement);
  #else
    float4 txSmokeValue1 = txSmokeNoise.SampleBias(samLinearSimple, uv * vUvMult1 + pin.Movement, 1); 
    float4 txSmokeValue2 = txSmokeNoise.SampleBias(samLinearSimple, uv * vUvMult2 + pin.Movement, 1);
  #endif
  float4 txSmokeValue = lerp(txSmokeValue1, txSmokeValue2, saturate(2 - pin.Size * 0.8));

  #ifdef NO_DEPTH_MAP
    float softK = 1.0;
  #else
    #ifdef DEPTH_LR
      float depthZ = getDepthAccurate2(pin.PosH);
    #else
      float depthZ = getDepthAccurate(pin.PosH);
    #endif
    float depthC = linearizeAccurate(pin.PosH.z);
    float softDepthK = lerp(10, 1, saturate(pin.Size * 4 - 1));
    softDepthK *= lerp(0.2, 1, saturate(2 - pin.Size * 0.8)) / max(1, (depthC / 50));
    softDepthK *= lerp(1, 4, saturate(1 - pin.Size * 10));
    float softK = saturate((depthZ - depthC) * softDepthK - txSmokeValue.a);
  #endif
  
  float spreadKPow = saturate(pin.Size / 1.2 - 0.5);
  float spreadKDif = saturate(pin.Size / 1.2 - 0.5);
  float3 toCamera = normalize(pin.PosC);

  #ifdef GBUFFER_NORMALS_REFLECTION
    float alpha = 1 - length(circ);
    alpha = saturate(alpha - txSmokeValue.a * lerp(0.6, 0.8, sizeK)); 
    alpha = pow(alpha, lerp(1, spreadKPow * 0.25 + 0.75, sizeK * pin.Thickness * saturate(pin.Opacity * 3 - 1)));
    // alpha *= softK;
    alpha = saturate(alpha * lerp(0.1, 1, (pin.Thickness + pin.Thickness * pin.Thickness * saturate(1 - spreadKPow)) * saturate(1.2 - spreadKPow) ));
    // alpha = pow(alpha, 0.8 + pin.Thickness * 0.2);
    alpha *= pin.Opacity;
    alpha = min(alpha, lerp(0.1, 0.6, pin.Thickness * pin.Thickness) + pin.Thickness * (1 - sizeK));
    alpha *= saturate(0.1 + sizeK);
  #else
    float alpha = 1 - length(circ);

    #ifdef SHADER_TILE
      alpha = saturate(alpha - txSmokeValue.a * lerp(0.6, 0.8, sizeK)); 
    #else
      float hugeK = saturate(pin.Size / 1.5 - 1);
      alpha = saturate(alpha - txSmokeValue.a * lerp(0.6, 0.8, sizeK) * lerp(1.2, 1, hugeK)); 
      alpha = pow(alpha, lerp(1, spreadKPow * 0.25 + 0.75, sizeK * pin.Thickness * saturate(pin.Opacity * 3 - 1) * saturate(1.5 - pin.Size)));
      alpha *= softK;
      alpha = saturate(alpha * lerp(0.3, 1, (pin.Thickness + pin.Thickness * pin.Thickness * saturate(1 - spreadKPow)) * saturate(1.2 - spreadKPow) ));
      alpha = pow(alpha, 1 - pin.Thickness * 0.2);
      alpha *= pin.Opacity;
      alpha *= saturate(1.25 - hugeK);
      alpha *= lerp(1, 0.6, pin.StuckK) * saturate(0.6 + sizeK);
    #endif
  #endif

  #ifdef SHADER_SOLID
    __clip(alpha - 0.1);
    
    uint2 pos = uint2(pin.PosH.xy);
    pos += pin.Offset2;
    uint2 xy2 = pos.xy % 2;
    // uint2 xy3 = pos.xy % 3;

    // if (alpha < 0.9 && xy3.x == 0 && xy3.y == 0) discard;
    // if (alpha < 0.8 && xy3.x == 2 && xy3.y == 1) discard;
    // if (alpha < 0.7 && xy3.x == 1 && xy3.y == 2) discard;
    // if (alpha < 0.4 && xy2.x == 0 && xy2.y == 1) discard;
    // if (alpha < 0.6 && xy2.x == 1 && xy2.y == 0) discard;
    // if (alpha < 0.8 && xy2.x == 0 && xy2.y == 0) discard;
    // if (alpha < 0.5 && xy3.x == 1 && xy3.y == 1) discard;
    // if (alpha < 0.4 && xy3.x == 0 && xy3.y == 2) discard;
    // if (alpha < 0.3 && xy3.x == 2 && xy3.y == 0) discard;
    // if (alpha < 0.2 && xy3.x == 0 && xy3.y == 1) discard;

    return result_red(pin);
  #else
    __clip(alpha - 0.01);
  #endif

  // alpha *= 0.01;
  
  float neutralK = (1 - saturate(alpha * 5)) * lerp(0.7, 1, spreadKDif);
  float backlitDot = saturate(-dot(toCamera, ksLightDirection.xyz));
  float backlit = pow(backlitDot, lerp(vBacklitExponent, 1, neutralK * neutralK)) * lerp(0.25, 0.5, neutralK) * vBacklitPower;

  #ifndef SHADER_MIRROR
    float2 txNm = pin.Tex * float2(-2, 2) * 0.7;
    txNm += (txSmokeValue.xy * 2 - 1) * 0.5;
    txNm = clamp(txNm, -1, 1);
    float3 toSide = normalize(pin.ToSide);
    float3 toUp = normalize(pin.ToUp);

    float edge = saturate(length(txNm));
    float notEdge = sqrt(1 - edge * edge);
    float3 normal = normalize(-toCamera * notEdge + toUp * txNm.y + toSide * txNm.x);
    float LdotN = saturate(dot(-normal, ksLightDirection.xyz));
      
    float diffuseValue = saturate(dot(-normal, ksLightDirection.xyz) * (1 + vDiffuseConcentration) - vDiffuseConcentration);
    diffuseValue = lerp(diffuseValue, min(saturate(-ksLightDirection.y), 0.5), neutralK);
    diffuseValue = lerp(diffuseValue, 0.2 * pin.StuckOcclusion, pin.StuckK);

    // float ambientMult = saturate(normal.y * 0.5 + 0.5);
    // ambientMult = lerp(ambientMult, 0.5, neutralK);
    // float3 ambientColor = (ksAmbientColor_sky.rgb + extBaseAmbient) * ambientMult * pin.StuckOcclusion;

    float3 ambientColor = lerp(getAmbientBaseAt(normal, 1), getAmbientBaseNonDirectional(), neutralK) * pin.StuckOcclusion;

    float3 color = vAmbient * ambientColor * pin.Occlusion
      + vDiffuse * ksLightColor.xyz * shadow * (diffuseValue + backlit); 

    float extraLightStrength = 0.5 + 0.5 * dot(normal, normalize(pin.LightingDir.xyz));
    color += saturate(lerp(extraLightStrength, 1, 0.25 + neutralK * 0.25)) * pin.Lighting 
      * pin.Occlusion * pin.StuckOcclusion;

    // color += pow(saturate(dot(pin.LightingDir.xyz, toCamera)), 3) * saturate(dot(normal, -toCamera)) * pin.Lighting 
    //   * pin.Occlusion * pin.StuckOcclusion;
  #else
    float3 color = (vAmbient * getAmbientBaseNonDirectional()
      + vDiffuse * ksLightColor.xyz * 0.5 * (1 + backlit)) * saturate(txSmokeValue.y * 0.4 + 0.6); 
  #endif
  color *= pow(inputColor.rgb, lerp(1.5 - pin.Thickness, 1, saturate(pin.DustMix)));
 
  /* {
    uint2 pos = uint2(pin.PosH.xy);
    uint2 xy2 = pos.xy % 2;
    uint2 xy3 = pos.xy % 3;
    float A = alpha * 3;
    if (A < 0.9 && xy3.x == 0 && xy3.y == 0) discard;
    if (A < 0.8 && xy3.x == 2 && xy3.y == 1) discard;
    if (A < 0.7 && xy3.x == 1 && xy3.y == 2) discard;
    if (A < 0.6 && xy3.x == 1 && xy3.y == 0) discard;
    if (A < 0.5 && xy3.x == 1 && xy3.y == 1) discard;
    if (A < 0.4 && xy3.x == 0 && xy3.y == 2) discard;
    if (A < 0.3 && xy3.x == 2 && xy3.y == 0) discard;
    if (A < 0.2 && xy3.x == 0 && xy3.y == 1) discard;
    alpha = 1;
  } */

  #ifdef SHADER_TILE
    float3 val = saturate(1 - length(pin.PosC) / 600);
    RESULT_TYPE ret;
    ret.result = float4(val, 0.1);
    return ret;
  #endif
  
  RETURN_BASE(color, alpha);
}