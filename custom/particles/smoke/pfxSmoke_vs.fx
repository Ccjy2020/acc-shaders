#include "pfxSmoke.hlsl"
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_shadows/_include_vs.fx"

#define AO_EXTRA_LIGHTING 1
#define GI_LIGHTING 0
#define LIGHTINGFX_KSDIFFUSE 1
#define LIGHTINGFX_NOSPECULAR
#define LIGHTINGFX_SIMPLEST
#ifndef SHADER_MIRROR
	#define LIGHTINGFX_FIND_MAIN
#endif
#define POS_CAMERA posCPerVertexLighting
#include "include_new/base/common_ps.fx"
#include "include_new/ext_lightingfx/_include_ps.fx"

#include "pfxSmoke_draw.hlsl"

static const float3 BILLBOARD[] = {
	float3(-1, -1, 0),
	float3(1, -1, 0),
	float3(-1, 1, 0),
	float3(-1, 1, 0),
	float3(1, -1, 0),
	float3(1, 1, 0),
};

StructuredBuffer<Particle> particleBuffer : register(t0);
StructuredBuffer<uint> aliveList : register(t1);
StructuredBuffer<Car> cars : register(t2);
Texture2D txTiles : register(t3);

float4 getPosH(float3 pos){
	float4 posW = float4(pos, 1);
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);
	return posH;
}

float3 getPosHDiv(float3 pos){
	float4 posH = getPosH(pos);
	return posH.xyz / posH.w;
}

float tileTestScreen(float3 posUV, float3 posW){
	float2 uv = posUV.xy * 0.5 + 0.5;
	uv.y = 1 - uv.y;
	float4 tileValue = txTiles.SampleLevel(samLinearClamp, uv, 0);
	float decodedDistance = (1 - tileValue.x) * 600;
	float actualDistance = length(posW - ksCameraPosition.xyz);
	// return actualDistance < decodedDistance;
	return pow(saturate(1 - actualDistance / decodedDistance), 1) * (1 + pow(saturate(tileValue.a * 4 - 3), 2) * 3);
}

float tileTest(float3 posW){
	return tileTestScreen(getPosHDiv(posW), posW);
}

float shadowContribution(float3 pos, float3 posC){	
	float4 p = float4(pos, 1);
	return getShadowBiasMult(posC, float3(0, 1, 0), 
		mul(p, ksShadowMatrix0), mul(p, ksShadowMatrix1), mul(p, ksShadowMatrix2), 0, 1);
}

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	PS_IN vout = (PS_IN)0;

	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;

	Particle particle = particleBuffer[aliveList[instanceID]];
	particle.size = min(particle.size, 3.5);
	float3 posOrig = particle.position;

	#ifdef GBUFFER_NORMALS_REFLECTION
		vout.PosH = 10;
		return vout;
	#endif

	// #ifdef SHADER_TILE
	// 	vout.PosH = 10;
	// 	return vout;
	// #endif

	float3 posC = particle.position - ksCameraPosition.xyz;
	float toCameraDistance = length(posC);
	float3 toCamera = posC / toCameraDistance;
  float3 side = normalize(cross(toCamera, float3(0, 1, 0)));
  float3 up = normalize(cross(toCamera, side));

	float3 posH1 = getPosHDiv(particle.position);
	float3 posH2 = getPosHDiv(particle.position + up * particle.size);
	float onScreenSize = abs((posH1 - posH2).y);

	float maxOnScreenSize = lerp(0.5 + pow(particle.random2, 4), 0.8, saturate(ksFOV / 25 - 1));
	float onScreenK = saturate(maxOnScreenSize - onScreenSize);
	if (particle.random3 < 0.1){
		onScreenK = 1 / max(onScreenK, 0.5);
	}
	particle.opacity *= sqrt(onScreenK);

	#if !defined(SHADER_TILE) && !defined(SHADER_MIRROR) 
		// float tileK = max(
		// 	max(
		// 		tileTest(posOrig - up * 0.3 + side * 0.3),
		// 		tileTest(posOrig + up * 0.3 + side * 0.3)),
		// 	max(
		// 		tileTest(posOrig - up * 0.3 - side * 0.3),
		// 		tileTest(posOrig + up * 0.3 - side * 0.3)));
		float tileK = max(
			tileTestScreen(posH1, particle.position),
			tileTestScreen((posH1 + posH2) / 2, particle.position));
		// float tileK = tileTest(posOrig);
		// particle.opacity *= max(particle.random2 > 0.95, lerp(tileK, 1, saturate(2 - onScreenSize * 4)));
		tileK = lerp(tileK, 1, saturate(2 - onScreenSize * 8));
		if (particle.random2 < 0.1){
			tileK = 1 / max(tileK, 0.5);
		}
		// tileK = lerp(tileK, 1, particle.random2 < 0.1);
		particle.opacity *= sqrt(tileK);
		// particle.color = packColor(float4(1 - tileK, tileK, 0, 1));
		// particle.opacity = tileK;
	#endif

	particle.opacity = saturate(particle.opacity);

	// #ifndef SHADER_TILE
	float pushedAgainstFix = particle.pushedAgainst;
	pushedAgainstFix *= pushedAgainstFix;
	pushedAgainstFix *= pushedAgainstFix;
	pushedAgainstFix = 1 + 4 * pushedAgainstFix * saturate(particle.lifeOut * 40);
	vout.Opacity = saturate(particle.life * particle.opacityK) * particle.opacity * pushedAgainstFix * unpackColor(particle.color).a;
	// #endif

	#ifdef SHADER_MIRROR
		[branch]
		if (particle.phase > 0 || vout.Opacity < 0.05){
			vout.PosH = 10;
			return vout;
		}
	#else
		[branch]
		if (vout.Opacity < 0.05){
			vout.PosH = 10;
			return vout;
		}
	#endif

	float sizeK = saturate(particle.size * 2 - 0.35);
	vout.Tex = BILLBOARD[vertexID].xy;
	vout.Size = particle.size;
	vout.DustMix = particle.dustMix;
	vout.Movement = particle.texMovement + particle.texMovementStatic;
	// vout.Life = particle.lifeOut;

	#ifndef SHADER_TILE
		vout.Thickness = saturate(particle.thickness + (1 - particle.growK) * 0.3);
		vout.Color = particle.color;
	#endif
	
	float3 quadPos = BILLBOARD[vertexID];
	quadPos *= particle.size;
	quadPos.x *= 1.2;

  vout.PosC = posC;
	#ifndef SHADER_TILE
 		vout.Fog = calculateFogNew(vout.PosC);
	#endif

	if (particle.growK >= 0){
		float yOffset = max(particle.size * lerp(0.33, 0.67, particle.random2) - 0.15, 0);
		particle.position.y += yOffset / (yOffset + 1);
		particle.position -= toCamera * (lerp(0.1, 0.2, sizeK) 
			+ particle.pushedAgainst * particle.pushedAgainst * 0.3 * saturate(1.3 - abs(toCamera.y)));
	}

	float3 velocity = normalize(particle.velocity) 
		* min(length(particle.velocity), 0.5) 
		* (0.5 + saturate(particle.size / 4 - 0.25) * 4);
	velocity *= lerp(0.8, 1.4, sizeK) * (particle.growK < 0 ? 0.5 : 1);

	float3 offset = side * quadPos.x;
	offset -= up * quadPos.y;
	particle.position += velocity * dot(normalize(velocity), normalize(offset));

	#ifndef SHADER_MIRROR
		vout.ToSide = -side;
		vout.ToUp = -up;
		offset *= 1.1 + abs(toCamera.y) * saturate(toCameraDistance / 40 - 0.2) * 0.7;
	#else
		offset *= 1.25;
	#endif

	float4 posW = float4(particle.position, 1);
	posW.xyz += offset;
	vout.PosH = mul(posW, ksView);
	vout.PosH = mul(vout.PosH, ksProjection);

	#if !defined(GBUFFER_NORMALS_REFLECTION) && !defined(SHADER_TILE)
		float3 cornerOffset = side * quadPos.x - up * quadPos.y;
		float4 posWPerVertexLighting = float4(particle.position + cornerOffset * 0.5, 1);

		vout.Shadow = (shadowContribution(particle.position + cornerOffset * 0.5, posC)
			+ shadowContribution(particle.position + cornerOffset * 0.9, posC)
			+ shadowContribution(particle.position + cornerOffset * 1.3, posC)) / 3;

		float3 posCPerVertexLighting = posWPerVertexLighting.xyz - ksCameraPosition.xyz;
		LFX_MainLight mainLight;
		LIGHTINGFX(vout.Lighting);
		#ifndef SHADER_MIRROR
			float lightingFocus = saturate(2 * mainLight.power / max(dot(vout.Lighting, 1), 1));
			vout.LightingDir = lerp(float3(0, 1, 0), mainLight.dir, lightingFocus);
		#endif
		vout.Lighting /= (1 + vout.Lighting * 0.5);
		vout.Lighting *= vDynamicLightingMult;
	#endif

	// TODO:
	// vout.TexXMult = particle.size / ((1 - abs(PdotV)) * length(particle.position2 - posOrig) + particle.size);
	// vout.TexXMult = 1;

	#if !defined(SHADER_MIRROR) && !defined(GBUFFER_NORMALS_REFLECTION) && !defined(SHADER_TILE)
		Car car = cars[particle.wheelID / 4];
		float3 occD = posWPerVertexLighting.xyz - car.shadowCenter;
		float occH = abs(dot(occD, car.shadowVecH));
		float occV = abs(dot(occD, car.shadowVecV));
		vout.Occlusion = lerp(car.shadowOpacity, 1, 
			saturate(saturate((max(occH, occV) - 0.75) * 2) + saturate(occD.y) ));
		vout.Occlusion = lerp(vout.Occlusion, 1, saturate((posWPerVertexLighting.y - particle.groundY) * 2 - 0.5));
		float stuckPos = 0.5 - 0.5 * cos(particle.stuckPos);
		vout.StuckK = saturate(1 - particle.lifeOut * 40);
		vout.StuckOcclusion = lerp(1, vout.Occlusion * stuckPos, vout.StuckK * 0.85);
	#endif
	
  #ifdef SHADER_SOLID
		vout.Offset2 = uint2(particle.random * 6, frac(particle.life * 100) * 6);
	#endif

	return vout;
}