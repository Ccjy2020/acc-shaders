#include "pfxSmoke.hlsl"
#include "include_new/base/_include_ps.fx"
#include "pfxSmoke_draw.hlsl"

Texture2D txSmokeNoise : register(t4);

float4 main(PS_IN_shadow pin) : SV_TARGET {
  float c = txSmokeNoise.Sample(samLinearSimple, pin.Tex * 0.2 + pin.TexMovement).a;  
  // float n = txNoise.SampleLevel(samPoint, pin.Tex, 0).r;
  // float alpha = pin.Opacity 
  //   * smoothstep(1, 0.4 * c, length(pin.Tex));

  float alpha = saturate(1 - length(pin.Tex));
  alpha = min(alpha, pin.Opacity);
  alpha = saturate(alpha * (1 - c)); 
  alpha *= saturate(pin.PosY);

  // clip(alpha - 0.1);

  uint2 pos = uint2(pin.PosH.xy);
  pos += pin.Offset2;
  uint2 xy2 = pos.xy % 2;
  uint2 xy3 = pos.xy % 3;

  // alpha *= 0.67;
  // if (alpha < 0.75 && dot(xy2, 1) == 0) discard;
  // if (alpha < 0.5 && xy2.x == 0) discard;
  // if (alpha < 0.25 && xy2.y == 0) discard;
  
  // alpha *= 0.5;
  // alpha -= 0.4;
  if (alpha < 0.9 && xy3.x == 0 && xy3.y == 0
    || alpha < 0.8 && xy3.x == 2 && xy3.y == 1
    || alpha < 0.7 && xy3.x == 1 && xy3.y == 2
    || alpha < 0.6 && xy3.x == 1 && xy3.y == 0
    || alpha < 0.5 && xy3.x == 1 && xy3.y == 1
    || alpha < 0.4 && xy3.x == 0 && xy3.y == 2
    || alpha < 0.3 && xy3.x == 2 && xy3.y == 0
    || alpha < 0.2 && xy3.x == 0 && xy3.y == 1
    || alpha < 0.1) {
    discard;
  }

  // if (alpha < 0.6 && dot(xy2, 1) == 2) discard;
  // if (alpha < 0.4 && dot(xy3, 1) < 2) discard;
  // if (alpha < 0.2 && dot(xy3, 1) < 3) discard;
  // if (alpha < 0.2 && dot(xy3, 1) < 4) discard;
  // if (alpha < 0.6 && xy3.y == 0) discard;
  // if (alpha < 0.3 && xy2.y == 0) discard;
  // if (alpha < 0.2 && xy2.y) discard;

  return 1;
}