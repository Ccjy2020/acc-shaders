#include "include/samplers.hlsl"
#include "pfxSmoke.hlsl"

RWStructuredBuffer<Particle> particleBuffer : register(u0);
RWStructuredBuffer<uint> aliveBuffer_CURRENT : register(u1);
RWStructuredBuffer<uint> aliveBuffer_NEW : register(u2);
RWStructuredBuffer<uint> deadBuffer : register(u3);
RWByteAddressBuffer counterBuffer : register(u4);
StructuredBuffer<EmittingSpot> emittingSpots : register(t19);
StructuredBuffer<Wheel> wheels : register(t1);
StructuredBuffer<Car> cars : register(t2);
Texture2D txGrass : register(t20);

static uint rng_state;

inline float rand(inout float seed, in float2 uv) {
	float result = frac(sin(seed * dot(uv, float2(12.9898, 78.233))) * 43758.5453);
	seed += 1.0;
	return result;
}

[numthreads(THREADCOUNT_EMIT, 1, 1)]
void main(uint3 DTid : SV_DispatchThreadID, uint3 Gid : SV_GroupThreadID) {
	uint emitCount = counterBuffer.Load(PARTICLECOUNTER_OFFSET_REALEMITCOUNT);

	EmittingSpot E = emittingSpots[DTid.y];
	if (DTid.x < min(emitCount, E.emitCount)) {
		float2 uv = float2(E.randomSeed, (float)DTid.x / (float)THREADCOUNT_EMIT);
		float seed = 0.12345;

		Wheel wheel = wheels[E.wheelID];
		Car car = cars[E.wheelID / 4];
		float speed = wheel.angularSpeed * wheel.radius;

		float2 mapUV = (E.contactPoint.xz - gMapPointB) / (gMapPointA - gMapPointB);
		float4 grass = txGrass.SampleLevel(samLinearClamp, mapUV, 0);
		bool isWetDirt = wheel.radius >= 0 && grass.a > 0.7;
		if (isWetDirt && rand(seed, uv) > 0.5) return;
		// if (E.wheelID > 1) return;

		Particle particle = (Particle)0;
		particle.growK = saturate(wheel.angularSpeed / 3 - 0.67);
		particle.thickness = saturate(saturate(E.thickness + (1 - particle.growK) * 0.2));

		particle.position = E.contactPoint;
		particle.velocity = wheel.dir * speed;
		particle.wheelID = E.wheelID;
		particle.stuckSpeed = wheel.angularSpeed;
		particle.stuckSide = rand(seed, uv) * 0.8;
		particle.stuckDrift = 2 * (1 + pow(rand(seed, uv), 2))
			// * (0.3 + saturate(rand(seed, uv) * 0.2 + car.carSpeed * pow(0.1, 1 + E.thickness * 2)))
			* (0.3 + saturate(rand(seed, uv) * 0.2 + car.carSpeed / 10) * 1.2)
			+ pow(saturate(wheel.steered * 4 - 0.5), 2) * 6;
		particle.random = rand(seed, uv);
		particle.random2 = rand(seed, uv);
		particle.random3 = frac(E.contactPoint.x / 10 + speed);
		particle.opacityK = 1 / (2 + particle.thickness * 2);
		particle.stuckPos = 0.01 + rand(seed, uv) / 20;
		particle.groundY = E.contactPoint.y;
		particle.phase = (float)DTid.x;
		particle.color = wheel.smokeColor;

		particle.life = (2 + 4 * pow(rand(seed, uv), 2) + 60 * particle.thickness * particle.thickness * pow(rand(seed, uv), 10)) 
			* (0.5 + 0.5 * saturate(wheel.angularSpeed / 4 - 1))
			* lerp(0.8, 1, particle.thickness);
		particle.size = lerp(wheel.emitSizeA, wheel.emitSizeB, rand(seed, uv));
		particle.texMovement.x = rand(seed, uv);
		particle.texMovement.y = rand(seed, uv);
		particle.neutralSpeedMult = 1;

		if (wheel.radius < 0) {
			particle.growK = E.splashMix * (0.5 + rand(seed, uv));
			particle.velocity = length(E.velocity * (0.3 + rand(seed, uv))) * normalize(E.velocity
				+ (float3(rand(seed, uv), rand(seed, uv), rand(seed, uv)) - 0.5) * E.dustMix);
			particle.stuckPos = 0;
			particle.stuckSpeed = 0;
			particle.stuckDrift = 0;
			particle.color = E.color;
			particle.size = E.size;
			particle.life = E.life * (0.5 + pow(rand(seed, uv), 10));
			// if (rand(seed, uv) > 0.2) return;
			// particle.life = 8;
			particle.opacityK = E.intensity * max(1 / (2 + particle.thickness * 2), 4 / particle.life);
			particle.lifeOut = 1;
			particle.groundY = E.groundY;
			particle.thickness = saturate(E.thickness);
			particle.stuckDrift = E.targetYVelocity;
		} else {
			particle.dustMix = E.dustMix;
			// particle.color = packColor(lerp(
			// 	unpackColor(wheel.smokeColor), 
			// 	lerp(unpackColor(xDustColorA), unpackColor(xDustColorB), rand(seed, uv)), 
			// 	E.dustMix));

			float4 color = lerp(
				unpackColor(wheel.smokeColor), 
				float4(lerp(unpackColor(xDustColorA), unpackColor(xDustColorB), 0.3).xyz, 1), 
				E.dustMix);

			if (isWetDirt){
				// color = lerp(color, float4(float3(0.3, 0.25, 0.2) * 0.5
				// 	+ float3(rand(seed, uv), rand(seed, uv), rand(seed, uv)) * 0.01, 1), 0.5);
				// particle.thickness = 0.6;
				// particle.thickness = 1;
				// particle.lifeOut = 1;
				// particle.stuckPos = 0;
				// particle.stuckSpeed = 0;
				// particle.stuckDrift = 0;
				// particle.dustMix = 3;
				// particle.size = 0;
				// particle.life = 8;
			}

			particle.color = packColor(color);
		}

		// new particle index retrieved from dead list (pop):
		uint deadCount;
		counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_DEADCOUNT, -1, deadCount);
		uint newParticleIndex = deadBuffer[deadCount - 1];

		// write out the new particle:
		particleBuffer[newParticleIndex] = particle;

		// and add index to the alive list (push):
		uint aliveCount;
		counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_ALIVECOUNT, 1, aliveCount);
		aliveBuffer_CURRENT[aliveCount] = newParticleIndex;
	}
}
