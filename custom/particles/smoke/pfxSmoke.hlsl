#include "include/common.hlsl"

float4 loadVector(uint2 vec) {
	return float4(f16tof32(vec.x), f16tof32(vec.x >> 16), f16tof32(vec.y), f16tof32(vec.y >> 16));
}

float4 loadVector(float4 vec) {
	return vec;
}

struct ParticleCounters {
	uint aliveCount;
	uint deadCount;
	uint realEmitCount;
	uint aliveCount_afterSimulation;
};

static const uint PARTICLECOUNTER_OFFSET_ALIVECOUNT = 0;
static const uint PARTICLECOUNTER_OFFSET_DEADCOUNT = PARTICLECOUNTER_OFFSET_ALIVECOUNT + 4;
static const uint PARTICLECOUNTER_OFFSET_REALEMITCOUNT = PARTICLECOUNTER_OFFSET_DEADCOUNT + 4;
static const uint PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION = PARTICLECOUNTER_OFFSET_REALEMITCOUNT + 4;

struct Wheel {
	float3 pos;
	float radius;

	float3 dir;
	float width;

	float3 up;
	float angularSpeed;

	float3 outside;
	float steered;

	float3 velocity;
	float isRear;

	float3 axisCenter;
	float axisRadius;

	uint smokeColor;
	float stuckOffset; // 1
	float flyoffStart; // 0
	float flyoffEnd; // 0.15

	float flyoffDelay; // 2
	float stuckMaxSpeed; // 8…12
	float blockStart; // 1 (0.4)
	float blockEnd; // 1 (0.6)

	float emitSizeA; // 0.08
	float emitSizeB; // 0.1
	float2 pad1;
};

struct CollisionPlane {
	float3 pos;
	float fwd_x;
	float3 up;
	float fwd_y;
	float3 side;
	float fwd_z;
};

struct Car {
	float3 carVelocity;
	float carSpeed;

	float3 shadowCenter;
	float shadowOpacity;

	float3 shadowVecH;
	float carWidth;

	float3 shadowVecV;
	float pad2;

	float4 bs[3];
	CollisionPlane centerPlane;
};

struct Particle {
	float3 position;
	float life;

	float3 velocity;
	float size;

	uint wheelID;
	float stuckPos;
	float stuckSpeed;
	float stuckSide;

	float random;
	float stuckDrift;
	float2 texMovement;

	float phase;
	float pushedAgainst;
	float2 texMovementStatic;

	float thickness;
	float random3;
	float lifeOut;
	float opacityK;

	float opacity;
	float groundY;
	float random2;
	float isOut;

	float growK;
	float dustMix;
	float neutralSpeedMult;
	uint color;
};

struct EmittingSpot {
	uint emitCount;
	float3 contactPoint;

	uint wheelID;
	float randomSeed;
	float intensity;
	float thickness;

	float dustMix; // spreadK
	float splashMix; // growK
	float life;
	float size;

	uint color;
	float3 velocity;

	float groundY;
	float targetYVelocity;
	float2 pad;
};

#define THREADCOUNT_EMIT 256
#define THREADCOUNT_SIMULATION 256
#define FORCE_FIELDS 8
#define COLLISION_PLANES 4

struct ForceField {
	float3 pos;
	float radiusSqr;
	float lifeOffset;
	float forceMult;
	float sizeMult;
	float pad;
};

struct ForceField2 {
	float3 pos;
	float radiusSqrInv;
	float3 velocity;
	float forceMult;
};

#if defined(TARGET_CS)
	cbuffer EmittedParticleCB : register(b8) {
		uint gEmittersCount;
		uint gEmitCountTotal;
		uint gMaxEmitCountPerGroup;
		float gEmitterRandomness;

		float gFrameTime;
		float3 gCameraPosition;

		float xFOVK;
		float3 xCameraDir;

		float2 gMapPointA;
		float2 gMapPointB;

		float4x4 xVP;
		float4x4 xVPInv;

		uint xDustColorA;
		uint xDustColorB;
		float xNearbyClipping;
		float gDensityReductionMult;

		float2 extWindVel;
		float extWindSpeed;
		float extWindWave;

		ForceField2 xForceFields[FORCE_FIELDS];
		CollisionPlane xCollisionPlanes[COLLISION_PLANES];
		uint4 checkCarsIDs;
	};
#endif

static const uint ARGUMENTBUFFER_OFFSET_DISPATCHEMIT = 0;
static const uint ARGUMENTBUFFER_OFFSET_DISPATCHSIMULATION = ARGUMENTBUFFER_OFFSET_DISPATCHEMIT + (3 * 4);
static const uint ARGUMENTBUFFER_OFFSET_DRAWPARTICLES = ARGUMENTBUFFER_OFFSET_DISPATCHSIMULATION + (3 * 4);

// uint packColor(float4 color){
// 	return uint(saturate(color.r) * 255.0) | (uint(saturate(color.g) * 255.0)
// 		| (uint(saturate(color.b) * 255.0) | uint(saturate(color.w) * 255.0) << 8) << 8) << 8;
// }

// float4 unpackColor(uint color){	
//   float4 result;
//   result.r = ((color >> 0) & 0x000000FF) / 255.0;
//   result.g = ((color >> 8) & 0x000000FF) / 255.0;
//   result.b = ((color >> 16) & 0x000000FF) / 255.0;
//   result.a = ((color >> 24) & 0x000000FF) / 255.0;
// 	return result;
// }