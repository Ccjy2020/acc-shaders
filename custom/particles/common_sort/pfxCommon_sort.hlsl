cbuffer SortConstantsCB : register(b8) {
	uint counterReadOffset;
	int3 job_params;
};

ByteAddressBuffer counterBuffer : register(t0);
StructuredBuffer<float> comparisonBuffer : register(t1);
RWStructuredBuffer<uint> indexBuffer : register(u0);
RWByteAddressBuffer indirectBuffers : register(u0);

#define __ReadSortElementCount__ counterBuffer.Load(counterReadOffset);