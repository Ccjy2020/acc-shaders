#define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 pos;
	float life;
	float intensity;
	// float3 pad;
};

struct Particle_packed {
	float3 pos;
	uint life_intensity;
};

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.life_intensity, _U.life, _U.intensity);

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 emitterPos;
		float intensity;
		float3 pad0;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN
	};
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		float4 PosH : SV_POSITION;
		float2 Tex : TEXCOORD1;
		float Intensity : TEXCOORD2;
	};
#endif

#include "particles/common/include_impl.hlsl"
