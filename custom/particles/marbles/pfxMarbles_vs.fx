#include "pfxMarbles.hlsl"

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD
	quadPos *= particle.size * saturate(particle.life * 2) * 2;

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif

  float3 side = normalize(cross(billboardAxis, float3(0, -1, 0)));
  float3 up = normalize(cross(billboardAxis, side));
	float3 offset = side * quadPos.x + up * quadPos.y;
	
	float4 posW = float4(particle.pos, 1);
	offset -= particle.collisionPlane1.xyz * dot(offset, particle.collisionPlane1.xyz) * 0.5;
	posW.xyz += offset;

	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

	PS_IN vout;
	vout.PosH = posH;
	vout.Tex = BILLBOARD[vertexID].xy;
	vout.AmbientMult = 0.07 * lerp(BILLBOARD[vertexID].y * 0.5 + 0.5, saturate(-billboardAxis.y), abs(billboardAxis.y));
	vout.CenterK = abs(billboardAxis.y);
	vout.RandomK = frac(particle.size * 1e8);

	#ifndef MODE_SHADOW
		vout.PosC = posW.xyz - ksCameraPosition.xyz;
		#ifdef MODE_GBUFFER
			GENERIC_PIECE_VELOCITY(posW, particle.velocity * gDeltaTime);
		#else
			vout.Fog = calculateFog(posV);
		#endif
	#endif

	return vout;
}