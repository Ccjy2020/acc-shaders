#include "pfxMarbles.hlsl"

void emit(inout Particle particle, EmittingSpot E, RAND_DECL) {
	particle.pos = E.emitterPos;
	particle.velocity = E.emitterVelocity;
	// particle.velocity.y += 4;
	// particle.pos += (float3(RAND, RAND, RAND) - 0.5) * length(particle.velocity) / 2;
	particle.velocity += (float3(RAND, RAND, RAND) - 0.5) * length(particle.velocity) / 10;
	particle.size = 0.01 * E.emitterSizeMult * (1 + RAND);
	particle.collisionPlane1 = E.collisionPlane1;
	particle.life = 4;
}
