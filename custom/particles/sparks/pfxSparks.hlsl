#define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 pos;
	float life;

	float3 velocity;
	float randomValue;

	float size;
	uint carIndex;
	float brightness;
	float lifePassedOutside;

	float4 collisionPlane1;
	float4 collisionPlane2;

	// float2 lastScreenPos;
	// float2 currentScreenPos;
	float3 carVelocity;
	float motionMult;

	float3 relativeVelocitySmooth;
	uint color;

	float pad1;
	float pad2;
	float spawnDensity;
	float lightStrength;

	float3 randomMotion;
	float pad3;
};

struct Particle_packed {
	float3 pos;
	float life;

	float3 velocity;
	float randomValue;

	float size;
	uint carIndex;
	float brightness;
	float lifePassedOutside;

	uint2 collisionPlane1xyz_pad1;
	uint2 collisionPlane2xyz_pad2;

	float collisionPlane1w;
	float collisionPlane2w;
	float spawnDensity;
	float lightStrength;

	uint2 carVelocity_motionMult;
	uint2 randomMotion_pad3;

	float3 relativeVelocitySmooth;
	uint color;
};

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.life, _U.life);\
	FN(_P.velocity, _U.velocity);\
	FN(_P.randomValue, _U.randomValue);\
	FN(_P.size, _U.size);\
	FN(_P.carIndex, _U.carIndex);\
	FN(_P.brightness, _U.brightness);\
	FN(_P.lifePassedOutside, _U.lifePassedOutside);\
	FN(_P.carVelocity_motionMult, _U.carVelocity, _U.motionMult);\
	FN(_P.randomMotion_pad3, _U.randomMotion, _U.pad3);\
	FN(_P.relativeVelocitySmooth, _U.relativeVelocitySmooth);\
	FN(_P.color, _U.color);\
	FN(_P.spawnDensity, _U.spawnDensity);\
	FN(_P.lightStrength, _U.lightStrength);\
	FN(_P.collisionPlane1w, _U.collisionPlane1.w);\
	FN(_P.collisionPlane2w, _U.collisionPlane2.w);\
	FN(_P.collisionPlane1xyz_pad1, _U.collisionPlane1.xyz, _U.pad1);\
	FN(_P.collisionPlane2xyz_pad2, _U.collisionPlane2.xyz, _U.pad2);

struct CarFloor {
	float3 normal;
	float distance;

	float3 center;
	float speed;

	float3 vec_h;
	float wheelsForce;

	float3 vec_v;
	float _pad2;

	float3 velocity;
	float _lightStrength;

	float4 wheels[4];
};

#if defined(TARGET_CS)
	StructuredBuffer<CarFloor> carFloors : register(t20);

	struct EmittingSpot {
		uint emitCount;
		float3 emitterPosBase;

		float3 emitterPosOffset0;
		float emitterSpeed;

		float3 emitterPosOffset1;
		uint carIndex;

		float3 emitterDirection0;
		float xParticleLifeSpan;

		float emitterSpreadMult2;
		float2 pad0;
		float xParticleLifeSpanRandomness;

		float4 collisionPlane1;
		float4 collisionPlane2;

		uint color;
		float emitterSpeedMin;
		float emitterSpreadMult;
		float lightChanceMult;
	};

	#define FORCE_FIELDS 16

	struct ForceField {
		float3 pos;
		float radius_sqr;
		float3 force;
		float push_away;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		uint2 xResolution;
		float xRatio;
		float xGravity;

		float xZFarP;
		float xZNearP;
		float xFOV;
		float xInteriorHeight;

		float4 xInteriorBounds[5];
		ForceField xForceFields[FORCE_FIELDS];
	};
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		PFX_PS_IN
		float Ratio : TEXCOORD4;
		#ifndef MODE_SHADOW
			float Brightness : TEXCOORD2;
			float GlowExtra : MISC0;
			uint Color : TEXCOORD3;
		#endif
	};

	cbuffer cbSparks : register(CBUFFER_DRAW_SLOT) {
		float brightnessBase;
		float brightnessExtra;
		float sizeMultiplier;
		float velocityMultiplier;
		float fadingBase;
		float fadingLifeLeftK;
		float motionBlurExtra;
		float gDeltaTime;
	}
#endif

#include "particles/common/include_impl.hlsl"
