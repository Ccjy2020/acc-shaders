
// #define GBUFFER_NORMALS_REFLECTION
// #define CREATE_MOTION_BUFFER
// #define USE_ALPHATEST

#ifdef MODE_LIGHTS
  // #define MIPMAPPED_GBUFFER 1
  #include "general/accSS.hlsl"
#endif

#include "pfxSparks.hlsl"

PS_OUT main(PS_IN pin) {
  float full = lerp(pin.Ratio, 1, pow(saturate((0.5 - 0.5 * pin.Tex.y) * 1.1), 10));
	#ifdef MODE_SHADOW
    float3 lighting = 0;
  #else
    float3 toCamera = normalize(pin.PosC);
    float4 inputColor = unpackColor(pin.Color);
    float3 lighting = inputColor.a * pow(inputColor.rgb, lerp(1.6, 1, pin.Ratio)) * pin.Brightness * 6.375;
    #ifndef MODE_LIGHTS
      lighting *= 1.4 * (pin.GlowExtra + 0.4 * lerp(0, 1 - pin.Ratio, pow(saturate((0.5 - 0.5 * pin.Tex.y) * 1.3), 4)));
    #endif
  #endif

  #ifdef MODE_LIGHTS
    float2 ssUV = pin.PosH.xy * gSize.zw;
    // ssUV = ssUV * ssgiResolutionDiv;
    float depth = ssGetDepth(ssUV);
    float3 origin = ssGetPos(ssUV, depth);
    float3 normal = ssGetNormal(ssUV);
    float3 toLight = origin - pin.PosC;
    lighting /= 2;
    float alpha = pow(saturate(1 - length(toLight) / pin.GlowExtra), 1.4) * saturate(-dot(toLight, normal) + 0.2);
    // alpha = 1;
  #else
    pin.Tex.y = 1 - 2 * pow(saturate(-pin.Tex.y * 0.5 + 0.5), lerp(1 + abs(pin.Tex.x) * 2, 1, pin.Ratio));
    float alpha = saturate((1 - dot2(pin.Tex)) * 10);
  #endif

  clip(alpha - 0.005);
  RETURN_BASE(lighting, alpha);
}