#define PASS_ALIVE_COUNT
#include "pfxSparks.hlsl"

// #ifndef DEPTH_COLLISIONS
// #define de_VP xVP
// #endif

float rand(float2 co){
  return frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453);
}

void process(inout Particle particle, uint3 DTid, uint aliveCount) {
	// float4 gSize = float4(1920, 1080, 1.0/1920.0, 1.0/1080.0);

	// float pixelRadius = mul(float4(particle.pos, 1), de_VP).w * max(gSize.z, gSize.w) * 10;
	// float radius = max(length(vin.NormalL), pixelRadius);
	// fade = length(vin.NormalL) / radius;
	// vin.PosL.xyz = vin.PosL.xyz - vin.NormalL + normalize(vin.NormalL) * radius;
	// float4 pos2D0 = mul(float4(particle.pos, 1), de_VP);
	// float4 pos2D1 = mul(float4(particle.pos + 0.01, 1), de_VP);
	// pos2D0.xyz /= pos2D0.w;
	// pos2D1.xyz /= pos2D1.w;
	// float len = length(pos2D1.xy / pos2D1.z - pos2D0.xy / pos2D0.z) * 1000;

	float toCamera = length(particle.pos - gCameraPosition);
	particle.size = (1 + particle.brightness * 0.2) * clamp(0.001 + toCamera / 1000, 0.005, 0.015);
	particle.carVelocity *= xRatio;

	if (particle.lightStrength < 1.1){
		particle.lightStrength = particle.lightStrength < min(100 / (float)aliveCount, 0.25) ? 2 : 1.5;
	}

	if (gFrameTime) {
		// float3 force = float3(0, -10, 0);
		float3 force = float3(0, xGravity, 0);
		force *= 0.5 * (particle.randomValue + saturate(-particle.randomMotion * 4));
		force += particle.randomMotion;

		for (uint i = 0; i < FORCE_FIELDS; i++) {
			float3 dist = particle.pos - xForceFields[i].pos;
			force += (xForceFields[i].force + normalize(dist) * xForceFields[i].push_away) * saturate(1 - dot(dist, dist) * xForceFields[i].radius_sqr);

			// float K = saturate(1 - dot(dist, dist) * xForceFields[i].radius_sqr);
			// force += xForceFields[i].force * (K < 1 ? 1 : 0) + normalize(dist) * xForceFields[i].push_away * K;
		}

		// integrate:
		particle.pos += particle.velocity * gFrameTime;

		// checking for car floor
		if (particle.carIndex) {
			CarFloor carFloor = carFloors[particle.carIndex];
			float3 fl_d = particle.pos - carFloor.center;
			float fl_t = dot(fl_d, carFloor.normal);
			float sz_m = 0.95 + particle.randomValue * 0.1;
			float fl_h = abs(dot(fl_d, carFloor.vec_h));
			float fl_v = abs(dot(fl_d, carFloor.vec_v));

			for (uint i = 0; i < 4; i++) {
				float3 dist = particle.pos - carFloor.wheels[i].xyz;
				dist -= carFloor.normal * dot(dist, carFloor.normal);
				float len = length(dist);
				float3 dir = dist / len;
				float K = len * carFloor.wheels[i].w;
				if (K < 0.6) particle.life = -1;
				force += dir * carFloor.wheelsForce * saturate(1 - K);
			}

			if (fl_h > sz_m || fl_v > 1) {
				if (!particle.lifePassedOutside) {
					force += carFloor.normal * particle.randomValue * (1 + particle.randomValue) * 500;
				}

				// drag:
				particle.velocity *= xRatio;
				particle.velocity += force * gFrameTime;
				particle.lifePassedOutside += gFrameTime;
			} else if (fl_t > 0) {
				particle.pos -= carFloor.normal * distanceToPlane(carFloor.normal, carFloor.distance, particle.pos);
			}
		} else {
			particle.velocity *= xRatio;
			particle.velocity += force * gFrameTime;
			particle.lifePassedOutside += gFrameTime;
		}

		particle.motionMult = saturate(particle.motionMult + gFrameTime * 30);

		if (particle.lifePassedOutside > 0.05 && xInteriorBounds[0].w) {
			float d = distanceToPlane(xInteriorBounds[0].xyz, xInteriorBounds[0].w, particle.pos);
			if (d < 0 && d > -xInteriorHeight) {
				float3 dn = xInteriorBounds[0].xyz;
				for (uint i = 1; i < 5; i++) {
					float di = distanceToPlane(xInteriorBounds[i].xyz, xInteriorBounds[i].w, particle.pos);
					if (di > d) {
						d = di;
						dn = xInteriorBounds[i].xyz;
					}
				}
				if (d < 0) {
					particle.pos += dn * (-d + 0.01);
					particle.velocity = reflect(particle.velocity, dn);
					particle.motionMult = 0;
				}
			}
		}

		float planeDistance1 = distanceToPlane(particle.collisionPlane1, particle.pos);
		bool collided = false;
		if (planeDistance1 < 0) {
			particle.velocity = reflect(particle.velocity, particle.collisionPlane1.xyz);
			particle.pos += particle.collisionPlane1.xyz * (-planeDistance1 + 0.01);
			particle.motionMult = 0;
			collided = true;
		}

		float planeDistance2 = distanceToPlane(particle.collisionPlane2, particle.pos);		
		if (!collided && planeDistance2 < 0) {
			particle.velocity = reflect(particle.velocity, particle.collisionPlane2.xyz);
			particle.pos += particle.collisionPlane2.xyz * (-planeDistance2 + 0.01);
			particle.motionMult = 0;
			collided = true;
		}

		float randomMotionLimit = 30 * saturate(planeDistance1 * 2 - 0.2);
		float3 randomVal = float3(rand(particle.pos.xy + particle.randomValue), rand(particle.pos.xz + particle.randomValue), rand(particle.pos.xz + particle.randomValue));
		particle.randomMotion += (randomVal - 0.5) * 100 * gFrameTime;
		particle.randomMotion = clamp(particle.randomMotion, -randomMotionLimit, randomMotionLimit);

		#ifdef DEPTH_COLLISIONS
			float3 posC = particle.pos - de_CameraPosition;
			float depthCalc = length(posC);
			float2 ssUV = ssGetUV(posC).xy;

			[branch]
			if (!collided && particle.lifePassedOutside > 0.2 && depthCalc < 350 && ssUV.x > 0 && ssUV.x < 1 && ssUV.y > 0 && ssUV.y < 1) {
				float3 p0 = ssGetPos(ssUV, txDepth.SampleLevel(samPointClamp, ssUV, 0));
				float3 surfaceNormal = ssNormalDecode(txNormals.SampleLevel(samPointClamp, ssUV, 0).xyz);
				float distanceDepth = dot(posC - p0, surfaceNormal);
				bool isNotFar = distanceDepth > -0.1 - lerp(0, toCamera / 200, saturate(surfaceNormal.y * 2 - 0.5));

				[branch]
				if (surfaceNormal.y > 0.35 && distanceDepth < 0.02 && isNotFar) {
					particle.velocity -= surfaceNormal * dot(particle.velocity, surfaceNormal) * 1.2;
					particle.pos += surfaceNormal * (-distanceDepth + 0.1);
					particle.motionMult = 0;
					collided = true;

					// float newPlaneDistance = dot(surfaceNormal, p0 + de_CameraPosition);
					// if (surfaceNormal.y > 0.35) {
					// 	if (distanceToPlane(surfaceNormal, newPlaneDistance, particle.pos) > distanceToPlane(particle.collisionPlane1, particle.pos)) {
					// 		if (particle.collisionPlane1.y == particle.collisionPlane2.y){
					// 			particle.collisionPlane2.xyz = surfaceNormal;
					// 			particle.collisionPlane2.w = newPlaneDistance;
					// 		}

					// 		particle.collisionPlane1.xyz = surfaceNormal;
					// 		particle.collisionPlane1.w = newPlaneDistance;
					// 	}
					// } else {
					// 	particle.collisionPlane2.xyz = surfaceNormal;
					// 	particle.collisionPlane2.w = newPlaneDistance;
					// }
				}
			}
  	#endif

		if (collided){
			particle.life = min(particle.life, 2);
		}
	}

	particle.life -= gFrameTime;
	// particle.relativeVelocitySmooth += ((particle.velocity - particle.carVelocity) / 10 - particle.relativeVelocitySmooth) / 20.0;
	particle.relativeVelocitySmooth += ((particle.velocity - particle.carVelocity) / 10 - particle.relativeVelocitySmooth) / 8.0;
}
