#ifndef _PFX_DECL_
  #define _PFX_DECL_

  #include "include/common.hlsl"
  #include "include/packing_cs.hlsl"

  #ifndef VERTICES_PER_PARTICLE
    #define VERTICES_PER_PARTICLE 6
  #endif

  #if defined(TARGET_VS) || defined(TARGET_PS)
    #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_MAIN_NOFX)
      #define MODE_MAIN_FX
    #endif
    
    #define CBUFFER_DRAW_SLOT b6
    #define TEXTURE_SLOT t4

    #ifdef MODE_GBUFFER
      #define GBUFFER_NORMALS_REFLECTION
      #define CREATE_MOTION_BUFFER
      #define USE_ALPHATEST
    #else
      #define USE_BACKLIT_FOG
    #endif

    #ifdef PARTICLES_WITHOUT_NORMALS
      #define _PFX_PS_IN_NORMAL
    #else
      #define _PFX_PS_IN_NORMAL float3 NormalW : NORMAL;
    #endif

    #ifdef MODE_SHADOW
      #define PFX_PS_IN \
        float4 PosH : SV_POSITION;\
		    float2 Tex : TEXCOORD1;
    #elif defined(MODE_GBUFFER)
      #define PFX_PS_IN \
        float4 PosH : SV_POSITION;\
        float3 PosC : POSITION;\
		    _PFX_PS_IN_NORMAL\
		    float2 Tex : TEXCOORD1;\
        float Fog : TEXCOORD6;\
		    MOTION_BUFFER        
    #else
      #define PFX_PS_IN \
        float4 PosH : SV_POSITION;\
        float3 PosC : POSITION;\
		    _PFX_PS_IN_NORMAL\
		    float2 Tex : TEXCOORD1;\
        float Fog : TEXCOORD6;
    #endif
  #endif

  #if defined(TARGET_VS)
    #include "include_new/base/_include_vs.fx"
    
    #if VERTICES_PER_PARTICLE == 6
      static const float3 BILLBOARD[] = {
        float3(-1, -1, 0),
        float3(1, -1, 0),
        float3(-1, 1, 0),
        float3(-1, 1, 0),
        float3(1, -1, 0),
        float3(1, 1, 0),
      };
    #endif
  #endif

  #if defined(TARGET_PS)
    #ifdef PARTICLES_WITHOUT_NORMALS
      #define GBUFF_NORMAL_W_SRC float3(0, 1, 0)
      #define GBUFF_NORMAL_W_PIN_SRC float3(0, 1, 0)
    #endif
    #include "include_new/base/_include_ps.fx"
    Texture2D txMain : register(TEXTURE_SLOT); 
  #endif

  #if defined(TARGET_CS)
    #ifdef PARTICLES_EMIT_2D
      #define THREADCOUNT_EMIT_SIDE 32
      #define THREADCOUNT_EMIT_TOTAL 1024
    #else
      #define THREADCOUNT_EMIT 256
    #endif
    #define THREADCOUNT_SIMULATION 256

    static const uint PARTICLECOUNTER_OFFSET_ALIVECOUNT = 0;
    static const uint PARTICLECOUNTER_OFFSET_DEADCOUNT = PARTICLECOUNTER_OFFSET_ALIVECOUNT + 4;
    static const uint PARTICLECOUNTER_OFFSET_REALEMITCOUNT = PARTICLECOUNTER_OFFSET_DEADCOUNT + 4;
    static const uint PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION = PARTICLECOUNTER_OFFSET_REALEMITCOUNT + 4;

    static const uint ARGUMENTBUFFER_OFFSET_DISPATCHEMIT = 0;
    static const uint ARGUMENTBUFFER_OFFSET_DISPATCHSIMULATION = ARGUMENTBUFFER_OFFSET_DISPATCHEMIT + (3 * 4);
    static const uint ARGUMENTBUFFER_OFFSET_DRAWPARTICLES = ARGUMENTBUFFER_OFFSET_DISPATCHSIMULATION + (3 * 4);

    #define CBUFFER_DRAW_SLOT b6
    #define CBUFFER_SIMULATE_SLOT b8

    #define CBUFFER_SIMULATE_GEN \
      uint gEmittersCount;\
      uint gEmitCountTotal;\
      uint gMaxEmitCountPerGroup;\
      float gEmitterRandomness;\
      float gFrameTime;\
      float3 gCameraPosition;

    #ifdef USE_GENERIC
      cbuffer cbSimulateGeneric : register(CBUFFER_SIMULATE_SLOT) {
        CBUFFER_SIMULATE_GEN
      };
    #endif

    #include "include/random_cs.hlsl"
    #include "include/samplers.hlsl"
    #include "include/collisions_cs.hlsl"
  #endif
#endif