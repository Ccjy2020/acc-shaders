#ifndef PARTICLES_PACKING
  Particle PFX_pack(Particle _U) { return _U; }
  Particle PFX_unpack(Particle _P) { return _P; }
#elif defined(PARTICLES_PACKING_RULES)
  Particle_packed PFX_pack(Particle _U){
    Particle_packed _P;
    PARTICLES_PACKING_RULES(__pack);
    return _P;
  }

  Particle PFX_unpack(Particle_packed _P){
    Particle _U;
    PARTICLES_PACKING_RULES(__unpk);
    return _U;
  }
#endif

#if defined(TARGET_VS)
  #ifdef PARTICLES_PACKING
    StructuredBuffer<Particle_packed> particleBuffer : register(t0);
  #else
    StructuredBuffer<Particle> particleBuffer : register(t0);
  #endif    
  StructuredBuffer<uint> aliveList : register(t1);

  #define LOAD_PARTICLE_BILLBOARD\
    uint vertexID = fakeIndex % VERTICES_PER_PARTICLE;\
    uint instanceID = fakeIndex / VERTICES_PER_PARTICLE;\
    Particle particle = PFX_unpack(particleBuffer[aliveList[instanceID]]);\
	  float3 quadPos = BILLBOARD[vertexID];
#endif

#if defined(TARGET_CS)

  #if defined(SUBTARGET_EMIT)
    #ifdef PARTICLES_PACKING
      RWStructuredBuffer<Particle_packed> particleBuffer : register(u0);
    #else
      RWStructuredBuffer<Particle> particleBuffer : register(u0);
    #endif    
    RWStructuredBuffer<uint> aliveBuffer_CURRENT : register(u1);
    RWStructuredBuffer<uint> aliveBuffer_NEW : register(u2);
    RWStructuredBuffer<uint> deadBuffer : register(u3);
    RWByteAddressBuffer counterBuffer : register(u4);
    StructuredBuffer<EmittingSpot> emittingSpots : register(t19);

    #ifdef PARTICLES_MESH_SPAWN
      #include "include/mesh_spawn_cs.hlsl"
    #endif

    #ifndef PARTICLE_CUSTOM_IMPL

      #ifndef PASS_INDEX_TYPE
        #ifdef PARTICLES_EMIT_2D
          #define PASS_INDEX_TYPE uint2
        #else
          #define PASS_INDEX_TYPE uint
        #endif
      #endif

      #ifdef PASS_INDEX
        void emit(inout Particle particle, EmittingSpot E, PASS_INDEX_TYPE index, RAND_DECL);
      #else
        void emit(inout Particle particle, EmittingSpot E, RAND_DECL);
      #endif

      #ifdef PARTICLES_EMIT_2D
        [numthreads(THREADCOUNT_EMIT_SIDE, THREADCOUNT_EMIT_SIDE, 1)]
      #else
        [numthreads(THREADCOUNT_EMIT, 1, 1)]
      #endif
      void main(uint3 DTid : SV_DispatchThreadID) {
        uint emitCount = counterBuffer.Load(PARTICLECOUNTER_OFFSET_REALEMITCOUNT);

        #ifdef PARTICLES_EMIT_2D
          EmittingSpot E = emittingSpots[DTid.z];
          bool isValid = true;
          uint2 indexToPass = DTid.xy;
          float randInitY = (float)DTid.x / (float)THREADCOUNT_EMIT_SIDE;
        #else
          EmittingSpot E = emittingSpots[DTid.y];
          bool isValid = DTid.x < min(emitCount, E.emitCount);
          uint indexToPass = DTid.x;
          float randInitY = (float)DTid.x / (float)THREADCOUNT_EMIT;
        #endif

        if (isValid) {
          RAND_INIT(0.12345, float2(gEmitterRandomness + dot(DTid, 0.01), randInitY));
          Particle particle = (Particle)0;

          #ifdef PASS_INDEX
            emit(particle, E, indexToPass, RAND_ARGS);
          #else
            emit(particle, E, RAND_ARGS);
          #endif

          // New particle index retrieved from dead list (pop):
          uint deadCount;
          counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_DEADCOUNT, -1, deadCount);
          uint newParticleIndex = deadBuffer[deadCount - 1];

          // Write out the new particle:
          particleBuffer[newParticleIndex] = PFX_pack(particle);

          // And add index to the alive list (push):
          uint aliveCount;
          counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_ALIVECOUNT, 1, aliveCount);
          aliveBuffer_CURRENT[aliveCount] = newParticleIndex;
        }
      }
    #endif
  #endif

  #if defined(SUBTARGET_SIMULATE)
    #ifdef PARTICLES_PACKING
      RWStructuredBuffer<Particle_packed> particleBuffer : register(u0);
    #else
      RWStructuredBuffer<Particle> particleBuffer : register(u0);
    #endif    
    RWStructuredBuffer<uint> aliveBuffer_CURRENT : register(u1);
    RWStructuredBuffer<uint> aliveBuffer_NEW : register(u2);
    RWStructuredBuffer<uint> deadBuffer : register(u3);
    RWByteAddressBuffer counterBuffer : register(u4);

    #ifdef PARTICLES_SORTING
      RWStructuredBuffer<float> distanceBuffer : register(u6);
    #endif

    #ifdef PASS_ALIVE_COUNT
      void process(inout Particle particle, uint3 DTid, uint aliveCount);
    #else
      void process(inout Particle particle, uint3 DTid);
    #endif

    [numthreads(THREADCOUNT_SIMULATION, 1, 1)]
    void main(uint3 DTid : SV_DispatchThreadID, uint Gid : SV_GroupIndex) {
      uint aliveCount = counterBuffer.Load(PARTICLECOUNTER_OFFSET_ALIVECOUNT);
      GroupMemoryBarrierWithGroupSync();

      if (DTid.x < aliveCount) {
        uint particleIndex = aliveBuffer_CURRENT[DTid.x];
        Particle particle = PFX_unpack(particleBuffer[particleIndex]);

        if (particle.life > 0) {
          #ifdef PASS_ALIVE_COUNT
            process(particle, DTid, aliveCount);
          #else
            process(particle, DTid);
          #endif
          
          #ifdef PARTICLES_SORTING
            float3 eyeVector = particle.pos - gCameraPosition;
            float distSQ = length(eyeVector) - particle.size / 2;
            distanceBuffer[particleIndex] = 1 - saturate(distSQ / 10000);
          #endif
      
          particleBuffer[particleIndex] = PFX_pack(particle);
          uint newAliveIndex;
          counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION, 1, newAliveIndex);
          aliveBuffer_NEW[newAliveIndex] = particleIndex;
        } else {
          uint deadIndex;
          counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_DEADCOUNT, 1, deadIndex);
          deadBuffer[deadIndex] = particleIndex;
        }
      }
    }

  #endif
#endif