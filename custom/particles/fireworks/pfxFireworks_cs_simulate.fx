#include "pfxFireworks.hlsl"

#define MAX_WIND_SPEED 20
#define WIND_COORDS_MULT 1

float2 windOffset(float3 pos, float windPower, float freqMult){
  #ifdef NO_WIND
    return 0;
  #else
    float waveOffset = saturate(0.1 + extWindSpeed / MAX_WIND_SPEED);
    float wave = sin(extWindWave * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17) 
      * sin(extWindWave * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13);
    return extWindVel * windPower * (1 + (wave + waveOffset) * 0.5);
  #endif
}

// void forceFields(inout Particle particle) {
//   for (uint i = 0; i < FORCE_FIELDS; i++) {
//     float3 diff = particle.pos - extForceFields[i].xyz;
// 		float dist = length(diff);
// 		float3 dir = diff / dist;
// 		// particle.velocity += dir * extForceFields[i].w / max(dist - particle.size, 0.01);
//   }
// }

void process(inout Particle particle, uint3 DTid) {
  float drag = 1 / (1 + 1.6 * gFrameTime);
	float bounceK = 1.2;
	if (length(particle.velocity) > 1){
		particle.pos += particle.velocity * gFrameTime;
	}
	particle.life -= gFrameTime;
	
	float3 neutralSpeed = normalize(particle.velocity) * 2;
	neutralSpeed.xz += windOffset(particle.pos, pow(saturate(particle.lifespan - particle.life), 2) * 0.5, 2.5);
	particle.velocity = lerp(neutralSpeed, particle.velocity, drag);

	particle.velocity.y += 0.5 * gGravity;
	// forceFields(particle);
}
