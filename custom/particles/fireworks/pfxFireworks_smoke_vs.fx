#include "pfxFireworks_smoke.hlsl"

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

	float size = particle.size * saturate(particle.life * 3) * 2;
	quadPos *= size;

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif

  float3 side = normalize(cross(billboardAxis, float3(0, 1, 0)));
  float3 up = normalize(cross(billboardAxis, side));
	float3 offset = side * quadPos.x + up * quadPos.y;

	float4 posW = float4(particle.pos, 1);
	posW.xyz += offset;

	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

	PS_IN vout;
	vout.PosH = posH;
	vout.Tex = BILLBOARD[vertexID].xy;
	vout.Color = unpackColor(particle.color).rgb * 4;
	vout.TexOffset = particle.uvOffset;
	vout.TexScale = 0.06 * particle.size;
	vout.Opacity = saturate(particle.life / particle.lifeSpan) * particle.opacity;
	vout.OffsetW = normalize(offset);

	#ifndef MODE_SHADOW
		vout.PosC = posW.xyz - ksCameraPosition.xyz;
		#ifdef MODE_GBUFFER
			GENERIC_PIECE_VELOCITY(posW, particle.velocity * gDeltaTime);
		#else
			vout.Fog = calculateFog(posV);
		#endif
	#endif

	return vout;
}