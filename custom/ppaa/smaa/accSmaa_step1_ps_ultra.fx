#define SMAA_PRESET_ULTRA
#include "accSmaa_common.hlsl"

float4 main(VS_Smaa_step1 pin) : SV_TARGET {
  return float4(SMAAColorEdgeDetectionPS(pin.Tex, pin.Offset, txDiffuse), 0, 0);
}