#define FXAA_PC 1
#define FXAA_HLSL_5 1
#include "FXAA3_11.hlsl"

struct FXAAShaderConstants
{
  // {x_} = 1.0/screenWidthInPixels
  // {_y} = 1.0/screenHeightInPixels
  float2 rcpFrame;

  float2 _packing0;

  // This must be from a constant/uniform.
  // {x___} = 2.0/screenWidthInPixels
  // {_y__} = 2.0/screenHeightInPixels
  // {__z_} = 0.5/screenWidthInPixels
  // {___w} = 0.5/screenHeightInPixels
  float4 rcpFrameOpt;

  // {x___} = -N/screenWidthInPixels  
  // {_y__} = -N/screenHeightInPixels
  // {__z_} =  N/screenWidthInPixels  
  // {___w} =  N/screenHeightInPixels 
  float4 rcpFrameOpt2;

  // This can effect sharpness.
  //   1.00 - upper limit (softer)
  //   0.75 - default amount of filtering
  //   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
  //   0.25 - almost off
  //   0.00 - completely off
  float fxaaQualitySubpix;

  // The minimum amount of local contrast required to apply algorithm.
  //   0.333 - too little (faster)
  //   0.250 - low quality
  //   0.166 - default
  //   0.125 - high quality 
  //   0.063 - overkill (slower)
  float fxaaQualityEdgeThreshold;

  // This used to be the FXAA_QUALITY__EDGE_THRESHOLD_MIN define.
  float fxaaQualityEdgeThresholdMin;

  // This used to be the FXAA_CONSOLE__EDGE_SHARPNESS define.
  float fxaaConsoleEdgeSharpness;

  // This used to be the FXAA_CONSOLE__EDGE_THRESHOLD define.
  float fxaaConsoleEdgeThreshold;

  // This used to be the FXAA_CONSOLE__EDGE_THRESHOLD_MIN define.
  float fxaaConsoleEdgeThresholdMin;

  float2 _packing1;
    //    
    // Extra constants for 360 FXAA Console only.
    // float4 fxaaConsole360ConstDir;
};

cbuffer cbData : register(b10) {
  FXAAShaderConstants g_FXAA;
}

SamplerState samLinearSimple : register(s7) {
  Filter = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  FxaaTex tex;
  tex.smpl = samLinearSimple;
  tex.tex = txDiffuse;
  return FxaaPixelShader(pin.Tex, float4(0, 0, 0, 0), tex, tex, tex, 
    g_FXAA.rcpFrame, 
    g_FXAA.rcpFrameOpt, 
    g_FXAA.rcpFrameOpt2, 
    g_FXAA.rcpFrameOpt2,
    g_FXAA.fxaaQualitySubpix,
    g_FXAA.fxaaQualityEdgeThreshold,
    g_FXAA.fxaaQualityEdgeThresholdMin,
    g_FXAA.fxaaConsoleEdgeSharpness,
    g_FXAA.fxaaConsoleEdgeThreshold,
    g_FXAA.fxaaConsoleEdgeThresholdMin,
    float4(0, 0, 0, 0));
}