// https://www.shadertoy.com/view/3dVXDc

#include "include/common.hlsl"
#define UI3 uint3(1597334673U, 3812015801U, 2798796415U)

#define mod(x, y) fmod(x, y)
#define fract(x) frac(x)

float3 h33(float3 p) {
  uint3 q = uint3(int3(p)) * UI3;
  return 2 * float3((q.x ^ q.y ^ q.z) * UI3) / float(0xFFFFFFFFU) - 1;
}

float gradient(float3 x, float frequency) {
  float3 p = floor(x);
  float3 w = fract(x);
  
  float3 u = w * w * w * (w * (w * 6 - 15) + 10);
  float3 ga = h33(mod(p + float3(0, 0, 0), frequency));
  float3 gb = h33(mod(p + float3(1, 0, 0), frequency));
  float3 gc = h33(mod(p + float3(0, 1, 0), frequency));
  float3 gd = h33(mod(p + float3(1, 1, 0), frequency));
  float3 ge = h33(mod(p + float3(0, 0, 1), frequency));
  float3 gf = h33(mod(p + float3(1, 0, 1), frequency));
  float3 gg = h33(mod(p + float3(0, 1, 1), frequency));
  float3 gh = h33(mod(p + float3(1, 1, 1), frequency));
  
  float va = dot(ga, w - float3(0, 0, 0));
  float vb = dot(gb, w - float3(1, 0, 0));
  float vc = dot(gc, w - float3(0, 1, 0));
  float vd = dot(gd, w - float3(1, 1, 0));
  float ve = dot(ge, w - float3(0, 0, 1));
  float vf = dot(gf, w - float3(1, 0, 1));
  float vg = dot(gg, w - float3(0, 1, 1));
  float vh = dot(gh, w - float3(1, 1, 1));

  return va + 
    u.x * (vb - va) + 
    u.y * (vc - va) + 
    u.z * (ve - va) + 
    u.x * u.y * (va - vb - vc + vd) + 
    u.y * u.z * (va - vc - ve + vg) + 
    u.z * u.x * (va - vb - ve + vf) + 
    u.x * u.y * u.z * (-va + vb + vc - vd + ve - vf - vg + vh);
}

float worley(float3 uv, float frequency) {    
  float3 id = floor(uv);
  float3 p = fract(uv);    
  float ret = 10000;
  for (int x = -1; x <= 1; x++) {
    for (int y = -1; y <= 1; y++) {
      for (int z = -1; z <= 1; z++) {
        float3 offset = float3(x, y, z);
        float3 h = h33(mod(id + offset, frequency)) * 0.4 + 0.3 + offset;
        ret = min(ret, dot2(p - h));
      }
    }
  }    
  return 1 - ret;
}

float perlinFBM(float3 p, float frequency, int octaves) {
  float G = exp2(-0.85);
  float amp = 1;
  float noise = 0;
  for (int i = 0; i < octaves; i++) {
    noise += amp * gradient(p * frequency, frequency);
    frequency *= 2;
    amp *= G;
  }
  return noise;
}

float worleyFBM(float3 p, float frequency) {
  return worley(p * frequency, frequency) * 0.625 +
    worley(p * frequency * 2, frequency * 2) * 0.25 +
    worley(p * frequency * 4, frequency * 4) * 0.125;
} 

