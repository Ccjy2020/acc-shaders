#ifndef _RAIN_HLSL_
#define _RAIN_HLSL_

#ifdef __cplusplus

#define ARG_OUT float&

#include <math/math.h>

namespace data
{
	using namespace math;

	#else
#define ARG_OUT out float
	#endif

	void rainCalculate(float sceneWet, float sceneWater, float puddleValue, float reliefValue,
		ARG_OUT retDamp, ARG_OUT retWetness, ARG_OUT retWater)
	{
		sceneWet = max(sceneWet, sceneWater * saturate(puddleValue * lerp(2.f, 4.f, sceneWater) + sceneWet * 2.f - 1.f));
		sceneWater = min(sceneWet, sceneWater);

		float reliefMult = reliefValue * saturate(2.f * sceneWater);
		float waterBase = pow(max(puddleValue * 1.2f, 0.f), lerp(3.f, 1.f, sqrt(sceneWater))) * lerp(1.5f, 3.f, reliefMult) * saturate(sceneWater * 10);
		retDamp = saturate(max(sceneWet * saturate(puddleValue * 10.f) * 2.f, waterBase * 4.f));
		retWetness = saturate(waterBase);
		retWater = saturate(saturate(waterBase - 1.f) * pow(retWetness, 2.f) * 3.f - 1.f);
	}

	#ifdef __cplusplus
}
#endif

#endif
