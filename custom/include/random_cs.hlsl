#define RAND_INIT(_SEED, _UV) float2 __uv = _UV; float __seed = _SEED;
#define RAND __rand(__seed, __uv)
#define RAND_DECL inout float __seed, inout float2 __uv
#define RAND_ARGS __seed, __uv

inline float __rand(inout float seed, in float2 uv) {
	float result = frac(sin(seed * dot(uv, float2(12.9898, 78.233))) * 43758.5453);
	seed += 1.0;
	return result;
}