#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float gGamma;
  float gBrightness;
  float gUseTonemapping;
  float gPad0;
}

float tonemap(float x, float P, float a, float m, float l, float c, float b) {
  float l0 = ((P - m) * l) / a;
  float L0 = m - m / a;
  float L1 = m + (1 - m) / a;
  float S0 = m + l0;
  float S1 = m + a * l0;
  float C2 = (a * P) / (P - S1);
  float CP = -C2 / P;
  float w0 = 1 - smoothstep(0, m, x);
  float w2 = step(m + l0, x);
  float w1 = 1 - w0 - w2;
  float T = m * pow(x / m, c) + b;
  float S = P - (P - S1) * exp(CP * (x - S0));
  float L = m + a * (x - m);
  return T * w0 + L * w1 + S * w2;
}

float tonemap(float x) {
  const float P = 5.0;  // max display brightness
  const float a = 1.0;  // contrast
  const float m = 0.22; // linear section start
  const float l = 0.4;  // linear section length
  const float c = 1.33; // black
  const float b = 0.0;  // pedestal
  return tonemap(x, P, a, m, l, c, b);
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 v = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
  float4 b = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 1) 
    + txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 2) * 0.3
    + txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 5) * 0.3;
  // v.rgb += max(b.rgb - 3, 0) * 0.1;
  // v.rgb = max(b.rgb, 0);

  v.rgb = max(v.rgb, 0);
  if (gUseTonemapping){
    v.rgb = float3(tonemap(v.r), tonemap(v.g), tonemap(v.b));
  }
  
  v.rgb *= gBrightness;
  v.rgb = pow(max(v.rgb, 0), gGamma);
  return v;
}