#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

struct VS_IN_collider {
  float3 PosL : POSITION;
  uint NormalL : NORMAL_ENCODED;
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

float4 toScreenSpaceAlt(float4 posL){
  float4 posH = mul(mul(posL, ksWorld), ksMVPInverse);
  if (posH.z < 0) posH.z = 0;
  return posH;
} 

PS_IN main(VS_IN_collider vin) {
  PS_IN vout;
  // vout.PosH = toScreenSpaceAlt(vin.PosL + float4(vin.NormalL, 0) * mul(mul(vin.PosL, ksView), ksProjection).w * 0.04);
  vout.PosH = toScreenSpaceAlt(float4(vin.PosL, 1));
  vout.Tex = 0;
  return vout;
}

