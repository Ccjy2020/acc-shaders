#include "accShadowDynamic.hlsl"

struct VS_IN_skinned {
  AC_INPUT_SKINNED_ELEMENTS
};

cbuffer cbData : register(b11) {
  float4 gPosition;
}

float4 toWorldSpace(float4 posL, float4 boneWeights, float4 boneIndices){
  float4 posW = 0;
  for (int i = 0; i < 4; i++){
    float weight = boneWeights[i];
    if (SKINNED_WEIGHT_CHECK(weight)){
      uint index = (uint)boneIndices[i];
      float4x4 bone = bones[index];
      posW += (mul(bone, posL) + gPosition) * weight;
    }
  }
  return posW;
} 

PS_IN main(VS_IN_skinned vin) {
  float4 posW = toWorldSpace(vin.PosL, vin.BoneWeights, vin.BoneIndices);
  return voutFill(posW.xyz, vin.Tex);
}

