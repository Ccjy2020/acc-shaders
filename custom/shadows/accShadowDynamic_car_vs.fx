#include "accShadowDynamic.hlsl"

cbuffer cbData : register(b11) {
  float4x4 gWorld;
}

PS_IN main(VS_IN_ac vin) {
  float4 posW = mul(vin.PosL, gWorld);
  return voutFill(posW.xyz, vin.Tex);
}

