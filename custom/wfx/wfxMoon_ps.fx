#define PS
#include "wfx_common.hlsl"
#include "wfx_common_sky.hlsl"

cbuffer cbExtMoon : register(b8) {
  float3 moonToSunDirection;
  float moonBrightness;
  float moonOpacity;
  float moonFogOpacity;
  float2 __padding_moon_0;

  float3 moonColor;
  float __padding_moon_1;
};

float4 main(PS_IN_Sky pin) : SV_TARGET {
  float4 txDiffuseValue = txDiffuse.SampleLevel(samLinear, pin.Tex, 0);
  clip(txDiffuseValue.a - 0.0001);

  float2 txNm = txDiffuseValue.xy * 2 - 1;
  float2 offsetBase = pin.Tex * 2 - 1;
  float2 offset = clamp(offsetBase - txNm * (1 - dot(offsetBase, offsetBase)), -1, 1);
  float3 normal = float3(offset.x, -offset.y, sqrt(1 - saturate(dot(offset, offset))));
  float litUp = saturate(dot(normal, moonToSunDirection) * 3) * txDiffuseValue.z;



  litUp = saturate(dot(txDiffuseValue.xyz * 2 - 1, moonToSunDirection) * 2);

  // float3 toCamera = normalize(pin.PosW);

  float3 toCamera = normalize(pin.PosW);
  float dirHeight = getHeight(toCamera);
  float zenithDensity = getZenithDensity(dirHeight);
  float skyEdge = getSkyEdge(dirHeight);
  float3 absorption = getSkyAbsorption(zenithDensity);

  float3 color = moonColor * moonBrightness * absorption * skyEdge * cStarsColor;
  float3 starsBlend = lerp(luminance(color), color, cStarsSaturation);
    
  float alpha = litUp * saturate(moonOpacity) * lerp(0.5, 2, txDiffuseValue.a);
  return withFogImpl(starsBlend, toCamera * ksFarPlane, toCamera, pin.LightH.x, moonFogOpacity, alpha);
}
