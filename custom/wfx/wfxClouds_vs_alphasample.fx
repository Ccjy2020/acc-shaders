#define USE_ALT_CBUFFER
#define USE_PS_FOG
#define CLOUDS
#include "wfx_common.hlsl"

PS_IN_CloudFX main(uint id: SV_VertexID) {
  PS_IN_CloudFX vout;
  vout.PosC = getPosW(id, vout.PosH, vout.Tex, 1);
  return vout;
}
