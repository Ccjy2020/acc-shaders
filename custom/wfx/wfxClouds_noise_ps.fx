Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float gSlice;
  float gPerlinFrequency;
  uint gPerlinOctaves;
  float gWorleyFrequency;
  float gShapeMult;
  float gShapeExp;
  float gShape0Mip;
  float gShape0Contribution;
  float gShape1Mip;
  float gShape1Contribution;
  float gShape2Mip;
  float gShape2Contribution;
}

#include "include/perlinworley.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  float perlin = abs(lerp(1, perlinFBM(float3(pin.Tex, gSlice), gPerlinFrequency, gPerlinOctaves), 0.5) * 2 - 1);
  float4 col = 0;
  col.g = worleyFBM(float3(pin.Tex + 100, gSlice + 100), gWorleyFrequency);
  col.b = worleyFBM(float3(pin.Tex + 100, gSlice + 100), gWorleyFrequency * 2);
  col.a = worleyFBM(float3(pin.Tex + 100, gSlice + 100), gWorleyFrequency * 4);
  col.r = remap(perlin, 0, 1, col.g, 1);
  return col;
}
