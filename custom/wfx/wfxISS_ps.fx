#define PS
#include "wfx_common.hlsl"

float3 calculateLight(float3 toCloud, float3 normal, float3 lightDir, float3 lightColor){
  float frontlitDot = dot(normal, lightDir);
  float frontlitK = saturate(lerp(1, frontlitDot, 0.6));
  float3 result = frontlitK * 0.5;

  float specularBase = saturate(dot(normalize(lightDir - toCloud), normal));
  result += pow(specularBase, 10.0);

  return result * lightColor;
}

float4 main(PS_IN_Sky pin) : SV_TARGET {
  float2 offset = pin.Tex * 2 - 1;
  float3 toCamera = normalize(pin.PosW);
  float litUp = 1;
  float3 color = colorOpacity.rgb;
  float alpha = saturate(colorOpacity.a * litUp) * pow(saturate((1 - length(offset)) * 1), 4);

  float texShare = saturate(remap(ksFOV, 20, 1, 0, 1));
  float4 txDiffuseValue = 0;

  [branch]
  if (texShare > 0){
    float4 noise1 = txNoise.SampleLevel(samLinear, pin.Tex * 0.15 + ksGameTime * 0.00021, 0);
    float4 noise2 = txNoise.SampleLevel(samLinear, pin.Tex * 0.17 - ksGameTime * 0.00017, 0);
    float4 noise3 = txNoise.SampleLevel(samLinear, pin.Tex * 0.01 - ksGameTime * 0.00003, 0);
    float4 noise = lerp(noise1, noise2, saturate(remap(noise3.x, 0.3, 0.7, 0, 1)));
    txDiffuseValue = txDiffuse.SampleLevel(samLinear, pin.Tex + (noise.xy - 0.5) * 0.03, (noise.z - 0.3) * 3);
    txDiffuseValue.rgb *= lerp(1, 0.8, saturate(noise.z - 0.3));
  }

  color = lerp(color * (1 - texShare), txDiffuseValue.rgb, texShare);
  alpha = lerp(alpha, txDiffuseValue.a, texShare);
  return withFogImpl(color, toCamera * ksFarPlane, toCamera, pin.LightH.x, 1, alpha);
}
