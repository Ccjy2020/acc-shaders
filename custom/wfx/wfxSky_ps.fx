// based on https://www.shadertoy.com/view/Ml2cWG by robobo1221

#define USE_PS_FOG
#define USE_BACKLIT_FOG
#include "../../recreated/include_new/base/_include_ps.fx"
#include "wfx_common_sky.hlsl"

Texture2D txStarMap : register(t19);

float3 sampleStars(float2 uv){
  #ifdef HIGH_QUALITY_STARS
    float2 textureResolution = float2(8192, 4096);
    uv = uv * textureResolution + 0.5;
    float2 i = floor(uv);
    float2 f = frac(uv);
    uv = i + f * f * (3 - 2 * f);
    uv = (uv - 0.5) / textureResolution;

    float2 sizeInv = float2(1./8192, 1./4096) * 1.5;
    float4 base = txStarMap.Sample(samLinear, uv);
    float4 blurred = 0
      + txStarMap.Sample(samLinear, uv + sizeInv * float2(0.5, 0.5), 0)
      + txStarMap.Sample(samLinear, uv + sizeInv * float2(0.5, -0.5), 0)
      + txStarMap.Sample(samLinear, uv + sizeInv * float2(-0.5, 0.5), 0)
      + txStarMap.Sample(samLinear, uv + sizeInv * float2(-0.5, -0.5), 0);
    float4 delta = base - blurred / 4;
    return (base + 0.8 * saturate(dot(float4(delta.xyz, 0.5), 1)) - 0.4).xyz;
  #else
    return txStarMap.Sample(samLinear, uv).xyz;
  #endif
}

float3 getAtmosphericScattering(float3 dir){
  float dirHeight = getHeight(dir);
  float zenithDensity = getZenithDensity(dirHeight);
  float skyEdge = getSkyEdge(dirHeight);
  float3 absorption = getSkyAbsorption(zenithDensity);

  float sunDotV = saturate(dot(dir, cSunDirection));
  float moonDotV = saturate(dot(dir, cMoonDirection));

  float3 sunMie = pow(sunDotV, cSunMieExp) * cacheSunAbsorption;
  float moonMie = pow(moonDotV, cMoonMieExp);

  float sunShape = getSunPoint(sunDotV);
  float moonShapeInv = 1.0 - getMoonPoint(moonDotV);
  
  float3 rayleighMult = 1.0 + getRayleigMultiplier(sunDotV);
  float3 sky = cSkyColor * zenithDensity * rayleighMult;  
  float3 totalSky = lerp(sky * absorption, sky / (sky + 0.5), sunPointDistMult);
  totalSky *= cacheSunAbsorption2;
  applyExtraGradients(dir, totalSky);

  float3 sunGenMult = skyEdge * cSunColor * cacheSunAbsorption2;
  float3 sunShapeTotal = sunShape * cSunBrightness * absorption * moonShapeInv * sunGenMult;
  totalSky += sunShapeTotal + dot(sunShapeTotal, 0.03);
  totalSky += sunMie * sunGenMult;
  totalSky += moonMie * skyEdge * cMoonMieColor;
  totalSky = max(totalSky, 0);

  [branch]
  if (cStarsBrightness > 0.001){
    float3 dirRotated = mul(dir, (float3x3)cStarMapRotation);
    float u = atan2(-dirRotated.x, dirRotated.z) / (2 * M_PI) + 0.5;
    float v = 0.5 - asin(dirRotated.y) / (M_PI / 2) * 0.5;
    float time = ksGameTime * 0.0004;
    float3 txStarsValue = sampleStars(float2(u, v)) * (1 - sunShape) * moonShapeInv * absorption * cStarsColor;
    float3 starsBlend = lerp(luminance(txStarsValue), txStarsValue, cStarsSaturation);
    return totalSky * cBrightnessMult + pow(abs(starsBlend), cStarsExponent) * cStarsBrightness;
  } else {
    return totalSky * cBrightnessMult;
  }
}

float4 main(PS_IN_Sky pin) : SV_TARGET {
  float3 toCamera = normalize(pin.PosW);
  float3 color = getAtmosphericScattering(toCamera);
  float fogMult = cFogMult + cFogMult2 * pow(saturate(cFogRange - toCamera.y * cFogRange), cFogMultExp);
  return withFogImpl(color, pin.PosW, toCamera, pin.Tex.x, fogMult, 1);
}
