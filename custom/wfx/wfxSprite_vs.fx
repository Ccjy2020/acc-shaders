#define VS
#include "wfx_common.hlsl"

PS_IN_Sky main(uint id: SV_VertexID) {
  PS_IN_Sky vout;
  vout.PosW = getPosW(id, vout.PosH, vout.Tex);
  vout.NormalL = 0;
  vout.LightH = 0;
  vout.LightH.x = calculateFogNew(vout.PosW);
  return vout;
}
