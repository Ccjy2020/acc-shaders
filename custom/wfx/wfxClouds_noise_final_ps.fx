#include "include/samplers.hlsl"
#include "include/perlinworley.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 base = 0;
  float4 avg = 0;
  float weight = 0;
  int R = 2;
  [unroll] for (int x = -R; x <= R; x++)
  [unroll] for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R * 2) continue;
    float4 val = txDiffuse.SampleLevel(samLinear, pin.Tex, 0, int2(x, y));
    if (x == 0 && y == 0) base = val;
    avg += val;
    weight += 1;
  }
  avg /= weight;
  return float4(avg.xyz, base.w);
}
