#include "flgGrassPiece.hlsl"

Texture2D<float2> txDiffuse : register(t0);

float2 main(VS_Move pin) : SV_TARGET {
  float2 v = txDiffuse.Sample(samLinearSimple, pin.Tex);
  v.y = 0;
  if (v.x < 0.96) {
    v.x = saturate(v.x - pin.ReduceBy);
  }

  // float drag = 1.0 / (1.0 + gFrameTime * 4);
  // v.zw = lerp(0.5, v.zw, drag);
  return v;
}