#include "include_new/base/cbuffers_vs.fx"
#include "flgGrassPiece.hlsl"

VS_Copy main(VS_IN vin) {
  VS_Copy vout;

  float4 posW = mul(float4(vin.PosL, 1), gTransform);
  float4 posH = mul(posW, gViewProj);

  vout.PosH = posH;
  vout.Tex = 0;

  float distance = posW.y - getY(posW.xz);
  vout.Opacity = (1 - saturate(distance * 3 - 0.1)) * 0.96;
  return vout;
}
