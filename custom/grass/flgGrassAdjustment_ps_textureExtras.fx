#include "flgGrassAdjustment.hlsl"

Texture2D txDiffuse : register(t0);

float4 main(PS_IN_Solid pin) : SV_TARGET {
  return applyExtras(applyTexture(txDiffuse.Sample(samLinearSimple, pin.Tex)) * gColor, pin.PosW);
}