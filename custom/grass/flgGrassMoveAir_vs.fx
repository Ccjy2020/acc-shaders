#include "include_new/base/cbuffers_vs.fx"
#include "flgGrassPiece.hlsl"

VS_Move main(uint id: SV_VertexID) {
  VS_Move vout;
  float2 ver = BILLBOARD[id];
  float4 posW = float4(ver.xy, 0, 1);
  vout.PosH = mul(posW, gViewProj);
  // vout.PosH = posW;
  vout.Tex = ver * 0.5 + 0.5;
  vout.Tex.y = 1 - vout.Tex.y;
  vout.ReduceBy = gFrameTime * 2;
  return vout;
}
