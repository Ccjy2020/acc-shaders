#include "flgGrass.hlsl"
#include "flgGrass_generation.hlsl"

#define DEFAULT_THRESHOLD_PARAMS float4(0.5, 0.05, 0.02, 0.35)
// #define DEBUG_COLOR_TEST
// #define DEBUG_FADEK
// #define FORGE_SIZE_MULT 1

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  float seed = 0.12345;

  float2 basePosXZ = gSpawnPointA + float2(float(threadID.x), float(threadID.y)) * gMapStep;
  RAND_INIT(basePosXZ + float2(gPassID * 37, gMapStep * 94));

  float maxDistance = (gSpawnPointB.x - gSpawnPointA.x) / 2;

  // SHUFFLE A BIT
  float2 posXZ = basePosXZ;
  posXZ += (float2(RAND, RAND) - 0.5) * gMapStep * 40;

  // UV & FACTORS FOR USING GATHER
  SamplingParams S = getSamplingParams(posXZ);
  if (S.isOutside) return;

  // POSITION
  float3 normal;
  float ao;
  float3 pos = float3(posXZ.x, calculateDepth(S, false, normal, ao), posXZ.y);
  if (abs(normal.y) < 0.4 || !isVisible(pos + float3(0, 0.3, 0))) return;
  
  // PARAMS
  GrassParams GP = getGrassParams(S);
  
  // COLOR
  float4 color = txColor.SampleLevel(samLinearClamp, S.mapUV, 0);
  float colorTest = colorThreshold(color.rgb, GP.mask_mainThreshold, GP.mask_redThreshold, GP.mask_minLuminance, GP.mask_maxLuminance);
  if (color.a < 0.7 || colorTest < 0.2 + RAND * 0.3) return;

  float blurryMap = txColor.SampleLevel(samLinearClamp, S.mapUV, 2).a;
  float edgeK = pow(blurryMap, 2);
  float singleness = sqrt(1 - blurryMap);
  colorTest *= edgeK;

  // LOW FREQ NOISE
  float4 lfreq = saturate(txNoise.SampleLevel(samLinearWrap, pos.xz / 40, 0));
  const float RAND_LF0 = lfreq.x;
  const float RAND_LF1 = lfreq.y;
  const float RAND_LF2 = lfreq.z;
  const float RAND_LF3 = lfreq.w;

  // RELATION TO CAMERA
	float3 posC = pos - gCameraPos.xyz;
  float distanceReal = length(posC);
	float3 toCamera = posC / distanceReal;

  // LOD DISAPPEARANCE
  float distanceHor = lerp(distanceReal, length(posC.xz), saturate(gPassID));
  float fadeK = saturate(1.6 - distanceHor / (maxDistance * 2) * 2.0);
  float lodFix = saturate(2 - distanceHor / (maxDistance * 2) * 2.4);
  if (fadeK <= 0.03) return;
 
  // DEFORMATION
  float deformation = saturate(getDeformation(pos.xz)).x;

  // ORIENTATION
	float3 up = normalize(-normal + float3(0, -2, 0));
  float2 rotation = normalize(float2(RAND - 0.5, RAND - 0.5));

  // CUT GRASS
  [branch]
  if (gDebugPasses) {
    float colorID = gFarPass ? 5 - gPassID : gPassID;
    if (colorID == 0) color.rgb = float3(1, 0, 0);
    else if (colorID == 1) color.rgb = float3(1, 1, 1);
    else if (colorID == 2) color.rgb = float3(0, 1, 0);
    else if (colorID == 3) color.rgb = float3(0, 0, 1);
    else if (colorID == 4) color.rgb = float3(0, 1, 1);
    else if (colorID == 5) color.rgb = float3(0.6, 0, 0.6);
    color.rgb /= 4;
  }

  #ifdef DEBUG_FADEK
    color.rgb = float3(fadeK, 1 - fadeK, 0) * 0.25;
  #endif

  // MATERIAL PARAMS
  float4 materialParams = txMaterialParams.SampleLevel(samLinearClamp, S.mapUV, 0);
  float diffuseK = materialParams.x / max(materialParams.y, 0.01);
  float specFade = saturate(1.2 - distanceReal / 40);
  float specMix = specFade * saturate(1 - materialParams.z / 0.04);
  specMix = 0;
  float specEXP = lerp(max(255 * materialParams.w, 1), 3, specMix);
  float specC = lerp(materialParams.z, 0.04, specMix);
  float specK = calculateSpecular(normal, -toCamera, specEXP) * specC;

  // CAR’S WIND
  float3 air = getAir(pos.xz);
  air -= sign(air) * min(pow(RAND, 2) * 0.05, abs(air));
  air = sign(air) * pow(air, 2);

  // SIZE
  #ifdef DEBUG_COLOR_TEST
    color.rgb = float3(colorTest, 1 - colorTest, 0) / 4;
  #endif

  float finHeight = 0.2 * lerp(0.5 + 0.5 * RAND, 0.75, GP.shape_tidy);
  float finWidth = finHeight * GP.shape_width;
  float tiltMult = lerp(0.5, 0.1, GP.shape_tidy);
	// finHeight *= 1 + distanceReal / 120;
	// finHeight *= 0.5 + 0.5 * saturate(RAND_LF0 + RAND_LF1);

  int texGroupID = gPassID <= 2 ? -1 : getGroup(GP.groupChances, RAND);
  int texPieceID = -1;
  if (texGroupID != -1){
    uint count = gTexGroupsCount[texGroupID];
    float randomValue = RAND * gTexGroupsChanceTotal[texGroupID];
    int texPieceIDInGroup = 0;
    for (uint i = 0; i < count; i++){
      if ((randomValue -= gTexGroups[texGroupID].pieces[i].chance) <= 0){
        texPieceIDInGroup = i;
        break;
      }
    }
    CustomTexCSPiece TP = gTexGroups[texGroupID].pieces[texPieceIDInGroup];
    if (TP.sizeMult.x != 0){
      texPieceID = texGroupID * 8 + texPieceIDInGroup;
      finWidth = finHeight * TP.sizeMult.x;
      finHeight *= TP.sizeMult.y;
      tiltMult = 0;
      lodFix = 1;
    }
  }

  // int texGroupID = -1;
  // int texPieceID = -1;

  up = normalize(up - air * 6 + normalize(float3(RAND - 0.5, 0, RAND - 0.5)) * (tiltMult + pow(deformation, 1 + RAND) * 5));
  color.rgb *= 1 - pow(deformation, 4) * 0.2;

  float sizeMult = GP.shape_size;
  #ifdef FORGE_SIZE_MULT
    sizeMult = FORGE_SIZE_MULT;
  #endif
  if (RAND > (gPassID < 2 ? 1 : 2) / sizeMult){
    return;
  }
  if (gPassID == 0) sizeMult = sizeMult * 0.5;
  if (gPassID == 1) sizeMult = sizeMult * 0.75;
  // if (gPassID <= 1) sizeMult = min(sizeMult, gPassID == 0 ? 2 : (sizeMult + 1) / 2);
  sizeMult *= colorTest;
  finHeight *= sizeMult;
  finWidth *= sqrt(sizeMult);

  float repeatBase = (gPassID == 0 && sizeMult < 0.5 ? 0.2 : 0.1) * (1 + RAND);
  float repeatK = round(clamp(edgeK * repeatBase / finWidth, 1, 4));
  if (gPassID >= 4) repeatK = 2;
  // repeatK = 1;
 
  float finalSizeMult = (1 + pow(deformation, 4)) * lodFix;
  finHeight *= finalSizeMult;
  finWidth *= finalSizeMult;

  // color.rgb = normal * 0.25 + 0.25;
  // color.rgb = float3(0.04, 0.08, 0.02);
  // color.rgb = lerp(color.rgb, float3(0.04, 0.08, 0.02), 0.1);
  // return;

  // PACKING
  FoliageFinVertex fin = (FoliageFinVertex)0;
  fin.position = pos;
  FLG_pack(fin.type_flip, float2(floor(RAND * gTexGrid.x), repeatK));
  FLG_pack(fin.color_fade, float4(color.rgb, fadeK));
  FLG_pack(fin.rotation_width_height, float4(rotation, finWidth, finHeight));
  FLG_pack(fin.normal_ao, float4(normal, ao));
  FLG_pack(fin.up_singleness, float4(up, singleness));
  FLG_pack(fin.shadowmult_passid_dif_spec, float4(lerp(0.4, 0.6, RAND) * GP.shape_cut, texPieceID, 
    diffuseK, specK * (0.5 + saturate(RAND_LF0))));
  buResult.Append(fin);
}