#ifndef TEX_BIAS
  #define TEX_BIAS 2
#endif

#ifndef BLUR_RADIUS
  #define BLUR_RADIUS 1
#endif

#ifndef OFFSET_MULT
  #define OFFSET_MULT 2
#endif

#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 val = 0;
  float4 count = 0.01;
  [unroll] for (int x = -BLUR_RADIUS; x <= BLUR_RADIUS; x++){
    [unroll] for (int y = -BLUR_RADIUS; y <= BLUR_RADIUS; y++){
      if (BLUR_RADIUS > 0 && abs(x) + abs(y) == 2 * BLUR_RADIUS) continue;
      float4 v = txDiffuse.SampleLevel(samLinearBorder0, pin.Tex, TEX_BIAS, int2(x, y) * OFFSET_MULT);
      float weight = 1 / (length(float2(x, y)) + 0.25);
      // float weight = 1;
      val += v * weight;
      count += saturate(dot(v, 10)) * weight;
      // count += weight;
    }
  }
  return val / count;
}