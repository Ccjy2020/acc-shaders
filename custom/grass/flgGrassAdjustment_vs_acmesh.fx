#include "include_new/base/cbuffers_vs.fx"
#include "flgGrassAdjustment.hlsl"

PS_IN_Solid main(VS_IN_ac vin) {
  PS_IN_Solid vout;
  float3 posW = mul(float4(vin.PosL.xyz, 1), gWorld).xyz;
  vout.PosH = mul(float4(posW, 1), gViewProj);
  vout.PosW = posW.xz;
  vout.Tex = vin.Tex;
  return vout;
}
