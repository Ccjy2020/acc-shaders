#include "flgGrass.hlsl"
#include "flgGrass_generation.hlsl"

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  float seed = 0.12345;

  float2 basePosXZ = gSpawnPointA + float2(float(threadID.x), float(threadID.y)) * gMapStep;
  RAND_INIT(basePosXZ + gPassID * 0.1837 + gMapStep * 0.6731);

  float freq = gMapStep;
  float negOffset = 0;
  float lodLevel = 0;

  float distanceBase = length(basePosXZ - gMapOrigin);
  float distanceG = distanceBase / ((gMapPointB.x - gMapPointA.x) / 2);
  float maxDistance = (gSpawnPointB.x - gSpawnPointA.x) / 2;
  float distanceK = distanceBase / (maxDistance * (1 - pow(RAND, 1) * 0.3));
  uint sparseMult = 1;
  float isConstant = gPassID > 3;

  freq *= sparseMult;
  float sizeMult = 1;
  float heightMultBase = 1;
  float heightMultExtra = saturate(1 - (distanceK - 0.85) / 0.15);

  // SHUFFLE A BIT
  float2 posXZ = basePosXZ;
  float2 randOffsetBase = float2(RAND, RAND) - 0.25;
  float2 randOffset = randOffsetBase * (isConstant ? gMapStep : freq);
  posXZ += randOffset;

  // UV & FACTORS FOR USING GATHER
  SamplingParams S = getSamplingParams(posXZ);
  if (S.isOutside) return;

  // POSITION
  float3 normal;
  float ao;
  float depth = calculateDepth(S, false, normal, ao);
  float3 pos = float3(posXZ.x, depth, posXZ.y);
  // if (sizeMult < 0.1 || abs(normal.y) < 0.4 || !isVisible(pos + float3(0, 0.3, 0))) return;
  
  GrassAdjustments A = getAdjustments(S);
  // if (A.heightMult * heightMultBase * heightMultExtra < 0.1) return;

  // COLOR
  float4 color = txColor.SampleLevel(samLinearClamp, S.mapUV, 0);
  if (color.a < 0.7) return;
  sizeMult *= saturate(0.2 + (color.a - 0.7) * 10);

  float colorBlurredMipMap = A.edgeMult + saturate(distanceBase / 120);
  float4 colorBlurred = txColor.SampleLevel(samLinearClamp, S.mapUV, colorBlurredMipMap);
  float singlenessSizeMult = pow(saturate(colorBlurred.a / color.a), 1 + colorBlurredMipMap / 2);
  sizeMult *= 0.2 + 0.8 * singlenessSizeMult;

  // LOW FREQ NOISE
  float4 lfreq = saturate(txNoise.SampleLevel(samLinearWrap, pos.xz / 200, 0));
  const float RAND_LF0 = lfreq.x;
  const float RAND_LF1 = lfreq.y;
  const float RAND_LF2 = lfreq.z;
  const float RAND_LF3 = lfreq.w;

  // TYPE
  float type;
  if (A.typeOffset > 0){
    float randK = pow(RAND_LF0, 1.6 / (1 + A.typeOffset));
    if (!isConstant && RAND_LF1 > A.typeOffset / 10) randK = 0;
    type = floor(7.99 * randK);
  } else {
    float randK = pow(RAND_LF0, 1.6 - A.typeOffset);
    if (!isConstant) randK = 0;
    type = floor(7.99 * randK);
  }

  // RELATION TO CAMERA
	float3 posC = pos - gCameraPos.xyz;
  float distanceReal = length(posC);
	float3 toCamera = posC / distanceReal;
  heightMultBase *= 1 + pow(abs(dot(toCamera, normal)), lerp(0.25, 4, pow(1 - saturate(distanceReal / 60), 4))) * saturate(distanceReal / 4 - 1);
  
  if (singlenessSizeMult > 0.9 && !isConstant){
    float heightMultDyn = heightMultExtra;
    if (RAND > 0.25) heightMultDyn *= saturate(saturate(abs(dot(toCamera, normal)) * 10 + distanceReal / 60 - 1.5));
    else heightMultDyn *= 1.5;
    heightMultExtra = lerp(heightMultExtra, heightMultDyn, saturate(A.heightMult * 2 - 1));
  }
  // if (heightMultBase * heightMultExtra < 0.05) return;
  heightMultExtra += 0.2 * pow(RAND, 40);

  if (isConstant){
    heightMultExtra *= 1 + (0.5 + saturate(type / 3)) * pow(RAND, 4) * 0.5;
  }

  // SIZE
  float heightLowFreq = lerp(0.3, 0.4, RAND) * (0.8 + 0.4 * pow(RAND_LF2, 4)) * heightMultExtra * 0.5;
  float finHeight = heightLowFreq;
  float finFade = saturate((1 - distanceG) * 2);

  float finWidth = lerp(0.3, 0.4, pow(RAND, 4)) 
    * (1 + distanceG)
    * (0.3 + sizeMult * 0.7);
  // finWidth *= 1 + 2 * pow(saturate(gPassID / 4), 1.6) * saturate(distanceG * 2);
  bool finFlip = RAND > 0.5;
  if (!isConstant){
    finHeight = lerp(finHeight, heightLowFreq, saturate(distanceReal / 20 - 1));
  }
  
  finHeight *= A.heightMult;
  finHeight *= 1 + sqrt(saturate(0.99 - normal.y)) * 2;
  finWidth *= max(A.heightMult, 1);
  // finFade *= 0.6 + 0.4 * A.heightMult;

  // DEFORMATION
  float3 deformation = saturate(getDeformation(pos.xz));
  float deformationTilt = deformation.x;
  ao *= 1 - deformation.y * 0.4;
  // color *= 1 - deformation.x * (0.05 + 0.15 * saturate(A.heightMult * 10 - 3)) * saturate(2 - distanceReal / 30);
  // finHeight *= 1 + deformation.x * 0.6 * saturate(gPassID - 1);
  // finWidth *= 1 + deformation.x * 0.2;
  if (deformation.x > 0.99){
    finHeight = lerp(0.04, 0.06, RAND);
    finWidth = lerp(0.2, 0.24, RAND);
    type = 0;
    deformationTilt = 0;
  }

  // ORIENTATION
	float3 up = normalize(-normal + float3(0, -2, 0));
  // up.xz += normalize(-toCamera.xz) * (1 - saturate(distanceReal - 0.5) + abs(toCamera.y) * 0.1) * (1 - deformation.x) * saturate(finHeight * 2);
	// up = normalize(up);

  float2 rotation = normalize(float2(RAND - 0.5, RAND - 0.5));  
  
  float3 side = cross(float3(rotation.x, 0, rotation.y), up);
  up = lerp(up, cross(side, up), deformationTilt * 0.8 + 0.2);
	up = normalize(up);

  // SINGLENESS FOR AO
  float singleness = estimateSingleness(S, toCamera, distanceReal);
  // float singlenessO = singleness;
  singleness = lerp(singleness, 1, saturate(deformation.z - deformation.y));
  // ao *= lerp(saturate(heightMult + 1 - A.heightMult), 1, singleness * singleness);

  // CUT GRASS
  singleness = lerp(0.8 + 0.2 * singleness, singleness, saturate(A.heightMult * 3 - 0.5));
  if (finHeight < 0.17){
    type = 0;
  }

  float colorID = gFarPass ? 5 - gPassID : gPassID;
  if (colorID == 0) color.rgb = float3(1, 0, 0);
  else if (colorID == 1) color.rgb = float3(1, 1, 1);
  else if (colorID == 2) color.rgb = float3(0, 1, 0);
  else if (colorID == 3) color.rgb = float3(0, 0, 1);
  else if (colorID == 4) color.rgb = float3(0, 1, 1);
  else if (colorID == 5) color.rgb = float3(0.6, 0, 0.6);
  color.rgb /= 2;

  // MATERIAL PARAMS
  float4 materialParams = txMaterialParams.SampleLevel(samLinearClamp, S.mapUV, 0);
  float diffuseK = materialParams.x / max(materialParams.y, 0.01);
  float specFade = saturate(1.2 - distanceReal / 40);
  float specMix = specFade * saturate(1 - materialParams.z / 0.04);
  float specEXP = lerp(max(255 * materialParams.w, 1), 3, specMix);
  float specC = lerp(materialParams.z, 0.04, specMix);
  float specK = calculateSpecular(normal, -toCamera, specEXP) * specC;

  float bent = pow(RAND, 4);
  float lodFix = saturate(1.6 - distanceK * 1.6) * saturate(1.6 - distanceReal / maxDistance * 0.67);
  lodFix = 1;
  // color.rgb = float3(lodFix, 1 - lodFix, 0) / 4;
  finHeight = 0.12 * (0.5 + 0.5 * RAND) * lodFix;
  finHeight *= 1 - bent * (0.5 - saturate(gPassID + gFarPass) * 0.15);
	finHeight *= 1 * (1 + distanceReal / 80);
  // finHeight *= finFade;
  finWidth = finHeight * (0.05 + RAND * 0.03); // * (1 + gPassID * saturate(0.2 + distanceReal / 60));
  if (finHeight < 0.04) return;

  if (gFarPass){
    finHeight *= 1.6 * saturate(0.4 + distanceReal / 30);
    finWidth *= 7 * saturate(0.2 + distanceReal / 30);
  } else {
    // finHeight *= 1 + saturate(distanceReal / 10 - 1) * 2;
    finHeight *= saturate(0.7 + gPassID);
    finWidth *= 1 + saturate(distanceReal / 15 - 0.2) * 4;
  }

  // if (gPassID > 2) finWidth = finHeight * 0.08;
  // if (gPassID == 2) finWidth = finHeight * 0.06;
  // if (gPassID == 1) finWidth = finHeight * 0.05;
  // if (gPassID == 0) finWidth = finHeight * 0.04;

  // PACKING
  FoliageFinVertex fin = (FoliageFinVertex)0;
  fin.position = pos;
  // FLG_pack(fin.type_flip, float2(type, finFlip ? 1 : 0));
  FLG_pack(fin.type_flip, float2((RAND * 2 - 1) * (2 + 4 * bent), 0.5 + RAND + pow(RAND, 4) + bent * 2));
  FLG_pack(fin.color_fade, float4(color.rgb, lodFix));
  FLG_pack(fin.rotation_width_height, float4(rotation, finWidth, finHeight));
  FLG_pack(fin.normal_ao, float4(normal, ao));
  FLG_pack(fin.up_singleness, float4(up, singleness));
  FLG_pack(fin.shadowmult_passid_dif_spec, float4(1 - deformation.y * 0.8, gPassID + gFarPass * 3, diffuseK, specK));
  buResult.Append(fin);
}