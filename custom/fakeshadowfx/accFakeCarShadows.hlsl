cbuffer cbExtFakeShadows : register(b6) {
  float transparency;
  float blurBase;
  float2 blurExtra;

  float shadowFactor;
  uint useGammaAdjustment;
  uint wheelMode;
  float carVaoValue;
  
  float concentration;
  float posVMult;
  float neonFix;
  float gDynamicLightsFactor;

  float4 shadowPlane;
  float4x4 shadowMatrix;
  float4x4 viewProjInv;
};
