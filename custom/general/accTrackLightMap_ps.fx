// #define MIPMAPPED_GBUFFER 4
#include "accSS.hlsl"

#define RADIUS 1
#define VERT_MULTIPLIER 10
#define SAMPLE_THRESHOLD 10.2

Texture2D<float> txSurfaceDepth : register(t3);

struct SamplingParams {
  float2 mapUV;
  float4 bilinearFactors;
};

SamplingParams getSamplingParams(float2 posXZ){
  SamplingParams ret;
  float2 mapRel = (posXZ - gMapPointB) / (gMapPointA - gMapPointB);
  ret.mapUV = mapRel;
  ret.bilinearFactors = bilinearFactors(ret.mapUV * gMapResolutionInv - 0.5);
  return ret;
}

float4 sampleTrackMap(float2 uv, float closeness){
  // return txColor.SampleLevel(samLinearBorder0, uv, 2);
  float dist = 1 - closeness;
  dist = dist / (1 + dist);
  return txColor.SampleLevel(samLinearBorder0, uv, clamp(0 + 15 * dist, 0, 3.7));

  float total = 0;
  float4 sum = 0;
  [unroll]
  for (int x = -1; x <= 1; x++)
  [unroll]
  for (int y = -1; y <= 1; y++){
    if (abs(x) + abs(y) == 2) continue;
    // if (abs(x) + abs(y) != 0) continue;
    // sum += txColor.SampleLevel(samLinearBorder0, uv, 3.5 - 3 * closeness, int2(x, y));
    sum += txColor.SampleLevel(samLinearBorder0, uv, 8 - 7.5 * closeness, int2(x, y));
    total++;
  }
  return sum / total;
}

#define DEPTH_FN min

float calculateDepth(SamplingParams S){
  float depthRaw = txSurfaceDepth.SampleLevel(samLinearClamp, S.mapUV, 0);
  return gMapOriginY - depthRaw * gMapDepthSize;
}

float calculateDepth(float2 uv){
  float depthRaw = txSurfaceDepth.SampleLevel(samLinearClamp, uv, 0);
  return gMapOriginY - depthRaw * gMapDepthSize;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float depth = ssGetDepth(pin.Tex);
  float3 origin = ssGetPos(pin.Tex, depth);
  float3 normal = ssGetNormal(pin.Tex);

  float2 offset = normal.xz == 0 ? 0 : normalize(normal.xz);
  SamplingParams S = getSamplingParams(origin.xz + offset * 0.5);
  float lightMapDepth = calculateDepth(S);

  float toGround = origin.y - lightMapDepth;
  float closeness = saturate(1 - toGround / 30);
  float2 mapUV = getSamplingParams(origin.xz + offset * (3 - 3 * closeness)).mapUV;
  float4 lightMap = sampleTrackMap(mapUV, closeness);
  float edgeK = min(min(mapUV.x, mapUV.y), min(1 - mapUV.x, 1 - mapUV.y));
  // lightMap.xyz = pow(saturate(lightMap.xyz), 0.9);

  return lightMap 
    * gTrackLightIntensity
    * saturate(-normal.y * 0.5 + 0.5) 
    // * closeness
    * saturate(edgeK * 4 + saturate(2 - dot2(origin.xz) / 10000));
}