struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

SamplerState samLinear : register(s0) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txColor : register(t0);
Texture2D txSSAO : register(t14);

float4 main(VS_Copy pin) : SV_TARGET {
  // return txColor.SampleLevel(samLinear, pin.Tex, 0);
  return txColor.SampleLevel(samLinear, pin.Tex, 0) 
    * (1 - txSSAO.SampleLevel(samLinearSimple, pin.Tex, 0).x);
}