#include "accSS.hlsl"

Texture2D<float> txHeatingMask : register(t0);

cbuffer cbDataExt : register(b11){
  float4x4 gTransform;
}

// float2 sampleShadow(float2 uv, float comparison){
//   float2 sum = 0;
//   float sampleScale = 0.002;
//   if (comparison >= 1) return 1; 

//   #define RAIN_DISK_SIZE 6
//   for (uint i = 0; i < RAIN_DISK_SIZE; ++i) {
//     float2 sampleUV = uv.xy + SAMPLE_POISSON(RAIN_DISK_SIZE, i) * sampleScale;
//     float comparisonUV = comparison - 0.001 * (1 + SAMPLE_POISSON_LENGTH(RAIN_DISK_SIZE, i));
//     sum.x += txRainShadow.SampleCmpLevelZero(samShadow, float3(sampleUV, 0), comparisonUV);
//     sum.y += txRainShadow.SampleCmpLevelZero(samShadow, float3(sampleUV, 1), comparisonUV);
//   }
//   return saturate(float2(sum.x, sum.y) / RAIN_DISK_SIZE);
// }

float sampleMask(float3 posW){
  float4 posH = mul(float4(posW, 1), gTransform);
  // return any(abs(posH.xy - 0.5) > 0.5) ? 1 : (txHeatingMask.SampleLevel(samLinearSimple, posH.xy, 0) == 1);
  return any(abs(posH.xy - 0.5) > 0.5) ? 1 : txHeatingMask.SampleCmpLevelZero(samShadow, posH.xy, posH.z);
}

float main(VS_Copy pin) : SV_TARGET {
  float3 origin = ssGetPos(pin.Tex, ssGetDepth(pin.Tex));
  float3 dir = normalize(origin);
  float4 randomValue = txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0);
  float3 posBase0 = ksCameraPosition.xyz - float3(0, lerp(1, 8, randomValue.x), 0);
  float3 posBase1 = ksCameraPosition.xyz - float3(0, lerp(1, 8, randomValue.y), 0);

  float ret = 0;
  for (int i = 0; i < 10; ++i){
    ret += sampleMask(posBase0 + dir * 200 * (0.5 + (1 + i + randomValue.z) / 10.));
    ret += sampleMask(posBase1 + dir * 200 * (0.5 + (1 + i + randomValue.z) / 10.));
  }

  // return sampleMask(ksCameraPosition.xyz);

  // return frac(ksCameraPosition.xyz + dir * 200).y;

  return ret / 20;
}