#include "accSSLR_flags.hlsl"
#include "include/common.hlsl"
#include "accSS_cbuffer.hlsl"
#include "include_new/ext_lightingfx/utils_ps.fx"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

Texture2D txColor : register(t0);
Texture2D txNormals : register(t1);
Texture2D<float> txDepth : register(t2);
Texture2D<float> txLinearDepth : register(t3);
Texture2D<float> txLinearDepth2 : register(t4);
Texture2D txReflections1 : register(t5);
Texture2D txReflections2 : register(t21);
Texture2D txSSLR : register(t14);
Texture2D txSSLRlr : register(t15);
Texture2D<float> txSSLRdim : register(t16);

#define SECTOR_LIGHTS 32

struct sLight {
  float3 pos;
  float rangeX;
  float3 dirW;
  float spotCosS0;
  float3 upW;
  float trim;
  float3 color;
  float rangeY;
};

cbuffer cbExtLighting : register(b6) {
  uint gLightsCount;
  float3 gPad0;
  sLight gLights[SECTOR_LIGHTS];
}

float guessSpecularExp(float blur){
  return lerp(400, 40, blur);
}

float guessSpecularStrength(float blur, float3 normalW){
  return clamp(lerp(10, -1, blur), 0, 4) * saturate(normalW.y * 2 - 1);
}

float3 calculateLight(sLight L, float3 posW, float3 normalW, float specularExp){
  float4 toLight = normalizeWithDistance(L.pos - posW);
  float attenuation = saturate(1 - toLight.w * L.rangeY);
  if (attenuation == 0) return 0;

  float LdotN = dot(toLight.xyz, normalW);
  float spotK = getSpotCone(L.dirW, toLight.xyz, L.spotCosS0);
  float edge = getEdge(L.upW, toLight.xyz, L.trim).x;
  float shadow = edge * spotK * LdotN * attenuation;
  return L.color * shadow * getSpecularComponent(normalW, posW, toLight.xyz, specularExp);
}

float ssGetDepth(float2 uv){
  return txDepth.SampleLevel(samLinearClamp, uv, 0).x;
  return txDepth.SampleLevel(samLinearBorder1, uv, 0).x;
}

float3 ssGetPos(float2 uv, float depth){
  float4 p = mul(float4(uv.xy, depth, 1), viewProjInv);
  return p.xyz / p.w;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float3 posW = ssGetPos(pin.Tex, ssGetDepth(pin.Tex));
  // return float4(frac(posW), 1);

  float4 vColor = txColor.SampleLevel(samPointClamp, pin.Tex, 0);
  float4 vReflections1 = txReflections1.SampleLevel(samPointClamp, pin.Tex, 0);
  float4 vReflections2 = txReflections2.SampleLevel(samPointClamp, pin.Tex, 0);

  // Silly way of storing SSLR_FORCE flag
  vReflections1.w = abs(vReflections1.w);

  // return vReflections1.a * 4;
  // return vReflections2.a * 4;
  // return txNormals.SampleLevel(samPointClamp, pin.Tex, 0);

  #ifndef SSLR_TRACE_EVERYTHING
    [branch]
    if (vReflections1.w < 0.001) {
      return vColor;
    }
  #endif

  float4 vNormal = txNormals.SampleLevel(samPointClamp, pin.Tex, 0);
  float3 normalW = normalize(ssNormalDecode(vNormal.xyz));

  float4 vSSLR = txSSLR.SampleLevel(samLinearSimple, pin.Tex, 0);
  float4 vSSLRlr = txSSLRlr.SampleLevel(samPointClamp, pin.Tex, 0);
  float4 vSSLRlr2 = txSSLR.SampleLevel(samLinearSimple, pin.Tex, 3.5);

  // float distanceV = saturate(max(vSSLR.z*0, vSSLRlr2.z));
  float distanceV = saturate(lerp(vSSLR.z, vSSLRlr2.z, vSSLRlr2.w));
  // float distanceV = saturate(vSSLRlr2.z);
  // return float4(distanceV, 1 - distanceV, 0, 1);

  #ifdef SSLR_SHOW_TRACED
    return txSSLRlr.SampleLevel(samPointClamp, pin.Tex, 0);
  #endif
  
  float blurAmount = vReflections2.a * 6 * lerp(lerp(0.03, 0.6, vReflections2.a * vReflections2.a), 1, distanceV);
  float blurK = saturate(blurAmount);
  float4 resultBase = txSSLRlr.SampleLevel(samLinearSimple, pin.Tex, blurAmount * 0.6);

  float reflPower = saturate(vReflections1.a);
  float dimReflection = 1 - txSSLRdim.SampleLevel(samLinearSimple, pin.Tex, blurAmount + 1);
  dimReflection = lerp(dimReflection, 1, saturate(blurAmount * 0.8 - 1) * 0.4);
  resultBase.w = sqrt(saturate(resultBase.w));

  float resultQuality = lerp(saturate((resultBase.w - 0.5) * 4), resultBase.w, blurK);
  float3 reflectionsColorMult = vReflections2.xyz;
  float3 underlyingSurface = max(vColor.rgb - vReflections1.rgb * resultQuality, 0);

  // return float4(vReflections1.rgb, 1);
  // if (any(vColor.rgb - vReflections1.rgb < -vColor.rgb*0.05)) return float4(3, 0, 3, 1);
  // return float4(vColor.rgb - vReflections1.rgb, 1);

  // resultColor = min(resultColor, lerp(1, 100, dimReflection));
  // float3 resultColor = lerp(resultBase.xyz, vSSLRlr.xyz, (1 - blurK) * saturate(vSSLRlr.w * 10));
  float3 resultColor = resultBase.xyz;
  float3 dimColor = min(0.3, luminance(resultColor));
  // resultColor = lerp(dimColor, resultColor, dimReflection);
  // resultColor *= lerp(0.2, 1, dimReflection);
  resultColor = lerp(resultColor / (1 + resultColor) * 0.2, resultColor, dimReflection);

  float3 extraComponent = 0;
  #ifdef WITH_SPECULAR_LIGHTING
    float specularC = guessSpecularStrength(vReflections2.a, normalW);

    [branch]
    if (specularC > 0){
      float specularExp = guessSpecularExp(vReflections2.a);
      for (uint i = 0; i < gLightsCount; i++){
        extraComponent += calculateLight(gLights[i], posW, normalW, specularExp);
      }
      extraComponent *= specularC;
    }
  #endif

  // return float4(vNormal.xyz * 0.5 + 0.5, 1);
  // return float4((float3)saturate(vNormal.y), 1);
  // return abs(vNormal - float4(0, 1, 0, 1)) * 10;
  // return float4(lerp(float3(1, 1, 1), resultColor * reflectionsColorMult, resultQuality), 1);
  // return float4(lerp(float3(0, 0, 0), pow(resultColor/4, 4), resultQuality), 1);
  // return float4(reflectionsColorMult, 1);
  return float4(underlyingSurface + (resultColor * resultQuality + extraComponent) * reflectionsColorMult * reflPower, 1);
  return float4(vColor.rgb - vReflections1.rgb + resultColor * reflPower * resultQuality 
    + float3(10, 10, 10) * reflPower * saturate(1 - resultQuality), 1);
}