#include "accSS_cbuffer.hlsl"

Texture2D txInput : register(t0);

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerState samPoint : register(s2) {
  Filter = POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  #ifdef BASIC_VERSION
    float kernel[9] = { 
      0.0, -1.0, 0.0,
      -1.0, 5.0, -1.0, 
      0.0, -1.0, 0.0
    };
    int2 offset[9] = { 
      int2(-1.0, -1.0), int2( 0.0, -1.0), int2( 1.0, -1.0),
      int2(-1.0,  0.0), int2( 0.0,  0.0), int2( 1.0,  0.0),
      int2(-1.0,  1.0), int2( 0.0,  1.0), int2( 1.0,  1.0)
    };
    float4 result = 0;
    for (int i = 0; i < 9; i++){
      result += txInput.Load(int3(pin.PosH.xy, 0), offset[i]) * kernel[i];
    }
    return result;
  #else
    float4 base = txInput.Load(int3(pin.PosH.xy, 0));
    float4 blurred = 0
      + txInput.SampleLevel(samLinearSimple, pin.Tex + gSize.zw * float2(0.5, 0.5), 0)
      + txInput.SampleLevel(samLinearSimple, pin.Tex + gSize.zw * float2(0.5, -0.5), 0)
      + txInput.SampleLevel(samLinearSimple, pin.Tex + gSize.zw * float2(-0.5, 0.5), 0)
      + txInput.SampleLevel(samLinearSimple, pin.Tex + gSize.zw * float2(-0.5, -0.5), 0);
    float4 delta = base - blurred / 4;
    return base + 0.36 * saturate(dot(float4(delta.xyz, 0.5), 1)) - 0.18;
  #endif
}