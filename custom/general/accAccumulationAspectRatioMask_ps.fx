#include "include/common.hlsl"

// Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  float gAspectRatio;
  float gAspectRatioInv;
  float gSamplesCount;
  float gProgress;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#define BAR_START float2(0.4, 0.95)
#define BAR_END float2(0.6, 0.955)

float2 mapBar(float2 uv){
  return remap(uv, BAR_START, BAR_END, 0, 1);
}

// float4 sampleColor(float2 uv){
//   return txDiffuse.SampleLevel(samPoint, uv, 0) / gProgress * gSamplesCount;
// }

float4 calculateBackground(float2 uv){
  if (gAspectRatio){
    float over = gAspectRatio < 1 ? abs(uv.x * 2 - 1) - gAspectRatio : abs(uv.y * 2 - 1) - gAspectRatioInv;
    if (over > 0) return 0.068 * lerp(0.61, 1, saturate(over * 20));
  }

  clip(-1);
  return 0;
}

float4 main(VS_Copy pin) : SV_TARGET {
  return calculateBackground(pin.Tex);
}