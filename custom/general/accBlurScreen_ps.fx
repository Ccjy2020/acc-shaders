#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D txNoise : register(t20);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float gStep;
  float3 gPad0;
}

float3 samplePoint(float2 uv){
  if (gStep == 4){
    uv = (uv * 2 - 1) * float2(0.95, 0.85) * 0.5 + 0.5;
  }
  return txDiffuse.SampleLevel(samLinearClamp, uv, 0).xyz;

  // float2 s = float2(16, 9);
  // float2 d = saturate(abs(uv * 2 - 1) * s - (s - 1));
  // return txDiffuse.SampleLevel(samLinearClamp, uv, 0).xyz * lerp(1, 0.85, saturate(length(d)) * gStep);
}

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  float4 currentShot = 0;

  float radius = 0.004;
  if (gStep == 2) radius = 0.008;
  else if (gStep == 3) radius = 0.024;
  else if (gStep == 4) radius = 0.064;

  // float2 random = normalize(txNoise.SampleLevel(samLinear, pin.PosH.xy / 256, 0).xy);
  float2 random = normalize(float2(sin(gStep), sin(gStep * 17.396)));

  #define DISK_SIZE 25
  for (uint i = 0; i < DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(DISK_SIZE, i);
    offset = reflect(offset, random);
    currentShot += float4(samplePoint(pin.Tex + offset * radius * float2(1, 16./9)), 1);
  }

  return currentShot / DISK_SIZE;
}

// #define R 2
// float4 main(VS_Copy pin) : SV_TARGET {
//   float4 currentShot = 0;
//   [unroll]
//   for (int x = -R; x <= R; x++)
//   [unroll]
//   for (int y = -R; y <= R; y++){
//     if (abs(x) + abs(y) == R + R) continue;
//     currentShot += float4(txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x, y)).xyz, 1);
//   }
//   currentShot /= currentShot.w;
//   return currentShot;
// }