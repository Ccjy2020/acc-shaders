#include "accIBL.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
	float3 color = PrefilterEnvMap(getRoughness(), getNormal(pin.Tex));
	// if (mipIndex == 4) color = max(color / (1 + color), 0);
	return float4(color, 1);
}