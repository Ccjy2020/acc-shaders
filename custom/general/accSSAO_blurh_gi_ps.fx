Texture2D txSsgi : register(t13);
#define SAMPLE_BLUR_TEX(UV) float4(BLUR_TEX.SampleLevel(samLinear, UV, 0), txSsgi.SampleLevel(samLinear, UV, 2).xyz)
#include "accSSAO_blurh_ps.fx"
