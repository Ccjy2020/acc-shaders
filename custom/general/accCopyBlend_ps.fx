SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerState samPoint : register(s2) {
  Filter = POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerState samLinearClamp : register(s15) {
  Filter = MIN_MAG_MIP_LINEAR;
  AddressU = CLAMP;
  AddressV = CLAMP;
  AddressW = CLAMP;
};

cbuffer cbData : register(b10) {
  float2 gPointerCoords;
  float2 gPointerClickCoords;
  float2 gGlowRadiusK;
  float gClickIntensity;
  float gModalActive;
  float gBlendWithBg;
  float gFluentDark;
  float2 _pad0;
}

Texture2D txUITop : register(t0);
Texture2D txUIBase : register(t1);
Texture2D txGlow : register(t2);
Texture2D txFluent : register(t3);
Texture2D txScene : register(t4);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 getBlurred(Texture2D tex, SamplerState samplerState, float2 uv, float blurMult){
  float4 b = tex.SampleLevel(samplerState, uv, blurMult);
  if (blurMult <= 0.005){
    return b;
  }

  [unroll]
  for (int y = -2; y <= 2; y++)
  [unroll]
  for (int x = -2; x <= 2; x++){
    if (abs(x) == 2 && abs(y) == 2 || x == 0 && y == 0) continue;
    b += tex.SampleLevel(samplerState, uv, blurMult, int2(x, y));
  }
  b /= 21;
  return b;
}

float3 fluentLight(float2 uv, float2 lightSource, float lightIntensity, float3 fluentColor){
  float2 delta = gPointerCoords - uv;
  delta *= gGlowRadiusK / 50;
  return saturate(1 - length(delta) / 4) * fluentColor * lightIntensity;
  // float distK = 1 / pow(3 + length(delta), 2);
  // return distK * fluentColor * lightIntensity * 20;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // UI color
  float4 uiColor = txUIBase.Sample(samLinearSimple, pin.Tex);
  // float gModalActive = 0;

  [branch]
  if (gModalActive > 0){
    uiColor = lerp(uiColor, 
      getBlurred(txUIBase, samLinearClamp, pin.Tex, gModalActive * 1.5), 
      saturate(gModalActive * 10));
    uiColor = lerp(uiColor, 1, gModalActive * 0.2);
  }

  float4 uiOverColor = txUITop.Sample(samLinearSimple, pin.Tex);

  uiColor.rgb = uiColor.rgb * (1 - uiOverColor.a) + uiOverColor.rgb;
  uiColor.a = 1 - (1 - uiColor.a) * (1 - uiOverColor.a);
  // uiColor.rgb /= 0.5 + 0.5 * max(uiColor.a, 0.001);

  // Preparing blurred scene with original UI alpha
  float blurMult = max(gModalActive, gBlendWithBg * saturate(uiColor.a)) * 5.5;
  float4 sceneColor = lerp(txScene.Sample(samLinearSimple, pin.Tex),
    getBlurred(txScene, samLinearClamp, pin.Tex, blurMult), saturate(uiColor.a / 0.02 + gModalActive));

  // sceneColor = getBlurred(txScene, samLinearClamp, pin.Tex, 3);
  sceneColor.rgb = sceneColor.rgb / (1 + sceneColor.rgb * 0.2);

  // Mixing it a bit of glow
  float4 glowColor = txGlow.Sample(samLinearSimple, pin.Tex);
  uiColor.rgb += 2 * glowColor.rgb * saturate(1 - uiColor.a * uiColor.a * 2 * dot(uiColor, 1));
  uiColor.a += dot(glowColor.rgb, 0.6) * saturate((0.5 - uiColor.a) * 10);

  // Mixing with blurred scene
  if (gBlendWithBg){
    uiColor.rgb = lerp(sceneColor.rgb, uiColor.rgb, uiColor.a);
    // uiColor.rgb = lerp(sceneColor.rgb, uiColor.rgb, 0);
  } else {
    uiColor.a = sqrt(saturate(uiColor.a));
  }

  // Adding a bit of Fluent Design light
  float4 fluentColor = txFluent.Sample(samPoint, pin.Tex) * 0.3;
  // return fluentColor;
  // fluentColor.rgb *= 1 - uiColor.a;
  // uiColor.rgb += fluentLight(pin.Tex, gPointerCoords, 1 - gClickIntensity * 0.5, fluentColor.rgb);
  // uiColor.rgb += fluentLight(pin.Tex, gPointerClickCoords, gClickIntensity, fluentColor.rgb);
  // uiColor.rgb = fluentLight(pin.Tex, gPointerCoords, 1, fluentColor.rgb) * (gFluentDark ? 1 : 1) * 1000;
  uiColor.rgb += fluentLight(pin.Tex, gPointerCoords, 1, fluentColor.rgb) * (gFluentDark ?  -0.5 : 1);
  // uiColor.rgb = pow(max(0, uiColor.rgb) * 1.7, 2.2);

  // Opaque unless very transparent
  if (gBlendWithBg){
    if (uiColor.a > 0.02 - gModalActive) uiColor.a = 1;
    else uiColor.a /= 0.02;
  }

  uiColor.rgb /= max(uiColor.a, 0.001);
  return uiColor;
}