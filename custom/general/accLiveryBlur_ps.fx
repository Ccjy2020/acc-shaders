#include "accSS.hlsl"

Texture2D txPreviousMap : register(t0);

#define R 3
float4 main(VS_Copy pin) : SV_TARGET {
  float4 acc = 0;
  float tot = 0;

  [unroll]
  for (int x = -3; x <= 3; x++)
  [unroll]
  for (int y = -3; y <= 3; y++){
    if (abs(x) + abs(y) >= 5) continue;
    acc += txPreviousMap.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x, y) * 2);
    tot++;
  }

  acc /= tot;

  float sat = max(acc.r, max(acc.g, acc.b)) - min(acc.r, min(acc.g, acc.b)); 
  return float4(lerp(luminance(acc.xyz), acc.xyz, lerp(2, 1.2, sat)), 1);
}