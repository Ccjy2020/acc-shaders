Texture2D txSsgi : register(t13);
#define SAMPLE_BLUR_TEX(UV) float4(1, txSsgi.SampleLevel(samLinear, UV, 0).xyz)
#include "accSSAO_blurh_ps.fx"
