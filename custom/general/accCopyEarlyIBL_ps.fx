#include "accIBL.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  return txFirst.Sample(samLinear, getNormal(pin.Tex));
}