#define NUM_SAMPLES 128
#include "accIBL.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
	return float4(PrefilterEnvMap(getRoughness(), getNormal(pin.Tex)), 1);
}