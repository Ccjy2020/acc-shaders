Texture2DMS<float4> inTex : register(t0); 
RWTexture2D<float4> outTex : register(u0);
 
[numthreads(16, 16, 1)]
void main(uint3 dispatchThreadId : SV_DispatchThreadID) {
  uint2 dim;
  uint sampleCount; 
  inTex.GetDimensions(dim.x, dim.y, sampleCount); 
  if (dispatchThreadId.x > dim.x || dispatchThreadId.y > dim.y) {
    return;
  } 
  float4 resValue = 0;
  for (uint i = 0; i < sampleCount; i++) {
    resValue += inTex.Load(dispatchThreadId.xy, i);
  }
  outTex[dispatchThreadId.xy] = resValue / (float)sampleCount;
} 