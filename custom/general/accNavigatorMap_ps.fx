SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD;
  float3 TangentH : TANGENT;
};

cbuffer cbData : register(b10) {
  float gRenderInBlack;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 v = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
  if (gRenderInBlack){
    return float4(0, 0, 0, pow(saturate(dot(v.rgb, 0.35)), 0.25));
  }
  return txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
}