#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float sampleBlurred(float2 uv, float blur){
  float ret = 0;
  float count = 0;
  for (int y = -3; y <= 3; y++){
    for (int x = -2; x <= 2; x++){
      ret += txDiffuse.SampleLevel(samLinearSimple, uv + float2(x * 0.08 * (0.3 + pow(blur, 2)), y * 0.05), 0.5 + 3 * blur);
      count++;
    }
  }
  return ret / count;
}

float main(VS_Copy pin) : SV_TARGET {
  // return 1 - txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);

  float blurLevel = abs(pin.Tex.y * 2 - 1);
  float shadow = sampleBlurred(pin.Tex, blurLevel);
  shadow = (1 - saturate(shadow * 1.5)) * (1 - blurLevel);

  float2 centerDif = pin.Tex * 2 - 1;
  centerDif.y = sign(centerDif.y) * max(abs(centerDif.y) * 2 - 1, 0);
  float edgeFalloff = saturate(4 - length(centerDif) * 4);
  return min(edgeFalloff, shadow) * 0.7;
}