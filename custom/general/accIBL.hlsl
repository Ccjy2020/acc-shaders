#include "include/common.hlsl"
#include "include/samplers.hlsl"

TextureCube txFirst : register(t0);
Texture2D txNoise : register(t20);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float4x4 transform;
  uint faceIndex;
  uint mipIndex;
  uint pad2;
}

float2 Hammersley(uint i, uint N) {
  float ri = reversebits(i) * 2.3283064365386963e-10f;
  return float2(float(i) / float(N), ri);
}

float3 ImportanceSampleGGX(float2 Xi, float Roughness, float3 N) {
  float a = Roughness * Roughness;

  float Phi = 2 * M_PI * Xi.x;
  float CosTheta = sqrt((1 - Xi.y) / (1 + (a * a - 1) * Xi.y));
  float SinTheta = sqrt(1 - CosTheta * CosTheta);

  float3 H;
  H.x = SinTheta * cos(Phi);
  H.y = SinTheta * sin(Phi);
  H.z = CosTheta;

  float3 UpVector = abs(N.z) < 0.999 ? float3(0, 0, 1) : float3(1, 0, 0);
  float3 TangentX = normalize(cross(UpVector, N));
  float3 TangentY = cross(N, TangentX);

  return TangentX * H.x + TangentY * H.y + N * H.z;
}

float3 GetNormal(uint face, float2 uv) {
	float2 debiased = uv * 2 - 1;
	switch (face) {
		case 0: return float3(1, -debiased.y, -debiased.x);
		case 1: return float3(-1, -debiased.y, debiased.x);
		case 2: return float3(debiased.x, 1, debiased.y);
		case 3: return float3(debiased.x, -1, -debiased.y);
		case 4: return float3(debiased.x, -debiased.y, 1);
	  default: return float3(-debiased.x, -debiased.y, -1);
	};
}

float D_GGX(float a, float NdotH) {
  float d = NdotH * NdotH * (a - 1) + 1;
  return a / (M_PI * d * d);
}

#ifndef NUM_SAMPLES
  #define NUM_SAMPLES 32
#endif

float3 PrefilterEnvMap(float fRoughness, float3 R) {
	float fTotalWeight = 0.0000001f;
	float3 vView = R;
	float3 vNormal = R;
	float3 vPrefilteredColor = 0;

	for (uint i = 0; i < NUM_SAMPLES; i++) {
    float2 vXi = Hammersley(i, NUM_SAMPLES);

    float3 vHalf = ImportanceSampleGGX(vXi, fRoughness, vNormal);
    float3 vLight = 2 * dot(vView, vHalf) * vHalf - vView;

    float fNdotL = saturate(dot(vNormal, vLight));
		if (fNdotL > 0) {
      // Vectors to evaluate pdf
      // float NdotH = saturate(dot(vNormal, vHalf));
      // float fVdotH = saturate(dot(vView, vHalf));

      // // Probability Distribution Function
      // float fPdf = D_GGX(fRoughness, NdotH) * NdotH / (4 * fVdotH);

      // // Solid angle represented by this sample
      // float fOmegaS = 1.0 / (NUM_SAMPLES * fPdf);

      // // Solid angle covered by 1 pixel with 6 faces that are EnvMapSize X EnvMapSize
      // float EnvMapSize = 64;
      // float fOmegaP = 4.0 * M_PI / (6.0 * EnvMapSize * EnvMapSize);
      // // Original paper suggest biasing the mip to improve the results
      // float fMipBias = 1;
      // float fMipLevel = max(0.5 * log2(fOmegaS / fOmegaP) + fMipBias, 0);
      // vPrefilteredColor += txFirst.SampleLevel(samLinear, vLight, fMipLevel).rgb * fNdotL;
      // fTotalWeight += fNdotL;

			vPrefilteredColor += txFirst.SampleLevel(samLinear, vLight, mipIndex).rgb * fNdotL;
			fTotalWeight += fNdotL;
		}
	}

	return vPrefilteredColor / fTotalWeight;
}

float getRoughness(){
  float roughness = saturate((float)(mipIndex - 1) / 5);
  roughness = pow(roughness, 6);
  return roughness;
}

float3 getNormal(float2 uv){
  float3 vec = float3(1 - uv * 2, -1);
  return mul(vec, (float3x3)transform);
}
