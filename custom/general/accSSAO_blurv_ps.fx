// #define MIPMAPPED_GBUFFER 4
#define BLUR_TEX txSsaoFirstStep
Texture2D txSsaoFirstStep : register(t0);

#include "accSS.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  float4 res = ppBlur(pin.Tex, float2(0, 1));
  res.x = ssaoOpacity * (1 - pow(saturate(res.x), ssaoExp)); 
  res.yzw = pow(max(res.yzw, 0), 1.2) * gSSGIIntensity; 
  return res;
}