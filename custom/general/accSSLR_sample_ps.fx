#include "include/common.hlsl"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

Texture2D txColor : register(t0);
Texture2D txNormals : register(t1);
Texture2D txSSLR : register(t14);

float4 main(VS_Copy pin) : SV_TARGET {
  float quality = 0;
  float4 vSSLR = 0;
  
  // [unroll] for (int y = -2; y <= 2; y++)
  // [unroll] for (int x = -2; x <= 2; x++){
  //   float4 sampled = txSSLR.SampleLevel(samLinearSimple, pin.Tex, 0, int2(x, y));
  //   float weight = sampled.w / (abs(y - 1) + abs(x - 1) + 1);
  //   if (weight > quality){
  //     vSSLR = sampled;
  //     quality = weight;
  //   }
  // }

  vSSLR = txSSLR.SampleLevel(samLinearSimple, pin.Tex, 0);
  if (vSSLR.w <= 0.001) return 0; 

  // float edge = saturate((min(vSSLR.x, 1 - vSSLR.x) + 0.05) / 0.05) 
  //   * saturate((min(vSSLR.y, 1 - vSSLR.y) + 0.05) / 0.05);
  float edge = 1;
  float3 color = txColor.Sample(samLinearClamp, vSSLR.xy).xyz;
  return float4(max(color / (1 + color * 0.15), 0), vSSLR.w * edge);
}