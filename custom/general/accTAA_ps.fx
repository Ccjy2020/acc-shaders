#include "include/common.hlsl"
#include "accSS_cbuffer.hlsl"

Texture2D txHistory : register(t0);
Texture2D txNow : register(t1);
Texture2D<float2> txMotion : register(t2);
Texture2D<float2> txMotionOld : register(t3);
Texture2D<float> txStencil : register(t4);
Texture2D<float> txStencilOld : register(t5);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float3 tonemap(float3 x){ return x / (x + 1); }
float3 inverseTonemap(float3 x){ return x > 0.9999 ? 0 : x / max(1 - x, 0.00001); }
inline bool isSaturated(float a) { return a == saturate(a); }
inline bool isSaturated(float2 a) { return isSaturated(a.x) && isSaturated(a.y); }

#define HDR_CORRECTION
#define HDR_CORRECTION_CLAMP
#define MINMAX_ROUNDED

// From "Temporal Reprojection Anti-Aliasing"
// https://github.com/playdeadgames/temporal
float3 clipAABB(float3 aabbMin, float3 aabbMax, float3 prevSample, float3 avg) {
	float3 p_clip = 0.5 * (aabbMax + aabbMin);
	float3 e_clip = 0.5 * (aabbMax - aabbMin);

	float3 v_clip = prevSample - p_clip;
	float3 v_unit = v_clip.rgb / e_clip;
	float3 a_unit = abs(v_unit);
	float ma_unit = max(a_unit.x, max(a_unit.y, a_unit.z));

	if (ma_unit > 1.0) {
		return p_clip + v_clip / ma_unit;
	} else {
		return prevSample; // point inside aabb
	}
}

bool isEdge(float2 uv, float stencilCenter){
	bool ret = false;
	[unroll] for (int x = -1; x <= 1; x++){
		[unroll] for (int y = -1; y <= 1; y++){
			if (x == 0 && y == 0) continue;
			if (abs(x) + abs(y) == 2) continue;
			ret = txStencil.SampleLevel(samPointClamp, uv, 0, int2(x, y)) != stencilCenter || ret;
		}
	}
	return ret;
}

float rand(float2 co){
  return frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453);
}

float3 rgbToYcc(float3 c) {
	return float3(c.x/4.0 + c.y/2.0 + c.z/4.0, c.x/2.0 - c.z/2.0, -c.x/4.0 + c.y/2.0 - c.z/4.0);
}

float3 yccToRgb(float3 c) {
	return saturate(float3(c.x + c.y - c.z, c.x + c.z, c.x - c.y - c.z));
}

static float kernel[9] = {
 0.0625, 0.125, 0.0625,
 0.125, 0.25, 0.125,
 0.0625, 0.125, 0.0625
};

float4 main(VS_Copy pin) : SV_TARGET {
  float2 velocity = txMotion.Load(int3(pin.PosH.xy, 0));
  float2 oldUv = pin.Tex - velocity;

	float4 nbh[9];
	nbh[0] = txNow.Load(int3(pin.PosH.xy, 0), int2(-1, -1));
	nbh[1] = txNow.Load(int3(pin.PosH.xy, 0), int2(0, -1));
	nbh[2] = txNow.Load(int3(pin.PosH.xy, 0), int2(1, -1));
	nbh[3] = txNow.Load(int3(pin.PosH.xy, 0), int2(-1, 0));
	nbh[4] = txNow.Load(int3(pin.PosH.xy, 0), int2(0, 0));
	nbh[5] = txNow.Load(int3(pin.PosH.xy, 0), int2(1, 0));
	nbh[6] = txNow.Load(int3(pin.PosH.xy, 0), int2(-1, 1));
	nbh[7] = txNow.Load(int3(pin.PosH.xy, 0), int2(0, 1));
	nbh[8] = txNow.Load(int3(pin.PosH.xy, 0), int2(1, 1));
	float4 nbhMin = nbh[0];
	float4 nbhMax = nbh[0];
	float4 nbhAvg = 0;
	float4 nbhBlur = 0;
	
	{
		[unroll]
		for (uint i = 1; i < 9; i++) {
			float4 n = nbh[i];
			nbhMin = min(nbhMin, n);
			nbhMax = max(nbhMax, n);
		}
	}

	{
		[unroll]
		for (uint i = 0; i < 9; i++) {
			float4 n = nbh[i];
			nbhAvg += float4(n.rgb, 1);
			nbhBlur += n * kernel[i];
		}
		nbhAvg /= nbhAvg.w;
	}

	#ifdef MINMAX_ROUNDED
		float4 cmin5 = min(nbh[1], min(nbh[3], min(nbh[4], min(nbh[5], nbh[7]))));
		float4 cmax5 = max(nbh[1], max(nbh[3], max(nbh[4], max(nbh[5], nbh[7]))));
		float4 cavg5 = (nbh[1] + nbh[3] + nbh[4] + nbh[5] + nbh[7]) / 5.0;
		nbhMin = 0.5 * (nbhMin + cmin5);
		nbhMax = 0.5 * (nbhMax + cmax5);
		nbhAvg = 0.5 * (nbhAvg + cavg5);
	#endif

	// Can’t avoid the linear filter here because point sampling could sample irrelevant pixels:
	float4 history = txHistory.SampleLevel(samLinearClamp, oldUv, 0);
	float4 current = nbh[4];

	// Loading stencil
	float stencil = txStencil.SampleLevel(samPointClamp, pin.Tex, 0);
	float stencilOld = txStencilOld.SampleLevel(samPointClamp, oldUv, 0);
	bool edge = isEdge(pin.Tex, stencil);
	bool forceExtraAA = abs(stencil - 0.5);

  #ifdef HDR_CORRECTION_CLAMP
		history.rgb = tonemap(history.rgb);
		nbhMin.rgb = tonemap(nbhMin.rgb);
		nbhMax.rgb = tonemap(nbhMax.rgb);
		// nbhBlur.rgb = tonemap(nbhBlur.rgb);
  #endif

	if (!(dot(history.rgb, 1) > -1e9 && dot(history.rgb, 1) < 1e9)){
		// TODO: Figure out how NaN appears in here
		history.rgb = nbhBlur.rgb;
	}

	// Clamping history
	float3 historyClamped = clamp(history.rgb, nbhMin.rgb, nbhMax.rgb);
	// float3 historyClamped = clipAABB(nbhMin.rgb, nbhMax.rgb, history.rgb, nbhAvg.rgb);
	history.rgb = lerp(history.rgb, historyClamped, forceExtraAA < 0.002 ? 0.1 : stencil ? 0.5 : 1);

  #ifdef HDR_CORRECTION_CLAMP
    history.rgb = inverseTonemap(history.rgb);
  #endif

	// Simple adjustment for different velocities:
	float currentSpeed = length(velocity);
	float oldSpeed = length(txMotionOld.Load(int3(pin.PosH.xy, 0)));
  float blendK = saturate(1.2 - abs(currentSpeed - oldSpeed) * 100);
	// return float4(abs(velocity) * 1000, 1, 1);

	// The linear filtering can cause blurry image, try to account for that:
	float subpixelCorrection = frac(max(abs(velocity.x) * gSize.x, abs(velocity.y) * gSize.y)) * 0.5;

	// Compute a nice blend factor:
	float blendFactor = saturate(lerp(0.05, 0.8, subpixelCorrection));

	// Stencil value >0.5 is for reduced blending rate:
	blendFactor = max(blendFactor, 0.25 * max(history.a, stencil * 2 - 1));

	// Stencil-based anti-ghosting:
	float stencilDif = history.a * 0.9;
	if (stencil != stencilOld){
		if (!edge){
			blendFactor = 1;
			// current = nbhBlur;
		}
		stencilDif = 1;
	}

	// Motion-based anti-ghosting to fix shadows nearby:
	// float motionBasedAntiGhosting = 1;
	if (!edge){
		float antiGhostingK = saturate(2 - length(velocity) * 200);
		// motionBasedAntiGhosting *= antiGhostingK;
		blendK *= antiGhostingK;
		current = lerp((nbhBlur + current) / 2, current, antiGhostingK);
	}

	// If information can not be found on the screen, revert to aliased image:
	blendFactor = isSaturated(oldUv) ? blendFactor : 1.0;

	if (forceExtraAA){
		current.rgb = lerp(nbhBlur.rgb, current.rgb, blendK * (1 - blendFactor));
	}

	#ifdef HDR_CORRECTION
		history.rgb = tonemap(history.rgb);
		current.rgb = tonemap(current.rgb);
	#endif

	// Do the temporal super sampling by linearly accumulating previous samples with the current one:
	float4 resolved = lerp(current, history, blendK * (1 - blendFactor));

	#ifdef HDR_CORRECTION
		resolved.rgb = inverseTonemap(resolved.rgb);
	#endif

	resolved.a = stencilDif;
	#ifdef VIEW_STENCIL
		return stencil == 0 ? 0 : float4(normalize(float2(
			rand(float2(stencil, stencil + 0.1)), 
			rand(float2(stencil, stencil + 0.2)))) * 1,
			saturate(stencil * 2 - 1) * 3,
			1);
	#endif
	return resolved; // * float4(1, motionBasedAntiGhosting, motionBasedAntiGhosting, 1);
}