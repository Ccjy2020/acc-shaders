SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txIcon : register(t0);
Texture2D txBg : register(t1);

cbuffer cbData : register(b10) {
  float gModeScaleDown;
  float gModeModernize;
  float gModePressed;
  float _pad0;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float2 scaleUV(float2 uv, float scale){
  float scaleInv = 1 / scale;
  return uv * scaleInv - (scaleInv - 1) / 2;
}

float4 sampl(Texture2D tex, float2 uv, float scale){
  float2 uvNew = scaleUV(uv, scale);
  if (any(abs(uvNew - 0.5) > 0.5)) return 0;
  return tex.Sample(samLinearSimple, uvNew);
}

float4 mix(float4 bg, float4 fg){
  return lerp(bg, fg, fg.a);
}

float4 modernize(float2 uv){
  float2 uvNew = scaleUV(uv, 1.4);
  if (length(uvNew - float2(0.51, 0.51)) > 0.35) return 0;
  float4 tex = sampl(txIcon, uvNew, 1);
  // return tex;
  return float4(tex.rgb, sqrt(saturate(tex.a * tex.r * 1.2)));
}

float4 main(VS_Copy pin) : SV_TARGET {
  if (gModeScaleDown){
    return mix(gModePressed ? float4(1, 0, 0, 1) : 0, sampl(txIcon, pin.Tex, 0.8));
  }

  if (gModeModernize){
    return mix(gModePressed ? float4(1, 0, 0, 1) : 0, modernize(pin.Tex));
  }

  float4 icon = txIcon.Sample(samLinearSimple, pin.Tex * 1.8 - 0.4);
  if (any(abs(pin.Tex * 1.8 - 0.9) > 0.5)) icon = 0;
  float4 bg = txBg.Sample(samLinearSimple, pin.Tex);
  return lerp(bg, icon, icon.a);
}