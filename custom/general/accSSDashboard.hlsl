float dashLuminance(float3 rgb){
  // return dot(rgb, float3(0.2125, 0.7154, 0.0721));
  // return dot(rgb, float3(0.3, 0.5, 0.2));
  return dot(rgb, 0.33);
}

float2 dashNormalEncode(float3 n) {
  // n = normalize(n);
  // if (abs(n.y) > 0.999) {
  //   n += float3(0.023, 0, -0.017);
  // }
  // if (n.y < -0.999) {
  //   n = float3(0.005, -0.995, 0.005);
  // }
  n = normalize(n);
  float p = sqrt(saturate(n.y * 8 + 8));
  return n.xz / max(p, 0.00001) + 0.5;
}

float3 dashNormalDecode(float2 enc) {
  float2 fenc = enc * 4 - 2;
  float f = dot(fenc, fenc);
  float g = sqrt(saturate(1 - f / 4));
  float3 n;
  n.xz = fenc * g;
  n.y = 1 - f / 2;
  return n;
}