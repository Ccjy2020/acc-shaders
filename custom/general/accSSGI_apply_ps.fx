struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

SamplerState samLinear : register(s0) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txColor : register(t0);
Texture2D txSSGI : register(t14);

float4 getGiValue21(float2 uv){  
  float4 resultBase = 0;
  [unroll] for (int x = -2; x <= 2; x++)
  [unroll] for (int y = -2; y <= 2; y++){
    if (abs(x) + abs(y) == 4) continue;
    resultBase += txSSGI.SampleLevel(samLinearSimple, uv, 2, int2(x, y));
  }
  resultBase /= 21;
  return resultBase;
}

float4 getGiValue9(float2 uv){  
  float4 resultBase = 0;
  [unroll] for (int x = -1; x <= 1; x++)
  [unroll] for (int y = -1; y <= 1; y++){
    resultBase += txSSGI.SampleLevel(samLinearSimple, uv, 2, int2(x, y));
  }
  resultBase /= 9;
  return resultBase;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // return txSSGI.SampleLevel(samLinearSimple, pin.Tex, 0);
  #ifdef DEBUG_VIEW
    return getGiValue9(pin.Tex) * 2;
  #endif
  return txColor.SampleLevel(samLinear, pin.Tex, 0)
    * (1 + pow(max(getGiValue9(pin.Tex), 0), 1.2));
}