#include "include_new/base/samplers_ps.fx"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_TARGET {
  float ret = txDiffuse.SampleLevel(samPointClamp, pin.Tex, 0);
  [unroll] for (int x = -2; x <= 2; x++)
  [unroll] for (int y = -2; y <= 2; y++){
    if (x == 0 && y == 0) continue;
    float sec = txDiffuse.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));
    if (ret > 0.999) ret = sec;
  }
  if (ret == 0) ret = 0.00001;
  else if (ret > 0.999) ret = 0;
  return ret;
}