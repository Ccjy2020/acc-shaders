struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

VS_Copy main(uint id: SV_VertexID) {
  VS_Copy vout;
  vout.Tex = float2((float)(id / 2) * 2, 1 - (float)(id % 2) * 2);
  vout.PosH = float4(
    (float)(id / 2) * 4 - 1,
    (float)(id % 2) * 4 - 1, 0.9999, 1);
  return vout;
}
