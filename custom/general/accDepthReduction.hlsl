#include "include/common.hlsl"

#ifdef FIRST_STEP
  Texture2D<float> txDiffuse : register(t0);
#else
  Texture2D<float2> txDiffuse : register(t0);
#endif

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float2 main(VS_Copy pin) : SV_TARGET {
  float2 r = float2(1, 0);
  [unroll]
  for (int i = -8; i <= 7; i++){
    float2 v = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0, OFFSET(i));
    r.x = min(r.x, v.x);
    r.y = max(r.y, v.y);
  }
  return r;
}