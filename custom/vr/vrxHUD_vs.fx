#define USE_PS_FOG
#include "include_new/base/_include_vs.fx"

PS_IN_PerPixelPosL main(VS_IN vin) {
  GENERIC_PIECE(PS_IN_PerPixelPosL);
  vout.PosL = vin.PosL.xyz;
  vout.NormalL = vin.NormalL.xyz;
  return vout;
}
