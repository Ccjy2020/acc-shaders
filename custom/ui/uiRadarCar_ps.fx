#include "include/common.hlsl"

cbuffer cbDataPerCar : register(b11) {
  float4x4 gWorld;
  float4 gColor;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

Texture2D txDiffuse : register(t0);

float4 main(VS_Copy pin) : SV_TARGET {
  // return txDiffuse.Sample(samLinear, pin.Tex);
  return gColor;
}