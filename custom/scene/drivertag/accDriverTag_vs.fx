#include "include_new/base/_include_vs.fx"

cbuffer cbData : register(b10) {
  float4 gPos[16];
  float2 gSize;
}

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
  float Opacity : TEXCOORD3;
  float2 TexRaw : TEXCOORD4;
};

static const float3 BILLBOARD[] = {
  float3(-1, -1, 0),
  float3(1, -1, 0),
  float3(-1, 1, 0),
  float3(-1, 1, 0),
  float3(1, -1, 0),
  float3(1, 1, 0),
};

PS_IN main(uint fakeIndex: SV_VertexID) {
  PS_IN vout;

  uint vertexID = fakeIndex % 6;
  uint instanceID = fakeIndex / 6;
  float3 quadPos = BILLBOARD[vertexID];

  if (gPos[instanceID].w < 0){
    vout = (PS_IN)0;
    vout.PosH = -1;
    return vout;
  }
  
  float2 offsetSigned = quadPos.xy;
  float3 posBase = gPos[instanceID].xyz + float3(0, 0.2, 0);
  float4 posW = float4(posBase, 1);
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

  float4 posW0 = float4(posBase + extDirUp, 1);
	float4 posV0 = mul(posW0, ksView);
	float4 posH0 = mul(posV0, ksProjection);
  float onScreen = length(posH0.xy / posH0.w - posH.xy / posH.w);

  // posH.xy += (offsetSigned + float2(0.9, 0)) * float2(1, 0.25) * (0.4 * onScreen / (0.25 + onScreen)) * posH.w;
  // posH.xy += (offsetSigned * float2(1, 0.95) + float2(0, 3)) * float2(1, 0.125) * 200 * extScreenSize.zw * posH.w;
  posH.xy += (offsetSigned * float2(1, 0.95) + float2(0, 3)) * gSize * posH.w;
  float2 offset = offsetSigned * float2(1, 0.95) * 0.5 + 0.5;
  // posH.xy += (offsetSigned + float2(0.9, 3)) * float2(1, 0.125) * 1024 * extScreenSize.zw * posH.w;

  vout.PosH = posH;
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = float2(offset.x, ((1 - offset.y) + gPos[instanceID].w) / 16);
  vout.TexRaw = offsetSigned;
  vout.Fog = calculateFog(posV);

  float distanceToCamera = length(vout.PosC);
  float fovMult = saturate(min(ksFOV, 50) * 0.0125);
  vout.Opacity = saturate(smoothstep(3, 4, distanceToCamera))
    * saturate(smoothstep(40, lerp(35, 20, fovMult), distanceToCamera * lerp(0.15, 1, fovMult)));
  return vout;
}
