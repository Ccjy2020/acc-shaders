#include "include/common.hlsl"
#include "include/rain.hlsl"

#define STREAM_TIME_HANG 1
#define STREAM_TIME_FLY 2
#define STREAM_GRAVITY -9.81

struct RainDrop {
  float3 pos;
	float fade;

	float size;
  float3 dir;
};

inline float rand(inout float seed, in float2 uv) {
	float result = frac(sin(seed * dot(uv, float2(12.9898, 78.233))) * 43758.5453);
	seed += 1.0;
	return result;
}

#if defined(TARGET_VS) || defined(TARGET_PS)

  #if defined(TARGET_PS)
    #ifdef PARTICLES_WITHOUT_NORMALS
      #define GBUFF_NORMAL_W_SRC float3(0, 1, 0)
      #define GBUFF_NORMAL_W_PIN_SRC float3(0, 1, 0)
    #endif
    #include "include_new/base/_include_ps.fx"
    // Texture2D txMain : register(TEXTURE_SLOT); 
  #endif

  cbuffer cbVPSBuffer : register(b11) {
    float gStretching;
    uint gUseColor;
    float2 gPad0;	

    float3 gRainDir;
    float gCarCollision;
    
    float4x4 gCarCollisionTransform;
  };

  #ifdef TARGET_PS
    Texture2D<float> txCarCollision : register(t1);
    float checkCarCollision(float3 pos){
      float4 uv = mul(float4(pos, 1), gCarCollisionTransform);
      return any(abs(uv - 0.5) > 0.5) ? 1 : saturate((txCarCollision.SampleLevel(samLinear, uv.xy, 0) - uv.z) * 50);
    }
  #endif

	struct PS_IN {
    float4 PosH : SV_POSITION;
    float2 Tex : TEXCOORD1;

    #ifdef WINDSCREEN_SPLASHES
      float3 Velocity : TEXCOORD2;
    #else
      float3 PosC : POSITION;
      float Fog : TEXCOORD6;
      float Opacity : COLOR2;
      // float StretchedK : COLOR3;
      // float EdgeK : COLOR4;
      float3 LightVal : COLOR5;
      float3 LightDir : POSITION3;
      float LightFocus : POSITION4;
      float3 DirUp : POSITION1;
      float3 DirSide : POSITION2;
      float NoiseOffset : TEXCOORD2;

      #ifdef STREAMS
        float SideK : TEXCOORD3;
        float BlurK : TEXCOORD4;
      #endif
    #endif
	};

  #define INTERIOR_TEST\
    float interiorMult = checkCarCollision(pin.PosC + ksCameraPosition.xyz);\
    if (interiorMult == 0){ clip(-1); return (PS_OUT)0; }

  #include "../common/rainUtils.hlsl"

#elif defined(TARGET_CS)

  #include "include/samplers_vs.hlsl"
  #define RAND rand(seed, uv)
  #define RAND_INIT(x) float seed = 0.12345;float2 uv = x;
  #define TIME_MULT 1
  // #define TIME_MULT 0.25
  #define RAIN_SPEED_BASE 15
  #define RAIN_SPEED (TIME_MULT * RAIN_SPEED_BASE)

  AppendStructuredBuffer<RainDrop> buResultDrops : register(u0);
  AppendStructuredBuffer<RainDrop> buResultSplashes : register(u1);
  AppendStructuredBuffer<RainDrop> buResultStreams : register(u2);
  Texture2DArray<float> txRainShadow : register(t0);
  Texture2D<float2> txRainPuddles : register(t1);
  // Texture2D<float> txRoofs : register(t2);

  struct StreamSource {
    float type;
    float3 p1;
    float3 p2;
    float param;
  };

  cbuffer cbCSBuffer : register(b11) {
    float2 gSpawnPointA;
    float gMapStep;
    float gTime;

    float3 gPos;
    uint gTripleFrustum;

    float3 gSize;
    float gClipNearby;

    float4x4 gRainShadowTransform;
    float4x4 gRainShadowTransformInv;

    float4 gViewFrustum1;
    float4 gViewFrustum2;
    float4 gViewFrustum3;
    float4 gViewFrustum4;

    float3 gCameraPosition;
    float gDensity;

    float3 gCameraVelocity;
    float gWetnessLevel;

    float gWaterLevel;
    float3 gRainDir;

    float3 gRainPos;
    float gSplashesDensity;

    float3 gRainDirFixed;
    float gPad1;

    float3 gRainPosFixed;
    float gPad2;

    StreamSource gStreamSources[128];
  }

  bool isVisible(float3 pos){
    if (gTripleFrustum){
      return dot(gViewFrustum1.xyz, pos) - gViewFrustum1.w > -0.5
        || dot(gViewFrustum2.xyz, pos) - gViewFrustum2.w > -0.5;
    } else {
      return dot(gViewFrustum1.xyz, pos) - gViewFrustum1.w > -0.5
        && dot(gViewFrustum2.xyz, pos) - gViewFrustum2.w > -0.5
        && dot(gViewFrustum3.xyz, pos) - gViewFrustum3.w > -0.5
        && dot(gViewFrustum4.xyz, pos) - gViewFrustum4.w > -0.5;
    }
  }

  float rainShadowEdge(float2 uv, float comparison){
    float mi = 1, ma = 0;
    [unroll] for (int y = -1; y <= 1; y++)
    [unroll] for (int x = -1; x <= 1; x++) {
      if (abs(x) + abs(y) != 2) continue;
      float v = txRainShadow.SampleCmpLevelZero(samShadow, float3(uv, 1), comparison, int2(x, y) * 2);
      mi = min(mi, v);
      ma = max(ma, v);
    }
    return saturate((ma - mi) * 10000);
  }

  float waterThickness(float3 rainPos){
    float4 posH = mul(float4(rainPos, 1), gRainShadowTransform);
    float2 puddleValue = txRainPuddles.SampleLevel(samLinearBorder0, posH.xy, 0);

    float damp, wetness, water;
    rainCalculate(gWetnessLevel, gWaterLevel, puddleValue.x, puddleValue.y, damp, wetness, water);
    return water;
  }

  float3 groundPos(float3 rainPos){
    float4 posH = mul(float4(rainPos, 1), gRainShadowTransform);
    float shadowValue = txRainShadow.SampleLevel(samLinearBorder0, float3(posH.xy, 1), 0);
    return mul(float4(posH.xy, shadowValue, 1), gRainShadowTransformInv).xyz;
  }

  float3 groundPosNoCar(float3 rainPos){
    float4 posH = mul(float4(rainPos, 1), gRainShadowTransform);
    float shadowValue = txRainShadow.SampleLevel(samLinearBorder0, float3(posH.xy, 0), 0);
    return mul(float4(posH.xy, shadowValue, 1), gRainShadowTransformInv).xyz;
  }

  float3 groundPosNoCar(float3 rainPos, out bool groundOccludedByCar){
    float4 posH = mul(float4(rainPos, 1), gRainShadowTransform);
    float shadowValue = txRainShadow.SampleLevel(samLinearBorder0, float3(posH.xy, 0), 0);
    groundOccludedByCar = txRainShadow.SampleLevel(samLinearBorder0, float3(posH.xy, 1), 0) != shadowValue;
    return mul(float4(posH.xy, shadowValue, 1), gRainShadowTransformInv).xyz;
  }

  float3 rainDropRandomDir(float2 rand){
    return normalize(float3(rand.x - 0.5, -5, rand.y - 0.5));
  }

  float3 rainDropSummaryDir(float3 randomDir){
    #ifdef USE_RAIN_DIR_FIXED
      return normalize(gRainDirFixed + randomDir * float3(1, 0, 1));
    #else
      return normalize(gRainDir + randomDir * float3(1, 0, 1));
    #endif
  }

  float3 rainDropPos(float2 basePosXZ, float3 randomDir, float rand){
    float3 gStart = gPos - gSize / 2;
    float3 pos = float3(basePosXZ.x, 0, basePosXZ.y) 
      #ifdef USE_RAIN_DIR_FIXED
        + gRainPosFixed 
      #else
        + gRainPos 
      #endif
      + randomDir * float3(1, 0, 1) * gTime * RAIN_SPEED 
      + float3(0, gSize.y * rand, 0);

    float randX = frac(rand * 1397);
    float randZ = frac(rand * 3971);
    return frac((pos - gStart) / gSize) * gSize + gStart 
      + float3(randX - 0.5, 0, randZ - 0.5) * gMapStep;
  }

#endif