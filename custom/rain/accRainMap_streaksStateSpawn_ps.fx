#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D txNoise : register(t20);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float Random : TEXCOORD1;
  float Opacity : TEXCOORD2;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float dist = length(pin.Tex);
  float2 noiseIn = pin.PosH.xy;
  float4 noiseLR1 = txNoise.SampleLevel(samLinearSimple, noiseIn / 512 + pin.Random, 0);
  float4 noiseLR2 = txNoise.SampleLevel(samLinearSimple, noiseIn / 256 + pin.Random, 0);
  float noiseOffset = noiseLR1.x * 0.25 + noiseLR2.x * 0.5;
  dist += noiseOffset;
  if (dist > 1) discard;
  // float rand = txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0).x;
  // return float4(pin.Opacity * saturate(noiseOffset * 2), 0, 0, 0);
  float isMoving = pin.Opacity > 0.5 && pin.Random > 0.95;
  // float isMoving = false;
  return float4(isMoving ? pin.Opacity : 0, !isMoving ? pin.Opacity : 0, 0, frac(pin.Random * 6196));
  // float dist = length(pin.Tex);

  // float2 nm = pin.Tex;

  // float opacity = saturate(pin.Life * 2 - dist * 0.25);
  // opacity *= saturate(remap(dist, 0.95, 1, 1, 0));
  // opacity *= saturate(remap(dist - lerp(0, 0.2, saturate(pin.Life)), 0.4, 0.3, 1, 0));
  // // opacity *= saturate(remap(dist, 0.9, 0.8, 1, 0));
  // opacity *= pow(saturate(cos((1 - dist + lerp(0.5, 0, saturate(pin.Life * 2)) - pin.Random) * 20)), 2);
  // // opacity *= saturate(remap(dist, 0.65, 0.6, 1, 0));

  // nm = lerp(-pin.Tex, pin.Tex, saturate(remap(dist, 0.9, 1.0, 0, 1)));

  // clip(opacity - 0.005);
  // return float4(nm * float2(-0.5, 0.5) + 0.5, 1, opacity);
  // return float4(nm * float2(-0.5, 0.5) + 0.5, 1, opacity);
}