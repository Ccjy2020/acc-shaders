#include "accRainDrops.hlsl"
#include "include_new/base/_include_vs.fx"

static const float3 BILLBOARD[] = {
  float3(-1, -1, 0),
  float3(1, -1, 0),
  float3(-1, 1, 0),
  float3(-1, 1, 0),
  float3(1, -1, 0),
  float3(1, 1, 0),
};

StructuredBuffer<RainDrop> particleBuffer : register(t0);

#ifndef MODE_MAIN_NOFX
	#include "include_new/base/common_ps.fx"
	#include "include_new/ext_lightingfx/_include_ps.fx"
#endif

PS_IN main(uint fakeIndex : SV_VERTEXID) {
  uint vertexID = fakeIndex % 6;
  uint instanceID = fakeIndex / 6;
  RainDrop particle = particleBuffer[instanceID];
  float3 quadPos = BILLBOARD[vertexID];

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
	float distance = length((particle.pos - ksCameraPosition.xyz).xz);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif
	
	float stretchedK = gStretching;
	// float stretchedK = 0.5 + 0.5 * sin(ksGameTime * 0.001);
	float distantFix = 1 + distance * 0.08;

  float3 side = normalize(cross(billboardAxis, -particle.dir));
  float3 up = normalize(cross(billboardAxis, side));
  // float3 side = normalize(cross(billboardAxis, -particle.dir));

	float widthMult = lerp(0.1, 0.04 * distantFix, stretchedK);
	float heightMult = lerp(0.1, lerp(1.2, 1.7, extSceneRain) * 0.8, stretchedK);
	float3 offset = (up * quadPos.y * heightMult + side * quadPos.x * widthMult) * particle.size;

	float4 posW = float4(particle.pos, 1);
	posW.xyz += offset;

	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

	PS_IN vout;
	vout.PosH = posH;
	vout.Tex = quadPos.xy;
	vout.Opacity = particle.fade;
	vout.NoiseOffset = dot(particle.pos, 0.1);

	vout.DirUp = up;
	vout.DirSide = side;

	#ifndef MODE_SHADOW
		vout.PosC = posW.xyz - ksCameraPosition.xyz;
		#ifdef MODE_GBUFFER
			GENERIC_PIECE_VELOCITY(posW, particle.velocity * gDeltaTime);
		#else
			vout.Fog = calculateFog(posV);
			CALCULATE_LIGHTING(vout);
		#endif
	#endif

	return vout;
}