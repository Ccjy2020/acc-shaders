#include "accRainWindscreen.hlsl"
#include "include/poisson.hlsl"

Texture2D txWiper : register(t0);
Texture2D txPrevious : register(t1);

float blur(float2 uv) {
  float ret = 0;

  #define PUDDLES_DISK_SIZE 16
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.03;
    ret += txWiper.SampleLevel(samLinearBorder0, uv + offset, 3.5).a;
  }

  return ret / PUDDLES_DISK_SIZE;
}

float2 D(float v){
  return float2(ddx(v), ddy(v));
}

cbuffer cbData : register(b11) {
  float4x4 gWipersTransform2;
  float4x4 gViewProjInv;
}

float4 main(PS_IN_wipersProject pin) : SV_TARGET {
  float4 previous = txPrevious.Load(int3(pin.PosH.xy, 0));
  float4 shape = txWiper.SampleLevel(samPointBorder0, pin.TexWiper, 0);
  float4 shapeLinear = txWiper.SampleLevel(samLinearBorder0, pin.TexWiper, 0);

  if (any(abs(pin.TexWiper - 0.5) > 0.499)){
    shape = 0;
    shapeLinear = 0;
  }

  // float blurred = blur(pin.TexWiper);
  // float2 velocity = D(blurred) * 50;
  // velocity = lerp(velocity, previous.xy, previous.a);

  float3 velocityW = mul(float3(shape.xy * 2 - 1, 0), (float3x3)gViewProjInv);
  float3 dirX = normalize(ddx(pin.PosW));
  float3 dirY = normalize(ddy(pin.PosW));

  // shape.z = frac(pin.PosH.x / 256 + sin(pin.PosH.y / 32) * 0.25);

  float2 velocity = abs(dot(velocityW, 1)) > 0.001 ? float2(dot(velocityW, dirX), dot(velocityW, dirY)) * 0.5 + 0.5 : 0;
  return float4(
    lerp(previous.xy, velocity, saturate(shape.w * 10)), 
    // lerp(previous.z, shape.z, saturate(shape.w * 10)), 
    shape.z ? shape.z : previous.z, 
    // shape.z, 
    // shape.w);
    shape.w);
    // max(
    //   shape.w > 0 ? 0.1 : shape.w > 0.5 ? 1 : 0, 
    //   previous.w > 0 && (previous.w < 0.9 || shape.w > 0) ? 0.98 :
    //     previous.w > 0.97 && previous.w < 0.99 ? 0.95 : 0));

  // float4 previous = txPrevious.SampleLevel(samLinearBorder0, pin.Tex, 0);
  // float2 shape = txWiper.SampleLevel(samLinearBorder0, pin.TexWiper, 0);
  // float blurred = blur(pin.TexWiper);
  // float2 velocity = D(blurred) * 50;
  // velocity = lerp(velocity, previous.xy, previous.a);
  // return float4(velocity, max(shape.y, previous.z - 0.05), max(max(shape.x, previous.z), previous.a - 0.1));

  // float4 p = txPrevious.SampleLevel(samLinearBorder0, pin.Tex, 0);
  // float v = txWiper.SampleLevel(samLinearBorder0, pin.TexWiper, 0);
  // float b = blur(pin.TexWiper);
  // float2 o = float2(ddx(b), ddy(b)) * 2;
  // float2 or = lerp(o + 0.5, p.xy, p.a);
  // // return float4(or, 0, max(v, saturate(length(o * 10))));
  // return float4(or, 0, max(v, p.a - 0.2));
}