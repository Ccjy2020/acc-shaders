#define STREAMS
#define WINDSCREEN_SPLASHES
#include "accRainDrops.hlsl"
#include "include_new/base/_include_vs.fx"

static const float3 BILLBOARD[] = {
  float3(-1, -1, 0),
  float3(1, -1, 0),
  float3(-1, 1, 0),
  float3(-1, 1, 0),
  float3(1, -1, 0),
  float3(1, 1, 0),
};

StructuredBuffer<RainDrop> particleBuffer : register(t2);

cbuffer cbSplashesData : register(b3) {
  float4x4 gCarTransformInv;
}

PS_IN main(uint fakeIndex : SV_VERTEXID) {
  uint vertexID = fakeIndex % 6;
  uint instanceID = fakeIndex / 6;

  RainDrop particle = particleBuffer[instanceID];
  float3 quad = BILLBOARD[vertexID];

	float4 posW = float4(particle.pos + float3(quad.x, 0, quad.y) * 0.1, 1);
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

  float3 velocityW = float3(0, STREAM_GRAVITY * max(particle.fade * STREAM_TIME_FLY, 0), 0);
  float3 velocityL = mul(velocityW, (float3x3)gCarTransformInv);

	PS_IN vout;
	vout.PosH = posH;
	// vout.PosH = lerp(float4(quad.xy, 0, 1), posH, 0.001);
  // vout.Velocity = (velocityL * 0.05) * 0.5 + 0.5;
  vout.Velocity = (velocityL * 0.05);
	vout.Tex = quad.xy;
	return vout;
}