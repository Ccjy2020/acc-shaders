#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D txNoise : register(t20);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#define R 1
float4 sampleBlur(float2 uv, float level, int mult){
  if (mult == 0){
    return txDiffuse.SampleLevel(samLinearSimple, uv, level);
  }

  float4 currentShot = 0;
  float totalWeight = 0;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    float weight = 1 + dot2(float2(x, y));
    currentShot += txDiffuse.SampleLevel(samLinearSimple, uv, level, int2(x, y) * mult) * weight;
    totalWeight += weight;
  }
  return currentShot / totalWeight;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 prev = txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  float4 prevB = sampleBlur(pin.Tex, 4, 2);

  float4 rnh = txNoise.SampleLevel(samLinear, pin.Tex * 4, 0);
  // float4 rnm = txNoise.SampleLevel(samLinear, pin.Tex * 2, 0);
  float4 rnd = txNoise.SampleLevel(samLinear, pin.Tex * float2(0.0, 0.5), 0)
    + (txNoise.SampleLevel(samLinear, pin.Tex * float2(0.1, 0.25), 0) * 0.5 - 0.25);
  // rnd = 0;
  // float4 rollDown = txDiffuse.SampleLevel(samPoint, pin.Tex - normalize(float2((rnd.x - 0.5) * pow(rnd.y, 3) * 2.5, 0.5)) * 0.003, 2);

  float2 rdUv = pin.Tex - normalize(float2((rnd.x - 0.5) * pow(rnd.y, 3) * 2.5, 0.5)) * 0.003 * saturate(25 * prevB.x) * lerp(1, 2, rnh.x);
  float4 rollDown = sampleBlur(rdUv, 1, 1);
  float4 rollDownB = sampleBlur(rdUv, 0, 0);
  float4 rollDownL = sampleBlur(rdUv - float2(0.03, 0), 2, 0);
  float4 rollDownR = sampleBlur(rdUv + float2(0.03, 0), 2, 0);

  rollDown.x = lerp(rollDownB.x, saturate(remap(rollDown.x, 0.3, 0.7, 0, 1)), 0.3);
  float rollingMult = max(1 - rollDownL.x, 1 - rollDownR.x); // * pow(saturate(prevB.x * 8), 4);
  rollingMult = 1;

  float moving = saturate(prevB.x * 1.4);
  float thick = rollDown.x * rollingMult + prev.y * moving;
  float thin = prev.y * (1 - moving) + rollDown.x * (1 - rollingMult);
  float running = max(prev.x, prev.z);

  if (running > 0.01 && running < 0.5 && rnh.x > 0.9){
    thin = max(thin, 0.1);
    running = 0;
  }

  if (running > 0.01 && moving < 0.01 && max(rnh.x, rnh.y) > 0.9){
    running = 0;
    thin = 0.8;
  }

  // thin = max(prev.x * rnd.w * 0.01, thin);

  if (thin > 1.4){
    thick = thin;
    thin = 0;
  }

  // return 0;
  return float4(saturate(thick - 0.004), saturate(thin - 0.001), min(running - 0.003, 0.1), rollDown.w);
}