#include "accRainWindscreen.hlsl"

Texture2D<float> txPrevious : register(t1);
Texture2D txWipers : register(t2);

// #define R 2
// float main(VS_Copy pin) : SV_TARGET {
//   float4 value = txInput.SampleLevel(samLinear, pin.Tex, 0);
//   return value.a;
// }

#include "include/poisson.hlsl"
float main(VS_Copy pin) : SV_TARGET {
  POISSON_AVG_LEVEL(float4, result, txInput, samLinearClamp, pin.Tex, 0.015, 16, 4.5);
  float blurred = result.a * 3;
  float previous = txPrevious.Load(int3(pin.PosH.xy, 0));
  float4 wiper = txWipers.SampleLevel(samLinearSimple, pin.Tex, 2.5);
  return (previous - gFadeSlow + blurred * gFadeFast * saturate((blurred - previous) * 4)) * (1 - wiper.a);
}
