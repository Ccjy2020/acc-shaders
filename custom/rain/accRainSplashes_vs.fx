#define SPLASHES
#include "accRainDrops.hlsl"
#include "include_new/base/_include_vs.fx"

static const float3 BILLBOARD[] = {
  float3(-1, -1, 0),
  float3(1, -1, 0),
  float3(-1, 1, 0),
  float3(-1, 1, 0),
  float3(1, -1, 0),
  float3(1, 1, 0),
};

StructuredBuffer<RainDrop> particleBuffer : register(t1);

#ifndef MODE_MAIN_NOFX
	#include "include_new/base/common_ps.fx"
	#include "include_new/ext_lightingfx/_include_ps.fx"
#endif

PS_IN main(uint fakeIndex : SV_VERTEXID) {
  uint vertexID = fakeIndex % 6;
  uint instanceID = fakeIndex / 6;
  RainDrop particle = particleBuffer[instanceID];
  float3 quadPos = BILLBOARD[vertexID];

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
	float distance = length((particle.pos - ksCameraPosition.xyz).xz);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif
	
	float stretchedK = gStretching;
	// float stretchedK = 0.5 + 0.5 * sin(ksGameTime * 0.001);
	float distantFix = 1 + distance * 0.08;

  float3 upBase = 0;
  if (dot(particle.dir, 1)){
    upBase = normalize(particle.dir);
  } else {
    particle.dir = float3(0, 1, 0);
  }
  float3 side = normalize(cross(billboardAxis, -particle.dir));
  float3 up = normalize(cross(billboardAxis, side));

	float3 offset = (up * quadPos.y * (1 + 0.2 * abs(dot(upBase, up))) + side * quadPos.x) * particle.size;
  particle.pos -= billboardAxis * particle.size;
	float4 posW = float4(particle.pos, 1);
	posW.xyz += offset;

	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

	PS_IN vout;
	vout.PosH = posH;
	vout.Tex = quadPos.xy;
	vout.Opacity = 1;
	vout.NoiseOffset = particle.fade;

	vout.DirUp = up;
	vout.DirSide = side;

	#ifndef MODE_SHADOW
		vout.PosC = posW.xyz - ksCameraPosition.xyz;
		#ifdef MODE_GBUFFER
			GENERIC_PIECE_VELOCITY(posW, particle.velocity * gDeltaTime);
		#else
			vout.Fog = calculateFog(posV);
	
			#ifndef MODE_MAIN_NOFX
				float4 txDiffuseValue = 1;
				// float3 normalW = normal_ao.xyz;
				// float3 normalW0 = normal_ao.xyz;
				// float3 normalW1 = bitNormal;
				// float normalMix = bitLightingFade * 0.3;
				float3 posCPerVertexLighting = vout.PosC;
				LFX_MainLight mainLight;
				float3 lightingFX = 0;
				LIGHTINGFX(lightingFX);
				#ifndef SHADER_MIRROR
					vout.LightFocus = saturate(1.4 * mainLight.power / max(dot(lightingFX, 1), 1));
					vout.LightDir = mainLight.dir;
				#endif
				vout.LightVal = lightingFX;
			#endif

			// vout.LightFocus = 0;
			// vout.LightDir = 0;
			// vout.LightVal = 0;
		#endif
	#endif

	return vout;
}