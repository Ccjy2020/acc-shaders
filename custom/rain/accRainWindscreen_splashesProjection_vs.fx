#include "accRainWindscreen.hlsl"

cbuffer cbData : register(b11) {
  float4x4 gTransform;
  float4x4 gShadowTransform;
  float3 gBreathPoint;
  float gBreathRadius;
}

PS_IN_splashesProject main(VS_IN_ac vin) {
  PS_IN_splashesProject vout;
  vout.PosH = texToScreen(vsLoadAltTex(vin.TangentPacked));

  float3 carPos = mul(vin.PosL, gWorld).xyz;
  vout.Tex = mul(float4(carPos, 1), gTransform).xy;
  vout.TexAlt = vsLoadAltTex(vin.TangentPacked);
  vout.PosShadow = mul(float4(carPos, 1), gShadowTransform).xyz - float3(0, 0, 0.002);
  vout.PosCar = carPos;
  return vout;
}
