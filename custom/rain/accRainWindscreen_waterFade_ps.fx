#include "accRainWindscreen.hlsl"

Texture2D txPrevious : register(t0);
Texture2D txWipers : register(t2);

float4 main(VS_Copy pin) : SV_TARGET {
  float4 wipers = txWipers.SampleLevel(samLinearSimple, pin.Tex, 0);
  float4 previous = txPrevious.Load(int3(pin.PosH.xy, 0));
  previous.z = max(previous.z - gFadeFast, 0) * (1 - wipers.a);
  previous.w = 0;
  previous.xy = 0;
  return previous;
}