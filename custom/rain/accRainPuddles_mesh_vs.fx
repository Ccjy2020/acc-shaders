#include "include_new/base/cbuffers_vs.fx"

cbuffer cbPiece : register(b11) {
  float4x4 gViewProj;
  float2 gArea0;
  float2 gArea1;
};

struct VS_IN {
  float3 PosL : POSITION;
  uint NormalL : NORMAL_ENCODED;
};

struct VS_Copy {
  float4 PosH : SV_POSITION;
};

VS_Copy main(VS_IN vin, uint vertexID : SV_VertexID) {
  VS_Copy vout;
  vout.PosH = mul(float4(vin.PosL, 1), gViewProj);
  return vout;
}
