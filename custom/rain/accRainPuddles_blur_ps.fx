#include "include/common.hlsl"
Texture2D<float> txPreviousMap : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#include "include/poisson.hlsl"

float main_(VS_Copy pin) : SV_TARGET {
  float ret = 0;
  float tot = 0;
  #define PUDDLES_DISK_SIZE 16

  float c = txPreviousMap.SampleLevel(samPoint, pin.Tex, 0);
  // float cx = ddx(c) * -abs(pin.PosH.xy / pin.Tex).x;
  // float cy = ddy(c) * -abs(pin.PosH.xy / pin.Tex).y;
  float cx = ddx(c) * 512;
  float cy = ddy(c) * 512;
  // float cx = ddx(c) * abs(pin.PosH.xy / pin.Tex).x;
  // float cy = ddy(c) * abs(pin.PosH.xy / pin.Tex).y;

  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    // float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.01;
    // float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.02;
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.005;
    float2 sampleUV = pin.Tex + offset;
    float v = txPreviousMap.SampleLevel(samPoint, sampleUV, 0);
    float w = v != 1;
    ret += (v + dot(1, offset * float2(cx, cy))) * w;
    tot += w;
  }
  return saturate(ret / max(tot, 0.1));
}

#define R 2
float main(VS_Copy pin) : SV_TARGET {
  float currentShot = 0;
  float totalWeight = 0;
  float maxValue = 0;
  float minValue = 1;

  {
    [unroll]
    for (int x = -R; x <= R; x++)
    [unroll]
    for (int y = -R; y <= R; y++){
      if (abs(x) + abs(y) == R + R) continue;
      float weight = 1;
      float v = txPreviousMap.SampleLevel(samPoint, pin.Tex, 0, int2(x, y) * 3);
      // float v = txPreviousMap.SampleLevel(samLinearSimple, pin.Tex, 0, int2(x, y) * 2);
      if (v > 0.99) weight = 0;
      currentShot += v * weight;
      totalWeight += weight;
      if (weight) {
        maxValue = max(v, maxValue);
        minValue = min(v, minValue);
      }
    }
  }

  if (totalWeight < 1){
    return 0;
  }

  currentShot /= totalWeight;
  return currentShot;
  
  float currentShot2 = 0;
  float totalWeight2 = 0;

  {
    [unroll]
    for (int x = -R; x <= R; x++)
    [unroll]
    for (int y = -R; y <= R; y++){
      if (abs(x) + abs(y) == R + R) continue;
      float weight = 1;
      float v = txPreviousMap.SampleLevel(samLinearSimple, pin.Tex, 0, int2(x, y) * 3);
      if (v > currentShot) weight = 0;
      currentShot2 += v * weight;
      totalWeight2 += weight;
    }
  }

  // return maxValue;
  // return minValue;
  // return lerp(currentShot, currentShot2 / totalWeight2, 0.3);
  // return currentShot - 0.000001;
  return currentShot;
}