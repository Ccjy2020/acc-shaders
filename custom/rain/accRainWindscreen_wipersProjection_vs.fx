#include "accRainWindscreen.hlsl"

PS_IN_wipersProject main(VS_IN_ac vin) {
  PS_IN_wipersProject vout;
  vout.PosH = texToScreen(vsLoadAltTex(vin.TangentPacked));
	float4 posW = mul(vin.PosL, gWorld);
  vout.PosW = posW.xyz;
  vout.Tex = vsLoadAltTex(vin.TangentPacked);
  vout.TexWiper = mul(posW, gWipersTransform).xy * 0.5 + 0.5;
  vout.TexWiper.y = 1 - vout.TexWiper.y;
  return vout;
}
