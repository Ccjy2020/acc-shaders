#define MAX_ITEMS 128
cbuffer cbRipple : register(b10) {
  float4 gItems[MAX_ITEMS];
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float Random : TEXCOORD1;
};

static const float2 BILLBOARD[] = {
	float2(-1, -1),
	float2(1, -1),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, -1),
	float2(1, 1),
};

static const float2 OFFSET[] = {
	float2(-2, -2),
	float2(-2, 0),
	float2(-2, 2),
	float2(0, -2),
	float2(0, 0),
	float2(0, 2),
	float2(2, -2),
	float2(2, 0),
	float2(2, 2),
};

VS_Copy main(uint id: SV_VertexID) {
  float4 item = gItems[id / (6 * 9)];
  float2 offset = OFFSET[(id / 6) % 9];
  float2 vertex = BILLBOARD[id % 6];

  VS_Copy vout;
  vout.Tex = vertex;
  vout.PosH = float4(offset + item.xy + vertex * lerp(0.04, 0.06, item.z) * 0.75, 0, 1);
  vout.Random = lerp(0.5, 1, frac(item.z * 31357.61267));
  return vout;
}
