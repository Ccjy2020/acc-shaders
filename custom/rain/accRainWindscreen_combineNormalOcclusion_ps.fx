#include "include/common.hlsl"

Texture2D txNormal : register(t0);
Texture2D txOcclusion : register(t1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

const static int2 OFFSETS[20] = {
  int2(-1, 0),
  int2(1, 0),
  int2(0, -1),
  int2(0, 1),
  int2(-1, -1),
  int2(-1, 1),
  int2(1, 1),
  int2(1, -1),
  int2(-2, 0),
  int2(2, 0),
  int2(0, -2),
  int2(0, 2),
  int2(-2, -1),
  int2(-2, 1),
  int2(2, -1),
  int2(2, 1),
  int2(-1, -2),
  int2(1, -2),
  int2(-1, 2),
  int2(1, 2),
};

#define TRY(X, Y) ret = found || tex.SampleLevel(samPoint, uv, 0, int2(X, Y)); found = found || dot(ret, 1);

float4 sampleAround(Texture2D tex, float2 uv){
  float4 ret = tex.SampleLevel(samPoint, uv, 0);
  if (dot(ret, 1)){
    return ret;
  } else {
    bool found = false;
    [unroll]
    for (uint i = 0; i < 20; ++i){
      ret = found || tex.SampleLevel(samPoint, uv, 0, OFFSETS[i]);
      found = found || dot(ret, 1);
    }
    return ret;
  }
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 normal = sampleAround(txNormal, pin.Tex);
  float4 occlusion = sampleAround(txOcclusion, pin.Tex);
  return float4(normalize(normal.xyz) * 0.5 + 0.5, occlusion.x);
}