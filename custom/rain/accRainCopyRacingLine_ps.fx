#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float paddingAdj = 0.54;
  pin.Tex = (pin.Tex * 2 - 1) * paddingAdj + 0.5;
  return txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0);
}