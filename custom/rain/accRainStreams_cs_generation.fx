#include "accRainDrops.hlsl"

[numthreads(64, 16, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  // float2 basePosXZ = gStreamPointA + float2(float(threadID.x), float(threadID.y)) * gStreamStep;

  // if (gDensity < RAND) return;
  // float3 dir = rainDropDir(RAND);
  // float3 pos = rainDropPos(basePosXZ, dir, RAND);

  StreamSource S = gStreamSources[threadID.y];
  RAND_INIT(S.p1.xz + threadID.x);

  uint type = (uint)S.type;
  float intensity = frac(S.type) / 0.99;
  float visible = max(gDensity * gWetnessLevel, gWetnessLevel * 0.1) - RAND;
  if (!type || RAND > intensity || visible < 0) return;

  float3 pos = S.p1;
  if (type == 1) pos = lerp(S.p1, S.p2, threadID.x / 256.);
  // if (type == 2 && threadID.x > 64) return;
  // 512 / 64 = 8

  RainDrop drop;

  float3 dir = normalize(gRainDir + float3(0, -3, 0));
  float phaseBase = fmod(RAND * (STREAM_TIME_FLY + STREAM_TIME_HANG) + gTime * 1, STREAM_TIME_FLY + STREAM_TIME_HANG) - STREAM_TIME_HANG;
  float phase = phaseBase < 0 ? phaseBase / STREAM_TIME_HANG : phaseBase / STREAM_TIME_FLY;
  pos += dir * 0.5 * -STREAM_GRAVITY * pow(max(phase * STREAM_TIME_FLY, 0), 2);

  drop.pos = pos;
  drop.dir = normalize(S.p2 - S.p1);
  drop.fade = phase;
  drop.size = 0.04 * (0.5 + RAND) * saturate(visible * 20 + saturate(gDensity * 20 - 19));
  buResultStreams.Append(drop);
}