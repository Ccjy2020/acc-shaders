#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float dist = length(pin.Tex);
  float2 nm = pin.Tex;

  clip(1 - dist);
  return dist > 0.61 ? saturate(remap(dist, 0.61, 1, 0, 1)) : 1;
}