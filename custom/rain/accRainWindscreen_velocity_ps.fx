#include "accRainWindscreen.hlsl"

#define R 2
float2 main(VS_Copy pin) : SV_TARGET {
  float2 currentShot = 0;
  float totalWeight = 0;

  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    // float weight = 1 / (1 + length(float2(x, y)));
    float4 v = txInput.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x, y));
    float w = lerp(0.1, 1, v.a);
    currentShot += v.xy * w;
    totalWeight += w;
  }

  return currentShot / totalWeight 
    // + (txNoise.SampleLevel(samLinear, pin.Tex, 0).xy - 0.5) * 0.1
    ;
}