#include "accRainWindscreen.hlsl"

PS_IN_project main(VS_IN_ac vin) {
  PS_IN_project vout;
  vout.PosH = texToScreen(vsLoadAltTex(vin.TangentPacked));
	vout.PosL = mul(vin.PosL, gWorld);
  vout.Tex = vsLoadAltTex(vin.TangentPacked);
  return vout;
}
