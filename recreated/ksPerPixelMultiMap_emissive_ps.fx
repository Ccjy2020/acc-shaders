#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define USE_GETNORMALW
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#include "lightingBounceBack.hlsl"
#include "emissiveMapping.hlsl"
// alias: ksPerPixelMultiMap_AT_emissive_ps

RESULT_TYPE main(PS_IN_NmExtra4 pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);

  float alpha;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);

  float4 emissiveMap;
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;

  float4 emissiveInput = txDiffuseValue;
  #ifndef MODE_KUNOS
    if (emDiffuseAlphaAsMultiplier3.x < 0) emissiveInput.a = alpha;
  #endif
  L.txEmissiveValue = getEmissiveValue(emissiveInput, pin.Tex, pin.Extra, emissiveMap);
  L.applyTxMaps(txMapsValue.xyz);
  RAINFX_SHINY(L);
  float3 lighting = calculateMapsLighting(L);
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_MAPSMASK * saturate(dot(emissiveMap, extBounceBackMask)));
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  RAINFX_REFLECTIVE(R);

  if (abs(emAlphaFromDiffuse) == 1) alpha = txDiffuseValue.a;
  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, pin.PosC, normalW, R);
  
  RAINFX_WATER(withReflection);
  RETURN(withReflection.xyz, emAlphaFromDiffuse < 0 ? withReflection.a : alpha);
}
