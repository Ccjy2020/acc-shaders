#define SUPPORTS_COLORFUL_AO
#define INCLUDE_FLAGS_CB
#define AO_MIX_INPUT 0
#include "include_new/base/_include_vs.fx"

float3 kunosWave(float2 uv) {
  float2 offset = sin(uv.xy * ksGameTime * frequency / 1000);
  return saturate(uv.x) * distortion * float3(offset.x, offset.xy);
}

PS_IN_PerPixel main(VS_IN vin) {
  float4 vinPosL = vin.PosL;
  float3 vinNormalL = vin.NormalL;
  vin.PosL.xyz += kunosWave(vin.Tex);

  GENERIC_PIECE(PS_IN_PerPixel);
  GENERIC_PIECE_MOTION(vin.PosL);
  #ifndef NO_SHADOWS
    vout.ShadowTex0.z -= 0.01;
    vout.ShadowTex1.z -= 0.01;
    vout.ShadowTex2.z -= 0.01;
  #endif
  return vout;
}
