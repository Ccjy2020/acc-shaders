#define A2C_SHARPENED
#define NO_CARPAINT
// #define NO_GBUFFER
#define NO_SSAO
// #define NO_EXTAMBIENT
#define INCLUDE_GRASS_CB
#define NO_EXTSPECULAR
#define RAINFX_STATIC_OBJECT
#define RAINFX_NO_RAINDROPS
#define LIGHTINGFX_GRASS
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"
#include "smoothA2C.hlsl"

RESULT_TYPE main(PS_IN_Grass pin PS_INPUT_EXTRA) {
  READ_VECTORS
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);

  float2 uv = pin.GrassThing * scale;
  float3 var = txVariation.Sample(samLinear, uv).xyz - (float3)0.5;
  var *= gain;
  txDiffuseValue.rgb = txDiffuseValue.rgb * var + txDiffuseValue.rgb;
  txDiffuseValue.a *= saturate(1 - boh);
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET_FOLIAGE(txDiffuseValue);

  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  RAINFX_REFLECTIVE_FOLIAGE(lighting);
  A2C_ALPHA(txDiffuseValue.a);
  RETURN_BASE(lighting, txDiffuseValue.a);
}
