#include "include_new/base/_include_ps.fx"
// #define OPTIMIZE_SKY

float4 main(PS_IN_Sky pin) : SV_TARGET {
  float3 dir, t;
  float v;
  dir = normalize(pin.PosW.xyz - ksCameraPosition.xyz);
  
  float3 color = getSkyColor(dir).rgb;
  color *= 1.5;

  v = saturate(dot(dir, -ksLightDirection.xyz));
  t.xy = pow(v, float2(4000, 40));
  t.z = pow(v, 4);

  #ifdef OPTIMIZE_SKY  
    // possibly faster
    v = dot(t, 20 * float3(0.5, 0.004, 0.0024));
  #else
    t.xyz = t.xyz * float3(0.5, 0.004, 0.0024);
    v = 20 * (t.x + t.y + t.z);
  #endif

  return float4(saturate(ksLightColor.r) * v * (ksLightColor.rgb * 2 - color) + color, 1);
}
