#define SUPPORTS_COLORFUL_AO
#define AO_MIX_INPUT 0
#define NO_CLOUD_SHADOW
// #define SHADOWS_FILTER_SIZE 4
// #define SHADOWS_FILTER_SIZE 1
// #define cascadeShadowStep cascadeShadowStep_custom
// #define USE_STRETCHED_SHADOWS
#define USE_OPTIMIZED_SHADOWS
#define NO_SHADOWS_CASCADES_TRANSITION
#define RAINFX_STATIC_OBJECT
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_shadows/_include_vs.fx"
#include "include_new/ext_functions/wind.fx"

float getShadow_step(float3 posBase, float3 vaoValue){
  float4 shadowPos = float4(posBase, 1);
  float4 tex0 = mul(shadowPos, ksShadowMatrix0);
  float4 tex1 = mul(shadowPos, ksShadowMatrix1);
  float4 tex2 = mul(shadowPos, ksShadowMatrix2);
  return getShadowBiasMult(posBase - ksCameraPosition.xyz, float3(0, 1, 0), tex0, tex1, tex2, 0, 1);
}

#define SHADOW_POINTS_COUNT 9
const static float3 SHADOW_POINTS[9] = {
  float3(4.2, 3.8, 4.2),
  float3(4.2, 3.8, -4.2),
  float3(-4.2, 3.8, 4.2),
  float3(-4.2, 3.8, -4.2),
  float3(0, 12.4, 0),
  float3(3.5, 14.8, 3.5),
  float3(3.5, 14.8, -3.5),
  float3(-3.5, 14.8, 3.5),
  float3(-3.5, 14.8, -3.5),
};

float getShadow_avg(float3 posBase, float3 vaoValue){
  float ret = 0;
  for (int i = 0; i < SHADOW_POINTS_COUNT; i++){
    ret += getShadow_step(posBase + SHADOW_POINTS[i], vaoValue);
  }
  return ret / SHADOW_POINTS_COUNT;
}

float4 toScreenSpaceAlt(float4 posL){
  float4 posW = mul(posL, ksWorld);
  float4 posV = mul(posW, ksView);
  return mul(posV, ksProjection);
}

PS_IN_Tree main(VS_IN vin) {
  float4 posL = vin.PosL;
  vin.PosL.xz += windOffset(vin.PosL.xyz, windFromTex(vin.Tex), 1) * 2;
  // vin.NormalL.xz += windOffset(vin.PosL.xyz, windFromTex(vin.Tex), 1.3) * length(vin.NormalL);
  GENERIC_PIECE_NOSHADOWS(PS_IN_Tree);

  #if !defined(MODE_SIMPLIFIED_FX) && defined(RECEIVE_SHADOWS)
    // float4 posH = vout.PosH;
    // posH.x = clamp(posH.x, -posH.w, posH.w);
    // float4 shadowPos4 = mul(posH, ksMVPInverse);
    // float3 shadowPos = shadowPos4.xyz / shadowPos4.w + ksCameraPosition.xyz;
    float3 shadowPos = vin.PosL.xyz;

    float3 ao;
    PREPARE_AO(ao);
    vout.Shadow = getShadow_avg(shadowPos, ao);
  #else
    vout.Shadow = 1;
  #endif

  // vout.NormalW = normalize(vout.NormalW);
  posL.xz += windOffsetPrev(posL.xyz, windFromTex(vin.Tex), 1) * 2;
  GENERIC_PIECE_MOTION(posL);
  return vout;
}
