#define NO_CARPAINT
#define ALPHATEST_THRESHOLD 0.5
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {
  float3 normalW = normalize(pin.NormalW);
  // float shadow = 1.0;
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float lightValue = saturate(dot(normalize(pin.PosC), -ksLightDirection.xyz) * shadow);
  float3 diffuse = ksLightColor.rgb * ksDiffuse * lightValue;
  float3 resultLight = calculateAmbient(normalW, (float4)1) + diffuse;
  float resultAlpha = txDiffuseValue.a < 0.5 ? txDiffuseValue.a * lightValue : txDiffuseValue.a;
  float3 resultColor = txDiffuseValue.rgb * resultLight;
  resultColor += calculateEmissive(pin.PosC, txDiffuseValue.rgb, 1);

  #ifdef NO_LIGHTING
    resultColor = 0.0;
  #endif  

	// resultAlpha = 1;
	// resultColor = shadow;
	// resultAlpha = 1;

  float3 toCamera = normalize(pin.PosC);
  RETURN_BASE(resultColor, resultAlpha);

  // return float4(saturate(pin.Fog) * (ksFogColor - resultColor) + resultColor, 
  //     resultAlpha);
}
