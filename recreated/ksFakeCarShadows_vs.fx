#include "include_new/base/_include_vs.fx"

float4 toScreenSpaceAlt(float4 posL, out float4 posW, out float4 posV){
  posW = mul(posL, ksWorld);
  posV = mul(posW, ksView);
  posV.xyz *= 0.997;
  return mul(posV, ksProjection);
}

PS_IN_FakeCarShadows main(VS_IN vin) {
  PS_IN_FakeCarShadows vout;
  float4 posW, posV;
  vout.PosH = toScreenSpaceAlt(vin.PosL, posW, posV);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  return vout;
}
