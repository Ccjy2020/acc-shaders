#define NO_CARPAINT
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  RETURN_BASE(lighting, txDiffuseValue.a);
}
