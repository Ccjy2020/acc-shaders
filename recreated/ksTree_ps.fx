#define SHADOWS_FILTER_SIZE 2
#define NO_CARPAINT
#define NO_SSAO
#define A2C_SHARPENED
// #define NO_SHADOWS
// #define NO_EXTAMBIENT
#define AMBIENT_SIMPLE_FN(x) saturate((x).y * 0.4 + 0.6)
#define NO_EXTSPECULAR
#define RAINFX_STATIC_OBJECT
#define RAINFX_NO_RAINDROPS
#define LIGHTINGFX_TREE
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"
#include "smoothA2C.hlsl"

float getCloudShadow(float3 posC){
  #if defined(MODE_KUNOS)
    return 1;
  #elif !defined(NO_SHADOWS)
    float2 cloudShadowTex = mul(float4(posC, 1), extCloudShadowMatrix).xy;
    return saturate(1 - txCloudShadow.SampleLevel(samLinearClamp, cloudShadowTex, 1.5) * extCloudShadowOpacity);
  #else
    return extCloudShadowBase;
  #endif
}

RESULT_TYPE main(PS_IN_Tree pin PS_INPUT_EXTRA) {
  float3 normalW = pin.NormalW;
  float shadow = pin.Shadow * getCloudShadow(pin.PosC) * saturate(dot(AO_LIGHTING, 0.5));
  float3 toCamera = normalize(pin.PosC);
  float4 txDiffuseValue = txDiffuse.SampleBias(samLinear, pin.Tex, -0.5);
  ADJUSTCOLOR_PV(txDiffuseValue);
  RAINFX_WET_FOLIAGE(txDiffuseValue);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  // #ifdef GBUFFER_NORMALS_REFLECTION
  //   txDiffuseValue.a *= txDiffuseValue.a * txDiffuseValue.a;
  // #endif 

  RAINFX_REFLECTIVE_FOLIAGE(lighting);
  A2C_ALPHA(txDiffuseValue.a);
  RETURN_BASE(lighting, txDiffuseValue.a);
}
