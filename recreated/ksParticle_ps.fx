#define INCLUDE_PARTICLE_CB
#define NO_CARPAINT
#define NO_EXTAMBIENT
// #define SHADOWS_FILTER_SIZE 2
// #define LIGHTINGFX_SIMPLEST
#define LIGHTINGFX_NOSPECULAR
#define LIGHTINGFX_KSDIFFUSE (ksDiffuse * 0.32 / 0.035)
#define LIGHTINGFX_TXDIFFUSE_COLOR txDiffuseValue.rgb
// #define SUPPORTS_AO

#define ALLOW_BRAKEDISCFX
#define ADVANCED_FAKE_SHADOWS
#define ALLOW_EXTRA_LIGHTING_MODELS
// #define ALLOW_EXTRA_SHADOW
#define ALLOW_TREE_SHADOWS
#define BLUR_SHADOWS
#define WINDSCREEN_RAIN

#include "include_new/base/_include_ps.fx"

float3 getNormalW(float4 txNormalValue, float3 normalW, float3 tangentW, float3 bitangentW, bool objectSpace){
  float3 T = tangentW;
  float3 B = bitangentW;
  float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
  float3x3 m = float3x3(T, normalW, B);
  return normalize(mul(transpose(m), txNormalAligned.xzy));
}

RESULT_TYPE main(PS_IN_Particle pin) {
  float3 color;
  float alpha;

  float3 toCamera = normalize(pin.PosC);
  float3 normalW;

  if (shaded) {
    float shadow = getShadow(pin.PosC, float3(0, 1, 0), SHADOWS_COORDS, 1);

    float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
    float4 txNormalValue = txNormal.Sample(samLinear, pin.Tex);

    bool hasNmMap = dot(txNormalValue, 1) != 0;
    float3 nX = normalize(pin.NormalW);
    float3 nY = float3(0, 1, 0);
    float3 nZ = normalize(cross(nY, nX));
    normalW = hasNmMap ? getNormalW(txNormalValue, nZ, nX, nY, false) : float3(0, 1, 0);

    float dotValue = saturate(dot(normalW, -ksLightDirection.xyz) * 0.8 + 0.2);
    float3 light = ksLightColor.rgb * dotValue;
    float3 ambient = max(0, getAmbientBaseAt(normalW, AO_LIGHTING) * (hasNmMap ? txNormalValue.w : 1));
    color = txDiffuseValue.rgb * (light * shadow * ksDiffuse * 0.32 / 0.035 + ambient * ksAmbient * 0.42 / 0.1);
    alpha = txDiffuseValue.a;

    // return float4(saturate(color) * 0.0001 + txNormalValue.xyz, alpha);

    // if (txNoise.SampleLevel(samLinearSimple, (pin.PosC + ksCameraPosition.xyz).zy, 0).x > 0.99){
    //   color += (float3)1e6;
    // }

    LIGHTINGFX(color);
  } else {
    float4 baseColor = txDiffuse.Sample(samLinearSimple, pin.Tex);
    color = ksLightColor.rgb * pin.Tex.y + getAmbientBaseNonDirectional();
    color *= baseColor.rgb;
    color *= pin.Color.rgb;
    color = lerp(color, baseColor.rgb * pin.Color.rgb, emissiveBlend);
    alpha = baseColor.a * saturate((pin.Extra - minDistance) / minDistance);
    normalW = 0;
  }

  clip(alpha * pin.Color.a - 0.01);
  RETURN_BASE(color, alpha * pin.Color.a);
}
