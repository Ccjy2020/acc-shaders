float sqr(float a){
  return a * a;
}

bool testBs(float3 posW, float3 bsPosW, float bsRadiusSqr){
  float3 bsVec = posW - bsPosW;
  float bsDist = dot(bsVec, bsVec);
  return bsDist < bsRadiusSqr;
}

float4 normalizeWithDistance(float3 vec){
  float dist = length(vec);
  return float4(vec / dist, dist);
}

float getDiffuseMultiplier(float3 normal, float3 lightDir) {  
  #ifdef LIGHTINGFX_SIMPLEST
    return 1;
  #elif defined LIGHTINGFX_TREE
    return 0.75; // saturate(lerp(1, dot(normal, lightDir), concentration * 0.25));
  #else
    return saturate(dot(normal, lightDir) * 0.88 + 0.12);
  #endif
}

float getDiffuseMultiplier(float3 normal0, float3 normal1, float normalMix, float3 lightDir) {  
  return lerp(getDiffuseMultiplier(normal0, lightDir), getDiffuseMultiplier(normal1, lightDir), normalMix);
}

float getDiffuseMultiplier(float3 normal, float3 lightDir, float concentration) {
  #ifdef LIGHTINGFX_SIMPLEST
    return 1;
  // #elif defined(LIGHTINGFX_EXTRA_FOCUSED)
  //   return pow(saturate(dot(normal, lightDir)), 3);
  #elif defined(LIGHTINGFX_GLASS_BACKLIT) && defined(ALLOW_LIGHTINGFX_BACKLIT)
    return abs(dot(normal, lightDir));
  #else
    return saturate(lerp(1, dot(normal, lightDir), concentration));
  #endif
}

float getDiffuseMultiplier(float3 normal0, float3 normal1, float normalMix, float3 lightDir, float concentration) {  
  return lerp(getDiffuseMultiplier(normal0, lightDir, concentration), getDiffuseMultiplier(normal1, lightDir, concentration), normalMix);
}

float3 closestPointOnSegment(float3 a, float3 ab, float distInv, float3 c, out float abValue) {
  abValue = dot(c - a, ab) * distInv;
  return a + saturate(abValue) * ab;
}

float lerpInv10(float val, float start, float lengthInv){
  // return saturate(1.0 - (val - start) * lengthInv);
  return saturate(start - val * lengthInv);
}

float lerpInv10(float val, float start){
  return saturate(start - val);
}

float lerpInv01(float val, float start, float lengthInv){
  return saturate(val * lengthInv - start);
}

/*float getTrimmedAttenuation(float rangeStart, float rangeLengthInv, float trimStart, float trimLengthInv, float d) {
  float df = max(lerpInv01(d, trimStart, trimLengthInv) - lerpInv10(d, rangeStart, rangeLengthInv), 0.0);
  return df * df;
}*/

float getTrimmedAttenuation(float rangeLengthInv, float trimStart, float trimLengthInv, float d) {
  float df = max(lerpInv01(d, trimStart, trimLengthInv) - saturate(d * rangeLengthInv), 0.0);
  return df * df;
}

float3 getDoubleSpotCone(float3 lightDirW, float3 toLight, 
    float spotCosStart, float3 attenuation,
    float secondSpotCosStart, float secondSpotCosLengthInv, float3 secondAttenuation){  
  float cosAngle = dot(lightDirW, -toLight);
  return lerpInv10(cosAngle, spotCosStart) * attenuation 
    + lerpInv10(cosAngle, secondSpotCosStart, secondSpotCosLengthInv) * secondAttenuation;
}

float getSpotCone(float3 lightDirW, float3 toLight, float spotCosStart){  
  float cosAngle = dot(lightDirW, -toLight);
  return lerpInv10(cosAngle, spotCosStart);
}

float3 getEdge(float3 lightUpW, float3 toLight, float3 spotEdgeOffset){  
  float up = dot(lightUpW, -toLight);
  return saturate(spotEdgeOffset - up);
}

float getAttenuation(float rangeStart, float rangeLengthInv, float d) {
  float df = lerpInv10(d, rangeStart, rangeLengthInv);
  return df * df;
}

float getNDotH(float3 normalW, float3 posW, float3 lightDir) {
  float3 toEye = normalize(posW);
  float3 halfway = normalize(toEye - lightDir);
  return saturate(dot(-halfway, normalW));
}

// float getNDotH(float3 normalW0, float3 normalW1, float normalMix, float3 posW, float3 lightDir) {
//   return abs(lerp(getNDotH(normalW0, posW, lightDir), getNDotH(normalW1, posW, lightDir), normalMix));
// }

float getNDotHL(float3 normalW, float3 posW, float3 lightDir) {
  float3 toEye = normalize(posW);
  float3 halfway = normalize(toEye + lightDir);
  return saturate(dot(halfway, normalW));
}

float calculateSpecularLight(float nDotH, float exp) {
  return pow(nDotH, max(exp, 0.1));
}

float getSpecularComponent(float3 normalW, float3 posW, float3 lightDir, float specularExp){
  float nDotH = getNDotH(normalW, posW, lightDir);
  return calculateSpecularLight(nDotH, specularExp);
}

float getSpecularComponent(float3 normalW0, float3 normalW1, float normalMix, float3 posW, float3 lightDir, float specularExp){
  return lerp(0, getSpecularComponent(normalW1, posW, lightDir, specularExp), normalMix);
}

float calculateSpecularLight_byValues(float3 normal, float3 position, float3 lightDir, float exp) {
  float nDotH = getNDotH(normal, position, lightDir);
  return calculateSpecularLight(nDotH, exp);
}
