#ifndef NO_SHADOWS
  #ifndef SHADOWS_COORDS
    #define SHADOWS_COORDS pin.ShadowTex0, pin.ShadowTex1, pin.ShadowTex2
  #endif

  void cascadeShadowBlur_step(float deltaZ, float4 shadowTex, float2 uvOffset, inout float result){
    float4 uv = shadowTex;
    float comparisonValue = uv.z - deltaZ;
    uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);      
    result += txShadow0.SampleCmpLevelZero(samShadow, uv.xy + uvOffset, comparisonValue);
  }

  float getShadow(float3 posC, float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2, float fallbackValue){
    float2 cloudShadowTex = mul(float4(posC, 1), extCloudShadowMatrix).xy;
    #ifdef TARGET_VS
      float cloudShadowsMult = saturate(1 - txCloudShadow.SampleLevel(samLinearClamp, cloudShadowTex, 2.5) * extCloudShadowOpacity);
    #else
      float cloudShadowsMult = saturate(1 - txCloudShadow.SampleLevel(samLinearClamp, cloudShadowTex, 1.5) * extCloudShadowOpacity);
    #endif

    [branch]
    if (noiseMult == -1){
      return cloudShadowsMult * perObjAmbientMultiplier;
    } else {
      float deltaZ = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1) * 4 * bias.x;

      float result = 0;
      float offset = lerp(0.5, txNoise.SampleLevel(samPoint, shadowTex0.xy * 1000, 0).x, noiseMult);
      float4 stepValue = (shadowTex1 - shadowTex0) / 9;

      [unroll]
      for (int i = 0; i < 3; i++){
        [unroll]
        for (int j = 0; j < 3; j++){
          cascadeShadowBlur_step(deltaZ, shadowTex0 + stepValue * (i * 3 + j + offset), textureSize * float2(i - 1, j - 1), result);
        }
      }

      return cloudShadowsMult * result / 9;
    }
  }
#endif