#ifndef AO_MIX_INPUT
  #define AO_MIX_INPUT vaoSecondaryMix
#endif

#ifdef SUPPORTS_OBJSP_AO
  #error No longer supported
#endif

#ifdef SUPPORTS_COLORFUL_AO
  #undef SUPPORTS_COLORFUL_AO
  #define SUPPORTS_NORMALS_AO
#endif

#if defined(ALLOW_PERVERTEX_AO) && defined(SUPPORTS_NORMALS_AO)
  #ifdef USE_AO_SPLITTING
    #define PREPARE_AO(x) x = lerp(vsLoadAo0(vin.TangentPacked), vsLoadAo1(vin.TangentPacked), AO_MIX_INPUT);
  #else
    #define PREPARE_AO(x) x = vsLoadAo0(vin.TangentPacked);
  #endif
#elif defined(ALLOW_PERVERTEX_AO) && defined(SUPPORTS_OBJSP_AO)
  #define PREPARE_AO(x) x = saturate(length(vin.NormalL) * 2.0 - 1.0);
#elif defined(ALLOW_PERVERTEX_AO) && defined(SET_AO_TO_ONE)
  #define PREPARE_AO(x) x = 1.0;
#else
  #define PREPARE_AO(x) ;
#endif

// #undef PREPARE_AO
// #define PREPARE_AO(x) x = vaoSecondaryMix;

