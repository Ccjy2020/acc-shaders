#include "include/samplers.hlsl"

#ifndef NO_SHADOWS
  Texture2D<float> txShadow0 : register(t6);
  Texture2D<float> txShadow1 : register(t7);
  Texture2D<float> txShadow2 : register(t8);
  Texture2D<float> txCloudShadow : register(t24);

  #include "include_new/ext_shadows/_include_ps.fx"
#else

  #ifndef SHADOWS_COORDS
    #define SHADOWS_COORDS 0, 0, 0
  #endif

  #define getShadow(posC, normalW, shadowTex0, shadowTex1, shadowTex2, fallbackValue) (extCloudShadowBase * luminance(vaoValue) * perObjAmbientMultiplier)
  #define getShadowBiasMult(posC, normalW, shadowTex0, shadowTex1, shadowTex2, biasMultiplier, fallbackValue) (extCloudShadowBase * luminance(vaoValue) * perObjAmbientMultiplier)

#endif