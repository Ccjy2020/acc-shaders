#define ksAmbient 0.3
#define ksDiffuse 0.2
#define ksSpecular 0
#define ksSpecularEXP 16
#define ksEmissive float3(0, 0, 0)
#define ksAlphaRef 0.2
