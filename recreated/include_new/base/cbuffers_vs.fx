#include "cbuffers_common.fx"

cbuffer cbBones : register(b13) {
  float4x4 bones[55];
}

#ifdef CREATE_MOTION_BUFFER
cbuffer cbBonesPrev : register(b10) {
  float4x4 bonesPrev[55];
}
#endif

#ifdef SUPPORTS_DISTANT_FIX
cbuffer cbDistantFix : register(b10) {
  float extDistantFixStart;
  float extDistantFixMult; // 0.01
}
#endif

#ifdef INCLUDE_FLAGS_CB 
cbuffer cbFlags : register(b10) {
  float frequency;
  float distortion;
  float2 boh;
}
#endif

// cbuffer cbExtFlags : register(b6) {
//   float3 flagFrom;
//   float windSpeed;
//   float3 flagTo;
//   float windDirection;
// }