#if defined(ALLOW_PERVERTEX_AO) && !defined(NO_PERVERTEX_AO)
  #define AO_NORMALS centroid float Ao : COLOR;
  #define AO_COLORFUL centroid float3 Ao : COLOR1;
#else
  #define AO_NORMALS 
  #define AO_COLORFUL 
#endif

#ifdef NO_SHADOWS
  #define SHADOWS_COORDS_ITEMS
#else
  #define SHADOWS_COORDS_ITEMS \
    float4 ShadowTex0 : TEXCOORD3;\
    float4 ShadowTex1 : TEXCOORD4;\
    float4 ShadowTex2 : TEXCOORD5;
#endif

#ifdef CREATE_MOTION_BUFFER
  #define MOTION_BUFFER float4 PosCS0 : TEXCOORD10; float4 PosCS1 : TEXCOORD5;
#else 
  #define MOTION_BUFFER
#endif

#if defined(ALLOW_RAINFX) && !defined(RAINFX_STATIC_OBJECT) && !defined(RAINFX_USE_PUDDLES_MASK) && !defined(RAINFX_NO_RAINDROPS)
  #define RAINFX_USE_REGISTERS 
#endif

#ifdef RAINFX_USE_REGISTERS
  #define RAINFX_REGISTERS\
    float3 PosR : POSITION2;\
    float3 NormalR : NORMAL2;
#else
  #define RAINFX_REGISTERS
#endif

#ifndef CUSTOM_STRUCT_FIELDS
  #define CUSTOM_STRUCT_FIELDS 
#else
  struct PS_IN_PerPixelCustom {
    float4 PosH : SV_POSITION;
    float3 NormalW : TEXCOORD0;
    float3 PosC : TEXCOORD1;
    float2 Tex : TEXCOORD2;
    CUSTOM_STRUCT_FIELDS
    MOTION_BUFFER
    float Fog : TEXCOORD6;
    SHADOWS_COORDS_ITEMS
    AO_COLORFUL
    RAINFX_REGISTERS
  };
#endif

struct PS_IN_GL {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD;
  float3 TangentH : TANGENT;
};

struct PS_IN_Font {
  float4 PosH : SV_POSITION;
  float3 Color : COLOR;
  float2 Tex : TEXCOORD;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  float3 TangentH : TANGENT;
};

struct PS_IN_ShadowGetAt {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD1;
};

struct PS_IN_PerPixel {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  float Fog : TEXCOORD6;
  SHADOWS_COORDS_ITEMS
  AO_COLORFUL
  RAINFX_REGISTERS
};

struct PS_IN_Tree {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  centroid float2 Tex : TEXCOORD2;
  centroid float Fog : TEXCOORD6;
  centroid float Shadow : TEXCOORD7;
  // #ifdef ALLOW_TREE_SHADOWS
  // SHADOWS_COORDS_ITEMS
  // #endif
  AO_COLORFUL
};

struct PS_IN_Multilayer {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  float3 PosW : TEXCOORD6;
  float Fog : TEXCOORD7;
  AO_COLORFUL
};

struct PS_IN_Grass {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  float Fog : TEXCOORD6;
  float2 GrassThing : TEXCOORD7;
  AO_COLORFUL
};

struct PS_IN_AtNs {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  float Fog : TEXCOORD6;
  AO_COLORFUL
};

struct PS_IN_Nm {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float Fog : TEXCOORD8;
  AO_NORMALS
  RAINFX_REGISTERS
};

struct PS_IN_Uv2 {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float2 Tex2 : TEXCOORD8;
  float Fog : TEXCOORD9;
  AO_NORMALS
};

struct PS_IN_Sky {
  float4 PosH : SV_POSITION;
  float3 PosW : TEXCOORD2;
  float3 NormalL : TEXCOORD3;
  float2 Tex : TEXCOORD0;
  MOTION_BUFFER
  float3 LightH : TEXCOORD4;
};

struct PS_IN_CloudFX {
  float4 PosH : SV_POSITION;
  float3 PosC : TEXCOORD0;
  float Fog : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  float3 ToUp : TEXCOORD3;
  float3 ToSide : TEXCOORD4;
};

struct PS_IN_Particle {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  MOTION_BUFFER
  float Extra : TEXCOORD1;
  float Fog : TEXCOORD2;
  float3 NormalW : TEXCOORD3;
  float3 PosC : TEXCOORD4;
  #ifndef NO_SHADOWS
    float4 ShadowTex0 : TEXCOORD5;
    float4 ShadowTex1 : TEXCOORD6;
    float4 ShadowTex2 : TEXCOORD7;
  #endif
  // AO_COLORFUL
};

struct PS_IN_SkidMark {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  centroid float SkidMarkThing : TEXCOORD6;
  float Fog : TEXCOORD7;
};

struct PS_IN_FakeCarShadows {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
  SHADOWS_COORDS_ITEMS
};

struct PS_IN_NmPosL {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float3 PosL : POSITION1;
  float3 NormalL : NORMAL1;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float Fog : TEXCOORD8;
  AO_NORMALS
  RAINFX_REGISTERS
};

struct PS_IN_PerPixelPosL {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float3 PosL : POSITIONLOCAL;
  float3 NormalL : NORMALLOCAL;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  float Fog : TEXCOORD6;
  SHADOWS_COORDS_ITEMS
  AO_COLORFUL
};

struct PS_IN_PerPixelExtra1 {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float Extra : EXTRAVALUE;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  float Fog : TEXCOORD6;
  SHADOWS_COORDS_ITEMS
  AO_COLORFUL
};

struct PS_IN_PerPixelExtra2 {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Extra : EXTRAVALUE;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  float Fog : TEXCOORD6;
  SHADOWS_COORDS_ITEMS
  AO_COLORFUL
};

struct PS_IN_NmExtra1 {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float Extra : EXTRAVALUE;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float Fog : TEXCOORD8;
  AO_NORMALS
};

struct PS_IN_NmExtra4 {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float4 Extra : EXTRAVALUE;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float Fog : TEXCOORD8;
  AO_NORMALS
  RAINFX_REGISTERS
};

struct PS_IN_Windows {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  nointerpolation float3 PosV : POSITIONLOCAL;
  nointerpolation float2 TexV : NORMALLOCAL;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float Fog : TEXCOORD8;
  AO_NORMALS
};