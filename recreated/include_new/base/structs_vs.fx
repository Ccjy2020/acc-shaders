#include "include/common.hlsl"

struct VS_IN {
  AC_INPUT_ELEMENTS
};

struct VS_IN_objsp {
  // Originally, it didn’t have TangentL, but now it’s used for all sorts of things, so why not
  AC_INPUT_ELEMENTS
};

struct VS_IN_Particle {
  float4 PosL : POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD;
};

struct VS_IN_Skinned {
  AC_INPUT_SKINNED_ELEMENTS
};