#include "include/common.hlsl"

float3 getRGB(float4 v){ return v.rgb; }
float3 getRGB(float3 v){ return v; }
float3 getRGB(float v){ return v; }

#if defined(MODE_SHADOW)
  
  struct PS_OUT {
    float result : SV_Target;
  };

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    clip(alpha - 0.1);\
    ps_result.result = 1;\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#elif defined(MODE_GBUFFER)

  struct PS_OUT {  
    float4 normal : SV_Target;
    #ifdef CREATE_REFLECTION_BUFFER
      float4 baseReflection : SV_Target1;
      float4 reflectionColorBlur : SV_Target2;
      #ifdef CREATE_MOTION_BUFFER
        float2 motion : SV_Target3;
        float stencil : SV_Target4;
      #endif
    #else
      #ifdef CREATE_MOTION_BUFFER
        float2 motion : SV_Target1;
        float stencil : SV_Target2;
      #endif
    #endif
  };

  #ifndef GET_MOTION
    #ifdef CREATE_MOTION_BUFFER
      float2 getMotion(float4 pos0, float4 pos1){
        float2 vVelocity = (pos0.xy / pos0.w) - (pos1.xy / pos1.w);
        vVelocity *= 0.5f;
        vVelocity.y *= -1;
        // if (motionRotatingNode) return float2(0, 0);
        return vVelocity;
      }
      #define GET_MOTION(x) getMotion(x.PosCS0, x.PosCS1)
    #else
      #define GET_MOTION(x) (float2)0
    #endif
  #endif

  #ifndef ALPHATEST_THRESHOLD
    #ifdef A2C_SHARPENED_THRESHOLD
      #define ALPHATEST_THRESHOLD A2C_SHARPENED_THRESHOLD
    #else
      #define ALPHATEST_THRESHOLD 0.01
    #endif
  #endif

  #ifndef STENCIL_VALUE
    #define STENCIL_VALUE extMotionStencil
  #endif

  #ifndef GBUFF_MASKING_MODE
    #define GBUFF_MASKING_MODE extExtraSharpLocalReflections == -17
  #endif

  PS_OUT prepareResult(float3 reflColor, float alpha, float2 reflectionPower, float3 reflectionColorMult, 
      float3 normalW, float2 motion, bool noReflection, float pinFog
      #ifdef ALLOW_RAINFX
      , bool taaDisable
      #endif
      ){
    PS_OUT ps_result;

    float reflMult = 1.0;
    #ifdef USE_ALPHATEST 
      clip(alpha - ALPHATEST_THRESHOLD);
      reflMult = alpha;
    #endif

    #ifdef ALLOW_RAINFX
      float stencilValue = taaDisable ? 1 : STENCIL_VALUE;
    #else
      float stencilValue = STENCIL_VALUE;
    #endif

    #ifdef CREATE_REFLECTION_BUFFER
      #ifndef GBUFF_SKIP_REFLECTION
      if (noReflection){
      #endif
        ps_result.normal = float4(ssNormalEncode(normalW), alpha);
        ps_result.baseReflection = 0;
        ps_result.reflectionColorBlur = 0;
        #ifdef CREATE_MOTION_BUFFER
          ps_result.motion = motion;
          ps_result.stencil = stencilValue;
        #endif
      #ifndef GBUFF_SKIP_REFLECTION
      } else {
        ps_result.normal = float4(ssNormalEncode(normalW), alpha);
        reflectionPower.y *= saturate(1 - abs(extExtraSharpLocalReflections));
        if (reflectionPower.y > 5.95) reflMult = 0;
        if (HAS_FLAG(FLAG_GBUFFERPREMULTIPLIEDALPHA)) reflMult *= alpha;
        reflMult = withFog(reflMult, pinFog, 1);
        float reflPowerAdj = reflectionPower.x * reflMult;
        #ifdef SSLR_FORCE
          if (SSLR_FORCE){ reflPowerAdj = -reflPowerAdj; }
        #else
          if (extExtraSharpLocalReflections < 0){ reflPowerAdj = -reflPowerAdj; }
        #endif
        ps_result.baseReflection = float4(reflColor.rgb * reflMult, reflPowerAdj);
        ps_result.reflectionColorBlur = float4(reflectionColorMult.rgb, reflectionPower.y / 6);
        #ifdef CREATE_MOTION_BUFFER
          ps_result.motion = motion;
          ps_result.stencil = stencilValue;
        #endif
      }
      #endif
    #else
      ps_result.normal = float4(ssNormalEncode(normalW), alpha);
      #ifdef CREATE_MOTION_BUFFER
        ps_result.motion = motion;
        ps_result.stencil = stencilValue;
      #endif
    #endif

    if (GBUFF_MASKING_MODE){
      ps_result.normal = 1;
      #ifdef CREATE_REFLECTION_BUFFER
        ps_result.baseReflection = 1 - alpha;
        ps_result.reflectionColorBlur = 1;
      #endif
      #ifdef CREATE_MOTION_BUFFER
        ps_result.motion = 1;
        ps_result.stencil = 1;
      #endif
    }

    return ps_result;
  }

  #ifndef GBUFF_NORMALMAPS_DISTANCE
    #define GBUFF_NORMALMAPS_DISTANCE 10
  #endif

  #ifndef GBUFF_NORMALMAPS_NEUTRAL
    #define GBUFF_NORMALMAPS_NEUTRAL normalize(vertexNormalW)
  #endif

  float3 fixNormal(float3 normalW, float3 vertexNormalW, float3 posC){
    return normalize(lerp(normalW, GBUFF_NORMALMAPS_NEUTRAL, saturate(dot(posC, posC) / (GBUFF_NORMALMAPS_DISTANCE * GBUFF_NORMALMAPS_DISTANCE) - 1) ));
  }

  #ifndef GBUFF_NORMAL_W_SRC
    #define GBUFF_NORMAL_W_SRC normalW
  #endif

  #ifndef GBUFF_NORMAL_W_PIN_SRC
    #define GBUFF_NORMAL_W_PIN_SRC pin.NormalW
  #endif

  #ifdef ALLOW_RAINFX
    #define GBU_ARG_RN ,RP.taaDisable
  #else
    #define GBU_ARG_RN
  #endif

  #define RESULT_TYPE PS_OUT

  #ifdef NO_GBUFFER
    #define RETURN(reflColor, alpha) clip(-1); return (PS_OUT)0;
    #define RETURN_BASE(reflColor, alpha) clip(-1); return (PS_OUT)0;
    #define RETURN_NOTHING clip(-1); return (PS_OUT)0;
  #elif defined(ZERO_GBUFFER)
    #define RETURN(reflColor, alpha) return (PS_OUT)0;
    #define RETURN_BASE(reflColor, alpha) return (PS_OUT)0;
    #define RETURN_NOTHING return (PS_OUT)0;
  #else
    #define RETURN(reflColor, alpha) return prepareResult(R.resultColor, alpha, float2(R.resultPower, R.resultBlur), \
      lerp(1, R.coloredReflectionsColor, R.coloredReflections),\
      fixNormal(GBUFF_NORMAL_W_SRC, GBUFF_NORMAL_W_PIN_SRC, pin.PosC), GET_MOTION(pin), false, pin.Fog GBU_ARG_RN);
    #define RETURN_BASE(reflColor, alpha) return prepareResult(getRGB(reflColor), alpha, 0, 0,\
      fixNormal(GBUFF_NORMAL_W_SRC, GBUFF_NORMAL_W_PIN_SRC, pin.PosC), GET_MOTION(pin), true, pin.Fog GBU_ARG_RN);
    #define RETURN_NOTHING clip(-1); return (PS_OUT)0;
  #endif

#elif defined(GBUFFER_GRASSFX)

  #define GRASS_PARAMS_MASK_ONLY
  #include "grass/flgGrass_configurations.hlsl"

  cbuffer cbGrassFXMap : register(b5) {
    PackedGrassParams gp[PACKED_GRASS_PARAMS_COUNT];
  }

  Texture2D txAdjustments : register(t18);
  GrassParams getGrassParams(float2 uv){
    return getGrassParams(gp, txAdjustments.SampleLevel(samLinearClamp, uv, 0));
  }

  #define VAR_INV_MASKING_MODE ksExposure
  #define FLAG_INV_MASKING_MODE (-17)

  float adjustAlpha(float v){
    if (VAR_INV_MASKING_MODE == FLAG_INV_MASKING_MODE){
      return 1 - v;
    }
    #ifdef USE_OPAQUE_FIX
      return ksAlphaRef ? v : 1;
    #else
      return v;
    #endif
  }

  float3 clipSaturation(float3 color, float2 posH){
    if (ksScreenWidth == 0 || VAR_INV_MASKING_MODE == FLAG_INV_MASKING_MODE) return color;    
    GrassParams GP = getGrassParams(posH / float2(ksScreenWidth, ksScreenHeight));
    float colorTest = colorThreshold(color.rgb, GP.mask_mainThreshold, GP.mask_redThreshold, GP.mask_minLuminance, GP.mask_maxLuminance);
    clip(colorTest - 0.2);
    return color;
  }

  #ifdef LIGHTINGFX_NOSPECULAR
    #define LIGHTINGFX_SPECULAR_EXP 1
    #define LIGHTINGFX_SPECULAR_COLOR 0
  #else
    #ifndef LIGHTINGFX_SPECULAR_EXP
      #define LIGHTINGFX_SPECULAR_EXP L.specularExp
    #endif
    #ifndef LIGHTINGFX_SPECULAR_COLOR
      #define LIGHTINGFX_SPECULAR_COLOR (L.specularValue * L.txSpecularValue)
    #endif
  #endif

  struct PS_OUT {
    float4 result : SV_Target;
    float4 normal_ao : SV_Target1;
    float4 amb_dif_spec_specexp : SV_Target2;
  };

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    clip(normalW.y - 0.2);\
    float3 reflColor_ = clipSaturation(getRGB(reflColor), pin.PosH.xy);\
    ps_result.result = float4(reflColor_, adjustAlpha(alpha));\
    ps_result.normal_ao.xyz = normalW * 0.5 + 0.5;\
    ps_result.normal_ao.w = dot((float3)AO_LIGHTING, 1.0/3.0);\
    ps_result.amb_dif_spec_specexp.x = INPUT_DIFFUSE_K;\
    ps_result.amb_dif_spec_specexp.y = INPUT_AMBIENT_K;\
    ps_result.amb_dif_spec_specexp.z = dot(LIGHTINGFX_SPECULAR_COLOR, 1./3.);\
    ps_result.amb_dif_spec_specexp.w = LIGHTINGFX_SPECULAR_EXP / 255;\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#elif defined(MODE_COLORSAMPLE)

  #define VAR_SPECIAL_MODE ksExposure
  #define FLAG_SPECIAL_MODE_SOLID (-19)
  #define FLAG_SPECIAL_MODE_FILTER (-20)

  float3 adjustColor(float3 color){
    if (VAR_SPECIAL_MODE == FLAG_SPECIAL_MODE_SOLID){
      return 1;
    }
    if (VAR_SPECIAL_MODE == FLAG_SPECIAL_MODE_FILTER){
      float value = dot(color, 3) - 1;
      clip(value);
      return saturate(value);
      return 1;
    }
    return color;
  }

  float adjustAlpha(float v){
    #ifdef USE_OPAQUE_FIX
      return ksAlphaRef ? v : 1;
    #else
      return v;
    #endif
  }

  struct PS_OUT {
    float4 result : SV_Target;
  };

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    ps_result.result = float4(adjustColor(getRGB(reflColor)), adjustAlpha(alpha));\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

  #define damageZones ((float4)0)
  #define blurLevel 0.0
  #define dirtyLevel 0.0

#elif defined(MODE_PUDDLES)

  struct PS_OUT {
    float2 result : SV_Target;
  };

  PS_OUT getResult(RainParams RP, float alpha){
    PS_OUT ps_result;    
    ps_result.result.x = clamp(RP.water, 0.01, 1);
    ps_result.result.y = clamp(RP.wetness, 0.01, 1);
    // ps_result.result = RP.water * 10000;
    // ps_result.result = RP.water;
    return ps_result;
  }

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha) return getResult(RP, alpha);
  #define RETURN_BASE(reflColor, alpha) return getResult(RP, alpha);
  #define RETURN_NOTHING return (PS_OUT)0;

#elif defined(MODE_COLORMASK)

  struct PS_OUT {
    float4 result : SV_Target;
  };

  cbuffer cbMaskParams : register(b5) {
    float3 extraColor;
    float opacityMult;
  }

  PS_OUT getResult(float3 c, float a){
    c += extraColor;
    c = max(c, 0.00001);
    c = c / max(c.r, max(c.g, c.b));
    a = saturate(a * opacityMult);
    PS_OUT ps_result;    
    ps_result.result = float4(saturate(lerp(1, c, a)), 1);
    return ps_result;
  }

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha) return getResult(getRGB(reflColor), alpha);
  #define RETURN_BASE(reflColor, alpha) return getResult(getRGB(reflColor), alpha);
  #define RETURN_NOTHING return (PS_OUT)0;

  #define damageZones ((float4)0)
  #define blurLevel 0.0
  #define dirtyLevel 0.0

#elif defined(MODE_EMISSIVESAMPLE)

  struct PS_OUT {
    float4 result : SV_Target;
  };

  PS_OUT getResult(float4 txDiffuseValue, float4 txEmissiveValue){
    PS_OUT ps_result;    
    #ifdef USE_TXEMISSIVE_AS_EMISSIVE
      ps_result.result = txEmissiveValue;
    #else
      ps_result.result = float4(luminance(txDiffuseValue.rgb), 0, 0, 0);
    #endif
    return ps_result;
  }

  #ifndef USE_TXEMISSIVE_AS_EMISSIVE
    #define TXEMISSIVE_VALUE 0
  #endif

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha) return getResult(txDiffuseValue, TXEMISSIVE_VALUE);
  #define RETURN_BASE(reflColor, alpha) return getResult(txDiffuseValue, TXEMISSIVE_VALUE);
  #define RETURN_NOTHING return (PS_OUT)0;

  #define damageZones ((float4)0)
  #define blurLevel 0.0
  #define dirtyLevel 0.0

#elif defined(MODE_LIGHTS)

  struct PS_OUT {
    float4 result : SV_Target;
  };

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    ps_result.result = float4(getRGB(reflColor) * alpha, 0);\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#elif defined(MODE_LIGHTMAP)

  struct PS_OUT {
    float4 result : SV_Target;
  };

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    ps_result.result = float4(getRGB(reflColor), alpha);\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#else

  #ifndef A2C_SHARPENED_THRESHOLD
    #define A2C_SHARPENED_THRESHOLD 0.7
  #endif

  float adjustAlpha(float alpha){
    // #ifdef A2C_SHARPENED
    //   alpha = saturate((alpha - A2C_SHARPENED_THRESHOLD) / max(fwidth(alpha), 0.0001) + 0.5);
    //   clip(alpha - 0.1);
    // #endif
    // #ifdef A2C_SHARPENED_SIMPLE
    //   clip(alpha - A2C_SHARPENED_THRESHOLD);
    //   alpha = 1;
    // #endif
    return alpha;
  }

  float3 applyAoMult(float3 color, float mult){
    #ifdef USE_PERVERTEX_AO_AS_MULT
      return color * mult;
    #endif
    return color;
  }

  float3 applyAoMult(float3 color, float4 mult){
    #ifdef USE_PERVERTEX_AO_AS_MULT
      return color * mult.xyz;
    #endif
    return color;
  }

  void ensureVarsUsed(inout float v){
    #ifdef ENSURE_VARS_USED
      v += saturate(extExtraSharpLocalReflections) * 0.00001;
    #endif
  }

  struct PS_OUT {
    float4 result : SV_Target;
  };

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    ensureVarsUsed(reflColor.r);\
    ps_result.result = withFog(applyAoMult(getRGB(reflColor), AO_LIGHTING), pin.Fog, adjustAlpha(alpha));\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#endif