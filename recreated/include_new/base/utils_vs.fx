// Utils

#ifndef EXCLUDE_PEROBJECT_CB

  float3 normals(float3 input){
    return mul(input, (float3x3)ksWorld);
  }

  float3 bitangent(float3 normalL, float3 tangentL){
    #ifndef NO_NORMALMAPS
      return mul(cross(tangentL, normalL), (float3x3)ksWorld);
    #else
      return 0;
    #endif
  }

#endif

#ifdef NO_SHADOWS
  #define SHADOWS_COORDS 0
  void shadows(float4 posW, float v){ }
  void noShadows(float v){ }
#else
  #define SHADOWS_COORDS vout.ShadowTex0, vout.ShadowTex1, vout.ShadowTex2
  void shadows(float4 posW, out float4 tex0, out float4 tex1, out float4 tex2){
    tex0 = mul(posW, ksShadowMatrix0);
    tex1 = mul(posW, ksShadowMatrix1);
    tex2 = mul(posW, ksShadowMatrix2);
  }

  void noShadows(out float4 tex0, out float4 tex1, out float4 tex2){
    tex0 = 0;
    tex1 = 0;
    tex2 = 0;
  }
#endif

#ifdef NO_REFLECTIONS
  #define isAdditive 1
#endif

// Fog

float calculateFogImpl(float4 posV){
  #if defined OPTIMIZE_FOG
    float c = pow(2, posV.z / ksFogLinear * (5.770780 * 2));
    return saturate((1 - c) / (1 + c)) * ksFogBlend;
  #elif defined FOG_NMUV2_VERSION
    float a = -posV.z / ksFogLinear * 5.770780;
    float b = pow(2, a);
    float c = pow(2, -a);
    float d = 1 / (b + c);
    return saturate((b - c) * d) * ksFogBlend;
  #elif defined FOG_PARTICLE_VERSION
    float a = -posV.z / ksFogLinear * 5.770780;
    float b = pow(2, a);
    float c = pow(2, -a);
    float d = b - c;
    float c0 = c + b;
    float c1 = 1 / c0;
    return saturate(d * c1) * ksFogBlend;
  #else
    float a = -posV.z / ksFogLinear * 5.770780;
    float b = pow(2, a);
    float c = pow(2, -a);
    return saturate((b - c) / (b + c)) * ksFogBlend;
  #endif
}

#ifdef MODE_KUNOS
  float calculateFogImpl(float4 posV, float4 posW, float3 posC){
    float c = pow(2, posV.z / ksFogLinear * 5.770780);
    float b = 1 / c;
    return saturate((b - c) / (b + c)) * ksFogBlend;
  }

  float calculateFogNew(float3 posC){
    return 0;
  }

  float calculateFogNew(float3 posC, float3 posN){
    return 0;
  }
#else
  float calculateFogNewFn(float3 posC, float3 posN){
    #ifdef USE_PS_FOG
      return 0;
    #else
      float dist = length(posC);
      posC.y += sign(posC.y) * (dist / 100);
      posN = posC / dist;
      float buggyPart = (1.0 - exp(-posC.y / ksFogLinear)) / posN.y;
      return ksFogBlend * pow(saturate(extFogConstantPiece * buggyPart), extFogExp);
    #endif
  }

  float calculateFogNewFn(float3 posC){
    return calculateFogNewFn(posC, normalize(posC));
  }

  float calculateFogImpl(float4 posV, float4 posW, float3 posC){
    [branch]
    if (extUseNewFog){
      return calculateFogNewFn(posC);
    } else {
      float c = pow(2, posV.z / ksFogLinear * 5.770780);
      float b = 1 / c;
      return saturate((b - c) / (b + c)) * ksFogBlend;
    }
  }

  float calculateFogNew(float3 posC){
    [branch]
    if (extUseNewFog){
      return calculateFogNewFn(posC);
    } else {
      return 0;
    }
  }

  float calculateFogNew(float3 posC, float3 posN){
    [branch]
    if (extUseNewFog){
      return calculateFogNewFn(posC, posN);
    } else {
      return 0;
    }
  }
#endif
#define calculateFog(x) calculateFogImpl(x, posW, vout.PosC)

// Local-to-sceen conversion

#ifndef EXCLUDE_PEROBJECT_CB

  #ifndef ALTER_POSW 
    #define ALTER_POSW(local, prevFrame, world) ;
  #endif

  #ifdef MODE_LIGHTMAP
    #define CLIP_PROBES 16
    cbuffer cbClipPlane : register(b13) {
      float gClipBaseWeight;
      float gClipBaseWeightY;
      float2 gClipPad0;
      float4 gClipProbes[CLIP_PROBES];
    }
    float findClipPlane(float2 posXZ){
      float2 valAndWeight = float2(gClipBaseWeightY, gClipBaseWeight);
      for (int i = 0; i < CLIP_PROBES; i++){
        valAndWeight += gClipProbes[i].zw / dot2(gClipProbes[i].xy - posXZ);
      }
      return valAndWeight.x / valAndWeight.y;
    }
    void lightMapClipping(inout float3 posW){
      float clipPlane = findClipPlane(posW.xz);
      if (clipPlane < posW.y) posW.y += 1e6;
    }
  #else
    void lightMapClipping(inout float3 posW){}
  #endif

  void alterPosW(float3 posL, bool prevFrame, inout float3 posW){  
    ALTER_POSW(posL, prevFrame, posW);
    lightMapClipping(posW);
  }

  void alterPosW(float3 posL, bool prevFrame, inout float4 posW){  
    alterPosW(posL, prevFrame, posW.xyz);
  }

  float4 toScreenSpace(float4 posL, out float4 posW, out float4 posV){
    #ifdef FIX_SCREENSPACE
      float4x4 ksWorldFixed = ksWorld;
      ksWorldFixed[3].xyz -= ksCameraPosition.xyz;

      float4x4 ksViewFixed = ksView;
      ksViewFixed[3].xyz = (float3)0;
      
      float4 posWfixed = mul(posL, ksWorldFixed);
      alterPosW(posL.xyz, false, posWfixed);

      posW = posWfixed + float4(ksCameraPosition.xyz, 0);
      posV = mul(posWfixed, ksViewFixed);
      return mul(posV, ksProjection);
    #else
      posW = mul(posL, ksWorld);
      alterPosW(posL.xyz, false, posW);
      posV = mul(posW, ksView);

      float4 posVL = posV;
      #if defined(SUPPORTS_DISTANT_FIX) && defined(ALLOW_DISTANT_FIX)
        [branch]
        if (extDistantFixStart > 0){
          float dist = length(posV.xyz);
          if (dist > extDistantFixStart){
            float newDist = extDistantFixStart + (dist - extDistantFixStart) * extDistantFixMult;
            posVL.xyz *= newDist / dist;
          }
        }
      #endif
      return mul(posVL, ksProjection);
    #endif
  }

  #ifndef SKINNED_WEIGHT_CHECK
    #define SKINNED_WEIGHT_CHECK(a) (a != 0)
  #endif

  #ifdef CREATE_MOTION_BUFFER
    float4 getMotion(float4 posL, float4 basePosH){
      float4 posW = mul(posL, ksWorldPrev);
      alterPosW(posL.xyz, true, posW);
      float4 posH = mul(posW, ksViewProjPrev);
      return posH;
    }

    float4 getMotionWorld(float4 posL, float4 basePosH){
      float4 posW = posL;
      alterPosW(posL.xyz, true, posW);
      float4 posH = mul(posW, ksViewProjPrev);
      return posH;
    }

    float4 getMotionVelocity(float4 posL, float3 velocity, float4 basePosH){
      float4 posW = posL;
      posW.xyz -= velocity;
      alterPosW(posL.xyz, true, posW);
      float4 posH = mul(posW, ksViewProjPrev);
      return posH;
    }

    float4 getMotionSkinned(float4 posL, float4 posWN, float4 boneWeights, float4 boneIndices, float4 basePosH){
      float4 posW = 0;
      for (int i = 0; i < 4; i++){
        float weight = boneWeights[i];
        if (SKINNED_WEIGHT_CHECK(weight)){
          uint index = (uint)boneIndices[i];
          float4x4 bone = bonesPrev[index];
          posW += (mul(bone, posL) + float4(ksWorldPrev[3].xyz, 0)) * weight;
        }
      }
      alterPosW(posL.xyz, true, posW);
      return mul(posW, ksViewProjPrev);
    }
  #endif /* CREATE_MOTION_BUFFER */

  // Skinned

  float4 toScreenSpaceSkinned(float4 posL, float3 normalL, float3 tangentPacked, float4 boneWeights, float4 boneIndices, 
      out float4 posW, out float3 normalW, out float3 tangentW, out float4 posV){
    #if ! defined FIX_SCREENSPACE

      posW = 0;
      normalW = 0;
      tangentW = 0;

      for (int i = 0; i < 4; i++){
        float weight = boneWeights[i];
        if (SKINNED_WEIGHT_CHECK(weight)){
          uint index = (uint)boneIndices[i];
          float4x4 bone = bones[index];
          posW += (mul(bone, posL) + float4(ksWorld[3].xyz, 0)) * weight;
          normalW += mul((float3x3)bone, normalL) * weight;
          tangentW += mul((float3x3)bone, vsLoadTangent(tangentPacked)) * weight;
        }
      }

      alterPosW(posL.xyz, false, posW);
      posV = mul(posW, ksView);
      return mul(posV, ksProjection);

    #else

      float4x4 ksViewFixed = ksView;
      ksViewFixed[3].xyz = (float3)0;

      float4 posWfixed = 0;
      normalW = 0;
      tangentW = 0;

      for (int i = 0; i < 4; i++){
        float weight = boneWeights[i];
        if (SKINNED_WEIGHT_CHECK(weight)){
          uint index = (uint)boneIndices[i];
          float4x4 bone = bones[index];
          bone[0].w -= ksCameraPosition.x;
          bone[1].w -= ksCameraPosition.y;
          bone[2].w -= ksCameraPosition.z;
          posWfixed += mul(bone, posL) * weight;
          normalW += mul((float3x3)bone, normalL) * weight;
          tangentW += mul((float3x3)bone, tangentL) * weight;
        }
      }

      posW.xyz += ksWorld[3].xyz; // or something like that
      alterPosW(posL.xyz, false, posWfixed);
      posW = posWfixed + float4(ksCameraPosition.xyz, 0);
      posV = mul(posWfixed, ksViewFixed);
      return mul(posV, ksProjection);

    #endif
  }
#endif /* !EXCLUDE_PEROBJECT_CB */

#ifdef CREATE_MOTION_BUFFER
  float4 getMotionStatic(float4 posW){
    return mul(posW, ksViewProjPrev);
  }
#endif