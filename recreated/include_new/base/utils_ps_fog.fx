#ifdef FOG_MULT_MODE
float withFogImpl(float baseResult, float3 posC, float3 toCamera, float fog, float fogMult, float alphaValue){
#else
float4 withFogImpl(float3 baseResult, float3 posC, float3 toCamera, float fog, float fogMult, float alphaValue){
#endif
  #ifdef USE_OPAQUE_FIX
    alphaValue = ksAlphaRef >= 0 ? alphaValue : 1;
  #endif

  #ifdef NO_FOG
    return float4(baseResult.rgb, alphaValue);
  #endif

  #if defined(USE_PS_FOG) && !defined(MODE_KUNOS)
    [branch]
    if (extUseNewFog){
      fog = ksFogBlend * pow(saturate(extFogConstantPiece * (1.0 - exp(-posC.y / ksFogLinear)) / toCamera.y), extFogExp);
    }
  #endif
  
  #ifdef FOG_MULT_MODE
    return baseResult * saturate(1 - fog * fogMult);
  #else
    #if defined(USE_BACKLIT_FOG) && !defined(MODE_KUNOS)
      float sunAmount = saturate(dot(-toCamera, ksLightDirection.xyz));
      float3 fogColor = ksFogColor + ksLightColor.xyz * pow(sunAmount, extFogBacklitExp) * extFogBacklitMult;
    #else
      float3 fogColor = ksFogColor;
    #endif
    return float4(lerp(baseResult.rgb, fogColor, saturate(fog * fogMult)), alphaValue);
  #endif
}

#ifdef USE_PS_FOG
  #define FOG_INPUT_POSC pin.PosC
#else
  #define FOG_INPUT_POSC 0
#endif

#if defined(USE_PS_FOG) || defined(USE_BACKLIT_FOG)
  #define FOG_INPUT_TOCAMERA toCamera
#else
  #define FOG_INPUT_TOCAMERA 0
#endif

#define withFog(_BASE_RESULT, _FOG, _ALPHA_VALUE) withFogImpl(_BASE_RESULT, FOG_INPUT_POSC, FOG_INPUT_TOCAMERA, _FOG, 1, _ALPHA_VALUE)
