#include "_flags.fx"

#include "include/common.hlsl"
#include "structs_vs.fx"
#include "structs_ps.fx"
#include "cbuffers_vs.fx"

#ifdef MODE_BAKED
#include "cbuffers_baked.fx"
#endif

#include "utils_vs.fx"
#include "../ext_vao/_include_vs.fx"

#ifdef ALLOW_RAINFX
  #include "rainFX.hlsl"
#else
  #define RAINFX_VERTEX(X)
#endif

#ifdef ALLOW_TREE_SHADOWS
  #define PS_TREE_INPUT PS_IN_PerPixel
#else
  #define PS_TREE_INPUT PS_IN_Tree
#endif

#ifdef CREATE_MOTION_BUFFER
  #ifndef ZERO_MOTION_BUFFER
    #define GENERIC_PIECE_MOTION(x)\
      vout.PosCS0 = vout.PosH;\
      vout.PosCS1 = getMotion(x, vout.PosH);
    #define GENERIC_PIECE_WORLD(x)\
      vout.PosCS0 = vout.PosH;\
      vout.PosCS1 = getMotionWorld(x, vout.PosH);
    #define GENERIC_PIECE_VELOCITY(x, vel)\
      vout.PosCS0 = vout.PosH;\
      vout.PosCS1 = getMotionVelocity(x, vel, vout.PosH);
    #define GENERIC_PIECE_STATIC(x)\
      vout.PosCS0 = vout.PosH;\
      vout.PosCS1 = getMotionStatic(x);
    #define GENERIC_PIECE_MOTION_SKINNED(x)\
      vout.PosCS0 = vout.PosH;\
      vout.PosCS1 = getMotionSkinned(x, posW, vin.BoneWeights, vin.BoneIndices, vout.PosH);
  #else
    #define GENERIC_PIECE_MOTION(x) vout.PosCS0 = float4(0, 0, 0, 1); vout.PosCS1 = float4(0, 0, 0, 1);
    #define GENERIC_PIECE_STATIC(x) vout.PosCS0 = float4(0, 0, 0, 1); vout.PosCS1 = float4(0, 0, 0, 1);
    #define GENERIC_PIECE_VELOCITY(x) vout.PosCS0 = float4(0, 0, 0, 1); vout.PosCS1 = float4(0, 0, 0, 1);
    #define GENERIC_PIECE_MOTION_SKINNED(x) vout.PosCS0 = float4(0, 0, 0, 1); vout.PosCS1 = float4(0, 0, 0, 1);
  #endif
#else
  #define GENERIC_PIECE_MOTION(x) 
  #define GENERIC_PIECE_STATIC(x) 
  #define GENERIC_PIECE_VELOCITY(x) 
  #define GENERIC_PIECE_MOTION_SKINNED(x) 
#endif

#define GENERIC_PIECE_NOSHADOWS(_OUTPUTTYPE)\
  _OUTPUTTYPE vout;\
  float4 posW, posV;\
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);\
  vout.NormalW = normals(vin.NormalL);\
  vout.PosC = posW.xyz - ksCameraPosition.xyz;\
  vout.Tex = vin.Tex;\
  vout.Fog = calculateFog(posV);\
  GENERIC_PIECE_MOTION(vin.PosL);\
  PREPARE_AO(vout.Ao);

#define GENERIC_PIECE(_OUTPUTTYPE)\
  GENERIC_PIECE_NOSHADOWS(_OUTPUTTYPE);\
  shadows(posW, SHADOWS_COORDS);

#ifdef NO_NORMALMAPS
  #define PREPARE_TANGENT
  #define GENERIC_PIECE_NM(_OUTPUTTYPE)\
    GENERIC_PIECE(_OUTPUTTYPE);
#else
  #define PREPARE_TANGENT \
    vout.TangentW = normals(vsLoadTangent(vin.TangentPacked));\
    vout.BitangentW = bitangent(vin.NormalL, vsLoadTangent(vin.TangentPacked))
  #define GENERIC_PIECE_NM(_OUTPUTTYPE)\
    GENERIC_PIECE(_OUTPUTTYPE);\
    PREPARE_TANGENT;
#endif

#define DISCARD_VERTEX(TYPE) TYPE vout = (TYPE)0; vout.PosH = -10; return vout;