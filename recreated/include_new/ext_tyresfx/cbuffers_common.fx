cbuffer _cbExtTyres : register(b6) {
  float3 wheelPos;
  float rimRadiusSqr;

  float3 dirUp;
  float tyreSquashDistance;

  float3 dirForward;
  float tyreSkewX;

  float3 dirSide;
  float tyreSkewY;

  float3 contactNormal;
  float profileSizeInv;

  float3 contactPos;
  float tyreWidthInv;

  float damageWidthK;
  float damageOffsetK;
  float skewMultInv;
  float damageOcclusionMult;

  float3 damageSpecSpecExpReflMult;
  float damageNormalsMult;

  float shadowBiasMult;
  float3 grassColorA;

  float dirtWidthK;
  float3 grassColorB;

  float dirtFade;
  float3 dirtColorA;

  float dirtSomething2;
  float3 dirtColorB;
};