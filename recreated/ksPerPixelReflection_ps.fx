#define CARPAINT_SIMPLE
#define SUPPORTS_AO
#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"

#include "include/poisson.hlsl"
float sampleNoise(float3 posW, float3 posC, float3 normalW){
  float3 side = normalize(cross(normalW, float3(0, 1, 0)));
  float3 input = posW + normalize(posC) * 2;
  float2 uv = float2(dot(side, input), input.y) * 0.03;
  POISSON_AVG(float4, result, txNoise, samLinearSimple, uv, 0.03, 8);
  return lerp(0.5, 1.5, result.x);
}

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS
  
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR;
  // L.txEmissiveValue *= sampleNoise(ksCameraPosition.xyz + pin.PosC, pin.PosC, pin.NormalW);

  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK);
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, 1);
  R.useBias = true;
  RAINFX_REFLECTIVE(R);
  float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, pin.PosC, normalW, R);
  
  RAINFX_WATER(withReflection);
  RETURN(withReflection, withReflection.a);
}
