// #define SUPPORTS_NORMALS_AO
#define SET_AO_TO_ONE
#include "include_new/base/_include_vs.fx"

PS_IN_Nm main(VS_IN vin) {
  GENERIC_PIECE_NM(PS_IN_Nm);
  return vout;
}
