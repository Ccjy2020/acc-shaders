#define SUPPORTS_COLORFUL_AO
#define SUPPORTS_DISTANT_FIX
#define USE_PS_FOG
#include "include_new/base/_include_vs.fx"
// alias: ksPerPixel_horizon_vs

PS_IN_PerPixel main(VS_IN vin) {
  GENERIC_PIECE(PS_IN_PerPixel);
  return vout;
}
