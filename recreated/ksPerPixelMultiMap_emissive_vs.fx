#define SUPPORTS_NORMALS_AO
#include "emissiveMapping.hlsl"
// alias: ksPerPixelMultiMap_NMDetail_emissive_vs
// alias: nePerPixelMultiMap_emissive_digitalScreen_vs
// alias: ksPerPixelMultiMap_AT_emissive_vs

PS_IN_NmExtra4 main(VS_IN vin) {
  PS_IN_NmExtra4 vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  vout.Extra = getExtraValue(vin.PosL.xyz);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  RAINFX_VERTEX(vout);
  return vout;
}
