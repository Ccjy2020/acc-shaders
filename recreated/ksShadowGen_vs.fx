#include "include_new/base/_include_vs.fx"
// alias: ksSelectedMesh_vs

float4 main(VS_IN vin) : SV_POSITION {
  float4 posW, posV;
  return toScreenSpace(vin.PosL, posW, posV);
}
