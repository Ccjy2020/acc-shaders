#define STENCIL_VALUE 0
#define GBUFF_MASKING_MODE true
#define GBUFF_NORMAL_W_SRC float3(0,1,0)
#define GBUFF_NORMAL_W_PIN_SRC float3(0,1,0)
#define GET_MOTION(x) (float2)0
#define NO_CARPAINT
#ifdef USE_FX_LOOK
  #define INPUT_AMBIENT_K 0.2
  #define INPUT_DIFFUSE_K 0.2
#endif
#include "include_new/base/_include_ps.fx"

#undef AO_LIGHTING
#define AO_LIGHTING 0

RESULT_TYPE main(PS_IN_SkidMark pin) {
  float3 normalW = normalize(pin.NormalW);
  float3 toCamera = normalize(pin.PosC);
  float shadow = getShadow(pin.PosC, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);

  #ifdef USE_FX_LOOK
    float4 txNoiseValue = txNoise.SampleLevel(samLinearSimple, (ksCameraPosition.xz + pin.PosC.xz) * 0.03, 0);
    float4 txNoiseX = txNoise.SampleLevel(samLinearSimple, float2(pin.Tex.x * 0.3, pin.Tex.y * 0.03), 0);
    float randFade = saturate((txNoiseValue.x * txNoiseValue.y + txNoiseValue.z * txNoiseValue.w) * 0.9);
    pin.Tex.x = (pin.Tex.x - 0.5) * (0.9 + randFade * 0.8 + lerp(2, 0, saturate(pin.SkidMarkThing * 3))) + 0.5;
  #endif

  float4 txDiffuseValue = txDiffuse.Sample(samLinearSimple, pin.Tex);
  #ifdef USE_FX_LOOK
    txDiffuseValue.a *= abs(pin.Tex.x - 0.5) < 0.5;
  #endif
  RAINFX_INIT;

  float alpha = txDiffuseValue.a * pin.SkidMarkThing;
  #ifdef USE_FX_LOOK
    alpha = saturate(alpha * lerp(1.2, 0.7, randFade) * lerp(1.2, 0, txNoiseX.x * txNoiseX.y));
  #endif

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  #ifdef USE_FX_LOOK
    L.specularValue = lerp(0, 0.1, pow(alpha, 2));
    L.specularExp = lerp(20, 60, pow(alpha, 2));
  #endif
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  float4 result = float4(lighting, alpha);
  #ifdef NO_LIGHTING
    result.rgb = 0.0;
  #endif  
  #ifdef ALLOW_RAINFX
    result.a *= 1 - RP.water;
    RP = (RainParams)0;
  #endif
  RETURN_BASE(result.rgb, result.a);
}
