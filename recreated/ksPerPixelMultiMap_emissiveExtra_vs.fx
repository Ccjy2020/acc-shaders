#define SUPPORTS_NORMALS_AO
#include "emissiveMappingExtra.hlsl"

PS_IN_NmExtra1 main(VS_IN vin) {
  PS_IN_NmExtra1 vout;
  float4 posW, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  vout.Extra = getExtraValue(vin.PosL.xyz);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  return vout;
}
