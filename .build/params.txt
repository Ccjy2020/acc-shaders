# ---
# Options:
# ---

# --force
# --verbose
# --single-thread
# --auto-mt

# ---
# Categories to skip:
# Masks are allowed
# ---

# Skip: custom
Skip: basic
# Skip: G-buf*
# Skip: main*
# Skip: main
# Skip: *-S
# Skip: simpler*
# Skip: simplest
# Skip: color sample
# Skip: grassFX
# Skip: light map

# # Process only: grassFX
# Process only: custom
# Process only: main
# Process only: main, FX
# Process only: main, FX, -S
# # Process only: *FS*
# Process only: main, FS
# Process only: G-buf*R
# Process only: main, FS, -S
# # Process only: G-buf*
# # Process only: main
# # Process only: simplest
# # Process only: emissive
# # Process only: main, FX, DS
# # Process only: light map
# # Process only: color mask
# # Process only: color sample
# # Process only: G-buf*R+A
# # Process only: wet, FX
# # Process only: wet, WM
# # Process only: wet, G*
# # Process only: we*
# # Process only: wet, G, R+A
# # Process only: wet, G, R
# # Process only: wet, P*
# # Process only: wet, P.
# # Process only: wet, W*
# # Process only: puddles
# Process only: simpler, FX

# ---
# Shaders to prepare for SDK (ksEditor):
# ---
# SDK: st*
# SDK: ks*emissive
# SDK: ksMultilayer_fresnel_nm4

# ---
# Shaders to compile:
# Comment “*” out to limit which shaders will be recompiled, to speed things up
# ---

# ksPerPixel_ps.fx
# nePerPixel_light*
# ksWindscreenFX_new_vs.fx
# ksWindscreenFX_new_ps.fx
# *_vs*
*
# accTAA_ps.fx
# uiRadar*
# accDriverTag_*
# ksPerPixelReflection_vs.fx
# nePerPixelMultiMap_NMDetail_fur*
ksTyresFX*
smGlass_ps.fx
ksMultilayer_fresnel_nm_ps.fx
smCarPaint_vs.fx
smCarPaint_ps.fx
ksPerPixelReflection_ps.fx
accCopy*
accShadow*
pfxMirage*
accFog*
accSSLR*
nePBR_MultiMap_NMDetail_ClearCoat_ps.fx
nePBR_MultiMap_Cloth_ps.fx
wfxSky_vs.fx
wfxSky_old_vs_reflections.fx

accFakeCarShadows_ps_fs.fx
accFakeCarShadows_ps_fs_reprojection.fx

*skinned*
ksPerPixel_ps.fx
ksPerPixelMultiMap_ps.fx
ksMultilayer_ps.fx
ksPerPixelMultiMap_emissive_ps.fx
ksPerPixelMultiMap_NMDetail_ps.fx
ksMultilayer_fresnel_nm_ps.fx
ksPerPixelMultiMap_damage_dirt_ps.fx
flgGrass_vs_fs.fx
smGlass_ps.fx
# smRefractingCover_ps.fx
# smRefractingCover_vs.fx
accFakeCarShadows_ps.fx
accFakeCarShadows_ps_simple.fx
accResolve*
wfxCloudsV2Q_ps.fx
# *
# accCopy*
# accResolve*
# accShadow*